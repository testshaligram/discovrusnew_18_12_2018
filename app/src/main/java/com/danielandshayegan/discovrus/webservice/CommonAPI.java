package com.danielandshayegan.discovrus.webservice;

import android.content.Context;
import android.util.Log;

import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.models.FollowPeopleData;
import com.danielandshayegan.discovrus.models.SavePostViewData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Shikha
 */

public class CommonAPI {

    public static void likePost(Context me, final String postId, String postUserId, boolean isLiked, final CallbackTask callBack) {

        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("UserID", postUserId);
            params.put("PostID", postId);
            params.put("LoginID", "2");//   TODO: change static id
            params.put("IsActive", String.valueOf(isLiked));

            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.LIKE_POST)
                        .setParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Log.e("Like Post api error", message);
                            }

                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void followUnfollowPeople(Context me, String followUserId, final CallbackTask callBack) {

        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("UserID", "2");//   TODO: change static id
            params.put("FollowUserID", followUserId);

            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.FOLLOW_PEOPLE)
                        .setParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Log.e("Like Post api error", message);
                            }

                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void viewPost(Context me, final String postId, final CallbackTask callBack) {

        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("PostID", postId);
            params.put("LoginID", "2");//   TODO: change static id

            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.VIEW_POST)
                        .setParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Log.e("Like Post api error", message);
                            }

                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}