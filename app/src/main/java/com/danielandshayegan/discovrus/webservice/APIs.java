package com.danielandshayegan.discovrus.webservice;

import com.danielandshayegan.discovrus.network.ApiClient;

public class APIs {

    static final String BASE_URL = ApiClient.WebService.baseUrl;
    public static final String BASE_IMAGE_PATH = ApiClient.WebService.imageUrl;

    public static final String GET_NEWSFEED_LIST = "PostAPI/GetPostPagging";

    public static final String GET_PROFILE = "PublicProfileAPI/GetProfile";

    public static final String GET_REFERENCES_USERID = "UserReferenceAPI/GetReferencesByUserId";
    public static final String LIKE_POST = "LikeAPI/SaveLike";
    public static final String FOLLOW_PEOPLE = "FollowPeopleAPI/FollowPeople";
    public static final String GET_CHART_LIST = "PublicProfileAPI/GetChartList";
    public static final String GET_HIGHLIGHTED_POST = "PublicProfileAPI/GetHighlightedPost";
    public static final String VIEW_POST = "PostViewAPI/SavePostView";
    public static final String GET_LIKE_PEOPLE_LIST = "LikeAPI/GetLikeList";
}