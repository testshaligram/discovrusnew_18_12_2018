package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class IndividualRegisterData extends Observable {


    /**
     * Data : {"AdminUserId":15,"CategoryId":0,"CategoryName":"","Description":"","ImagePath":"","FirstName":"p","LastName":"p","Email":"ppp1@gmail.com","Password":"HFm6yIbrZWtNj8HJ48K86Q==","Location":"Pune","UserImagePath":"/Files/User/user_20180611_040234774_testing_4.jpg","AdminUserRoleName":"Individual","UserRole":null,"BusinessName":"","WorkingAt":"shaligram","ImageName":"user_20180611_040234774_testing_4.jpg","Message":"Thank you for registreing with us","IsEmail":true,"IsLinkedin":false,"IsInstagram":false,"SocialmediaId":"","IsVerified":false}
     * Success : true
     * Message : ["Thank you for registreing with us"]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * AdminUserId : 15
         * CategoryId : 0
         * CategoryName :
         * Description :
         * ImagePath :
         * FirstName : p
         * LastName : p
         * Email : ppp1@gmail.com
         * Password : HFm6yIbrZWtNj8HJ48K86Q==
         * Location : Pune
         * UserImagePath : /Files/User/user_20180611_040234774_testing_4.jpg
         * AdminUserRoleName : Individual
         * UserRole : null
         * BusinessName :
         * WorkingAt : shaligram
         * ImageName : user_20180611_040234774_testing_4.jpg
         * Message : Thank you for registreing with us
         * IsEmail : true
         * IsLinkedin : false
         * IsInstagram : false
         * SocialmediaId :
         * IsVerified : false
         */

        private int AdminUserId;
        private int CategoryId;
        private String CategoryName;
        private String Description;
        private String ImagePath;
        private String FirstName;
        private String LastName;
        private String Email;
        private String Password;
        private String Location;
        private String UserImagePath;
        private String AdminUserRoleName;
        private Object UserRole;
        private String BusinessName;
        private String WorkingAt;
        private String ImageName;
        private String Message;
        private boolean IsEmail;
        private boolean IsLinkedin;
        private boolean IsInstagram;
        private String SocialmediaId;
        private boolean IsVerified;

        public int getAdminUserId() {
            return AdminUserId;
        }

        public void setAdminUserId(int AdminUserId) {
            this.AdminUserId = AdminUserId;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String CategoryName) {
            this.CategoryName = CategoryName;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public String getImagePath() {
            return ImagePath;
        }

        public void setImagePath(String ImagePath) {
            this.ImagePath = ImagePath;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getAdminUserRoleName() {
            return AdminUserRoleName;
        }

        public void setAdminUserRoleName(String AdminUserRoleName) {
            this.AdminUserRoleName = AdminUserRoleName;
        }

        public Object getUserRole() {
            return UserRole;
        }

        public void setUserRole(Object UserRole) {
            this.UserRole = UserRole;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getWorkingAt() {
            return WorkingAt;
        }

        public void setWorkingAt(String WorkingAt) {
            this.WorkingAt = WorkingAt;
        }

        public String getImageName() {
            return ImageName;
        }

        public void setImageName(String ImageName) {
            this.ImageName = ImageName;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }

        public boolean isIsEmail() {
            return IsEmail;
        }

        public void setIsEmail(boolean IsEmail) {
            this.IsEmail = IsEmail;
        }

        public boolean isIsLinkedin() {
            return IsLinkedin;
        }

        public void setIsLinkedin(boolean IsLinkedin) {
            this.IsLinkedin = IsLinkedin;
        }

        public boolean isIsInstagram() {
            return IsInstagram;
        }

        public void setIsInstagram(boolean IsInstagram) {
            this.IsInstagram = IsInstagram;
        }

        public String getSocialmediaId() {
            return SocialmediaId;
        }

        public void setSocialmediaId(String SocialmediaId) {
            this.SocialmediaId = SocialmediaId;
        }

        public boolean isIsVerified() {
            return IsVerified;
        }

        public void setIsVerified(boolean IsVerified) {
            this.IsVerified = IsVerified;
        }
    }
}
