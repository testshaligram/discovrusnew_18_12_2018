package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class PostCommentData extends Observable {

    /**
     * Data : {"CommentId":53,"PostId":1601,"UserId":12,"Comment":"love it....","CreatedBy":12,"CommentCount":1}
     * Success : true
     * Message : ["Save Successfully."]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * CommentId : 53
         * PostId : 1601
         * UserId : 12
         * Comment : love it....
         * CreatedBy : 12
         * CommentCount : 1
         */

        private int CommentId;
        private int PostId;
        private int UserId;
        private String Comment;
        private int CreatedBy;
        private int CommentCount;

        public int getCommentId() {
            return CommentId;
        }

        public void setCommentId(int CommentId) {
            this.CommentId = CommentId;
        }

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getComment() {
            return Comment;
        }

        public void setComment(String Comment) {
            this.Comment = Comment;
        }

        public int getCreatedBy() {
            return CreatedBy;
        }

        public void setCreatedBy(int CreatedBy) {
            this.CreatedBy = CreatedBy;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int CommentCount) {
            this.CommentCount = CommentCount;
        }
    }
}
