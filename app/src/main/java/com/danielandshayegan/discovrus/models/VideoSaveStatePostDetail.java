package com.danielandshayegan.discovrus.models;

import com.danielandshayegan.discovrus.ui.fragment.ClickPostVideoFragment;
import com.danielandshayegan.discovrus.ui.fragment.VideoDetailsFragment;

public class VideoSaveStatePostDetail {

    // region Fields
    private String videoUrl;
    private long currentPosition;
    private float currentVolume;
    private ClickPostVideoFragment.PlaybackState playbackState;
    private String castInfo;
    // endregion

    public VideoSaveStatePostDetail(){

    }

    // region Getters
    public String getVideoUrl() {
        return videoUrl;
    }

    public long getCurrentPosition() {
        return currentPosition;
    }

    public float getCurrentVolume() {
        return currentVolume;
    }

    public ClickPostVideoFragment.PlaybackState getPlaybackState() {
        return playbackState;
    }

    public String getCastInfo() {
        return castInfo;
    }

    // endregion

    // region Setters
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void setCurrentPosition(long currentPosition) {
        this.currentPosition = currentPosition;
    }

    public void setCurrentVolume(float currentVolume) {
        this.currentVolume = currentVolume;
    }

    public void setPlaybackState(ClickPostVideoFragment.PlaybackState playbackState) {
        this.playbackState = playbackState;
    }

    public void setCastInfo(String castInfo) {
        this.castInfo = castInfo;
    }

    // endregion
}
