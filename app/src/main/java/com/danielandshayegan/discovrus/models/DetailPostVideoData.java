package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailPostVideoData {

    /**
     * Data : {"PostId":1756,"UserId":32,"ImagePath":"/Files/NewsFeed/Video/e80ba309-69a0-49f5-8c2e-0e9754797acc.mov","OriginalImage":"","FileId":5004,"FileName":"3f773934-7007-4adf-aaa1-070dbaaac3f4.mov","Description":"text","ThumbFileName":null,"PostPath":null,"UserRole":null,"PostType":null,"Type":"Video","Title":null,"IsHighLight":false,"isDetailedPost":false,"TagUserID":"1,2","Lat":"1","Long":"2","Distance":"0.5 m","Time":"3 h","HashTag":"zzzz","LikeCount":0,"CommentCount":0,"ViewCount":0}
     * Success : true
     * Message : ["Video Uploaded Successfully"]
     * Count :
     */

    @SerializedName("Data")
    private DataBean Data;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Message")
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PostId : 1756
         * UserId : 32
         * ImagePath : /Files/NewsFeed/Video/e80ba309-69a0-49f5-8c2e-0e9754797acc.mov
         * OriginalImage :
         * FileId : 5004
         * FileName : 3f773934-7007-4adf-aaa1-070dbaaac3f4.mov
         * Description : text
         * ThumbFileName : null
         * PostPath : null
         * UserRole : null
         * PostType : null
         * Type : Video
         * Title : null
         * IsHighLight : false
         * isDetailedPost : false
         * TagUserID : 1,2
         * Lat : 1
         * Long : 2
         * Distance : 0.5 m
         * Time : 3 h
         * HashTag : zzzz
         * LikeCount : 0
         * CommentCount : 0
         * ViewCount : 0
         */

        @SerializedName("PostId")
        private int PostId;
        @SerializedName("UserId")
        private int UserId;
        @SerializedName("ImagePath")
        private String ImagePath;
        @SerializedName("OriginalImage")
        private String OriginalImage;
        @SerializedName("FileId")
        private int FileId;
        @SerializedName("FileName")
        private String FileName;
        @SerializedName("Description")
        private String Description;
        @SerializedName("ThumbFileName")
        private Object ThumbFileName;
        @SerializedName("PostPath")
        private Object PostPath;
        @SerializedName("UserRole")
        private Object UserRole;
        @SerializedName("PostType")
        private Object PostType;
        @SerializedName("Type")
        private String Type;
        @SerializedName("Title")
        private Object Title;
        @SerializedName("IsHighLight")
        private boolean IsHighLight;
        @SerializedName("isDetailedPost")
        private boolean isDetailedPost;
        @SerializedName("TagUserID")
        private String TagUserID;
        @SerializedName("Lat")
        private String Lat;
        @SerializedName("Long")
        private String Long;
        @SerializedName("Distance")
        private String Distance;
        @SerializedName("Time")
        private String Time;
        @SerializedName("HashTag")
        private String HashTag;
        @SerializedName("LikeCount")
        private int LikeCount;
        @SerializedName("CommentCount")
        private int CommentCount;
        @SerializedName("ViewCount")
        private int ViewCount;

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getImagePath() {
            return ImagePath;
        }

        public void setImagePath(String ImagePath) {
            this.ImagePath = ImagePath;
        }

        public String getOriginalImage() {
            return OriginalImage;
        }

        public void setOriginalImage(String OriginalImage) {
            this.OriginalImage = OriginalImage;
        }

        public int getFileId() {
            return FileId;
        }

        public void setFileId(int FileId) {
            this.FileId = FileId;
        }

        public String getFileName() {
            return FileName;
        }

        public void setFileName(String FileName) {
            this.FileName = FileName;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public Object getThumbFileName() {
            return ThumbFileName;
        }

        public void setThumbFileName(Object ThumbFileName) {
            this.ThumbFileName = ThumbFileName;
        }

        public Object getPostPath() {
            return PostPath;
        }

        public void setPostPath(Object PostPath) {
            this.PostPath = PostPath;
        }

        public Object getUserRole() {
            return UserRole;
        }

        public void setUserRole(Object UserRole) {
            this.UserRole = UserRole;
        }

        public Object getPostType() {
            return PostType;
        }

        public void setPostType(Object PostType) {
            this.PostType = PostType;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public Object getTitle() {
            return Title;
        }

        public void setTitle(Object Title) {
            this.Title = Title;
        }

        public boolean isIsHighLight() {
            return IsHighLight;
        }

        public void setIsHighLight(boolean IsHighLight) {
            this.IsHighLight = IsHighLight;
        }

        public boolean isIsDetailedPost() {
            return isDetailedPost;
        }

        public void setIsDetailedPost(boolean isDetailedPost) {
            this.isDetailedPost = isDetailedPost;
        }

        public String getTagUserID() {
            return TagUserID;
        }

        public void setTagUserID(String TagUserID) {
            this.TagUserID = TagUserID;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String Lat) {
            this.Lat = Lat;
        }

        public String getLong() {
            return Long;
        }

        public void setLong(String Long) {
            this.Long = Long;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String Distance) {
            this.Distance = Distance;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String Time) {
            this.Time = Time;
        }

        public String getHashTag() {
            return HashTag;
        }

        public void setHashTag(String HashTag) {
            this.HashTag = HashTag;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int LikeCount) {
            this.LikeCount = LikeCount;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int CommentCount) {
            this.CommentCount = CommentCount;
        }

        public int getViewCount() {
            return ViewCount;
        }

        public void setViewCount(int ViewCount) {
            this.ViewCount = ViewCount;
        }
    }
}
