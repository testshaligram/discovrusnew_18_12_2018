package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LikePeopleData {

    @SerializedName("UserId")
    @Expose
    private String UserId;

    @SerializedName("UserName")
    @Expose
    private String UserName;

    @SerializedName("UserImagePath")
    @Expose
    private String UserImagePath;

    @SerializedName("UserRoleType")
    @Expose
    private String UserRoleType;

    @SerializedName("BusinessName")
    @Expose
    private String BusinessName;

    @SerializedName("WorkingAt")
    @Expose
    private String WorkingAt;

    @SerializedName("IsFollowing")
    @Expose
    private String IsFollowing;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserImagePath() {
        return UserImagePath;
    }

    public void setUserImagePath(String userImagePath) {
        UserImagePath = userImagePath;
    }

    public String getUserRoleType() {
        return UserRoleType;
    }

    public void setUserRoleType(String userRoleType) {
        UserRoleType = userRoleType;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public String getWorkingAt() {
        return WorkingAt;
    }

    public void setWorkingAt(String workingAt) {
        WorkingAt = workingAt;
    }

    public String getIsFollowing() {
        return IsFollowing;
    }

    public void setIsFollowing(String isFollowing) {
        IsFollowing = isFollowing;
    }
}