package com.danielandshayegan.discovrus.models;

import java.util.ArrayList;
import java.util.List;

public class AnalyticsData {
    private boolean Success;
    private String Count;
    private DataBean Data;
    private List<?> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public List<?> getMessage() {
        return Message;
    }

    public void setMessage(List<?> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        private ArrayList<DaysArray> DayWiseAnalyticDetails;
        private int FollowersOnline;
        private int NewFollowers;
        private int Interactions;
        private ArrayList<MonthArray> MonthWiseAnalyticDetails;

        public ArrayList<DaysArray> getDayWiseAnalyticDetails() {
            return DayWiseAnalyticDetails;
        }

        public void setDayWiseAnalyticDetails(ArrayList<DaysArray> dayWiseAnalyticDetails) {
            DayWiseAnalyticDetails = dayWiseAnalyticDetails;
        }

        public int getFollowersOnline() {
            return FollowersOnline;
        }

        public void setFollowersOnline(int followersOnline) {
            FollowersOnline = followersOnline;
        }

        public int getNewFollowers() {
            return NewFollowers;
        }

        public void setNewFollowers(int newFollowers) {
            NewFollowers = newFollowers;
        }

        public int getInteractions() {
            return Interactions;
        }

        public void setInteractions(int interactions) {
            Interactions = interactions;
        }

        public ArrayList<MonthArray> getMonthWiseAnalyticDetails() {
            return MonthWiseAnalyticDetails;
        }

        public void setMonthWiseAnalyticDetails(ArrayList<MonthArray> monthWiseAnalyticDetails) {
            MonthWiseAnalyticDetails = monthWiseAnalyticDetails;
        }
    }

    public static class DaysArray {
        private String Day;
        private String DayName;
        private int Likes;
        private int Comments;
        private int Clicks;
        private int Total;

        public String getDay() {
            return Day;
        }

        public void setDay(String day) {
            Day = day;
        }

        public String getDayName() {
            return DayName;
        }

        public void setDayName(String dayName) {
            DayName = dayName;
        }

        public int getLikes() {
            return Likes;
        }

        public void setLikes(int likes) {
            Likes = likes;
        }

        public int getComments() {
            return Comments;
        }

        public void setComments(int comments) {
            Comments = comments;
        }

        public int getClicks() {
            return Clicks;
        }

        public void setClicks(int clicks) {
            Clicks = clicks;
        }

        public int getTotal() {
            return Total;
        }

        public void setTotal(int total) {
            Total = total;
        }
    }

    public static class MonthArray {
        private int Months;
        private int Year;
        private int Likes;
        private int Comments;
        private int Clicks;
        private int Total;

        public int getMonths() {
            return Months;
        }

        public void setMonths(int months) {
            Months = months;
        }

        public int getYear() {
            return Year;
        }

        public void setYear(int year) {
            Year = year;
        }

        public int getLikes() {
            return Likes;
        }

        public void setLikes(int likes) {
            Likes = likes;
        }

        public int getComments() {
            return Comments;
        }

        public void setComments(int comments) {
            Comments = comments;
        }

        public int getClicks() {
            return Clicks;
        }

        public void setClicks(int clicks) {
            Clicks = clicks;
        }

        public int getTotal() {
            return Total;
        }

        public void setTotal(int total) {
            Total = total;
        }
    }
}