package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TermsOfuseModel {
    /**
     * Data : [{"TermsUseId":1,"TermsUse":"you must access and use our Services only for legal, authorized, and acceptable purposes. You will not use (or assist others in using) our Services in ways that: (a) violate, misappropriate, or infringe the rights of WhatsApp, our users, or others, including privacy, publicity, intellectual property, or other proprietary rights; (b) are illegal, obscene, defamatory, threatening, intimidating, harassing, hateful, racially, or ethnically offensive, or instigate or encourage conduct that would be illegal, or otherwise inappropriate, including promoting violent crimes; (c) involve publishing falsehoods, misrepresentations, or misleading statements; (d) impersonate someone; (e) involve sending illegal or impermissible communications such as bulk messaging, auto-messaging, auto-dialing, and the like; or (f) involve any non-personal use of our Services unless otherwise authorized by us."}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * TermsUseId : 1
         * TermsUse : you must access and use our Services only for legal, authorized, and acceptable purposes. You will not use (or assist others in using) our Services in ways that: (a) violate, misappropriate, or infringe the rights of WhatsApp, our users, or others, including privacy, publicity, intellectual property, or other proprietary rights; (b) are illegal, obscene, defamatory, threatening, intimidating, harassing, hateful, racially, or ethnically offensive, or instigate or encourage conduct that would be illegal, or otherwise inappropriate, including promoting violent crimes; (c) involve publishing falsehoods, misrepresentations, or misleading statements; (d) impersonate someone; (e) involve sending illegal or impermissible communications such as bulk messaging, auto-messaging, auto-dialing, and the like; or (f) involve any non-personal use of our Services unless otherwise authorized by us.
         */

        @SerializedName("TermsUseId")
        private int TermsUseId;
        @SerializedName("TermsUse")
        private String TermsUse;

        public int getTermsUseId() {
            return TermsUseId;
        }

        public void setTermsUseId(int TermsUseId) {
            this.TermsUseId = TermsUseId;
        }

        public String getTermsUse() {
            return TermsUse;
        }

        public void setTermsUse(String TermsUse) {
            this.TermsUse = TermsUse;
        }
    }
}
