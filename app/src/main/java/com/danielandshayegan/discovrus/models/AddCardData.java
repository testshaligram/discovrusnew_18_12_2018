package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddCardData {

    /**
     * Data : {"CustomerStripeId":"cus_DSSKu2zwM5yWBZ","CardID":"card_1D4nqVHDuTzFCAhXLLybcHl3"}
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Data")
    private DataBean Data;
    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Message")
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * CustomerStripeId : cus_DSSKu2zwM5yWBZ
         * CardID : card_1D4nqVHDuTzFCAhXLLybcHl3
         */

        @SerializedName("CustomerStripeId")
        private String CustomerStripeId;
        @SerializedName("CardID")
        private String CardID;

        public String getCustomerStripeId() {
            return CustomerStripeId;
        }

        public void setCustomerStripeId(String CustomerStripeId) {
            this.CustomerStripeId = CustomerStripeId;
        }

        public String getCardID() {
            return CardID;
        }

        public void setCardID(String CardID) {
            this.CardID = CardID;
        }
    }
}
