package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class FollowPeopleData extends Observable {

    /**
     * Data : {"ID":25,"UserID":42,"FollowUserID":35,"isActive":false,"Message":"Unfollowed Successfully."}
     * Success : true
     * Message : ["Save Successfully."]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * ID : 25
         * UserID : 42
         * FollowUserID : 35
         * isActive : false
         * Message : Unfollowed Successfully.
         */

        private int ID;
        private int UserID;
        private int FollowUserID;
        private boolean isActive;
        private String Message;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public int getFollowUserID() {
            return FollowUserID;
        }

        public void setFollowUserID(int FollowUserID) {
            this.FollowUserID = FollowUserID;
        }

        public boolean isIsActive() {
            return isActive;
        }

        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public String getMessage() {
            return Message;
        }

        public void setMessage(String Message) {
            this.Message = Message;
        }
    }
}
