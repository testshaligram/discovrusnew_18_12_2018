package com.danielandshayegan.discovrus.models;

import java.util.List;

public class GeneralSettingsData {

    /**
     * Data : [{"LoginId":12,"Comments":false,"Likes":false,"Mentions":false,"NewFollowers":false,"References":true,"Stories":true,"TrendingNow":true,"SavePhotos":false,"SaveVideos":false,"AllowStorySharing":false,"SaveStories":false}]
     * Success : true
     * Message : []
     * Count :
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<?> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<?> getMessage() {
        return Message;
    }

    public void setMessage(List<?> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * LoginId : 12
         * Comments : false
         * Likes : false
         * Mentions : false
         * NewFollowers : false
         * References : true
         * Stories : true
         * TrendingNow : true
         * SavePhotos : false
         * SaveVideos : false
         * AllowStorySharing : false
         * SaveStories : false
         */

        private int LoginId;
        private boolean Comments;
        private boolean Likes;
        private boolean Mentions;
        private boolean NewFollowers;
        private boolean References;
        private boolean Stories;
        private boolean TrendingNow;
        private boolean SavePhotos;
        private boolean SaveVideos;
        private boolean AllowStorySharing;
        private boolean SaveStories;

        public int getLoginId() {
            return LoginId;
        }

        public void setLoginId(int LoginId) {
            this.LoginId = LoginId;
        }

        public boolean isComments() {
            return Comments;
        }

        public void setComments(boolean Comments) {
            this.Comments = Comments;
        }

        public boolean isLikes() {
            return Likes;
        }

        public void setLikes(boolean Likes) {
            this.Likes = Likes;
        }

        public boolean isMentions() {
            return Mentions;
        }

        public void setMentions(boolean Mentions) {
            this.Mentions = Mentions;
        }

        public boolean isNewFollowers() {
            return NewFollowers;
        }

        public void setNewFollowers(boolean NewFollowers) {
            this.NewFollowers = NewFollowers;
        }

        public boolean isReferences() {
            return References;
        }

        public void setReferences(boolean References) {
            this.References = References;
        }

        public boolean isStories() {
            return Stories;
        }

        public void setStories(boolean Stories) {
            this.Stories = Stories;
        }

        public boolean isTrendingNow() {
            return TrendingNow;
        }

        public void setTrendingNow(boolean TrendingNow) {
            this.TrendingNow = TrendingNow;
        }

        public boolean isSavePhotos() {
            return SavePhotos;
        }

        public void setSavePhotos(boolean SavePhotos) {
            this.SavePhotos = SavePhotos;
        }

        public boolean isSaveVideos() {
            return SaveVideos;
        }

        public void setSaveVideos(boolean SaveVideos) {
            this.SaveVideos = SaveVideos;
        }

        public boolean isAllowStorySharing() {
            return AllowStorySharing;
        }

        public void setAllowStorySharing(boolean AllowStorySharing) {
            this.AllowStorySharing = AllowStorySharing;
        }

        public boolean isSaveStories() {
            return SaveStories;
        }

        public void setSaveStories(boolean SaveStories) {
            this.SaveStories = SaveStories;
        }
    }
}
