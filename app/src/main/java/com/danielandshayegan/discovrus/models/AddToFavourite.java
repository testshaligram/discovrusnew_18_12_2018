package com.danielandshayegan.discovrus.models;

import java.util.List;

public class AddToFavourite {


    /**
     * Data : [{"PostId":1890,"UserID":139,"IsHighLight":false,"IsFavourite":true,"Follow":false}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PostId : 1890
         * UserID : 139
         * IsHighLight : false
         * IsFavourite : true
         * Follow : false
         */

        private int PostId;
        private int UserID;
        private boolean IsHighLight;
        private boolean IsFavourite;
        private boolean Follow;

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public boolean isIsHighLight() {
            return IsHighLight;
        }

        public void setIsHighLight(boolean IsHighLight) {
            this.IsHighLight = IsHighLight;
        }

        public boolean isIsFavourite() {
            return IsFavourite;
        }

        public void setIsFavourite(boolean IsFavourite) {
            this.IsFavourite = IsFavourite;
        }

        public boolean isFollow() {
            return Follow;
        }

        public void setFollow(boolean Follow) {
            this.Follow = Follow;
        }
    }
}
