package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ConnectPeopleData implements Serializable {

    /**
     * Data : [{"UserId":1,"UserName":"Parth Patel","UserType":"Business","UserImagePath":"/Files/User/user_20180622_071247833_ProfileImage.png","PageNumber":0,"PageSize":0,"Search":null},{"UserId":15,"UserName":"Prakash Parmar","UserType":"Business","UserImagePath":"/Files/User/user_20180625_124830964_cropped3439667019326222880.jpg","PageNumber":0,"PageSize":0,"Search":null},{"UserId":66,"UserName":"Tejas Padia","UserType":"Business","UserImagePath":"/Files/User/user_20180705_064531279_image.png","PageNumber":0,"PageSize":0,"Search":null},{"UserId":71,"UserName":"Nitin Patel","UserType":"Individual","UserImagePath":"/Files/User/user_20180709_025105345_ProfileImage.png","PageNumber":0,"PageSize":0,"Search":null}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean implements Serializable {
        /**
         * UserId : 1
         * UserName : Parth Patel
         * UserType : Business
         * UserImagePath : /Files/User/user_20180622_071247833_ProfileImage.png
         * PageNumber : 0
         * PageSize : 0
         * Search : null
         */

        @SerializedName("UserId")
        private int UserId;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserType")
        private String UserType;
        @SerializedName("UserImagePath")
        private String UserImagePath;
        @SerializedName("PageNumber")
        private int PageNumber;
        @SerializedName("PageSize")
        private int PageSize;
        @SerializedName("Search")
        private Object Search;
        public boolean isSelected;

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String UserType) {
            this.UserType = UserType;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public int getPageNumber() {
            return PageNumber;
        }

        public void setPageNumber(int PageNumber) {
            this.PageNumber = PageNumber;
        }

        public int getPageSize() {
            return PageSize;
        }

        public void setPageSize(int PageSize) {
            this.PageSize = PageSize;
        }

        public Object getSearch() {
            return Search;
        }

        public void setSearch(Object Search) {
            this.Search = Search;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
