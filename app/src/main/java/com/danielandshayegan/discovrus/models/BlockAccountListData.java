package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BlockAccountListData  {
    /**
     * Data : [{"UserID":12,"UserName":"hiren dixit","UserImage":"/Files/User/user_20180625_120336140_cropped1603633929.jpg","CreatedDate":"2018-11-13T08:06:36.097"}]
     * Success : true
     * Message : []
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<?> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<?> getMessage() {
        return Message;
    }

    public void setMessage(List<?> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * UserID : 12
         * UserName : hiren dixit
         * UserImage : /Files/User/user_20180625_120336140_cropped1603633929.jpg
         * CreatedDate : 2018-11-13T08:06:36.097
         */

        @SerializedName("UserID")
        private int UserID;
        @SerializedName("UserType")
        private String UserType;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImage")
        private String UserImage;
        @SerializedName("CreatedDate")
        private String CreatedDate;
        @SerializedName("BusinessName")
        private String BusinessName;

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String businessName) {
            BusinessName = businessName;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String userType) {
            UserType = userType;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserImage() {
            return UserImage;
        }

        public void setUserImage(String UserImage) {
            this.UserImage = UserImage;
        }

        public String getCreatedDate() {
            return CreatedDate;
        }

        public void setCreatedDate(String CreatedDate) {
            this.CreatedDate = CreatedDate;
        }
    }
}
