package com.danielandshayegan.discovrus.models;


import java.util.List;
import java.util.Observable;

public class LoginData extends Observable {

    /**
     * Data : {"Email":"hiren.d@sgit.in","Password":"XgQc9BHq14ONsN3WGlu2Lg==","RememberMe":false,"UserId":12,"FirstName":"hiren","LastName":"dixit","AdminUserRoleName":"Individual","Location":"ahmdabad","CategoryId":8,"CategoryName":"Government","Description":"Public service provided by the goverment","ImagePath":"/files/category/Government.png","Islogin":false,"SocialmediaId":"","BusinessName":"","WorkingAt":"Shaligram ","IsExistingUser":true,"IsEmail":true,"IsLinkedin":false,"IsInstagram":false,"IsFacebook":false,"UserImagePath":"/Files/User/user_20180625_120336140_cropped1603633929.jpg","IsVerified":true,"MessageType":0}
     * Success : true
     * Message : ["Login Successfully."]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * Email : hiren.d@sgit.in
         * Password : XgQc9BHq14ONsN3WGlu2Lg==
         * RememberMe : false
         * UserId : 12
         * FirstName : hiren
         * LastName : dixit
         * AdminUserRoleName : Individual
         * Location : ahmdabad
         * CategoryId : 8
         * CategoryName : Government
         * Description : Public service provided by the goverment
         * ImagePath : /files/category/Government.png
         * Islogin : false
         * SocialmediaId :
         * BusinessName :
         * WorkingAt : Shaligram
         * IsExistingUser : true
         * IsEmail : true
         * IsLinkedin : false
         * IsInstagram : false
         * IsFacebook : false
         * UserImagePath : /Files/User/user_20180625_120336140_cropped1603633929.jpg
         * IsVerified : true
         * MessageType : 0
         */

        private String Email;
        private String Password;
        private boolean RememberMe;
        private int UserId;
        private String FirstName;
        private String LastName;
        private String AdminUserRoleName;
        private String Location;
        private int CategoryId;
        private String CategoryName;
        private String Description;
        private String UserImagePath;
        private boolean Islogin;
        private String SocialmediaId;
        private String BusinessName;
        private String WorkingAt;
        private boolean IsExistingUser;
        private boolean IsEmail;
        private boolean IsLinkedin;
        private boolean IsInstagram;
        private boolean IsFacebook;
        private boolean IsVerified;
        private int MessageType;

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String Password) {
            this.Password = Password;
        }

        public boolean isRememberMe() {
            return RememberMe;
        }

        public void setRememberMe(boolean RememberMe) {
            this.RememberMe = RememberMe;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String FirstName) {
            this.FirstName = FirstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String LastName) {
            this.LastName = LastName;
        }

        public String getAdminUserRoleName() {
            return AdminUserRoleName;
        }

        public void setAdminUserRoleName(String AdminUserRoleName) {
            this.AdminUserRoleName = AdminUserRoleName;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int CategoryId) {
            this.CategoryId = CategoryId;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String CategoryName) {
            this.CategoryName = CategoryName;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }


        public boolean isIslogin() {
            return Islogin;
        }

        public void setIslogin(boolean Islogin) {
            this.Islogin = Islogin;
        }

        public String getSocialmediaId() {
            return SocialmediaId;
        }

        public void setSocialmediaId(String SocialmediaId) {
            this.SocialmediaId = SocialmediaId;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getWorkingAt() {
            return WorkingAt;
        }

        public void setWorkingAt(String WorkingAt) {
            this.WorkingAt = WorkingAt;
        }

        public boolean isIsExistingUser() {
            return IsExistingUser;
        }

        public void setIsExistingUser(boolean IsExistingUser) {
            this.IsExistingUser = IsExistingUser;
        }

        public boolean isIsEmail() {
            return IsEmail;
        }

        public void setIsEmail(boolean IsEmail) {
            this.IsEmail = IsEmail;
        }

        public boolean isIsLinkedin() {
            return IsLinkedin;
        }

        public void setIsLinkedin(boolean IsLinkedin) {
            this.IsLinkedin = IsLinkedin;
        }

        public boolean isIsInstagram() {
            return IsInstagram;
        }

        public void setIsInstagram(boolean IsInstagram) {
            this.IsInstagram = IsInstagram;
        }

        public boolean isIsFacebook() {
            return IsFacebook;
        }

        public void setIsFacebook(boolean IsFacebook) {
            this.IsFacebook = IsFacebook;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public boolean isIsVerified() {
            return IsVerified;
        }

        public void setIsVerified(boolean IsVerified) {
            this.IsVerified = IsVerified;
        }

        public int getMessageType() {
            return MessageType;
        }

        public void setMessageType(int MessageType) {
            this.MessageType = MessageType;
        }
    }
}
