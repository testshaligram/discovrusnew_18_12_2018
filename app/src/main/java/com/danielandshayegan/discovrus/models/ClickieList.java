package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClickieList {

    /**
     * Data : [{"ID":1,"UserID":0,"Type":"P","ImagePath":"/files/Clickie/dark-blue-clickie.png","Text":"","isActive":true,"FileName":null,"ImageMappingPath":null},{"ID":2,"UserID":0,"Type":"P","ImagePath":"/files/Clickie/green-clickie.png","Text":"","isActive":true,"FileName":null,"ImageMappingPath":null},{"ID":3,"UserID":0,"Type":"P","ImagePath":"/files/Clickie/multicolor-clickie.png","Text":"","isActive":true,"FileName":null,"ImageMappingPath":null},{"ID":4,"UserID":0,"Type":"P","ImagePath":"/files/Clickie/orange-clickie.png","Text":"","isActive":true,"FileName":null,"ImageMappingPath":null},{"ID":5,"UserID":0,"Type":"P","ImagePath":"/files/Clickie/peach-clickie.png","Text":"","isActive":true,"FileName":null,"ImageMappingPath":null},{"ID":6,"UserID":0,"Type":"P","ImagePath":"/files/Clickie/purple-clickie.png","Text":"","isActive":true,"FileName":null,"ImageMappingPath":null},{"ID":7,"UserID":0,"Type":"P","ImagePath":"/files/Clickie/red-clickie.png","Text":"","isActive":true,"FileName":null,"ImageMappingPath":null},{"ID":8,"UserID":0,"Type":"T","ImagePath":"/files/Clickie/sky-blue-clickie.png","Text":"this is text","isActive":true,"FileName":null,"ImageMappingPath":null}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * ID : 1
         * UserID : 0
         * Type : P
         * ImagePath : /files/Clickie/dark-blue-clickie.png
         * Text :
         * isActive : true
         * FileName : null
         * ImageMappingPath : null
         */

        @SerializedName("ID")
        private int ID;
        @SerializedName("UserID")
        private int UserID;
        @SerializedName("Type")
        private String Type;
        @SerializedName("ImagePath")
        private String ImagePath;
        @SerializedName("Text")
        private String Text;
        @SerializedName("isActive")
        private boolean isActive;
        @SerializedName("FileName")
        private Object FileName;
        @SerializedName("ImageMappingPath")
        private Object ImageMappingPath;

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public String getImagePath() {
            return ImagePath;
        }

        public void setImagePath(String ImagePath) {
            this.ImagePath = ImagePath;
        }

        public String getText() {
            return Text;
        }

        public void setText(String Text) {
            this.Text = Text;
        }

        public boolean isIsActive() {
            return isActive;
        }

        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public Object getFileName() {
            return FileName;
        }

        public void setFileName(Object FileName) {
            this.FileName = FileName;
        }

        public Object getImageMappingPath() {
            return ImageMappingPath;
        }

        public void setImageMappingPath(Object ImageMappingPath) {
            this.ImageMappingPath = ImageMappingPath;
        }
    }
}
