package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserProfileData {
    /**
     * Data : [{"UserID":12,"UserName":"hiren dixit","Email":"hiren.d@sgit.in","UserType":"Individual","UserImagePath":"/Files/User/user_20180625_120336140_cropped1603633929.jpg","Text":"You begin with a text, you sculpt information, you chisel away whats not needed, you come to the point, make things clear, add value, you re a content person, you like words.","Location":"ahmdabad","Lat":"","Long":"","IsFollow":true,"Followers":11,"Following":9,"post":[{"PostId":1799,"FileId":5036,"Description":"","Title":"","PostPath":"/Files/NewsFeed/Photo/c7fa3e0f-b966-46a4-bdee-bad2e43569a5.png","Type":"Photo","Liked":false,"Commented":true,"LikeCount":6,"CommentCount":11,"ViewCount":99,"ThumbFileName":"","PostCreatedDate":"09/12/2018 05:03:23","PageNumber":0,"PageSize":0,"TotalRecord":238,"Month":"Sep 18"},{"PostId":1795,"FileId":0,"Description":"<u>Cdd<\/u>","Title":"tfff","PostPath":"","Type":"Text","Liked":false,"Commented":false,"LikeCount":4,"CommentCount":0,"ViewCount":7,"ThumbFileName":"","PostCreatedDate":"09/10/2018 12:44:16","PageNumber":0,"PageSize":0,"TotalRecord":238,"Month":"Sep 18"},{"PostId":1794,"FileId":5032,"Description":"","Title":"","PostPath":"/Files/NewsFeed/Photo/5685bbf9-664c-44c3-9028-07410b49a717.png","Type":"Photo","Liked":false,"Commented":false,"LikeCount":0,"CommentCount":0,"ViewCount":5,"ThumbFileName":"","PostCreatedDate":"09/10/2018 12:43:19","PageNumber":0,"PageSize":0,"TotalRecord":238,"Month":"Sep 18"}]}]
     * Success : true
     * Message : ["Success"]
     * Count : 0/1.19
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * UserID : 12
         * UserName : hiren dixit
         * Email : hiren.d@sgit.in
         * UserType : Individual
         * UserImagePath : /Files/User/user_20180625_120336140_cropped1603633929.jpg
         * Text : You begin with a text, you sculpt information, you chisel away whats not needed, you come to the point, make things clear, add value, you re a content person, you like words.
         * Location : ahmdabad
         * Lat :
         * Long :
         * IsFollow : true
         * Followers : 11
         * Following : 9
         * post : [{"PostId":1799,"FileId":5036,"Description":"","Title":"","PostPath":"/Files/NewsFeed/Photo/c7fa3e0f-b966-46a4-bdee-bad2e43569a5.png","Type":"Photo","Liked":false,"Commented":true,"LikeCount":6,"CommentCount":11,"ViewCount":99,"ThumbFileName":"","PostCreatedDate":"09/12/2018 05:03:23","PageNumber":0,"PageSize":0,"TotalRecord":238,"Month":"Sep 18"},{"PostId":1795,"FileId":0,"Description":"<u>Cdd<\/u>","Title":"tfff","PostPath":"","Type":"Text","Liked":false,"Commented":false,"LikeCount":4,"CommentCount":0,"ViewCount":7,"ThumbFileName":"","PostCreatedDate":"09/10/2018 12:44:16","PageNumber":0,"PageSize":0,"TotalRecord":238,"Month":"Sep 18"},{"PostId":1794,"FileId":5032,"Description":"","Title":"","PostPath":"/Files/NewsFeed/Photo/5685bbf9-664c-44c3-9028-07410b49a717.png","Type":"Photo","Liked":false,"Commented":false,"LikeCount":0,"CommentCount":0,"ViewCount":5,"ThumbFileName":"","PostCreatedDate":"09/10/2018 12:43:19","PageNumber":0,"PageSize":0,"TotalRecord":238,"Month":"Sep 18"}]
         */

        private int UserID;
        private String UserName;
        private String Email;

        private String WorkingAt;
        private String UserType;
        private String BusinessName;
        private String UserImagePath;
        private String Text;
        private String Location;
        private String Lat;
        private String Long;
        private boolean IsFollow;
        private int Followers;
        private int Following;
        @SerializedName("post")
        private List<PostListData.Post> post;

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int UserID) {
            this.UserID = UserID;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String Email) {
            this.Email = Email;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String UserType) {
            this.UserType = UserType;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String businessName) {
            BusinessName = businessName;
        }

        public boolean isFollow() {
            return IsFollow;
        }

        public void setFollow(boolean follow) {
            IsFollow = follow;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getText() {
            return Text;
        }

        public void setText(String Text) {
            this.Text = Text;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String Lat) {
            this.Lat = Lat;
        }

        public String getLong() {
            return Long;
        }

        public void setLong(String Long) {
            this.Long = Long;
        }

        public boolean isIsFollow() {
            return IsFollow;
        }

        public void setIsFollow(boolean IsFollow) {
            this.IsFollow = IsFollow;
        }

        public int getFollowers() {
            return Followers;
        }

        public void setFollowers(int Followers) {
            this.Followers = Followers;
        }

        public int getFollowing() {
            return Following;
        }

        public void setFollowing(int Following) {
            this.Following = Following;
        }

        public List<PostListData.Post> getPost() {
            return post;
        }

        public String getWorkingAt() {
            return WorkingAt;
        }

        public void setWorkingAt(String workingAt) {
            WorkingAt = workingAt;
        }


        public void setPost(List<PostListData.Post> post) {
            this.post = post;
        }

        /*public static class PostBean {
            *//**
             * PostId : 1799
             * FileId : 5036
             * Description :
             * Title :
             * PostPath : /Files/NewsFeed/Photo/c7fa3e0f-b966-46a4-bdee-bad2e43569a5.png
             * Type : Photo
             * Liked : false
             * Commented : true
             * LikeCount : 6
             * CommentCount : 11
             * ViewCount : 99
             * ThumbFileName :
             * PostCreatedDate : 09/12/2018 05:03:23
             * PageNumber : 0
             * PageSize : 0
             * TotalRecord : 238
             * Month : Sep 18
             *//*

            private int PostId;
            private int FileId;
            private String Description;
            private String Title;
            private String PostPath;
            private String Type;
            private boolean Liked;
            private boolean Commented;
            private int LikeCount;
            private int CommentCount;
            private int ViewCount;
            private String ThumbFileName;
            private String PostCreatedDate;
            private int PageNumber;
            private int PageSize;
            private int TotalRecord;
            private String Month;

            public int getPostId() {
                return PostId;
            }

            public void setPostId(int PostId) {
                this.PostId = PostId;
            }

            public int getFileId() {
                return FileId;
            }

            public void setFileId(int FileId) {
                this.FileId = FileId;
            }

            public String getDescription() {
                return Description;
            }

            public void setDescription(String Description) {
                this.Description = Description;
            }

            public String getTitle() {
                return Title;
            }

            public void setTitle(String Title) {
                this.Title = Title;
            }

            public String getPostPath() {
                return PostPath;
            }

            public void setPostPath(String PostPath) {
                this.PostPath = PostPath;
            }

            public String getType() {
                return Type;
            }

            public void setType(String Type) {
                this.Type = Type;
            }

            public boolean isLiked() {
                return Liked;
            }

            public void setLiked(boolean Liked) {
                this.Liked = Liked;
            }

            public boolean isCommented() {
                return Commented;
            }

            public void setCommented(boolean Commented) {
                this.Commented = Commented;
            }

            public int getLikeCount() {
                return LikeCount;
            }

            public void setLikeCount(int LikeCount) {
                this.LikeCount = LikeCount;
            }

            public int getCommentCount() {
                return CommentCount;
            }

            public void setCommentCount(int CommentCount) {
                this.CommentCount = CommentCount;
            }

            public int getViewCount() {
                return ViewCount;
            }

            public void setViewCount(int ViewCount) {
                this.ViewCount = ViewCount;
            }

            public String getThumbFileName() {
                return ThumbFileName;
            }

            public void setThumbFileName(String ThumbFileName) {
                this.ThumbFileName = ThumbFileName;
            }

            public String getPostCreatedDate() {
                return PostCreatedDate;
            }

            public void setPostCreatedDate(String PostCreatedDate) {
                this.PostCreatedDate = PostCreatedDate;
            }

            public int getPageNumber() {
                return PageNumber;
            }

            public void setPageNumber(int PageNumber) {
                this.PageNumber = PageNumber;
            }

            public int getPageSize() {
                return PageSize;
            }

            public void setPageSize(int PageSize) {
                this.PageSize = PageSize;
            }

            public int getTotalRecord() {
                return TotalRecord;
            }

            public void setTotalRecord(int TotalRecord) {
                this.TotalRecord = TotalRecord;
            }

            public String getMonth() {
                return Month;
            }

            public void setMonth(String Month) {
                this.Month = Month;
            }
        }*/
    }
}