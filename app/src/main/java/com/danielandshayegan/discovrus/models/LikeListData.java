package com.danielandshayegan.discovrus.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class LikeListData {

    /**
     * Data : [{"UserId":32,"UserName":"Pooja  Dixit","UserImagePath":"/Files/User/user_20180626_043617714_Desert.jpg","UserRoleType":"Individual","BusinessName":"","WorkingAt":"abc","IsFollowing":false,"IsFollowers":false},{"UserId":3,"UserName":"Jess1  Joe1","UserImagePath":"/Files/User/user_20180622_073454486_ProfileImage.png","UserRoleType":"Individual","BusinessName":"","WorkingAt":"Indra rights","IsFollowing":false,"IsFollowers":false},{"UserId":15,"UserName":"Prakash  Parmar","UserImagePath":"/Files/User/user_20180625_124830964_cropped3439667019326222880.jpg","UserRoleType":"Business","BusinessName":"VM","WorkingAt":"","IsFollowing":true,"IsFollowers":false},{"UserId":12,"UserName":"hiren  dixit","UserImagePath":"/Files/User/user_20180625_120336140_cropped1603633929.jpg","UserRoleType":"Individual","BusinessName":"","WorkingAt":"Shaligram ","IsFollowing":false,"IsFollowers":false}]
     * Success : true
     * Message : ["Success"]
     * Count : 4
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean implements Parcelable {
        /**
         * UserId : 32
         * UserName : Pooja  Dixit
         * UserImagePath : /Files/User/user_20180626_043617714_Desert.jpg
         * UserRoleType : Individual
         * BusinessName :
         * WorkingAt : abc
         * IsFollowing : false
         * IsFollowers : false
         */


        private int UserId;
        private String UserName;
        private String UserImagePath;
        private String UserRoleType;
        private String BusinessName;
        private String WorkingAt;
        private boolean IsFollowing;
        private boolean IsFollowers;

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getUserRoleType() {
            return UserRoleType;
        }

        public void setUserRoleType(String UserRoleType) {
            this.UserRoleType = UserRoleType;
        }

        public String getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(String BusinessName) {
            this.BusinessName = BusinessName;
        }

        public String getWorkingAt() {
            return WorkingAt;
        }

        public void setWorkingAt(String WorkingAt) {
            this.WorkingAt = WorkingAt;
        }

        public boolean isIsFollowing() {
            return IsFollowing;
        }

        public void setIsFollowing(boolean IsFollowing) {
            this.IsFollowing = IsFollowing;
        }

        public boolean isIsFollowers() {
            return IsFollowers;
        }

        public void setIsFollowers(boolean IsFollowers) {
            this.IsFollowers = IsFollowers;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.UserId);
            dest.writeString(this.UserName);
            dest.writeString(this.UserImagePath);
            dest.writeString(this.UserRoleType);
            dest.writeString(this.BusinessName);
            dest.writeString(this.WorkingAt);
            dest.writeByte(this.IsFollowing ? (byte) 1 : (byte) 0);
            dest.writeByte(this.IsFollowers ? (byte) 1 : (byte) 0);
        }

        public DataBean() {
        }

        protected DataBean(Parcel in) {
            this.UserId = in.readInt();
            this.UserName = in.readString();
            this.UserImagePath = in.readString();
            this.UserRoleType = in.readString();
            this.BusinessName = in.readString();
            this.WorkingAt = in.readString();
            this.IsFollowing = in.readByte() != 0;
            this.IsFollowers = in.readByte() != 0;
        }

        public static final Parcelable.Creator<DataBean> CREATOR = new Parcelable.Creator<DataBean>() {
            @Override
            public DataBean createFromParcel(Parcel source) {
                return new DataBean(source);
            }

            @Override
            public DataBean[] newArray(int size) {
                return new DataBean[size];
            }
        };
    }
}
