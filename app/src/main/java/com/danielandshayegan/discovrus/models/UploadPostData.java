package com.danielandshayegan.discovrus.models;

import java.util.List;
import java.util.Observable;

public class UploadPostData extends Observable{


    /**
     * Data : {"PostId":0,"UserId":10,"UserName":null,"ImagePath":"/Files/NewsFeed/Photo/2fa7204d-3e06-4c59-9884-8ec3ca9f4663.jpg","FileId":3266,"FileName":"testing_5.jpg","Description":null,"ThumbFileName":null,"Email":null,"Location":null,"PostPath":null,"UserRole":null,"BusinessName":null,"PostType":null,"Type":"Photo","Title":null,"PostCreatedDate":null,"LikeCount":0,"CommentCount":0,"ViewCount":0,"UpdatedDate":null}
     * Success : true
     * Message : ["Image Uploaded Successfully"]
     * Count :
     */

    private DataBean Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public DataBean getData() {
        return Data;
    }

    public void setData(DataBean Data) {
        this.Data = Data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PostId : 0
         * UserId : 10
         * UserName : null
         * ImagePath : /Files/NewsFeed/Photo/2fa7204d-3e06-4c59-9884-8ec3ca9f4663.jpg
         * FileId : 3266
         * FileName : testing_5.jpg
         * Description : null
         * ThumbFileName : null
         * Email : null
         * Location : null
         * PostPath : null
         * UserRole : null
         * BusinessName : null
         * PostType : null
         * Type : Photo
         * Title : null
         * PostCreatedDate : null
         * LikeCount : 0
         * CommentCount : 0
         * ViewCount : 0
         * UpdatedDate : null
         */

        private int PostId;
        private int UserId;
        private Object UserName;
        private String ImagePath;
        private int FileId;
        private String FileName;
        private Object Description;
        private Object ThumbFileName;
        private Object Email;
        private Object Location;
        private Object PostPath;
        private Object UserRole;
        private Object BusinessName;
        private Object PostType;
        private String Type;
        private Object Title;
        private Object PostCreatedDate;
        private int LikeCount;
        private int CommentCount;
        private int ViewCount;
        private Object UpdatedDate;

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public Object getUserName() {
            return UserName;
        }

        public void setUserName(Object UserName) {
            this.UserName = UserName;
        }

        public String getImagePath() {
            return ImagePath;
        }

        public void setImagePath(String ImagePath) {
            this.ImagePath = ImagePath;
        }

        public int getFileId() {
            return FileId;
        }

        public void setFileId(int FileId) {
            this.FileId = FileId;
        }

        public String getFileName() {
            return FileName;
        }

        public void setFileName(String FileName) {
            this.FileName = FileName;
        }

        public Object getDescription() {
            return Description;
        }

        public void setDescription(Object Description) {
            this.Description = Description;
        }

        public Object getThumbFileName() {
            return ThumbFileName;
        }

        public void setThumbFileName(Object ThumbFileName) {
            this.ThumbFileName = ThumbFileName;
        }

        public Object getEmail() {
            return Email;
        }

        public void setEmail(Object Email) {
            this.Email = Email;
        }

        public Object getLocation() {
            return Location;
        }

        public void setLocation(Object Location) {
            this.Location = Location;
        }

        public Object getPostPath() {
            return PostPath;
        }

        public void setPostPath(Object PostPath) {
            this.PostPath = PostPath;
        }

        public Object getUserRole() {
            return UserRole;
        }

        public void setUserRole(Object UserRole) {
            this.UserRole = UserRole;
        }

        public Object getBusinessName() {
            return BusinessName;
        }

        public void setBusinessName(Object BusinessName) {
            this.BusinessName = BusinessName;
        }

        public Object getPostType() {
            return PostType;
        }

        public void setPostType(Object PostType) {
            this.PostType = PostType;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public Object getTitle() {
            return Title;
        }

        public void setTitle(Object Title) {
            this.Title = Title;
        }

        public Object getPostCreatedDate() {
            return PostCreatedDate;
        }

        public void setPostCreatedDate(Object PostCreatedDate) {
            this.PostCreatedDate = PostCreatedDate;
        }

        public int getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(int LikeCount) {
            this.LikeCount = LikeCount;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int CommentCount) {
            this.CommentCount = CommentCount;
        }

        public int getViewCount() {
            return ViewCount;
        }

        public void setViewCount(int ViewCount) {
            this.ViewCount = ViewCount;
        }

        public Object getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(Object UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }
    }
}
