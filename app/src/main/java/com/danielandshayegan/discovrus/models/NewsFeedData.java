package com.danielandshayegan.discovrus.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.danielandshayegan.discovrus.enums.NewsFeedTypeEnum;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NewsFeedData implements Parcelable {

    @SerializedName("UserID")
    @Expose
    private String UserID;

    @SerializedName("UserName")
    @Expose
    private String UserName;

    @SerializedName("BusinessName")
    @Expose
    private String BusinessName;

    @SerializedName("Email")
    @Expose
    private String Email;

    @SerializedName("UserType")
    @Expose
    private String UserType;

    @SerializedName("UserImagePath")
    @Expose
    private String UserImagePath;

    @SerializedName("Location")
    @Expose
    private String Location;
    @SerializedName("Text")
    @Expose
    private String Text;
    @SerializedName("WorkingAt")
    @Expose
    private String WorkingAt;
    @SerializedName("Lat")
    @Expose
    private Double Lat;
    @SerializedName("Long")
    @Expose
    private Double Long;
    @SerializedName("IsFollow")
    @Expose
    private boolean IsFollow;
    @SerializedName("Followers")
    @Expose
    private int Followers;
    @SerializedName("Following")
    @Expose
    private int Following;

    @SerializedName("Follow")
    @Expose
    private boolean Follow;

    @SerializedName("post")
    @Expose
    private ArrayList<PostData> postData;

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getBusinessName() {
        return BusinessName;
    }

    public void setBusinessName(String businessName) {
        BusinessName = businessName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getUserImagePath() {
        return UserImagePath;
    }

    public void setUserImagePath(String userImagePath) {
        UserImagePath = userImagePath;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getDisplayLocation() {
        return getLocation().substring(0, getLocation().indexOf(','));
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public Double getLat() {
        return Lat;
    }

    public void setLat(Double lat) {
        Lat = lat;
    }

    public Double getLong() {
        return Long;
    }

    public void setLong(Double aLong) {
        Long = aLong;
    }

    public boolean isFollow() {
        return IsFollow;
    }

    public int getFollowers() {
        return Followers;
    }

    public void setFollowers(int followers) {
        Followers = followers;
    }

    public int getFollowing() {
        return Following;
    }

    public void setFollowing(int following) {
        Following = following;
    }

    public boolean getFollow() {
        return Follow;
    }

    public void setFollow(boolean follow) {
        Follow = follow;
    }

    public ArrayList<PostData> getPostData() {
        return postData;
    }

    public void setPostData(ArrayList<PostData> postData) {
        this.postData = postData;
    }

    @Override
    public String toString() {
        return getUserType().equalsIgnoreCase("Individual") ? getUserName() : getBusinessName();
    }

    public String getWorkingAt() {
        return WorkingAt;
    }

    public void setWorkingAt(String workingAt) {
        WorkingAt = workingAt;
    }
    public static class PostData implements Parcelable {
        @SerializedName("UserID")
        @Expose
        private String UserID;

        @SerializedName("PostId")
        @Expose
        private String PostId;

        @SerializedName("FileId")
        @Expose
        private String FileId;

        @SerializedName("Description")
        @Expose
        private String Description;

        @SerializedName("Title")
        @Expose
        private String Title;

        @SerializedName("PostPath")
        @Expose
        private String PostPath;

        @SerializedName("OriginalImage")
        @Expose
        private String OriginalImage;

        @SerializedName("Type")
        @Expose
        private String Type;

        private int newsFeedTypeEnum;
//TODO:Make it array
        /*@SerializedName("TagUserID")
        @Expose
        private String TagUserID;*/

        @SerializedName("IsHighLight")
        @Expose
        private String IsHighLight;

        @SerializedName("isDetailedPost")
        @Expose
        private String isDetailedPost;

        @SerializedName("Lat")
        @Expose
        private String Lat;

        @SerializedName("Long")
        @Expose
        private String Long;

        @SerializedName("Distance")
        @Expose
        private String Distance;

        @SerializedName("Time")
        @Expose
        private String Time;

        @SerializedName("LikeCount")
        @Expose
        private String LikeCount;

        @SerializedName("CommentCount")
        @Expose
        private String CommentCount;

        @SerializedName("ViewCount")
        @Expose
        private String ViewCount;

        @SerializedName("Liked")
        @Expose
        private Boolean Liked;

        @SerializedName("Commented")
        @Expose
        private Boolean Commented;

        @SerializedName("Viewed")
        @Expose
        private Boolean Viewed;

        @SerializedName("IsPaid")
        @Expose
        private String IsPaid;

        @SerializedName("PaidAmount")
        @Expose
        private String PaidAmount;

        @SerializedName("ThumbFileName")
        @Expose
        private String ThumbFileName;
        @SerializedName("Month")
        @Expose
        private String Month;

        @SerializedName("PostCreatedDate")
        @Expose
        private String PostCreatedDate;

        @SerializedName("IsSave")
        @Expose
        private String IsSave;

        @SerializedName("IsCommentOrNot")
        @Expose
        private String IsCommentOrNot;

        @SerializedName("PostTimeAgo")
        @Expose
        private String PostTimeAgo;

        public String getUserID() {
            return UserID;
        }

        public void setUserID(String userID) {
            UserID = userID;
        }

        public String getPostId() {
            return PostId;
        }

        public void setPostId(String postId) {
            PostId = postId;
        }

        public String getFileId() {
            return FileId;
        }

        public void setFileId(String fileId) {
            FileId = fileId;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String title) {
            Title = title;
        }

        public String getPostPath() {
            return PostPath;
        }

        public void setPostPath(String postPath) {
            PostPath = postPath;
        }

        public String getOriginalImage() {
            return OriginalImage;
        }

        public void setOriginalImage(String originalImage) {
            OriginalImage = originalImage;
        }

        public String getType() {
            return Type;
        }

        public void setType(String type) {
            Type = type;
            setNewsFeedTypeEnum(NewsFeedTypeEnum.valueOf(Type).getIntType());
        }

        public int getNewsFeedTypeEnum() {
            return newsFeedTypeEnum;
        }

        public void setNewsFeedTypeEnum(int newsFeedTypeEnum) {
            this.newsFeedTypeEnum = newsFeedTypeEnum;
        }

/*        public String getTagUserID() {
            return TagUserID;
        }

        public void setTagUserID(String tagUserID) {
            TagUserID = tagUserID;
        }*/

        public String getIsHighLight() {
            return IsHighLight;
        }

        public void setIsHighLight(String isHighLight) {
            IsHighLight = isHighLight;
        }

        public String getIsDetailedPost() {
            return isDetailedPost;
        }

        public void setIsDetailedPost(String isDetailedPost) {
            this.isDetailedPost = isDetailedPost;
        }

        public String getLat() {
            return Lat;
        }

        public void setLat(String lat) {
            Lat = lat;
        }

        public String getLong() {
            return Long;
        }

        public void setLong(String aLong) {
            Long = aLong;
        }

        public String getDistance() {
            return Distance;
        }

        public void setDistance(String distance) {
            Distance = distance;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String time) {
            Time = time;
        }

        public String getLikeCount() {
            return LikeCount;
        }

        public void setLikeCount(String likeCount) {
            LikeCount = likeCount;
        }

        public String getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(String commentCount) {
            CommentCount = commentCount;
        }

        public String getViewCount() {
            return ViewCount;
        }

        public void setViewCount(String viewCount) {
            ViewCount = viewCount;
        }

        public Boolean getLiked() {
            return Liked;
        }

        public void setLiked(Boolean liked) {
            Liked = liked;
        }

        public Boolean getIsCommented() {
            return Commented;
        }

        public void setCommented(Boolean commented) {
            Commented = commented;
        }

        public Boolean getViewed() {
            return Viewed;
        }

        public void setViewed(Boolean viewed) {
            Viewed = viewed;
        }

        public String getIsPaid() {
            return IsPaid;
        }

        public void setIsPaid(String isPaid) {
            IsPaid = isPaid;
        }

        public String getPaidAmount() {
            return PaidAmount;
        }

        public void setPaidAmount(String paidAmount) {
            PaidAmount = paidAmount;
        }

        public String getThumbFileName() {
            return ThumbFileName;
        }

        public void setThumbFileName(String thumbFileName) {
            ThumbFileName = thumbFileName;
        }

        public String getPostCreatedDate() {
            return PostCreatedDate;
        }

        public void setPostCreatedDate(String postCreatedDate) {
            PostCreatedDate = postCreatedDate;
        }

        public String getIsSave() {
            return IsSave;
        }

        public void setIsSave(String isSave) {
            IsSave = isSave;
        }

        public Boolean getCommented() {
            return Commented;
        }

        public String getIsCommentOrNot() {
            return IsCommentOrNot;
        }

        public void setIsCommentOrNot(String isCommentOrNot) {
            IsCommentOrNot = isCommentOrNot;
        }

        public String getPostTimeAgo() {
            return PostTimeAgo;
        }

        public void setPostTimeAgo(String postTimeAgo) {
            PostTimeAgo = postTimeAgo;
        }

        public String getMonth() {
            return Month;
        }

        public void setMonth(String month) {
            Month = month;
        }
        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.UserID);
            dest.writeString(this.PostId);
            dest.writeString(this.FileId);
            dest.writeString(this.Description);
            dest.writeString(this.Title);
            dest.writeString(this.PostPath);
            dest.writeString(this.OriginalImage);
            dest.writeString(this.Type);
            dest.writeInt(this.newsFeedTypeEnum);
            dest.writeString(this.IsHighLight);
            dest.writeString(this.isDetailedPost);
            dest.writeString(this.Lat);
            dest.writeString(this.Long);
            dest.writeString(this.Distance);
            dest.writeString(this.Time);
            dest.writeString(this.LikeCount);
            dest.writeString(this.CommentCount);
            dest.writeString(this.ViewCount);
            dest.writeValue(this.Liked);
            dest.writeValue(this.Commented);
            dest.writeValue(this.Viewed);
            dest.writeString(this.IsPaid);
            dest.writeString(this.PaidAmount);
            dest.writeString(this.ThumbFileName);
            dest.writeString(this.PostCreatedDate);
            dest.writeString(this.IsSave);
            dest.writeString(this.IsCommentOrNot);
            dest.writeString(this.PostTimeAgo);
        }

        public PostData() {
        }

        protected PostData(Parcel in) {
            this.UserID = in.readString();
            this.PostId = in.readString();
            this.FileId = in.readString();
            this.Description = in.readString();
            this.Title = in.readString();
            this.PostPath = in.readString();
            this.OriginalImage = in.readString();
            this.Type = in.readString();
            this.newsFeedTypeEnum = in.readInt();
            this.IsHighLight = in.readString();
            this.isDetailedPost = in.readString();
            this.Lat = in.readString();
            this.Long = in.readString();
            this.Distance = in.readString();
            this.Time = in.readString();
            this.LikeCount = in.readString();
            this.CommentCount = in.readString();
            this.ViewCount = in.readString();
            this.Liked = (Boolean) in.readValue(Boolean.class.getClassLoader());
            this.Commented = (Boolean) in.readValue(Boolean.class.getClassLoader());
            this.Viewed = (Boolean) in.readValue(Boolean.class.getClassLoader());
            this.IsPaid = in.readString();
            this.PaidAmount = in.readString();
            this.ThumbFileName = in.readString();
            this.PostCreatedDate = in.readString();
            this.IsSave = in.readString();
            this.IsCommentOrNot = in.readString();
            this.PostTimeAgo = in.readString();
        }

        public Creator<PostData> getCREATOR() {
            return CREATOR;
        }

        public static final Parcelable.Creator<PostData> CREATOR = new Parcelable.Creator<PostData>() {
            @Override
            public PostData createFromParcel(Parcel source) {
                return new PostData(source);
            }

            @Override
            public PostData[] newArray(int size) {
                return new PostData[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.UserID);
        dest.writeString(this.UserName);
        dest.writeString(this.BusinessName);
        dest.writeString(this.Email);
        dest.writeString(this.UserType);
        dest.writeString(this.UserImagePath);
        dest.writeString(this.Location);
        dest.writeByte(this.Follow ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.postData);
    }

    public NewsFeedData() {
    }

    protected NewsFeedData(Parcel in) {
        this.UserID = in.readString();
        this.UserName = in.readString();
        this.BusinessName = in.readString();
        this.Email = in.readString();
        this.UserType = in.readString();
        this.UserImagePath = in.readString();
        this.Location = in.readString();
        this.Follow = in.readByte() != 0;
        this.postData = in.createTypedArrayList(PostData.CREATOR);
    }

    public static final Parcelable.Creator<NewsFeedData> CREATOR = new Parcelable.Creator<NewsFeedData>() {
        @Override
        public NewsFeedData createFromParcel(Parcel source) {
            return new NewsFeedData(source);
        }

        @Override
        public NewsFeedData[] newArray(int size) {
            return new NewsFeedData[size];
        }
    };
}