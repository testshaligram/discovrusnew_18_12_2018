package com.danielandshayegan.discovrus.models;

import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;

import java.util.List;
import java.util.Observable;

public class CategoryList extends Observable{

    /**
     * Data : [{"CategoryId":1,"CategoryName":"Beauty","Description":"this is Beauty","ImagePath":"/files/category/Beauty.png"},{"CategoryId":2,"CategoryName":"Charity","Description":"this is Charity","ImagePath":"/files/category/Charity.png"},{"CategoryId":3,"CategoryName":"Creative","Description":"this is Creative","ImagePath":"/files/category/Creative.png"}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    private List<CategoriesTableModel> Data;
    private boolean Success;
    private String Count;
    private List<String> Message;

    public List<CategoriesTableModel> getData() {
        return Data;
    }

    public void setData(List<CategoriesTableModel> data) {
        Data = data;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * CategoryId : 1
         * CategoryName : Beauty
         * Description : this is Beauty
         * ImagePath : /files/category/Beauty.png
         */

        private int CategoryId;
        private String CategoryName;
        private String Description;
        private String ImagePath;

        public int getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(int categoryId) {
            CategoryId = categoryId;
        }

        public String getCategoryName() {
            return CategoryName;
        }

        public void setCategoryName(String categoryName) {
            CategoryName = categoryName;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getImagePath() {
            return ImagePath;
        }

        public void setImagePath(String imagePath) {
            ImagePath = imagePath;
        }
    }
}
