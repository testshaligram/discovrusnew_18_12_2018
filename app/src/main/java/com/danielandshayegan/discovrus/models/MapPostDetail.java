package com.danielandshayegan.discovrus.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MapPostDetail {

    /**
     * Data : [{"PostId":1256,"UserId":12,"Lat":null,"Long":null,"UserName":"hiren dixit","UserImagePath":"/Files/User/user_20180625_120336140_cropped1603633929.jpg","PostPath":"/Files/NewsFeed/Photo/9143451e-6879-44f5-837a-64bcb17e771e.png","ThumbFileName":null,"Type":"Photo","UserType":"Individual","Location":"ahmdabad","Title":"","Description":"","Distance":3.721,"Time":"06/28/2018 10:32:32","CommentLikeCount":0,"IsCommentLike":false,"CommentCount":0,"IsCommented":false,"PostCountInWeek":1}]
     * Success : true
     * Message : ["Success"]
     * Count :
     */

    @SerializedName("Success")
    private boolean Success;
    @SerializedName("Count")
    private String Count;
    @SerializedName("Data")
    private List<DataBean> Data;
    @SerializedName("Message")
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * PostId : 1256
         * UserId : 12
         * Lat : null
         * Long : null
         * UserName : hiren dixit
         * UserImagePath : /Files/User/user_20180625_120336140_cropped1603633929.jpg
         * PostPath : /Files/NewsFeed/Photo/9143451e-6879-44f5-837a-64bcb17e771e.png
         * ThumbFileName : null
         * Type : Photo
         * UserType : Individual
         * Location : ahmdabad
         * Title :
         * Description :
         * Distance : 3.721
         * Time : 06/28/2018 10:32:32
         * CommentLikeCount : 0
         * IsCommentLike : false
         * CommentCount : 0
         * IsCommented : false
         * PostCountInWeek : 1
         */

        @SerializedName("PostId")
        private int PostId;
        @SerializedName("UserId")
        private int UserId;
        @SerializedName("Lat")
        private Object Lat;
        @SerializedName("Long")
        private Object Long;
        @SerializedName("UserName")
        private String UserName;
        @SerializedName("UserImagePath")
        private String UserImagePath;
        @SerializedName("PostPath")
        private String PostPath;
        @SerializedName("ThumbFileName")
        private Object ThumbFileName;
        @SerializedName("Type")
        private String Type;
        @SerializedName("UserType")
        private String UserType;
        @SerializedName("Location")
        private String Location;
        @SerializedName("Title")
        private String Title;
        @SerializedName("Description")
        private String Description;
        @SerializedName("Distance")
        private double Distance;
        @SerializedName("Time")
        private String Time;
        @SerializedName("CommentLikeCount")
        private int CommentLikeCount;
        @SerializedName("IsCommentLike")
        private boolean IsCommentLike;
        @SerializedName("CommentCount")
        private int CommentCount;
        @SerializedName("IsCommented")
        private boolean IsCommented;
        @SerializedName("PostCountInWeek")
        private int PostCountInWeek;

        public int getPostId() {
            return PostId;
        }

        public void setPostId(int PostId) {
            this.PostId = PostId;
        }

        public int getUserId() {
            return UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        public Object getLat() {
            return Lat;
        }

        public void setLat(Object Lat) {
            this.Lat = Lat;
        }

        public Object getLong() {
            return Long;
        }

        public void setLong(Object Long) {
            this.Long = Long;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getPostPath() {
            return PostPath;
        }

        public void setPostPath(String PostPath) {
            this.PostPath = PostPath;
        }

        public Object getThumbFileName() {
            return ThumbFileName;
        }

        public void setThumbFileName(Object ThumbFileName) {
            this.ThumbFileName = ThumbFileName;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }

        public String getUserType() {
            return UserType;
        }

        public void setUserType(String UserType) {
            this.UserType = UserType;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public String getTitle() {
            return Title;
        }

        public void setTitle(String Title) {
            this.Title = Title;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String Description) {
            this.Description = Description;
        }

        public double getDistance() {
            return Distance;
        }

        public void setDistance(double Distance) {
            this.Distance = Distance;
        }

        public String getTime() {
            return Time;
        }

        public void setTime(String Time) {
            this.Time = Time;
        }

        public int getCommentLikeCount() {
            return CommentLikeCount;
        }

        public void setCommentLikeCount(int CommentLikeCount) {
            this.CommentLikeCount = CommentLikeCount;
        }

        public boolean isIsCommentLike() {
            return IsCommentLike;
        }

        public void setIsCommentLike(boolean IsCommentLike) {
            this.IsCommentLike = IsCommentLike;
        }

        public int getCommentCount() {
            return CommentCount;
        }

        public void setCommentCount(int CommentCount) {
            this.CommentCount = CommentCount;
        }

        public boolean isIsCommented() {
            return IsCommented;
        }

        public void setIsCommented(boolean IsCommented) {
            this.IsCommented = IsCommented;
        }

        public int getPostCountInWeek() {
            return PostCountInWeek;
        }

        public void setPostCountInWeek(int PostCountInWeek) {
            this.PostCountInWeek = PostCountInWeek;
        }
    }
}
