package com.danielandshayegan.discovrus.models;

import java.util.List;

public class ReferenceListByUserId {

    /**
     * Data : [{"FromUserID":2,"ToUserID":66,"ReferenceID":"1,2,3,4","UpdatedDate":"07/11/2018 13:24:59","UserImagePath":"/Files/User/user_20180622_071726058_cropped1807688146.jpg","UserName":"Hiren Dixit","Location":"ahmedabad, gujarat","ReferenceText":[{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]},{"FromUserID":3,"ToUserID":66,"ReferenceID":"1,2","UpdatedDate":"07/11/2018 13:25:19","UserImagePath":"/Files/User/user_20180622_073454486_ProfileImage.png","UserName":"Jess1 Joe1","Location":"London, UK","ReferenceText":[{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]},{"FromUserID":1,"ToUserID":66,"ReferenceID":"1,2,3,4","UpdatedDate":"07/11/2018 13:11:23","UserImagePath":"/Files/User/user_20180622_071247833_ProfileImage.png","UserName":"Parth Patel","Location":"Ahmedabad Area, India","ReferenceText":[{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]}]
     * Success : true
     * Message : ["Success"]
     * Count : 3
     */

    private boolean Success;
    private String Count;
    private List<DataBean> Data;
    private List<String> Message;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean Success) {
        this.Success = Success;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }

    public List<DataBean> getData() {
        return Data;
    }

    public void setData(List<DataBean> Data) {
        this.Data = Data;
    }

    public List<String> getMessage() {
        return Message;
    }

    public void setMessage(List<String> Message) {
        this.Message = Message;
    }

    public static class DataBean {
        /**
         * FromUserID : 2
         * ToUserID : 66
         * ReferenceID : 1,2,3,4
         * UpdatedDate : 07/11/2018 13:24:59
         * UserImagePath : /Files/User/user_20180622_071726058_cropped1807688146.jpg
         * UserName : Hiren Dixit
         * Location : ahmedabad, gujarat
         * ReferenceText : [{"ReferenceID":1,"ReferenceText":"Reliable"},{"ReferenceID":2,"ReferenceText":"Fast Service"},{"ReferenceID":3,"ReferenceText":"Value For Money"}]
         */

        private int FromUserID;
        private int ToUserID;
        private String ReferenceID;
        private String UpdatedDate;
        private String UserImagePath;
        private String UserName;
        private String Location;
        private List<ReferenceTextBean> ReferenceText;

        public int getFromUserID() {
            return FromUserID;
        }

        public void setFromUserID(int FromUserID) {
            this.FromUserID = FromUserID;
        }

        public int getToUserID() {
            return ToUserID;
        }

        public void setToUserID(int ToUserID) {
            this.ToUserID = ToUserID;
        }

        public String getReferenceID() {
            return ReferenceID;
        }

        public void setReferenceID(String ReferenceID) {
            this.ReferenceID = ReferenceID;
        }

        public String getUpdatedDate() {
            return UpdatedDate;
        }

        public void setUpdatedDate(String UpdatedDate) {
            this.UpdatedDate = UpdatedDate;
        }

        public String getUserImagePath() {
            return UserImagePath;
        }

        public void setUserImagePath(String UserImagePath) {
            this.UserImagePath = UserImagePath;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getLocation() {
            return Location;
        }

        public void setLocation(String Location) {
            this.Location = Location;
        }

        public List<ReferenceTextBean> getReferenceText() {
            return ReferenceText;
        }

        public void setReferenceText(List<ReferenceTextBean> ReferenceText) {
            this.ReferenceText = ReferenceText;
        }

        public static class ReferenceTextBean {
            /**
             * ReferenceID : 1
             * ReferenceText : Reliable
             */

            private int ReferenceID;
            private String ReferenceText;
            private boolean clickable;
            private String reviewId;

            public int getReferenceID() {
                return ReferenceID;
            }

            public void setReferenceID(int ReferenceID) {
                this.ReferenceID = ReferenceID;
            }

            public String getReferenceText() {
                return ReferenceText;
            }

            public void setReferenceText(String ReferenceText) {
                this.ReferenceText = ReferenceText;
            }

            public boolean isClickable() {
                return clickable;
            }

            public void setClickable(boolean clickable) {
                this.clickable = clickable;
            }

            public String getReviewId() {
                return reviewId;
            }

            public void setReviewId(String reviewId) {
                this.reviewId = reviewId;
            }
        }
    }
}
