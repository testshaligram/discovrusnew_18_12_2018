package com.danielandshayegan.discovrus.enums;

/**
 * Created by SHIKHA
 */
public enum NewsFeedTypeEnum {
    Photo("Photo", 0), Text("Text", 1), Video("Video", 2), PhotoAndText("PhotoAndText", 3);

    private String type;
    private int intType;

    private NewsFeedTypeEnum(String s, int intType) {
        type = s;
        this.intType = intType;
    }

    public boolean equalsType(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return type.equals(otherName);
    }

    public String toString() {
        return this.type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIntType() {
        return intType;
    }

    public void setIntType(int intType) {
        this.intType = intType;
    }


}