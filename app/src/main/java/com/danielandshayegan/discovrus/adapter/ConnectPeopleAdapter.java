package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.RowConnectPeopleBinding;
import com.danielandshayegan.discovrus.models.ConnectPeopleData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.fragment.DetailPostConnectPeopleFragment;

public class ConnectPeopleAdapter extends BaseAdapter<ConnectPeopleData.DataBean> {

    // region Member Variables
    private ConnectPeopleAdapter.FooterViewHolder footerViewHolder;
    static Context mContext;
    // endregion

    // region Constructors
    public ConnectPeopleAdapter() {
        super();
    }

    public ConnectPeopleAdapter(Context context) {
        this.mContext = context;
    }
    // endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        RowConnectPeopleBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_connect_people, parent, false);

        final PeopleViewHolder holder = new PeopleViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PeopleViewHolder holder = (PeopleViewHolder) viewHolder;

        final ConnectPeopleData.DataBean people = getItem(position);
        if (people != null) {
            holder.bind(people);
        }
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new ConnectPeopleData.DataBean());
    }

    // region Inner Classes
    public static class PeopleViewHolder extends RecyclerView.ViewHolder {
        RowConnectPeopleBinding mBinder;

        public PeopleViewHolder(RowConnectPeopleBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(ConnectPeopleData.DataBean data) {

            mBinder.txtUserName.setText(data.getUserName());
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder);

            Glide.with(mContext)
                    .load(ApiClient.WebService.imageUrl + data.getUserImagePath())
                    .apply(options)
                    .into(mBinder.imgProfilePic);

            if (data.isSelected()) {
                mBinder.imgSelected.setVisibility(View.VISIBLE);
                mBinder.txtUserName.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            } else {
                mBinder.imgSelected.setVisibility(View.GONE);
                mBinder.txtUserName.setTextColor(mContext.getResources().getColor(R.color.secondary_text_dark));
            }

            mBinder.main.setOnClickListener(view -> {
                if (DetailPostConnectPeopleFragment.peopleFragment.selectedPeopleList.containsKey(String.valueOf(data.getUserId()))) {
                    data.setSelected(false);
                    DetailPostConnectPeopleFragment.peopleFragment.selectedPeopleList.remove(String.valueOf(data.getUserId()));
                    mBinder.imgSelected.setVisibility(View.GONE);
                    mBinder.txtUserName.setTextColor(mContext.getResources().getColor(R.color.secondary_text_dark));
                    DetailPostConnectPeopleFragment.peopleFragment.mBinding.uiConnectPeople.chipsView.removeChipBy(data);
                } else {
                    data.setSelected(true);
                    DetailPostConnectPeopleFragment.peopleFragment.selectedPeopleList.put(String.valueOf(data.getUserId()), data);
                    mBinder.imgSelected.setVisibility(View.VISIBLE);
                    mBinder.txtUserName.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    DetailPostConnectPeopleFragment.peopleFragment.mBinding.uiConnectPeople.chipsView.addChip(data.getUserName(), ApiClient.WebService.imageUrl + data.getUserImagePath(), data, false);
                }
            });

        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }

}
