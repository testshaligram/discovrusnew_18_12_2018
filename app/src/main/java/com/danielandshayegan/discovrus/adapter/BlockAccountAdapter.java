package com.danielandshayegan.discovrus.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ItemBlockAccountBinding;
import com.danielandshayegan.discovrus.models.BlockAccountListData;
import com.danielandshayegan.discovrus.models.HiddenDataListModel;
import com.danielandshayegan.discovrus.network.ApiClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BlockAccountAdapter extends RecyclerView.Adapter<BlockAccountAdapter.DataHolder> {
    List<BlockAccountListData.DataBean> data;
    Context context;
    UnblockAccountListner unblockAccountListner;

    public BlockAccountAdapter(List<BlockAccountListData.DataBean> data, Context context,UnblockAccountListner unblockAccountListner) {
        this.data = data;
        this.context=context;
        this.unblockAccountListner=unblockAccountListner;
    }

    @NonNull
    @Override
    public BlockAccountAdapter.DataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemBlockAccountBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_block_account, parent, false);
        return new DataHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataHolder holder, int position) {
        BlockAccountListData.DataBean blockdata = data.get(position);

        if(blockdata.getUserType().equals("Business"))
            holder.itemBlockAccountBinding.txtCommentTitle.setText(blockdata.getBusinessName());
        else
            holder.itemBlockAccountBinding.txtCommentTitle.setText(blockdata.getUserName());

        String date = blockdata.getCreatedDate().replaceAll("T", " ");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("MMM dd, yyyy");
        String targetDateValue = targetFormat.format(sourceDate);

        holder.itemBlockAccountBinding.txtCommentDuration.setText("Blocked " + targetDateValue);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_placeholder)
                .error(R.drawable.user_placeholder);

        Glide.with(context)
                .load(ApiClient.WebService.imageUrl + blockdata.getUserImage())
                .apply(options)
                .into(holder.itemBlockAccountBinding.imgUserProile);
    }

    @Override
    public int getItemCount() {

        if (data != null)
            return data.size();
        else return 0;
    }

    public class DataHolder extends RecyclerView.ViewHolder {
        ItemBlockAccountBinding itemBlockAccountBinding;

        public DataHolder(ItemBlockAccountBinding itemBlockAccountBinding) {
            super(itemBlockAccountBinding.getRoot());
            this.itemBlockAccountBinding = itemBlockAccountBinding;
            itemBlockAccountBinding.commentDelete.setOnClickListener(view -> {
                if (unblockAccountListner != null) {
                    unblockAccountListner.onUnblockAccountListner(data.get(getAdapterPosition()));
                }
            });
        }
    }
    public interface UnblockAccountListner {
        void onUnblockAccountListner(BlockAccountListData.DataBean dataBean);
    }
}