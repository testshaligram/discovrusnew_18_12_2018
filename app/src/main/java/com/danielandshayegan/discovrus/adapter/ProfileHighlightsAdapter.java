package com.danielandshayegan.discovrus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CustomHtml;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemHighlightListBinding;
import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.common.BaseActivityNew;
import com.danielandshayegan.discovrus.ui.newsfeed.NewsFeedDetailsFragment;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.webservice.CommonAPI;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class ProfileHighlightsAdapter extends BaseAdapter<PostListData.Post> {

    // region Member Variables
    private FooterViewHolder footerViewHolder;
    static Context mContext;
    private int selectedPosition = 0;
    // endregion

    // region Constructors
    public ProfileHighlightsAdapter() {
        super();
    }

    public ProfileHighlightsAdapter(Context context) {
        this.mContext = context;
    }
    // endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemHighlightListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_highlight_list, parent, false);

        final PostInnerViewHolder holder = new PostInnerViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PostInnerViewHolder holder = (PostInnerViewHolder) viewHolder;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int devicewidth = displaymetrics.widthPixels / 2;

        final PostListData.Post post = getItem(position);
        if (post != null) {
            holder.bind(post, (int) (devicewidth - mContext.getResources().getDimension(R.dimen._5sdp)));
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new PostListData.Post());
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        notifyDataSetChanged();
    }

    // region Inner Classes
    public class PostInnerViewHolder extends RecyclerView.ViewHolder {
        ItemHighlightListBinding mBinder;
        private DefaultTrackSelector trackSelector;

        public PostInnerViewHolder(ItemHighlightListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(PostListData.Post postData, int deviceHalfWidth) {

            mBinder.commentsCountTv.setText(String.valueOf(postData.getCommentCount()));
            mBinder.favoritesCountTv.setText(String.valueOf(postData.getLikeCount()));
            mBinder.viewsCountTv.setText(String.valueOf(postData.getViewCount()));
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .error(R.drawable.placeholder_image);

            switch (postData.getType()) {
                case "Photo":
                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.VISIBLE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.GONE);
                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.onlyImageIv);

                    break;
                case "Video":
                    mBinder.videoConstraint.setVisibility(View.VISIBLE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.GONE);

                    RequestOptions options1 = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder_video)
                            .error(R.drawable.placeholder_video);

                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getThumbFileName())
                            .apply(options1)
                            .into(mBinder.videoThumbIv);

                    break;
                case "Text":
                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.VISIBLE);

//                    mBinder.titleTv.setText(postData.getTitle());
//                    mBinder.descTv.setText(postData.getDescription());

                    String title = "";
                    try {
                        title = URLDecoder.decode(postData.getTitle(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String desc = "";
                    try {
                        desc = URLDecoder.decode(postData.getDescription(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    final CaseInsensitiveAssetFontLoader fontLoader = new CaseInsensitiveAssetFontLoader(mContext.getApplicationContext(), "fonts");
                    mBinder.titleTv.setText(CustomHtml.fromHtml(title, fontLoader));
                    mBinder.descTv.setText(CustomHtml.fromHtml(desc, fontLoader));
                    if (desc.contains("<ul>")) {
                        mBinder.descTv.setText(Html.fromHtml(desc));
                        switch (Utils.fontName) {
                            case "dancing_script_bold":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/dancing_script_bold.ttf"));
                                break;
                            case "hanaleifill_regular":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/hanaleifill_regular.ttf"));
                                break;
                            case "merienda_bold":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/merienda_bold.ttf"));
                                break;
                            case "opificio_light_rounded":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/opificio_light_rounded.ttf"));
                                break;
                            default:
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/avenir_roman.ttf"));
                                break;
                        }
                    }

                    break;
                case "PhotoAndText":
                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.VISIBLE);
                    mBinder.textConstraint.setVisibility(View.GONE);

                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.imageWithTextIv);
//                    mBinder.imgTitleTv.setText(postData.getTitle());
//                    mBinder.imgDescTv.setText(postData.getDescription());

                    String ttl = "";
                    try {
                        ttl = URLDecoder.decode(postData.getTitle(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String dsc = "";
                    try {
                        dsc = URLDecoder.decode(postData.getDescription(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    final CaseInsensitiveAssetFontLoader fontLoader1 = new CaseInsensitiveAssetFontLoader(mContext.getApplicationContext(), "fonts");
                    mBinder.imgTitleTv.setText(CustomHtml.fromHtml(ttl, fontLoader1));
                    mBinder.imgDescTv.setText(CustomHtml.fromHtml(dsc, fontLoader1));
                    if (dsc.contains("<ul>")) {
                        mBinder.imgDescTv.setText(Html.fromHtml(dsc));
                        switch (Utils.fontName) {
                            case "dancing_script_bold":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/dancing_script_bold.ttf"));
                                break;
                            case "hanaleifill_regular":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/hanaleifill_regular.ttf"));
                                break;
                            case "merienda_bold":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/merienda_bold.ttf"));
                                break;
                            case "opificio_light_rounded":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/opificio_light_rounded.ttf"));
                                break;
                            default:
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getApplicationContext().getAssets(), "fonts/avenir_roman.ttf"));
                                break;
                        }
                    }

                    break;
            }

            if (postData.isLiked())
                mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
            else
                mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));

            if (postData.isViewed())
                mBinder.viewIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_view));
            else
                mBinder.viewIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_view_icon));

            if (postData.isCommented())
                mBinder.commentIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
            else
                mBinder.commentIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));


            mBinder.favoriteIv.setOnClickListener(v -> {
                manageLikeClick(items.get(getAdapterPosition()), getAdapterPosition());
            });

            mBinder.mainCard.setOnClickListener(v -> {
                if (!items.get(getAdapterPosition()).isViewed())
                    manageViewClick(items.get(getAdapterPosition()), getAdapterPosition());
                ((BaseActivityNew) mContext).replaceFragment(new NewsFeedDetailsFragment());
            });

            mBinder.viewIv.setOnClickListener(v -> {
                if (!items.get(getAdapterPosition()).isViewed())
                    manageViewClick(items.get(getAdapterPosition()), getAdapterPosition());
                ((BaseActivityNew) mContext).replaceFragment(new NewsFeedDetailsFragment());
            });


        }

    }

    private void manageLikeClick(PostListData.Post npd, int position) {
        try {
            items.get(position).setLiked(!npd.isLiked());
            int n = items.get(position).getLikeCount();
            if (items.get(position).isLiked()) {
                n++;
            } else {
                n--;
            }
            items.get(position).setLikeCount(n);
            notifyDataSetChanged();
            CommonAPI.likePost(mContext, String.valueOf(npd.getPostId()), String.valueOf(npd.getUserID()), items.get(position).isLiked(), new CallbackTask() {
                @Override
                public void onFail(Object object) {

                }

                @Override
                public void onSuccess(Object object) {

                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void manageViewClick(PostListData.Post npd, int position) {
        items.get(position).setViewed(true);
        int n = items.get(position).getViewCount();
        n++;
        items.get(position).setViewCount(n);
        notifyDataSetChanged();
        CommonAPI.viewPost(mContext, String.valueOf(npd.getPostId()), new CallbackTask() {
            @Override
            public void onFail(Object object) {

            }

            @Override
            public void onSuccess(Object object) {

            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }


    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }

}
