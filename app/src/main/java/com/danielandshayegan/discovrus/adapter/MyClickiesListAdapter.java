package com.danielandshayegan.discovrus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemClickiesListBinding;
import com.danielandshayegan.discovrus.models.ClickieList;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.fragment.ClickiesFragment;
import com.danielandshayegan.discovrus.utils.LayoutToImage;

import java.io.ByteArrayOutputStream;

public class MyClickiesListAdapter extends BaseAdapter<ClickieList.DataBean> {

    // region Member Variables
    private PostInnerListAdapter.FooterViewHolder footerViewHolder;
    static Context mContext;
    public static boolean isVisible = false;
    // endregion

    // region Constructors
    public MyClickiesListAdapter() {
        super();
    }

    public MyClickiesListAdapter(Context context) {
        this.mContext = context;
    }
    // endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemClickiesListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_clickies_list, parent, false);

        final ClickiesViewHolder holder = new ClickiesViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        holder.mBinder.clickieIv.setOnLongClickListener(view -> {
            isVisible = true;
            ClickiesFragment.clickiesFragment.mBinding.uiClickies.btnDone.setVisibility(View.VISIBLE);
            notifyDataSetChanged();
            return false;
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final PostInnerListAdapter.FooterViewHolder holder = new PostInnerListAdapter.FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final ClickiesViewHolder holder = (ClickiesViewHolder) viewHolder;
        final ClickieList.DataBean data = getItem(position);
        holder.bind(data);

    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        PostInnerListAdapter.FooterViewHolder holder = (PostInnerListAdapter.FooterViewHolder) viewHolder;
        footerViewHolder = holder;
//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new ClickieList.DataBean());
    }

    // region Inner Classes
    public static class ClickiesViewHolder extends RecyclerView.ViewHolder {
        ItemClickiesListBinding mBinder;

        public ClickiesViewHolder(ItemClickiesListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(ClickieList.DataBean clickiesData) {

//            mBinder.clickieIv.setImageResource(clickiesData);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.clickie_1)
                    .error(R.drawable.clickie_1);

            Glide.with(mContext)
                    .load(ApiClient.WebService.imageUrl + clickiesData.getImagePath())
                    .apply(options)
                    .into(mBinder.clickieIv);

            if (isVisible) {
                mBinder.imgDelete.setVisibility(View.VISIBLE);
//                mBinder.main.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.wobble));
                mBinder.main.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.shake));


                /*final ScaleAnimation growAnim = new ScaleAnimation(1.0f, 1.05f, 1.0f, 1.05f);
                final ScaleAnimation shrinkAnim = new ScaleAnimation(1.05f, 1.0f, 1.05f, 1.0f);

                growAnim.setDuration(500);
                shrinkAnim.setDuration(500);

                mBinder.main.setAnimation(growAnim);
                growAnim.start();

                growAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mBinder.main.setAnimation(shrinkAnim);
                        shrinkAnim.start();
                    }
                });
                shrinkAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        mBinder.main.setAnimation(growAnim);
                        growAnim.start();
                    }
                });*/
            } else {
                mBinder.imgDelete.setVisibility(View.GONE);
                mBinder.main.clearAnimation();
            }

            mBinder.imgDelete.setOnClickListener(view -> {
                ClickiesFragment.clickiesFragment.deleteClickie(clickiesData.getID());
            });

            mBinder.clickieIv.setOnClickListener(view -> {
                LayoutToImage layoutToImage = new LayoutToImage(mContext, mBinder.clickieIv);
                Bitmap bitmap = layoutToImage.convert_layout();
//                String bitmapPath = MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap,"title", null);
//                Uri bitmapUri = Uri.parse(bitmapPath);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                Intent intent = new Intent();
                intent.putExtra("imageByteArray", byteArray);
                ((Activity) mContext).setResult(Activity.RESULT_OK, intent);
                ((Activity) mContext).finish();
            });

        }

    }

    public void hideDelete() {
        isVisible = false;
        notifyDataSetChanged();
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }
}