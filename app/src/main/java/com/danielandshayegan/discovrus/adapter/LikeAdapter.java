package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.LayoutLikeListBinding;
import com.danielandshayegan.discovrus.models.LikeListData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.activity.LikesActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class LikeAdapter extends RecyclerView.Adapter<LikeAdapter.MyViewHolder> {

    Context me;
    ArrayList<LikeListData.DataBean> likedList = new ArrayList<>();
    PostListData.Post dataBean;

    public LikeAdapter(Context me, ArrayList<LikeListData.DataBean> likedList, PostListData.Post dataBean) {
        this.me = me;
        this.likedList = likedList;
        this.dataBean = dataBean;
    }

    @NonNull
    @Override
    public LikeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutLikeListBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_like_list, parent, false);
        LikeAdapter.MyViewHolder holder = new LikeAdapter.MyViewHolder(mBinder);
        return holder;
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public void onBindViewHolder(@NonNull LikeAdapter.MyViewHolder holder, int position) {
        Picasso.with(me).
                load(ApiClient.WebService.imageUrl + likedList.get(position).getUserImagePath()).
                into(holder.binding.imgUser);

        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(me, LikesActivity.class);
                intent.putExtra("postDetails", dataBean);
                me.startActivity(intent);
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LayoutLikeListBinding binding;

        public MyViewHolder(LayoutLikeListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}