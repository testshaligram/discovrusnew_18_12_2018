package com.danielandshayegan.discovrus.adapter;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CustomHtml;
import com.danielandshayegan.discovrus.custome_veiws.autoplayvideos.AAH_CustomViewHolder;
import com.danielandshayegan.discovrus.custome_veiws.autoplayvideos.AAH_VideosAdapter;
import com.danielandshayegan.discovrus.databinding.ItemSaveDataBinding;
import com.danielandshayegan.discovrus.models.SavePostListData;
import com.danielandshayegan.discovrus.models.postDetailData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailActivity;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailVideoActivity;
import com.danielandshayegan.discovrus.ui.fragment.SavePostFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

public class SavePostAdapter extends AAH_VideosAdapter {

    private final SavePostListData list;
    //    private final Picasso picasso;
    ItemSaveDataBinding mBinder;
    SavePostFragment mContext;
    private static final int TYPE_VIDEO = 0, TYPE_TEXT = 1;

    public class MyViewHolder extends AAH_CustomViewHolder {
        ItemSaveDataBinding mBinder;
        //to mute/un-mute video (optional)
        boolean isMuted;

        public MyViewHolder(View x) {
            super(x);
        }

        public MyViewHolder(ItemSaveDataBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        //override this method to get callback when video starts to play
        @Override
        public void videoStarted() {
            super.videoStarted();
            mBinder.videoPlayIv.setVisibility(View.GONE);
            mBinder.imgPlayback.setVisibility(View.VISIBLE);
            if (isMuted) {
                muteVideo();
//                mBinder.imgVol.setImageResource(R.drawable.ic_mute);
                mBinder.imgVol.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_mute));
            } else {
                unmuteVideo();
//                mBinder.imgVol.setImageResource(R.drawable.ic_unmute);
                mBinder.imgVol.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_unmute));
            }
        }

        @Override
        public void pauseVideo() {
            super.pauseVideo();
//            img_playback.setImageResource(R.drawable.ic_play);
            mBinder.videoPlayIv.setVisibility(View.VISIBLE);
            mBinder.imgPlayback.setVisibility(View.GONE);
        }
    }

    public SavePostAdapter(SavePostListData list_urls, SavePostFragment mContext) {
        this.list = list_urls;
        this.mContext = mContext;
    }

    @Override
    public AAH_CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*if (viewType==TYPE_TEXT) {
            View itemView1 = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_text, parent, false);
            return new MyTextViewHolder(itemView1);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_card, parent, false);
            return new MyViewHolder(itemView);
        }*/

        mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_save_data, parent, false);

        return new SavePostAdapter.MyViewHolder(mBinder);

    }

    @Override
    public void onBindViewHolder(final AAH_CustomViewHolder holder, int position) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext.getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 2;

        //if you need 4-5-6 anything fix imageview in height
//            int deviceheight = displaymetrics.heightPixels / 4;

//        holder.mBinder.mainCard.getLayoutParams().width = devicewidth - dpToPx(20);

        //if you need same height as width you can set devicewidth in holder.image_view.getLayoutParams().height
//        card.getLayoutParams().height = devicewidth - 50;

        int deviceHalfWidth = (int) (devicewidth - mContext.getResources().getDimension(R.dimen._3sdp));
        int deviceFullWidth = (int) (displaymetrics.widthPixels);

        List<SavePostListData.DataBean.PostBean> postData = list.getData().get(0).getPost();

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.placeholder_image);

        switch (postData.get(position).getType()) {
            case "Photo":
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                layoutParams.width = deviceHalfWidth;
                if (position == 0)
                    layoutParams.setMargins(0, (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                else
                    layoutParams.setMargins((int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            0, (int) mContext.getResources().getDimension(R.dimen._5sdp));
                mBinder.mainCard.setLayoutParams(layoutParams);
                ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                lp.height = devicewidth;
                mBinder.postViewConstraint.setLayoutParams(lp);
//mBinder.onlyImageIv.setScaleType(ImageView.ScaleType.FIT_XY);
                mBinder.videoConstraint.setVisibility(View.GONE);
                mBinder.imageConstraint.setVisibility(View.VISIBLE);
                mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                mBinder.textConstraint.setVisibility(View.GONE);
                Glide.with(mContext)
                        .load(ApiClient.WebService.imageUrl + postData.get(position).getPostPath())
                        .apply(options)
                        .into(mBinder.onlyImageIv);

                break;
            case "Video":
                ConstraintLayout.LayoutParams layoutParams1 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                layoutParams1.width = deviceHalfWidth;
                if (position == 0)
                    layoutParams1.setMargins(0, (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                else
                    layoutParams1.setMargins((int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            0, (int) mContext.getResources().getDimension(R.dimen._5sdp));
                mBinder.mainCard.setLayoutParams(layoutParams1);
                ConstraintLayout.LayoutParams lp1 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                lp1.height = devicewidth;
                mBinder.postViewConstraint.setLayoutParams(lp1);
                mBinder.videoConstraint.setVisibility(View.VISIBLE);
                mBinder.imageConstraint.setVisibility(View.GONE);
                mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                mBinder.textConstraint.setVisibility(View.GONE);

                RequestOptions options1 = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder_video)
                        .error(R.drawable.placeholder_video);


                //todo
                holder.setImageUrl(ApiClient.WebService.imageUrl + postData.get(position).getThumbFileName());
                holder.setVideoUrl(ApplicationClass.getProxy().getProxyUrl(ApiClient.WebService.imageUrl + postData.get(position).getPostPath()));

                //load image into imageview
                if (postData.get(position).getThumbFileName() != null && !postData.get(position).getThumbFileName().isEmpty()) {
//                    picasso.load(holder.getImageUrl()).config(Bitmap.Config.RGB_565).into(holder.getAAH_ImageView());
                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.get(position).getThumbFileName())
                            .apply(options1)
                            .into(holder.getAAH_ImageView());
                }

                holder.setLooping(true); //optional - true by default

                //to play pause videos manually (optional)
                mBinder.imgPlayback.setOnClickListener(v -> {
                    if (holder.isPlaying()) {
                        holder.pauseVideo();
                        holder.setPaused(true);
                    } else {
                        holder.playVideo();
                        holder.setPaused(false);
                    }
                });

                mBinder.videoPlayIv.setOnClickListener(view -> {
                    if (holder.isPlaying()) {
                        holder.pauseVideo();
                        holder.setPaused(true);
                    } else {
                        holder.playVideo();
                        holder.setPaused(false);
                    }
                });

                //to mute/un-mute video (optional)
                mBinder.imgVol.setOnClickListener(v -> {
                    if (((SavePostAdapter.MyViewHolder) holder).isMuted) {
                        holder.unmuteVideo();
                        mBinder.imgVol.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_unmute));
//                        mBinder.imgVol.setImageResource(R.drawable.ic_unmute);
                    } else {
                        holder.muteVideo();
//                        mBinder.imgVol.setImageResource(R.drawable.ic_mute);
                        mBinder.imgVol.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_mute));
                    }
                    ((SavePostAdapter.MyViewHolder) holder).isMuted = !((SavePostAdapter.MyViewHolder) holder).isMuted;
                });

                if (postData.get(position).getPostPath() == null) {
                    mBinder.imgVol.setVisibility(View.GONE);
                    mBinder.imgPlayback.setVisibility(View.GONE);
                } else {
                    mBinder.imgVol.setVisibility(View.VISIBLE);
                    mBinder.imgPlayback.setVisibility(View.GONE);
                    mBinder.videoPlayIv.setVisibility(View.VISIBLE);
                }

                break;
            case "Text":
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                if (postData.get(position).isCommented())
                    layoutParams2.width = deviceHalfWidth;
                else
                    layoutParams2.width = deviceHalfWidth;
                if (position == 0)
                    layoutParams2.setMargins(0, (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                else
                    layoutParams2.setMargins((int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            0, (int) mContext.getResources().getDimension(R.dimen._5sdp));
                mBinder.mainCard.setLayoutParams(layoutParams2);

                if (postData.get(position).isCommented()) {
//                    mBinder.textConstraint.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    FrameLayout.LayoutParams imageParams = (FrameLayout.LayoutParams) mBinder.textConstraint.getLayoutParams();
                    imageParams.setMargins((int) mContext.getResources().getDimension(R.dimen._9sdp), (int) mContext.getResources().getDimension(R.dimen._9sdp),
                            (int) mContext.getResources().getDimension(R.dimen._9sdp), (int) mContext.getResources().getDimension(R.dimen._9sdp));
                    mBinder.textConstraint.setLayoutParams(imageParams);
                    mBinder.textConstraint.setBackground(mContext.getResources().getDrawable(R.drawable.new_half_post_bg));
                } else {
//                    mBinder.textConstraint.setBackgroundColor(mContext.getResources().getColor(R.color.bubble_bg));
                    ConstraintLayout.LayoutParams imageParams = (ConstraintLayout.LayoutParams) mBinder.textConstraint.getLayoutParams();
                    imageParams.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._3sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._3sdp));
                    mBinder.textConstraint.setLayoutParams(imageParams);
                    mBinder.textConstraint.setBackground(mContext.getResources().getDrawable(R.drawable.new_half_post_bg));
                }
                if (!postData.get(position).isCommented()) {
                    ConstraintLayout.LayoutParams lp2 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                    lp2.height = devicewidth;
                    mBinder.postViewConstraint.setLayoutParams(lp2);
                } else {
                    ConstraintLayout.LayoutParams lp2 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                    lp2.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    mBinder.postViewConstraint.setLayoutParams(lp2);
                }

                mBinder.videoConstraint.setVisibility(View.GONE);
                mBinder.imageConstraint.setVisibility(View.GONE);
                mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                mBinder.textConstraint.setVisibility(View.VISIBLE);
                String title = "";
                try {
                    title = URLDecoder.decode(postData.get(position).getTitle(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String desc = "";
                try {
                    desc = URLDecoder.decode(postData.get(position).getDescription(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                final CaseInsensitiveAssetFontLoader fontLoader = new CaseInsensitiveAssetFontLoader(mContext.mActivity.getApplicationContext(), "fonts");
                mBinder.titleTv.setText(CustomHtml.fromHtml(title, fontLoader));
                mBinder.descTv.setText(CustomHtml.fromHtml(desc, fontLoader));
                if (desc.contains("<ul>")) {
                    mBinder.descTv.setText(Html.fromHtml(desc));
                    switch (Utils.fontName) {
                        case "dancing_script_bold":
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                            break;
                        case "hanaleifill_regular":
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                            break;
                        case "merienda_bold":
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                            break;
                        case "opificio_light_rounded":
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                            break;
                        default:
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                            break;
                    }
                }
//                mBinder.titleTv.setText(postData.getTitle());
//                mBinder.descTv.setText(postData.getDescription());

                break;
            case "PhotoAndText":
                ConstraintLayout.LayoutParams layoutParams3 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                layoutParams3.width = deviceHalfWidth;
                if (position == 0)
                    layoutParams3.setMargins(0, (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                else
                    layoutParams3.setMargins((int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            0, (int) mContext.getResources().getDimension(R.dimen._5sdp));
                mBinder.mainCard.setLayoutParams(layoutParams3);
                ConstraintLayout.LayoutParams lp3 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                lp3.height = devicewidth;
                mBinder.postViewConstraint.setLayoutParams(lp3);
                mBinder.videoConstraint.setVisibility(View.GONE);
                mBinder.imageConstraint.setVisibility(View.GONE);
                mBinder.imageWithTextConstraint.setVisibility(View.VISIBLE);
                mBinder.textConstraint.setVisibility(View.GONE);

                Glide.with(mContext)
                        .load(ApiClient.WebService.imageUrl + postData.get(position).getPostPath())
                        .apply(options)
                        .into(mBinder.imageWithTextIv);

                String ttl = "";
                try {
                    ttl = URLDecoder.decode(postData.get(position).getTitle(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String dsc = "";
                try {
                    dsc = URLDecoder.decode(postData.get(position).getDescription(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                final CaseInsensitiveAssetFontLoader fontLoader1 = new CaseInsensitiveAssetFontLoader(mContext.mActivity.getApplicationContext(), "fonts");
                mBinder.imgTitleTv.setText(CustomHtml.fromHtml(ttl, fontLoader1));
                mBinder.imgDescTv.setText(CustomHtml.fromHtml(dsc, fontLoader1));
                if (dsc.contains("<ul>")) {
                    mBinder.imgDescTv.setText(Html.fromHtml(dsc));
                    switch (Utils.fontName) {
                        case "dancing_script_bold":
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                            break;
                        case "hanaleifill_regular":
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                            break;
                        case "merienda_bold":
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                            break;
                        case "opificio_light_rounded":
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                            break;
                        default:
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                            break;
                    }
                }
//                mBinder.imgTitleTv.setText(postData.getTitle());
//                mBinder.imgDescTv.setText(postData.getDescription());

                break;
        }
        if (postData.get(position).getUserType().equalsIgnoreCase("Business")) {
            mBinder.txtTitle.setText(postData.get(position).getBusinessName());
        } else
            mBinder.txtTitle.setText(postData.get(position).getUserName());

        mBinder.mainCard.setOnClickListener(view -> {
            SavePostFragment.savePostFragment.savePostView(postData.get(position).getPostId());
            String postType = postData.get(position).getType();
            switch (postType) {
                case "Video":
                    SavePostFragment.savePostFragment.getPostVideoDetails(postData.get(position).getPostId());
                    break;
                case "Photo":
                    SavePostFragment.savePostFragment.getPostImageDetails(postData.get(position).getPostId());
                    break;
                case "Text":
                    SavePostFragment.savePostFragment.getPostTextDetails(postData.get(position).getPostId());
                    break;
                case "PhotoAndText":
                    SavePostFragment.savePostFragment.getPostImageDetails(postData.get(position).getPostId());
                    break;
            }
        });

        mBinder.imgSavedPost.setOnClickListener(view -> {
            SavePostFragment.savePostFragment.unSavePost(position, postData.get(position).getPostId());
        });

/*
        mBinder.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("postDetails", list);
                Intent intent = new Intent(mContext.getActivity(), ClickPostDetailActivity.class);
                intent.putExtra("clickFragmentName", "postVideo");
                intent.putExtras(bundle);
                mContext.getActivity().startActivityForResult(intent, 510);
                mContext.getActivity().overridePendingTransition(0, 0);

                Intent intent = new Intent(mContext.getActivity(), ClickPostDetailActivity.class);
                intent.putExtra("clickFragmentName", "postText");
                intent.putExtra("postId", String.valueOf(postDetailData.getPostId()));
                mContext.startActivity(intent);
                mContext.getActivity().overridePendingTransition(0, 0);

            }
        });
*/
    }

    public List<SavePostListData.DataBean.PostBean> getList() {
        return list.getData().get(0).getPost();
    }

    @Override
    public int getItemCount() {
        return list.getData().get(0).getPost().size();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.getData().get(0).getPost().get(position).getType().equalsIgnoreCase("Video")) {
            return TYPE_VIDEO;
        } else return TYPE_TEXT;
    }

}
