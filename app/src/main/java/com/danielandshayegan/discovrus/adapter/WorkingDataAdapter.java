package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;

import java.util.ArrayList;
import java.util.List;

public class WorkingDataAdapter extends ArrayAdapter<String> {

    private Context context;
    private List<String> workingAtDataList;
    ViewHolder viewHolder;


    public WorkingDataAdapter(Context context, ArrayList<String> list) {
        super(context, 0, list);
        context = context;
        workingAtDataList = list;
    }

    private static class ViewHolder {
        private TextView itemView;


    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.row_working_data, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.itemView = (TextView) convertView.findViewById(R.id.txt_working_data);


            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (workingAtDataList.size() > 0) {
            viewHolder.itemView.setText(workingAtDataList.get(position).toString());
        }


        return convertView;
    }

}
