package com.danielandshayegan.discovrus.adapter;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.applozic.mobicomkit.uiwidgets.people.contact.DeviceContactSyncService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.autoplayvideos.AAH_CustomRecyclerView;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemPostListBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.activity.ProfileDetailActivity;
import com.danielandshayegan.discovrus.ui.fragment.PostManagementFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class PostListAdapter extends BaseAdapter<PostListData.PostList> {

    // region Member Variables
    private FooterViewHolder footerViewHolder;
    List<PostListData.Post> innerList;
    PostManagementFragment mContext;
    ItemPostListBinding mBinding;
    private RecyclerView recyclerView;

// endregion

    // region Constructors
    public PostListAdapter() {
        super();
    }

    public PostListAdapter(PostManagementFragment context, RecyclerView recyclerView) {
        mContext = context;
        this.recyclerView = recyclerView;
    }
// endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemPostListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_post_list, parent, false);

        final PostInnerViewHolder holder = new PostInnerViewHolder(mBinder, mContext);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });
        return holder;
    }


    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PostInnerViewHolder holder = (PostInnerViewHolder) viewHolder;

        final PostListData.PostList post = getItem(position);
        if (post != null) {
            holder.bind(post, post.getPost());
        }
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;

//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new PostListData.PostList());
    }

    public void addInnerData(List<PostListData.Post> tempinner) {
        this.innerList = tempinner;
    }

    public void setScrolled(int firstVisibleItemPosition) {
        //Log.e("setScrolled: ", "pos" + firstVisibleItemPosition);
        PostInnerViewHolder postInnerViewHolder = (PostInnerViewHolder) recyclerView.findViewHolderForAdapterPosition(firstVisibleItemPosition);
        if (postInnerViewHolder != null) {
            postInnerViewHolder.mBinder.postByUserRv.playAvailableVideos(0);
            stopOtherVideo(firstVisibleItemPosition);
        }
    }

    public void stopOtherVideo(int position) {
        for (int i = 0; i < items.size(); i++) {
            if (i != position) {
                PostInnerViewHolder postInnerViewHolder = (PostInnerViewHolder) recyclerView.findViewHolderForAdapterPosition(i);
                if (postInnerViewHolder != null) {
                    postInnerViewHolder.mBinder.postByUserRv.stopVideos();
                }
            }
        }
    }

    public void stopVideo(int firstVisibleItemPosition) {
        PostInnerViewHolder postInnerViewHolder = (PostInnerViewHolder) recyclerView.findViewHolderForAdapterPosition(firstVisibleItemPosition);
        if (postInnerViewHolder != null) {
            postInnerViewHolder.mBinder.postByUserRv.stopVideos();
        }
    }

    // region Inner Classes
    public static class PostInnerViewHolder extends RecyclerView.ViewHolder implements OnItemClickListener, OnReloadClickListener {
        private PostInnerListAdapterNew postInnerListAdapter;
        PostManagementFragment mContext;
        ItemPostListBinding mBinder;

        public PostInnerViewHolder(ItemPostListBinding mBinder, PostManagementFragment mContext) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
            this.mContext = mContext;
        }

        // region Helper Methods
        private void bind(PostListData.PostList searchData, List<PostListData.Post> categorizedVehicles) {
            setPostUserData(searchData);
            setRecyclerView(mBinder.postByUserRv, searchData.getPost());
        }

        private void setRecyclerView(AAH_CustomRecyclerView recyclerView, List<PostListData.Post> list) {
//            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false));

            //todo before setAdapter
            recyclerView.setActivity(mContext.mActivity);

            //optional - to play only first visible video
            recyclerView.setPlayOnlyFirstVideo(true); // false by default

            //optional - by default we check if url ends with ".mp4". If your urls do not end with mp4, you can set this param to false and implement your own check to see if video points to url
            recyclerView.setCheckForMp4(false); //true by default

            recyclerView.setVisiblePercent(60); // percentage of View that needs to be visible to start playing

           /* //optional - download videos to local storage (requires "android.permission.WRITE_EXTERNAL_STORAGE" in manifest or ask in runtime)
            recyclerView.setDownloadPath(Environment.getExternalStorageDirectory() + "/MyVideo"); // (Environment.getExternalStorageDirectory() + "/Video") by default

            recyclerView.setDownloadVideos(true); // false by default*/

            //extra - start downloading all videos in background before loading RecyclerView
           /* List<String> urls = new ArrayList<>();
            for (PostListData.Post object : list) {
                if (object.getPostPath() != null && object.getType().equalsIgnoreCase("Video"))
                    urls.add(ApiClient.WebService.imageUrl + object.getPostPath());
            }
            recyclerView.preDownload(urls);*/

//            PagerSnapHelper snapHelper = new PagerSnapHelper();
            recyclerView.setOnFlingListener(null);
//            recyclerView.clearOnScrollListeners();
//            snapHelper.attachToRecyclerView(recyclerView);

            postInnerListAdapter = new PostInnerListAdapterNew(list, mContext);

//            postInnerListAdapter.setOnItemClickListener(this);
//            postInnerListAdapter.setOnReloadClickListener(this);

            recyclerView.setItemAnimator(new SlideInUpAnimator());
            recyclerView.setAdapter(postInnerListAdapter);

            try {
                recyclerView.smoothScrollBy(1, 0);
                recyclerView.smoothScrollBy(-1, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
//                    if (newState == 0) {
//                        homeCategoryInnerAdapter.setSelectedPosition(linearLayoutManager.findFirstCompletelyVisibleItemPosition());
//                    }
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                }
            });

            postInnerListAdapter.addAll(list);*/
        }

        private void setPostUserData(PostListData.PostList data) {
            if (data != null) {
                mBinder.postTimeTv.setText("");
                if (data.getUserType().equalsIgnoreCase("Business"))
                    mBinder.userNameTv.setText(data.getBusinessName());
                else
                    mBinder.userNameTv.setText(data.getUserName());
                if (data.getLocation() != null && data.getLocation().contains(",")) {
                    String location = data.getLocation().split(",")[0];
                    if (location != null && location.equalsIgnoreCase(""))
                        location = data.getLocation().split(",")[1];
                    mBinder.userLocationTv.setText(location);
                } else
                    mBinder.userLocationTv.setText(data.getLocation());

                if (data.getPost() != null && data.getPost().size() > 0) {
                    String dateStr = data.getPost().get(0).getPostCreatedDate();
                    if (dateStr.length() <= 10) {
                        mBinder.postTimeTv.setText(dateStr);
                    } else {
                        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
                        SimpleDateFormat outputDf = new SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH);
                        df.setTimeZone(TimeZone.getTimeZone("UTC"));
                        Date date = null;
                        try {
                            date = df.parse(dateStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        outputDf.setTimeZone(TimeZone.getDefault());
                        String formattedDate = outputDf.format(date);
                        if (formattedDate != null)
                            mBinder.postTimeTv.setText(formattedDate);
                    }
                }
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.user_placeholder)
                        .error(R.drawable.user_placeholder);


                Glide.with(mContext)
                        .load(ApiClient.WebService.imageUrl + data.getUserImagePath())
                        .apply(options)
                        .into(mBinder.userProfileIv);

                if (data.isFollow()) {
                    mBinder.followTv.setText(mContext.getResources().getString(R.string.following));
                    mBinder.followTv.setBackground(mContext.getResources().getDrawable(R.drawable.shape_rounded_border_accent));
                    mBinder.followTv.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                } else {
                    mBinder.followTv.setText(mContext.getResources().getString(R.string.follow));
                    mBinder.followTv.setBackground(mContext.getResources().getDrawable(R.drawable.shape_rounded_border_btn));
                    mBinder.followTv.setTextColor(Color.WHITE);
                }

                mBinder.followTv.setOnClickListener(v -> {
                    if (!PostManagementFragment.isDisable) {
                        String text = mBinder.followTv.getText().toString();
                        if (text.equalsIgnoreCase(mContext.getResources().getString(R.string.follow))) {
                            mBinder.followTv.setText(mContext.getResources().getString(R.string.following));
                            mBinder.followTv.setBackground(mContext.getResources().getDrawable(R.drawable.shape_rounded_border_accent));
                            mBinder.followTv.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
                        } else {
                            mBinder.followTv.setText(mContext.getResources().getString(R.string.follow));
                            mBinder.followTv.setBackground(mContext.getResources().getDrawable(R.drawable.shape_rounded_border_btn));
                            mBinder.followTv.setTextColor(Color.WHITE);
                        }
                        PostManagementFragment.managementFragment.followPeople(data.getUserID());
                    } else if (PostManagementFragment.isDisable) {
                        PostManagementFragment.managementFragment.trendinUi();
                    }
                });

                mBinder.hideIv.setOnClickListener(v -> {
                    if (!PostManagementFragment.isDisable) {
                        PostManagementFragment.managementFragment.SaveHiddenUser(data.getUserID());
                    } else {
                        PostManagementFragment.managementFragment.trendinUi();
                    }
                });

                mBinder.messageIv.setOnClickListener(view -> {
                    Intent intentDeviceSync = new Intent(mContext.mActivity, DeviceContactSyncService
                            .class);
                    DeviceContactSyncService.enqueueWork(mContext.mActivity, intentDeviceSync);

                    Applozic.getInstance(mContext.mActivity).enableDeviceContactSync(true);

                    Intent intent1 = new Intent(mContext.mActivity, ConversationActivity.class);
                    intent1.putExtra(ConversationUIService.USER_ID, String.valueOf(data.getUserID()));
                    if (data.getUserType().equalsIgnoreCase("Business"))
                        intent1.putExtra(ConversationUIService.DISPLAY_NAME, data.getBusinessName()); //put it for displaying the title.
                    else
                        intent1.putExtra(ConversationUIService.DISPLAY_NAME, data.getUserName()); //put it for displaying the title.
                    intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
                    mContext.mActivity.startActivity(intent1);
                });

                if (data.getUserID() == App_pref.getAuthorizedUser(mContext.mActivity).getData().getUserId()) {
                    mBinder.followTv.setVisibility(View.GONE);
                    mBinder.hideIv.setVisibility(View.GONE);
                    mBinder.messageIv.setVisibility(View.GONE);
                } else {
                    mBinder.followTv.setVisibility(View.VISIBLE);
                    mBinder.hideIv.setVisibility(View.VISIBLE);
                    mBinder.messageIv.setVisibility(View.VISIBLE);
                }


                mBinder.userProfileIv.setOnClickListener(v -> {
                    if (data.getUserType().equalsIgnoreCase("Business")) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", data.getUserID());
                        /*mContext.getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.fragment_replace, BusinessProfileFragment.newInstance(bundle), "")
                                .commit();*/

                        Intent intent = new Intent(mContext.mActivity, ProfileDetailActivity.class);
                        intent.putExtra("clickFragmentName", "BusinessProfile");
                        intent.putExtra("fromProfileTab", false);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                        mContext.mActivity.overridePendingTransition(0, 0);

                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", data.getUserID());
                       /* mContext.getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.fragment_replace, IndividualProfileFragment.newInstance(bundle), "")
                                .commit();*/
                        Intent intent = new Intent(mContext.mActivity, ProfileDetailActivity.class);
                        intent.putExtra("clickFragmentName", "IndividualProfile");
                        intent.putExtra("fromProfileTab", false);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                        mContext.mActivity.overridePendingTransition(0, 0);
                    }
                });

                mBinder.userNameTv.setOnClickListener(view -> {
                    if (data.getUserType().equalsIgnoreCase("Business")) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", data.getUserID());
                        Intent intent = new Intent(mContext.mActivity, ProfileDetailActivity.class);
                        intent.putExtra("clickFragmentName", "BusinessProfile");
                        intent.putExtra("fromProfileTab", false);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                        mContext.mActivity.overridePendingTransition(0, 0);

                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", data.getUserID());
                        Intent intent = new Intent(mContext.mActivity, ProfileDetailActivity.class);
                        intent.putExtra("clickFragmentName", "IndividualProfile");
                        intent.putExtra("fromProfileTab", false);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                        mContext.mActivity.overridePendingTransition(0, 0);
                    }
                });

                mBinder.userLocationTv.setOnClickListener(view -> {
                    if (data.getUserType().equalsIgnoreCase("Business")) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", data.getUserID());
                        Intent intent = new Intent(mContext.mActivity, ProfileDetailActivity.class);
                        intent.putExtra("clickFragmentName", "BusinessProfile");
                        intent.putExtra("fromProfileTab", false);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                        mContext.mActivity.overridePendingTransition(0, 0);

                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", data.getUserID());
                        Intent intent = new Intent(mContext.mActivity, ProfileDetailActivity.class);
                        intent.putExtra("clickFragmentName", "IndividualProfile");
                        intent.putExtra("fromProfileTab", false);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                        mContext.mActivity.overridePendingTransition(0, 0);
                    }
                });

                mBinder.postTimeTv.setOnClickListener(view -> {
                    if (data.getUserType().equalsIgnoreCase("Business")) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", data.getUserID());
                        Intent intent = new Intent(mContext.mActivity, ProfileDetailActivity.class);
                        intent.putExtra("clickFragmentName", "BusinessProfile");
                        intent.putExtra("fromProfileTab", false);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                        mContext.mActivity.overridePendingTransition(0, 0);

                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", data.getUserID());
                        Intent intent = new Intent(mContext.mActivity, ProfileDetailActivity.class);
                        intent.putExtra("clickFragmentName", "IndividualProfile");
                        intent.putExtra("fromProfileTab", false);
                        intent.putExtras(bundle);
                        mContext.startActivity(intent);
                        mContext.mActivity.overridePendingTransition(0, 0);
                    }
                });

            }
        }

        @Override
        public void onItemClick(int position, View view) {

            /*CategorizedVehicleList.CategorizedVehicles searchData = homeCategoryInnerAdapter.getItem(position);
            //item click
            view.getContext().startActivity(new Intent(view.getContext(), VehicleSubCategoryActivity.class).putExtra("VehicleListId", searchData.getVehicleListId()).putExtra("title", searchData.getListName()).putExtra("jsonData", getJsonData(searchData.getVehicleListId())));
            ((AppCompatActivity) mContext).overridePendingTransition(R.anim.enter, R.anim.no_anim);*/
        }

        @Override
        public void onReloadClick() {

        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

    }

}