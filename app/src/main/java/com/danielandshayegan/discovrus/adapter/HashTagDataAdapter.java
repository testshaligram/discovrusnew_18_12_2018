package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.models.HashTagListModel;

import java.util.List;

public class HashTagDataAdapter extends ArrayAdapter<HashTagListModel> {

    private Context context;
    private List<HashTagListModel> dataList;
    ViewHolder viewHolder;


    public HashTagDataAdapter(Context context, List<HashTagListModel> list) {
        super(context, 0, list);
        context = context;
        dataList = list;
    }

    private static class ViewHolder {
        private TextView itemView;


    }

    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.row_working_data, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.itemView = (TextView) convertView.findViewById(R.id.txt_working_data);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        HashTagListModel hashTagListModel = dataList.get(position);

        if (dataList.size() > 0) {
            viewHolder.itemView.setText(hashTagListModel.getData().get(position).getTag().toString());
            Toast.makeText(context, hashTagListModel.getData().get(position).getTag().toString(), Toast.LENGTH_SHORT).show();
        }


        return convertView;
    }


}
