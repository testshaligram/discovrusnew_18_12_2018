package com.danielandshayegan.discovrus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemClickiesListBinding;
import com.danielandshayegan.discovrus.models.ClickieList;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.activity.MakeClickieActivity;

public class ClickiesListAdapter extends BaseAdapter<ClickieList.DataBean> {

    // region Member Variables
    private PostInnerListAdapter.FooterViewHolder footerViewHolder;
    static Context mContext;
    // endregion

    // region Constructors
    public ClickiesListAdapter() {
        super();
    }

    public ClickiesListAdapter(Context context) {
        this.mContext = context;
    }
    // endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemClickiesListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_clickies_list, parent, false);

        final ClickiesViewHolder holder = new ClickiesViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final PostInnerListAdapter.FooterViewHolder holder = new PostInnerListAdapter.FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final ClickiesViewHolder holder = (ClickiesViewHolder) viewHolder;
        final ClickieList.DataBean data = getItem(position);
        holder.bind(data);

    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        PostInnerListAdapter.FooterViewHolder holder = (PostInnerListAdapter.FooterViewHolder) viewHolder;
        footerViewHolder = holder;
//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new ClickieList.DataBean());
    }

    // region Inner Classes
    public static class ClickiesViewHolder extends RecyclerView.ViewHolder {
        ItemClickiesListBinding mBinder;

        public ClickiesViewHolder(ItemClickiesListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(ClickieList.DataBean clickiesData) {

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.clickie_1)
                    .error(R.drawable.clickie_1);

            Glide.with(mContext)
                    .load(ApiClient.WebService.imageUrl + clickiesData.getImagePath())
                    .apply(options)
                    .into(mBinder.clickieIv);

            Log.e("image path", ApiClient.WebService.imageUrl + clickiesData.getImagePath());

//            mBinder.clickieIv.setImageResource(clickiesData);

            mBinder.clickieIv.setOnClickListener(view -> {
                ((Activity) mContext).startActivityForResult(new Intent(mContext, MakeClickieActivity.class).putExtra("Image", clickiesData.getImagePath()), 301);
            });

        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }
}