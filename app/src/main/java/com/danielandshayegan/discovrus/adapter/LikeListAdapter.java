package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ItemFollowListBinding;
import com.danielandshayegan.discovrus.models.LikeListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;

import java.util.List;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.followPeople;
import static com.danielandshayegan.discovrus.utils.Utils.getCompositeDisposable;


public class LikeListAdapter extends RecyclerView.Adapter<LikeListAdapter.DataViewHolder> {
    private List<LikeListData.DataBean> dataList;
    Context context;
    boolean forFollowers;

    public LikeListAdapter(List<LikeListData.DataBean> dataList, Context context, boolean forFollowers) {
        this.context = context;
        this.dataList = dataList;
        this.forFollowers = forFollowers;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFollowListBinding mBinding = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_follow_list, parent, false);
        return new DataViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        LikeListData.DataBean followData = dataList.get(position);
        if (followData.getUserRoleType().equals("Business")) {
            holder.mBinding.txtUserName.setText(followData.getBusinessName());
            holder.mBinding.txtBusinessName.setVisibility(View.GONE);
        } else {
            holder.mBinding.txtUserName.setText(followData.getUserName());
            holder.mBinding.txtBusinessName.setText(followData.getWorkingAt());
            holder.mBinding.txtBusinessName.setVisibility(View.VISIBLE);
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_placeholder)
                .error(R.drawable.user_placeholder);
        if (followData.getUserId() == App_pref.getAuthorizedUser(context).getData().getUserId()) {
            holder.mBinding.imgFollow.setVisibility(View.GONE);
        } else {
            holder.mBinding.imgFollow.setVisibility(View.VISIBLE);
            if (followData.isIsFollowing()) {
                holder.mBinding.imgFollow.setImageResource(R.drawable.ic_hide_icon);
            } else {
                holder.mBinding.imgFollow.setImageResource(R.drawable.profile_plus_icon);
            }
        }

        Glide.with(context)
                .load(ApiClient.WebService.imageUrl + followData.getUserImagePath())
                .apply(options)
                .into(holder.mBinding.imgUserProile);
       /* if (forFollowers) {
            if (followData.isIsFollowing()) {
                holder.mBinding.imgFollow.setImageResource(R.drawable.ic_hide_icon);
            } else {
                holder.mBinding.imgFollow.setImageResource(R.drawable.profile_plus_icon);
            }

        } else {
            if (followData.isIsFollowers()) {
                holder.mBinding.imgFollow.setImageResource(R.drawable.ic_hide_icon);
            } else {
                holder.mBinding.imgFollow.setImageResource(R.drawable.profile_plus_icon);
            }
        }*/

        holder.mBinding.imgFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!followData.isIsFollowing()) {
                    //follow that user
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(context).
                                            create(WebserviceBuilder.class).
                                            followPeople(followData.getUserId(), App_pref.getAuthorizedUser(context).getData().getUserId())
                                    , getCompositeDisposable(), followPeople, new SingleCallback() {
                                        @Override
                                        public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                            followData.setIsFollowing(true);
                                            notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

                                        }
                                    });
                } else {
                    //unfollow that user

                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(context).
                                            create(WebserviceBuilder.class).
                                            followPeople(followData.getUserId(), App_pref.getAuthorizedUser(context).getData().getUserId())
                                    , getCompositeDisposable(), followPeople, new SingleCallback() {
                                        @Override
                                        public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                            followData.setIsFollowing(false);
                                            notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

                                        }
                                    });

                }
            }
        });

       /* if (listType.equals("Follower"))
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    followPeople(followUserId, loginId)
                            , getCompositeDisposable(), followPeople, this);
        else
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    followPeople(loginId, followUserId)
                            , getCompositeDisposable(), followPeople, this);*/

    }


    @Override
    public int getItemCount() {
        if (dataList != null) {
            return dataList.size();
        } else {
            return 0;
        }
    }

    class DataViewHolder extends RecyclerView.ViewHolder {
        ItemFollowListBinding mBinding;

        DataViewHolder(ItemFollowListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            /*mBinding.imgFollow.setOnClickListener(view->{
                if(buttonClickListner!=null){
                    buttonClickListner.onFollowButtonClick(dataList.get(getAdapterPosition()));
                }
            });*/
        }
    }

    public interface FollowButtonClickListner {
        void onFollowButtonClick(LikeListData.DataBean dataBean);
    }
}
