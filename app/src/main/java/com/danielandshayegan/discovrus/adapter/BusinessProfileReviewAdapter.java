package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemBusinessReviewListBinding;
import com.danielandshayegan.discovrus.models.ReferenceListByUserId;
import com.danielandshayegan.discovrus.network.ApiClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

public class BusinessProfileReviewAdapter extends BaseAdapter<ReferenceListByUserId.DataBean> {

    // region Member Variables
    private FooterViewHolder footerViewHolder;
    Context mContext;

// endregion

    // region Constructors
    public BusinessProfileReviewAdapter() {
        super();
    }

    public BusinessProfileReviewAdapter(Context context) {
        mContext = context;
    }
// endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemBusinessReviewListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_business_review_list, parent, false);

        final PostInnerViewHolder holder = new PostInnerViewHolder(mBinder, mContext);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPos = holder.getAdapterPosition();
                if (adapterPos != RecyclerView.NO_POSITION) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(adapterPos, holder.itemView);
                    }
                }
            }
        });
        return holder;
    }


    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onReloadClickListener != null) {
                    onReloadClickListener.onReloadClick();
                }
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PostInnerViewHolder holder = (PostInnerViewHolder) viewHolder;

        final ReferenceListByUserId.DataBean post = getItem(position);
        if (post != null) {
            List<ReferenceListByUserId.DataBean.ReferenceTextBean> list = post.getReferenceText();
            if (position == 0) {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setClickable(true);
                    list.get(i).setReviewId(post.getReferenceID());
                }
                holder.bind(post, list);
            } else {
                for (int i = 0; i < list.size(); i++) {
                    list.get(i).setClickable(false);
                    list.get(i).setReviewId(post.getReferenceID());
                }
                holder.bind(post, list);
            }
        }

    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;

//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new ReferenceListByUserId.DataBean());
    }

   /* public void addInnerData(List<ReferencesListModel.DataBean> tempinner) {
        this.innerList = tempinner;
    }*/

    // region Inner Classes
    public static class PostInnerViewHolder extends RecyclerView.ViewHolder implements OnItemClickListener, OnReloadClickListener {
        private BusinessReviewsInnerListAdapter businessReviewsInnerListAdapter;
        Context mContext;
        ItemBusinessReviewListBinding mBinder;

        public PostInnerViewHolder(ItemBusinessReviewListBinding mBinder, Context mContext) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
            this.mContext = mContext;
        }

        // region Helper Methods
        private void bind(ReferenceListByUserId.DataBean searchData, List<ReferenceListByUserId.DataBean.ReferenceTextBean> innerList) {
            setPostUserData(searchData);
            setRecyclerView(mBinder.recyclerReviews, innerList);
        }

        private void setRecyclerView(RecyclerView recyclerView, List<ReferenceListByUserId.DataBean.ReferenceTextBean> list) {
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false));

//            PagerSnapHelper snapHelper = new PagerSnapHelper();
            recyclerView.setOnFlingListener(null);
//            recyclerView.clearOnScrollListeners();
//            snapHelper.attachToRecyclerView(recyclerView);

            businessReviewsInnerListAdapter = new BusinessReviewsInnerListAdapter(mContext);

            businessReviewsInnerListAdapter.setOnItemClickListener(this);
            businessReviewsInnerListAdapter.setOnReloadClickListener(this);

            recyclerView.setItemAnimator(new SlideInUpAnimator());
            recyclerView.setAdapter(businessReviewsInnerListAdapter);

            businessReviewsInnerListAdapter.addAll(list);
        }

        private void setPostUserData(ReferenceListByUserId.DataBean data) {
            if (data != null) {
                mBinder.txtPostTime.setText("");
                mBinder.txtUserLocation.setText(data.getLocation());
                mBinder.txtUserName.setText(data.getUserName());

                String dateStr = data.getUpdatedDate();
                if (dateStr.length() <= 10) {
                    mBinder.txtPostTime.setText(dateStr);
                } else {
                    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
                    SimpleDateFormat outputDf = new SimpleDateFormat("MM/dd/yyyy hh:mm", Locale.ENGLISH);
                    df.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date date = null;
                    try {
                        date = df.parse(dateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    outputDf.setTimeZone(TimeZone.getDefault());
                    String formattedDate = outputDf.format(date);

                    SimpleDateFormat format = new SimpleDateFormat("d", Locale.ENGLISH);
                    String onlyDay = format.format(date);

                    if (onlyDay.endsWith("1") && !onlyDay.endsWith("11"))
                        format = new SimpleDateFormat("d'st' MMM, yyyy", Locale.ENGLISH);
                    else if (onlyDay.endsWith("2") && !onlyDay.endsWith("12"))
                        format = new SimpleDateFormat("d'nd' MMM, yyyy", Locale.ENGLISH);
                    else if (onlyDay.endsWith("3") && !onlyDay.endsWith("13"))
                        format = new SimpleDateFormat("d'rd' MMM, yyyy", Locale.ENGLISH);
                    else
                        format = new SimpleDateFormat("d'th' MMM, yyyy", Locale.ENGLISH);
//                        format = new SimpleDateFormat("EE MMM d'th', yyyy", Locale.ENGLISH);

                    Date yourDate = null;
                    try {
                        yourDate = outputDf.parse(formattedDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String finalDate = format.format(yourDate);
                    mBinder.txtPostTime.setText(finalDate);
                }
                RequestOptions options = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.user_placeholder)
                        .error(R.drawable.user_placeholder);


                Glide.with(mContext)
                        .load(ApiClient.WebService.imageUrl + data.getUserImagePath())
                        .apply(options)
                        .into(mBinder.imgUserProfile);

            }
        }

        @Override
        public void onItemClick(int position, View view) {

            /*CategorizedVehicleList.CategorizedVehicles searchData = homeCategoryInnerAdapter.getItem(position);
            //item click
            view.getContext().startActivity(new Intent(view.getContext(), VehicleSubCategoryActivity.class).putExtra("VehicleListId", searchData.getVehicleListId()).putExtra("title", searchData.getListName()).putExtra("jsonData", getJsonData(searchData.getVehicleListId())));
            ((AppCompatActivity) mContext).overridePendingTransition(R.anim.enter, R.anim.no_anim);*/
        }

        @Override
        public void onReloadClick() {

        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {
        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

    }

}