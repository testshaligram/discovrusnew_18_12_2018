package com.danielandshayegan.discovrus.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.applozic.mobicomkit.uiwidgets.people.contact.DeviceContactSyncService;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.VerticalDividerDecoration;
import com.danielandshayegan.discovrus.databinding.ItemLoadMoreBinding;
import com.danielandshayegan.discovrus.databinding.RowPostImageTextBinding;
import com.danielandshayegan.discovrus.databinding.RowPostListFooterBinding;
import com.danielandshayegan.discovrus.databinding.RowPostListHeaderBinding;
import com.danielandshayegan.discovrus.databinding.RowPostVideoTextBinding;
import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.interfaces.onMediaClick;
import com.danielandshayegan.discovrus.models.NewsFeedData;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.common.BaseActivityNew;
import com.danielandshayegan.discovrus.ui.common.BaseBinder;
import com.danielandshayegan.discovrus.ui.newsfeed.NewsFeedDetailsFragment;
import com.danielandshayegan.discovrus.ui.newsfeed.NewsFeedFragment;
import com.danielandshayegan.discovrus.ui.videos.ExoUtils;
import com.danielandshayegan.discovrus.ui.videos.SinglePlayerActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.webservice.APIs;
import com.danielandshayegan.discovrus.webservice.CommonAPI;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;

import java.util.ArrayList;

import im.ene.toro.CacheManager;
import im.ene.toro.PlayerSelector;
import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.exoplayer.Playable;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

public class NewsFeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //TODO: paid post then full width
    Context context;
    ArrayList<NewsFeedData> newsFeedList = new ArrayList<>();

    public static float VOLUME = 0;
    public static boolean MUTE = true;

    protected boolean showLoader;
    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOADER = 2;
    private LayoutInflater inflater;
    private RowPostListHeaderBinding binding;

    public NewsFeedAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public NewsFeedAdapter(Context context, ArrayList<NewsFeedData> newsFeedList) {
        this.context = context;
        this.newsFeedList = newsFeedList;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_LOADER) {
            ItemLoadMoreBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_load_more, parent, false);
            return new LoaderViewHolder(mBinder);
        } else {
            RowPostListHeaderBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_post_list_header, parent, false);
            return new NewsFeedAdapter.NewsFeedsViewHolder(mBinder);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof LoaderViewHolder) {
            LoaderViewHolder loaderViewHolder = (LoaderViewHolder) holder;
            //  if (showLoader) {
            loaderViewHolder.binding.progressBar.setVisibility(View.VISIBLE);
            // } else {
            //     loaderViewHolder.binding.progressBar.setVisibility(View.GONE);
            //}
        } else {
            NewsFeedData newsFeedData = newsFeedList.get(position);

            binding = ((NewsFeedsViewHolder) holder).binding;

            binding.postByUserRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            binding.postByUserRv.setHasFixedSize(true);
            binding.postByUserRv.addItemDecoration(new VerticalDividerDecoration());
            binding.postByUserRv.setPlayerSelector(PlayerSelector.DEFAULT);
            binding.postByUserRv.setAdapter(new NewsFeedItemAdapter(context, newsFeedData));

            binding.setItem(newsFeedData);
            binding.executePendingBindings();

            if (App_pref.getAuthorizedUser(context) != null) {
                binding.grpMessageFollow.setVisibility(newsFeedData.getUserID().equalsIgnoreCase(String.valueOf(App_pref.getAuthorizedUser(context).getData().getUserId())) ? View.GONE : View.VISIBLE);
                //  binding.ivHidePost.setVisibility(newsFeedData.getUserID().equalsIgnoreCase(String.valueOf(App_pref.getAuthorizedUser(context).getData().getUserId())) ? View.GONE : View.VISIBLE);
            }

            binding.btnFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    CommonAPI.followUnfollowPeople(context, newsFeedData.getUserID(), new CallbackTask() {
                        @Override
                        public void onFail(Object object) {

                        }

                        @Override
                        public void onSuccess(Object object) {
                            newsFeedData.setFollow(!newsFeedData.getFollow());
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });
                }
            });

            binding.ivSendMessage.setOnClickListener(view -> {
                Intent intentDeviceSync = new Intent(context, DeviceContactSyncService
                        .class);
                DeviceContactSyncService.enqueueWork(context, intentDeviceSync);

                Applozic.getInstance(context).enableDeviceContactSync(true);

                Intent intent1 = new Intent(context, ConversationActivity.class);
                intent1.putExtra(ConversationUIService.USER_ID, String.valueOf(newsFeedData.getUserID()));
                if (newsFeedData.getUserType().equalsIgnoreCase("Business"))
                    intent1.putExtra(ConversationUIService.DISPLAY_NAME, newsFeedData.getBusinessName()); //put it for displaying the title.
                else
                    intent1.putExtra(ConversationUIService.DISPLAY_NAME, newsFeedData.getUserName()); //put it for displaying the title.
                intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
                context.startActivity(intent1);
            });
        }
    }

    @Override
    public int getItemCount() {
        if (newsFeedList == null || newsFeedList.size() == 0) {
            return 0;
        }
        if (NewsFeedFragment.totalItemCount <= NewsFeedFragment.limit) {
            return newsFeedList.size();
        } else {
            Log.e("getItemCount return", newsFeedList.size() + 1 + "");
            return (newsFeedList.size() + 1);
        }
    }

    @SuppressLint("LongLogTag")
    @Override
    public int getItemViewType(int position) {
        if (position != 0 && position == (newsFeedList.size())) {
            Log.e("getItemViewType position", position + "");
            return VIEW_TYPE_LOADER;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    public void setItems(ArrayList<NewsFeedData> items) {
        this.newsFeedList.clear();
        newsFeedList = new ArrayList<>(items);
        notifyDataSetChanged();
    }

    public class NewsFeedsViewHolder extends RecyclerView.ViewHolder {
        RowPostListHeaderBinding binding;

        public NewsFeedsViewHolder(RowPostListHeaderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class LoaderViewHolder extends RecyclerView.ViewHolder {
        ItemLoadMoreBinding binding;

        public LoaderViewHolder(ItemLoadMoreBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    class NewsFeedItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements CacheManager, onMediaClick {

        Context context;
        NewsFeedData newsFeedData;
        int halfWidth, fullWidth;
        ConstraintSet set;
        RowPostImageTextBinding imageTextBinding;
        RowPostVideoTextBinding videoTextBinding;
        private int currentPosition = -1;
        private Object currentPlayer;
        static final String EXTRA_MEDIA_URI = "toro:demo:custom:player:uri";  // Uri
        static final String EXTRA_MEDIA_ORDER = "toro:demo:custom:player:order";  // int
        static final String EXTRA_MEDIA_DESCRIPTION = "toro:demo:custom:player:description"; // String
        static final String EXTRA_MEDIA_PLAYBACK_INFO = "toro:demo:custom:player:info"; // PlaybackInfo
        static final String EXTRA_MEDIA_PLAYER_SIZE = "toro:demo:custom:player:player_size";  // Point
        static final String EXTRA_MEDIA_VIDEO_SIZE = "toro:demo:custom:player:video_size";  // Point
        static final String EXTRA_DEFAULT_FULLSCREEN = "toro:demo:custom:player:fullscreen";  // boolean
        public static final int RQ_PLAYBACK_INFO = 1021;
        NewsFeedData.PostData npd;

        public NewsFeedItemAdapter(Context context, NewsFeedData NewsFeedData) {
            this.context = context;
            this.newsFeedData = NewsFeedData;
            halfWidth = Utils.getHalfScreenWidth(context);
            fullWidth = Utils.getDeviceWidth(context);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case 0: //NewsFeedTypeEnum.Photo*/
                case 1: //NewsFeedTypeEnum.Text*/
                case 3: //NewsFeedTypeEnum.PhotoAndText*/
                    RowPostImageTextBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_post_image_text, parent, false);
                    return new NewsFeedItemAdapter.PostImageTextViewHolder(mBinder);

                case 2: //NewsFeedTypeEnum.Video
                    RowPostVideoTextBinding mBinder1 = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_post_video_text, parent, false);
                    return new NewsFeedItemAdapter.PostVideoTextViewHolder(mBinder1);

                default:
                    RowPostImageTextBinding mBinder2 = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_post_image_text, parent, false);
                    return new NewsFeedItemAdapter.PostImageTextViewHolder(mBinder2);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

            npd = newsFeedData.getPostData().get(position);

            switch (npd.getNewsFeedTypeEnum()) {
                case 0: //NewsFeedTypeEnum.Photo*/
                    imageTextBinding = ((PostImageTextViewHolder) holder).binding;

                    manageImageTextCommonTasks(npd, imageTextBinding, true, position);

                    imageTextBinding.viewFooter.ivLike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            npd = newsFeedData.getPostData().get(position);
                            manageLikeClick(npd, position);
                        }
                    });

                    imageTextBinding.viewFooter.tvLikeCount.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            npd = newsFeedData.getPostData().get(position);
                            manageLikeClick(npd, position);
                        }
                    });

                    imageTextBinding.ivTextBackgorund.setVisibility(View.GONE);
                    imageTextBinding.tvTextTitle.setVisibility(View.GONE);
                    imageTextBinding.tvTextDesc.setVisibility(View.GONE);
                    imageTextBinding.tvImageTitle.setVisibility(View.GONE);
                    imageTextBinding.tvImageDesc.setVisibility(View.GONE);
                    imageTextBinding.ivPostImage.setVisibility(View.VISIBLE);

                    ((PostImageTextViewHolder) holder).binding.executePendingBindings();
                    break;

                case 1: //NewsFeedTypeEnum.Text
                    imageTextBinding = ((PostImageTextViewHolder) holder).binding;

                    if (npd.getIsPaid() != null && npd.getIsPaid().equalsIgnoreCase("true")) {
                        if (position == 0) {
                            manageImageTextCommonTasks(npd, imageTextBinding, false, position);
                        } else {
                            manageImageTextCommonTasks(npd, imageTextBinding, true, position);
                        }
                    } else {
                        manageImageTextCommonTasks(npd, imageTextBinding, true, position);
                    }

                    imageTextBinding.ivTextBackgorund.setVisibility(View.VISIBLE);
                    imageTextBinding.tvTextTitle.setVisibility(View.VISIBLE);
                    imageTextBinding.tvTextDesc.setVisibility(View.VISIBLE);
                    imageTextBinding.tvImageTitle.setVisibility(View.GONE);
                    imageTextBinding.tvImageDesc.setVisibility(View.GONE);
                    imageTextBinding.ivPostImage.setVisibility(View.GONE);

                    imageTextBinding.viewFooter.ivLike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            npd = newsFeedData.getPostData().get(position);
                            manageLikeClick(npd, position);
                        }
                    });

                    imageTextBinding.viewFooter.tvLikeCount.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            npd = newsFeedData.getPostData().get(position);
                            manageLikeClick(npd, position);
                        }
                    });

                    ((PostImageTextViewHolder) holder).binding.executePendingBindings();
                    break;

                case 2: //NewsFeedTypeEnum.Video
                    videoTextBinding = ((PostVideoTextViewHolder) holder).binding;

                    ((PostVideoTextViewHolder) holder).bind(npd);

                    set = new ConstraintSet();
                    set.clone(videoTextBinding.constraintParent);
                    set.constrainWidth(R.id.cv_video_text, fullWidth);

                    if (position == 0) {
                        set.constrainHeight(R.id.cv_video_text, (int) context.getResources().getDimension(R.dimen._230sdp));
                    } else {
                        set.constrainHeight(R.id.cv_video_text, halfWidth);
                    }
                    set.applyTo(videoTextBinding.constraintParent);

                    videoTextBinding.ivTextBackground.setVisibility(View.GONE);
                    videoTextBinding.tvTextTitle.setVisibility(View.GONE);
                    videoTextBinding.tvTextDesc.setVisibility(View.GONE);

                    BaseBinder.loadThumbnail(context, videoTextBinding.playerView, APIs.BASE_IMAGE_PATH + npd.getThumbFileName(), 0, 0);

                    if (App_pref.getAuthorizedUser(context) != null)
                        videoTextBinding.ivOptions.setVisibility(npd.getUserID().equalsIgnoreCase(String.valueOf(App_pref.getAuthorizedUser(context).getData().getUserId())) ? View.VISIBLE : View.GONE);

                    // ((PostVideoTextViewHolder) holder).binding.playerView.setUseArtwork(false);

                    videoTextBinding.viewFooter.ivLike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            npd = newsFeedData.getPostData().get(position);
                            manageLikeClick(npd, position);
                        }
                    });

                    videoTextBinding.viewFooter.tvLikeCount.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            npd = newsFeedData.getPostData().get(position);
                            manageLikeClick(npd, position);
                        }
                    });

                    videoTextBinding.viewFooter.ivLike.setSelected(npd.getLiked());
                    videoTextBinding.viewFooter.ivComment.setSelected(npd.getIsCommented());
                    videoTextBinding.viewFooter.ivViewedByMe.setSelected(npd.getViewed());

                    videoTextBinding.setItem(npd);
                    videoTextBinding.executePendingBindings();
                    videoTextBinding.viewFooter.setItem(npd);
                    videoTextBinding.viewFooter.executePendingBindings();

                    videoTextBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //TODO: Make View button selected and redirect to post details screen
                            videoTextBinding.viewFooter.ivViewedByMe.setImageResource(R.drawable.ic_green_view);
                            manageViewClick(npd, position);
                            ((BaseActivityNew) context).replaceFragment(new NewsFeedDetailsFragment());
                        }
                    });
                    break;

                case 3: //NewsFeedTypeEnum.PhotoAndText
                    imageTextBinding = ((PostImageTextViewHolder) holder).binding;

                    imageTextBinding.ivTextBackgorund.setVisibility(View.GONE);
                    imageTextBinding.ivPostImage.setVisibility(View.VISIBLE);
                    imageTextBinding.tvTextTitle.setVisibility(View.GONE);
                    imageTextBinding.tvTextDesc.setVisibility(View.GONE);
                    imageTextBinding.tvImageTitle.setVisibility(View.VISIBLE);
                    imageTextBinding.tvImageDesc.setVisibility(View.VISIBLE);

                    manageImageTextCommonTasks(npd, imageTextBinding, true, position);

                    imageTextBinding.viewFooter.ivLike.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            npd = newsFeedData.getPostData().get(position);
                            manageLikeClick(npd, position);
                        }
                    });

                    imageTextBinding.viewFooter.tvLikeCount.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            npd = newsFeedData.getPostData().get(position);
                            manageLikeClick(npd, position);
                        }
                    });
                    ((PostImageTextViewHolder) holder).binding.executePendingBindings();
                    break;
            }
        }

        private void manageViewClick(NewsFeedData.PostData npd, int position) {
            newsFeedData.getPostData().get(position).setViewed(true);
            int n = Integer.parseInt(newsFeedData.getPostData().get(position).getViewCount());
            n++;
            newsFeedData.getPostData().get(position).setViewCount(String.valueOf(n));
            notifyDataSetChanged();
            CommonAPI.viewPost(context, npd.getPostId(), new CallbackTask() {
                @Override
                public void onFail(Object object) {

                }

                @Override
                public void onSuccess(Object object) {

                }

                @Override
                public void onFailure(Throwable t) {

                }
            });
        }

        private void manageLikeClick(NewsFeedData.PostData npd, int position) {
            try {
                newsFeedData.getPostData().get(position).setLiked(!npd.getLiked());
                int n = Integer.parseInt(newsFeedData.getPostData().get(position).getLikeCount());
                if (newsFeedData.getPostData().get(position).getLiked()) {
                    n++;
                } else {
                    n--;
                }
                newsFeedData.getPostData().get(position).setLikeCount(String.valueOf(n));
                notifyDataSetChanged();
                CommonAPI.likePost(context, npd.getPostId(), npd.getUserID(), newsFeedData.getPostData().get(position).getLiked(), new CallbackTask() {
                    @Override
                    public void onFail(Object object) {

                    }

                    @Override
                    public void onSuccess(Object object) {

                    }

                    @Override
                    public void onFailure(Throwable t) {

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return newsFeedData.getPostData().size();
        }

        @Override
        public void onPlay(Object player, int adapterPosition) {
            try {
                if (currentPosition == adapterPosition)
                    return;
                if (currentPlayer instanceof PostVideoTextViewHolder)
                    ((PostVideoTextViewHolder) currentPlayer).handleOtherPlay();
                this.currentPlayer = player;
                this.currentPosition = adapterPosition;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Nullable
        @Override
        public Object getKeyForOrder(int order) {
            return null;
        }

        @Nullable
        @Override
        public Integer getOrderForKey(@NonNull Object key) {
            return null;
        }

        public class PostImageTextViewHolder extends RecyclerView.ViewHolder// extends BasePostVH
        {
            RowPostImageTextBinding binding;

            public PostImageTextViewHolder(ViewDataBinding binding) {
                super(binding.getRoot());
                this.binding = (RowPostImageTextBinding) binding;
            }
/*
            @Override
            public void bindContent(NewsFeedItemAdapter adapter, NewsFeedData.PostData post) {

            }

            @Override
            public void recycledView() {

            }*/
        }

        public class PostVideoTextViewHolder extends RecyclerView.ViewHolder implements ToroPlayer// extends BasePostVH
        {
            RowPostVideoTextBinding binding;
            ExoPlayerViewHelper helper;
            private Uri mediaUri;
            private View btnMute;

            public PostVideoTextViewHolder(ViewDataBinding binding) {
                super(binding.getRoot());
                this.binding = (RowPostVideoTextBinding) binding;
                btnMute = this.binding.playerView.findViewById(R.id.btnVolume);
                btnMute.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        v.setSelected(!v.isSelected());
                        if (!MUTE) {
                            //mute
                            if (VOLUME == 0) {
                                //get the volume before set to mute
                                VOLUME = helper.getVolume();
                            }
                            MUTE = true;
                            helper.setVolume(0);
                        } else {
                            //unmute
                            helper.setVolume(VOLUME);
                            MUTE = false;
                        }
                    }
                });

                ViewCompat.setTransitionName(((RowPostVideoTextBinding) binding).playerView, context.getString(R.string.transition_feed_video));
                View btnFullScreen = (((RowPostVideoTextBinding) binding).playerView.findViewById(R.id.btnFullScreen));
                ((View) btnFullScreen.getParent()).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openDetail(false);
                    }
                });
                btnFullScreen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openDetail(true);
                    }
                });
            }

            private void openDetail(boolean fullscreen) {
                //static
                fullscreen = false;
                View view = ((RowPostVideoTextBinding) binding).playerView;
                Point viewSize = new Point(view.getWidth(), view.getHeight());
                Point videoSize = new Point(view.getWidth(), view.getHeight());
                if (view instanceof PlayerView && ((PlayerView) view).getPlayer() != null) {
                    Player player = ((PlayerView) view).getPlayer();
                    Format videoFormat = player instanceof SimpleExoPlayer ?  //
                            ((SimpleExoPlayer) player).getVideoFormat() : null;
                    if (videoFormat != null
                            && videoFormat.width != Format.NO_VALUE
                            && videoFormat.height != Format.NO_VALUE) {
                        videoSize.set(videoFormat.width, videoFormat.height);
                    }
                }

                Intent intent = new Intent(context, SinglePlayerActivity.class);
                Bundle extras = new Bundle();
                extras.putInt(EXTRA_MEDIA_ORDER, getAdapterPosition());
                extras.putParcelable(EXTRA_MEDIA_URI, mediaUri);
                extras.putParcelable(EXTRA_MEDIA_PLAYBACK_INFO, getCurrentPlaybackInfo());
                extras.putParcelable(EXTRA_MEDIA_PLAYER_SIZE, viewSize);
                extras.putParcelable(EXTRA_MEDIA_VIDEO_SIZE, videoSize);
                extras.putBoolean(EXTRA_DEFAULT_FULLSCREEN, fullscreen);
                intent.putExtras(extras);
                // EventBus.getDefault().postSticky(new PostDetailEvent(post));

               /* Intent intent = createIntent(activity, getAdapterPosition(), mediaUri,  //
                        getCurrentPlaybackInfo(), viewSize, videoSize, fullscreen, post);*/
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, view, ViewCompat.getTransitionName(view));
                ((Activity) context).startActivityForResult(intent, RQ_PLAYBACK_INFO, options.toBundle());
            }

             /*if (resultCode == Activity.RESULT_OK && requestCode == RQ_PLAYBACK_INFO && data != null) {
                PlaybackInfo info = data.getParcelableExtra(RESULT_EXTRA_PLAYBACK_INFO);
                int order = data.getIntExtra(RESULT_EXTRA_PLAYER_ORDER, -1);
                if (order >= 0 && binding.rvFeed != null) {
                    binding.rvFeed.setPlayerSelector(PlayerSelector.NONE);
                    binding.rvFeed.savePlaybackInfo(order, info);
                    binding.rvFeed.setPlayerSelector(selector);
                }
            }*/

            @NonNull
            @Override
            public View getPlayerView() {
                return binding.playerView;
            }

            @NonNull
            @Override
            public PlaybackInfo getCurrentPlaybackInfo() {
                return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
            }

            @Override
            public void initialize(@NonNull Container container, @NonNull PlaybackInfo playbackInfo) {
                if (mediaUri == null) throw new IllegalStateException("mediaUri is null.");
                if (helper == null) {
                    helper = new ExoPlayerViewHelper(this, mediaUri, ExoUtils.createExo(context), listener);
                }

                helper.initialize(playbackInfo);
//        binding.mainContent.setUseArtwork(false);
//        BaseBinder.loadThumbnail(activity, binding.mainContent, post.getPostVideoThumbnail(),post.getVideo_width(),post.getVideo_height());
                setMute(MUTE);
                if (MUTE) {
                    if (VOLUME == 0) {
                        //get the volume before set to mute
                        VOLUME = helper.getVolume();
                    }
                    helper.setVolume(0);
                } else {
                    if (VOLUME != 0)
                        helper.setVolume(VOLUME);
                    MUTE = false;
                }

            }

            @Override
            public void play() {
                if (helper != null) helper.play();
            }

            @Override
            public void pause() {
                if (helper != null) helper.pause();
            }

            @Override
            public boolean isPlaying() {
                return helper != null && helper.isPlaying();
            }

            @Override
            public void release() {
                if (helper != null) {
//                helper.setEventListener(null);
                    helper.release();
                    helper = null;
                }
            }

            @Override
            public boolean wantsToPlay() {
                return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.50;
            }

            @Override
            public int getPlayerOrder() {
                return getAdapterPosition();
            }

            @Override
            public void onSettled(Container container) {
                //Do nothing
            }

            public void setMute(boolean mute) {
                btnMute.setSelected(mute);
            }

            public void handleOtherPlay() {
                try {
                    if (binding != null) {
                        binding.playerView.getPlayer().setPlayWhenReady(false);
                    }
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }

            private Playable.EventListener listener = new Playable.DefaultEventListener() {
                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    super.onPlayerStateChanged(playWhenReady, playbackState);
                    Log.e("Playbackstate", playbackState + "");

                    binding.prog.setVisibility(playbackState != 6 ? View.GONE : View.VISIBLE);
//            if (post != null && playbackState == 3 && post.getHeight() == ViewGroup.LayoutParams.WRAP_CONTENT) {
////                post.setHeight(binding.mainContent.getHeight());
//            }
                    /*
                     * Media Player States
                     * 0 STATE_NONE
                     * 1 STATE_STOPPED
                     * 2 STATE_PAUSED
                     * 3 STATE_PLAYING
                     * 4 ACTION_PLAY
                     * 5 STATE_REWINDING
                     * 6 STATE_BUFFERING
                     * 7 STATE_ERROR
                     * 8 STATE_CONNECTING
                     * 9*/
                    if (playbackState == 4) {
                        //  binding.playerView.getPlayer().seekToDefaultPosition();
                        //  binding.playerView.getPlayer().setPlayWhenReady(false);
                    }

                    if (playbackState == 3) {
                        binding.playerView.getPlayer().setRepeatMode(Player.REPEAT_MODE_ALL);
                        binding.playerView.getPlayer().setPlayWhenReady(true);
                    }
                    if (playWhenReady)
                        onPlay(this, getAdapterPosition());
                    Log.e("playbackstate", " " + playbackState + " " + playWhenReady);
                }
            };

            public void bind(NewsFeedData.PostData item) {
                if (item != null && item.getPostPath() != null) {
                    mediaUri = Uri.parse(APIs.BASE_IMAGE_PATH + item.getPostPath());
                }
            }
        }

        @Override
        public int getItemViewType(int position) {
            return newsFeedData.getPostData().get(position).getNewsFeedTypeEnum();
        }

        private void manageImageTextCommonTasks(NewsFeedData.PostData npd, RowPostImageTextBinding binding, boolean ifHalfWidthApply, int position) {
            set = new ConstraintSet();
            set.clone(binding.constraintParent);

            if (ifHalfWidthApply) {
                set.constrainWidth(R.id.cv_image, halfWidth);
                set.constrainHeight(R.id.cv_image, halfWidth);
            } else {
                set.constrainWidth(R.id.cv_image, fullWidth);
            }
            set.applyTo(binding.constraintParent);

            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(binding.constraintParent);
            constraintSet.setMargin(binding.ivOptions.getId(), ConstraintSet.END, 10);
            constraintSet.setMargin(binding.ivOptions.getId(), ConstraintSet.TOP, 50);
            constraintSet.applyTo(binding.constraintParent);

            binding.constraintParent.invalidate();

            if (App_pref.getAuthorizedUser(context) != null)
                binding.ivOptions.setVisibility(npd.getUserID().equalsIgnoreCase(String.valueOf(App_pref.getAuthorizedUser(context).getData().getUserId())) ? View.VISIBLE : View.GONE);

            binding.viewFooter.ivLike.setSelected(npd.getLiked());
            binding.viewFooter.ivComment.setSelected(npd.getIsCommented());
            binding.viewFooter.ivViewedByMe.setSelected(npd.getViewed());

            binding.setItem(npd);
            binding.executePendingBindings();
            binding.viewFooter.setItem(npd);
            binding.viewFooter.executePendingBindings();

            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    binding.viewFooter.ivViewedByMe.setImageResource(R.drawable.ic_green_view);
                    manageViewClick(npd, position);
                    Bundle b = new Bundle();
                    b.putParcelable("PostData", npd);
                    NewsFeedDetailsFragment fragment = new NewsFeedDetailsFragment();
                    fragment.setArguments(b);
                    ((BaseActivityNew) context).replaceFragment(fragment);
                }
            });
        }

        public abstract class BasePostVH extends RecyclerView.ViewHolder implements View.OnClickListener {
            protected final Activity activity;
            private RowPostListFooterBinding footerBinding;
            private AdapterClick onAdapterClick;

            public BasePostVH(ViewDataBinding itemView) {
                super(itemView.getRoot());
                this.activity = (Activity) itemView.getRoot().getContext();
                final View footerView = itemView.getRoot().findViewById(R.id.viewFooter);
                if (footerView != null)
                    footerBinding = DataBindingUtil.bind(footerView);

                //setup click
                if (footerBinding != null) {
                    footerBinding.ivLike.setOnClickListener(this);
                    footerBinding.tvLikeCount.setOnClickListener(this);
                    footerBinding.ivComment.setOnClickListener(this);
                    footerBinding.tvCommentsCount.setOnClickListener(this);
                    footerBinding.ivViewedByMe.setOnClickListener(this);
                    footerBinding.tvViewedByMeCount.setOnClickListener(this);
                }
            }

            @Override
            public void onClick(View v) {
                if (onAdapterClick != null) {
                    onAdapterClick.onClick(v, this, getAdapterPosition());
                }
            }

            public void setOnClick(AdapterClick mAdapterClick) {
                this.onAdapterClick = mAdapterClick;
            }
        }
    }
}

interface AdapterClick {
    public void onClick(View view, NewsFeedAdapter.NewsFeedItemAdapter.BasePostVH holder, int position);
}