package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.RowTrendingNowBinding;
import com.danielandshayegan.discovrus.models.TrendingNowModel;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TrendingNowAdapter extends RecyclerView.Adapter<TrendingNowAdapter.MyViewHolder> {


    List<TrendingNowModel.DataBean> dataBeanList;
    Context context;
    public static String IMAGE_PATH = ApiClient.WebService.imageUrl;
    private TrensingDataClicListner listener;

    public TrendingNowAdapter(List<TrendingNowModel.DataBean> dataBeanList, Context context, TrensingDataClicListner listener) {
        this.dataBeanList = dataBeanList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        RowTrendingNowBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_trending_now, parent, false);
        return new MyViewHolder(mBinder);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (dataBeanList.get(position).getTrendingImagePath() != null) {
            Picasso.with(context).
                    load(IMAGE_PATH + dataBeanList.get(position).getTrendingImagePath()).
                    into(holder.mBinder.imgTrending);

            /*holder.mBinder.imgTrending.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemSelected(dataBeanList.get(position));
                }
            });


            Log.e("dataList1", "" + dataBeanList.get(position).getTrendingID());
            EventDataObject.Trending eventData = new EventDataObject.Trending(dataBeanList.get(position));
            GlobalBus.getBus().post(eventData);*/

            holder.mBinder.txtTitle.setText(Html.fromHtml(dataBeanList.get(position).getTitle()));



        }
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowTrendingNowBinding mBinder;

        public MyViewHolder(RowTrendingNowBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemSelected(dataBeanList.get(getAdapterPosition()));

                }
            });
        }
    }

    public interface TrensingDataClicListner {

        void onItemSelected(TrendingNowModel.DataBean trendingNowModel);
    }


}
