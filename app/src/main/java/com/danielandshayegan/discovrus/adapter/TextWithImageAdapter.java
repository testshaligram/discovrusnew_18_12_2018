package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CustomHtml;
import com.danielandshayegan.discovrus.databinding.ItemHighlightListBinding;
import com.danielandshayegan.discovrus.databinding.RowImageBinding;
import com.danielandshayegan.discovrus.databinding.RowTextBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

public class TextWithImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_IMAGE = 0;
    private static final int ITEM_TEXT = 1;
    List<String> modelList;
    PostListData.Post data;
    private Context context;

    public TextWithImageAdapter(List<String> modelList, PostListData.Post data, Context context) {
        this.modelList = modelList;
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM_IMAGE:
                RowImageBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_image, parent, false);
                return new DataImageViewHolder(mBinding);
            case ITEM_TEXT:
                RowTextBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_text, parent, false);
                return new DataTextViewHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (modelList.get(position).equalsIgnoreCase("Image")) {
            DataImageViewHolder dataImageViewHolder = (DataImageViewHolder) holder;
            dataImageViewHolder.bind(position);
        } else {
            DataTextViewHolder dataTextViewHolder = (DataTextViewHolder) holder;
            dataTextViewHolder.bind(position);
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public List<String> getData() {
        return modelList;
    }

    class DataImageViewHolder extends RecyclerView.ViewHolder {
        RowImageBinding mBinding;

        DataImageViewHolder(RowImageBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(int position) {
            if (data.getPostPath() != null) {
                Picasso.with(context).
                        load(ApiClient.WebService.imageUrl + data.getPostPath()).
                        into(mBinding.imgPost);
            }
        }
    }

    class DataTextViewHolder extends RecyclerView.ViewHolder {
        RowTextBinding mBinding;

        DataTextViewHolder(RowTextBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        public void bind(int position) {
            final CaseInsensitiveAssetFontLoader fontLoader = new CaseInsensitiveAssetFontLoader(context.getApplicationContext(), "fonts");
            if (data.getTitle() != null) {
//                    mBinding.uiClickPost.txtTitle.setText(String.valueOf(dataBean.getTitle()));
                String title = "";
                try {
                    title = URLDecoder.decode(data.getTitle(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                mBinding.txtTitlePost.setText(CustomHtml.fromHtml(title, fontLoader));

            }
            if (data.getDescription() != null && !data.getDescription().equalsIgnoreCase("")) {
                String desc = "";
                try {
                    desc = URLDecoder.decode(data.getDescription(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                mBinding.txtDescriptionPost.setText(CustomHtml.fromHtml(desc, fontLoader));
                if (desc.contains("<ul>")) {
                    mBinding.txtDescriptionPost.setText(Html.fromHtml(desc));
                    switch (Utils.fontName) {
                        case "dancing_script_bold":
                            mBinding.txtDescriptionPost.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/dancing_script_bold.ttf"));
                            break;
                        case "hanaleifill_regular":
                            mBinding.txtDescriptionPost.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/hanaleifill_regular.ttf"));
                            break;
                        case "merienda_bold":
                            mBinding.txtDescriptionPost.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/merienda_bold.ttf"));
                            break;
                        case "opificio_light_rounded":
                            mBinding.txtDescriptionPost.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/opificio_light_rounded.ttf"));
                            break;
                        default:
                            mBinding.txtDescriptionPost.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/avenir_roman.ttf"));
                            break;
                    }
                }
            }

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (modelList.get(position).equalsIgnoreCase("Image")) {
            return ITEM_IMAGE;
        } else {
            return ITEM_TEXT;
        }
    }
}