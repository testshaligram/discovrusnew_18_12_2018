package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemBusinessReviewInnerListBinding;
import com.danielandshayegan.discovrus.models.ReferenceListByUserId;
import com.danielandshayegan.discovrus.ui.fragment.BusinessProfileFragment;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;

import java.util.ArrayList;

public class BusinessReviewsInnerListAdapter extends BaseAdapter<ReferenceListByUserId.DataBean.ReferenceTextBean> {

    // region Member Variables
    private FooterViewHolder footerViewHolder;
    static Context mContext;
    private int selectedPosition = 0;
    // endregion

    // region Constructors
    public BusinessReviewsInnerListAdapter() {
        super();
    }

    public BusinessReviewsInnerListAdapter(Context context) {
        this.mContext = context;
    }
    // endregion

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }
    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemBusinessReviewInnerListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_business_review_inner_list, parent, false);

        final PostInnerViewHolder holder = new PostInnerViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PostInnerViewHolder holder = (PostInnerViewHolder) viewHolder;

        final ReferenceListByUserId.DataBean.ReferenceTextBean post = getItem(position);
        if (post != null) {
            holder.bind(post);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
//        holder.loadingImageView.setMaskOrientation(LoadingImageView.MaskOrientation.LeftToRight);
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new ReferenceListByUserId.DataBean.ReferenceTextBean());
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        notifyDataSetChanged();
    }

    // region Inner Classes
    public static class PostInnerViewHolder extends RecyclerView.ViewHolder {
        ItemBusinessReviewInnerListBinding mBinder;
        private DefaultTrackSelector trackSelector;

        public PostInnerViewHolder(ItemBusinessReviewInnerListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(ReferenceListByUserId.DataBean.ReferenceTextBean data) {

            String[] spiltList = data.getReviewId().split(",");
            for (int i = 0; i < spiltList.length; i++) {
                if (Integer.parseInt(spiltList[i]) == data.getReferenceID()) {
                    mBinder.txtReviews.setBackground(mContext.getResources().getDrawable(R.drawable.shape_round_gradiant));
                }
            }
            mBinder.txtReviews.setText(data.getReferenceText());

            mBinder.txtReviews.setOnClickListener(view -> {
                if (BusinessProfileFragment.businessProfileFragment.userId != BusinessProfileFragment.businessProfileFragment.logInUserId) {
                    if (data.isClickable()) {
                        int id = data.getReferenceID();
                        String[] spiltList1 = data.getReviewId().split(",");
                        ArrayList<String> arrayList = new ArrayList<>();
                        for (int i = 0; i < spiltList1.length; i++) {
                            arrayList.add(spiltList1[i]);
                            Log.e("item", arrayList.get(i));
                        }
                        if (arrayList.contains(String.valueOf(id))) {
                            Log.e("Index of id", "" + arrayList.indexOf(String.valueOf(id)));
                            arrayList.remove(arrayList.indexOf(String.valueOf(id)));
                        } else {
                            arrayList.add(String.valueOf(id));
                        }
                        String idString = "";
                        for (int i = 0; i < arrayList.size(); i++) {
                            if (idString.equalsIgnoreCase(""))
                                idString = arrayList.get(i);
                            else
                                idString = idString + "," + arrayList.get(i);
                        }
                        Log.e("IdString", "----------" + idString);
                        BusinessProfileFragment.businessProfileFragment.saveUserReference(idString);
                    }
                }

            });
        }


    }
}

