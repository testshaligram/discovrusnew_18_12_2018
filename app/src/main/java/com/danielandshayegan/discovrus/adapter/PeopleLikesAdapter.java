package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.LayoutLikeListBinding;
import com.danielandshayegan.discovrus.models.LikePeopleData;
import com.danielandshayegan.discovrus.models.NewsFeedData;
import com.danielandshayegan.discovrus.ui.activity.LikesActivity;
import com.danielandshayegan.discovrus.ui.common.BaseBinder;
import com.danielandshayegan.discovrus.webservice.APIs;

import java.util.ArrayList;

public class PeopleLikesAdapter extends RecyclerView.Adapter<PeopleLikesAdapter.MyViewHolder> {

    Context me;
    ArrayList<LikePeopleData> likedList = new ArrayList<>();
    NewsFeedData.PostData dataBean;

    public PeopleLikesAdapter(Context me, ArrayList<LikePeopleData> likedList, NewsFeedData.PostData dataBean) {
        this.me = me;
        this.likedList = likedList;
        this.dataBean = dataBean;
    }

    @NonNull
    @Override
    public PeopleLikesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutLikeListBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_like_list, parent, false);
        PeopleLikesAdapter.MyViewHolder holder = new PeopleLikesAdapter.MyViewHolder(mBinder);
        return holder;
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public void onBindViewHolder(@NonNull PeopleLikesAdapter.MyViewHolder holder, int position) {
        BaseBinder.setUserImageUrl(holder.binding.imgUser, APIs.BASE_IMAGE_PATH + likedList.get(position).getUserImagePath());

        holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(me, LikesActivity.class);
                intent.putExtra("postDetails", dataBean);
                me.startActivity(intent);
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LayoutLikeListBinding binding;

        public MyViewHolder(LayoutLikeListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}