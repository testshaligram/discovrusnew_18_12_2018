package com.danielandshayegan.discovrus.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.RowClickPostBinding;
import com.danielandshayegan.discovrus.databinding.RowCommentReplyBinding;
import com.danielandshayegan.discovrus.models.CommentReplyData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class PostCommentReplyAdapter extends RecyclerView.Adapter<PostCommentReplyAdapter.MyViewHolder> {

    List<CommentReplyData.DataBean> dataBeanList;
    Context context;
    public static String IMAGE_PATH = ApiClient.WebService.imageUrl;
    private DeletePosCommentReplyListner clickPosCommentListner;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    boolean isDeleted = false;
    private LikeCommentReplyListner likeCommentListner;


    public PostCommentReplyAdapter(List<CommentReplyData.DataBean> dataBeanList, Context context, DeletePosCommentReplyListner clickPosCommentListner, LikeCommentReplyListner likeCommentListner) {
        this.dataBeanList = dataBeanList;
        this.context = context;
        this.clickPosCommentListner = clickPosCommentListner;
        this.likeCommentListner = likeCommentListner;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowCommentReplyBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.row_comment_reply, parent, false);
        return new MyViewHolder(mBinder);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (dataBeanList.get(position).getUserImagePath() != null) {
            Picasso.with(context).
                    load(IMAGE_PATH + dataBeanList.get(position).getUserImagePath()).
                    into(holder.mBinder.imgCommentUserProile);
        }
        String comment = "";
        try {
            comment = URLDecoder.decode(dataBeanList.get(position).getComment(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        holder.mBinder.txtCommentTitle.setText(comment);
        String dateStr = dataBeanList.get(position).getCommentDate();
        if (dateStr.length() <= 10) {
            holder.mBinder.txtCommentDuration.setText(dateStr + "  - " + dataBeanList.get(position).getUserName());
        } else {
            if (dateStr != null) {
//                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date value = null;
                try {
                    value = formatter.parse(dateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm aa"); //this format changeable
                dateFormatter.setTimeZone(TimeZone.getDefault());
                dateStr = dateFormatter.format(value);
                if (!TextUtils.isEmpty(dateStr)) {
                    holder.mBinder.txtCommentDuration.setText(dateStr + " - " + dataBeanList.get(position).getUserName());
                }
            }
        }

        binderHelper.bind(holder.mBinder.swipeLayout, String.valueOf(dataBeanList.get(position).getCommentId()));
        holder.bind();

        if (dataBeanList.get(position).isCanDelete())
            holder.mBinder.commentDelete.setVisibility(View.VISIBLE);
        else
            holder.mBinder.commentDelete.setVisibility(View.GONE);

        if (dataBeanList.get(position).isIsCommentLike())
            holder.mBinder.btnCommentLike.setBackground(context.getResources().getDrawable(R.drawable.ic_red_like));
        else
            holder.mBinder.btnCommentLike.setBackground(context.getResources().getDrawable(R.drawable.heart_click_post));

        String countToPrint = Utils.convertLikes(dataBeanList.get(position).getLikeReplyCount());
        holder.mBinder.btnCommentLike.setText(countToPrint);

    }


    public void restoreStates(Bundle inState) {
        binderHelper.restoreStates(inState);
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        RowCommentReplyBinding mBinder;

        public MyViewHolder(RowCommentReplyBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;


        }

        public void bind() {
            mBinder.commentDelete.setOnClickListener(v -> new AlertDialog.Builder(context)
                    .setTitle("Delete Alert")
                    .setMessage("Are you sure you want to delete this comment?")
                    .setPositiveButton(R.string.Ok, (dialog, whichButton) -> {
                        dialog.dismiss();
                        clickPosCommentListner.onCommentDeleted(dataBeanList.get(getAdapterPosition()));
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(getAdapterPosition() != -1) {
                                    dataBeanList.remove(getAdapterPosition());
                                    notifyItemRemoved(getAdapterPosition());
                                }
                            }
                        }, 500);
                    })
                    .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    })
                    .show());

            mBinder.btnCommentLike.setOnClickListener(v -> {
                boolean isActive = !dataBeanList.get(getAdapterPosition()).isIsCommentLike();

                if (isActive) {
                    // int likeCount = Integer.parseInt(mBinder.favoritesCountTv.getText().toString());
                    int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinder.btnCommentLike.getText().toString()));
                    int addCount = likeCount + 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinder.btnCommentLike.setText(countToPrint);
                    dataBeanList.get(getAdapterPosition()).setLikeReplyCount(Integer.valueOf(countToPrint));
                    dataBeanList.get(getAdapterPosition()).setIsCommentLike(isActive);
                    mBinder.btnCommentLike.setBackground(context.getResources().getDrawable(R.drawable.ic_red_like));
                } else {
                    // int likeCount = Integer.parseInt(mBinder.favoritesCountTv.getText().toString());
                    int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinder.btnCommentLike.getText().toString()));
                    if (likeCount != 0) {
                        int minusLikeCount = likeCount - 1;
                        String countToPrint = Utils.convertLikes(minusLikeCount);
                        mBinder.btnCommentLike.setText(countToPrint);
                        dataBeanList.get(getAdapterPosition()).setLikeReplyCount(Integer.valueOf(countToPrint));
                        dataBeanList.get(getAdapterPosition()).setIsCommentLike(isActive);
                        mBinder.btnCommentLike.setBackground(context.getResources().getDrawable(R.drawable.heart_click_post));
                    }
                }
                likeCommentListner.onLikeComment(dataBeanList.get(getAdapterPosition()));
            });
        }
    }

    public interface DeletePosCommentReplyListner {

        void onCommentDeleted(CommentReplyData.DataBean clickpostData);

        void onCommentReplyClick(CommentReplyData.DataBean clickpostData);
    }

    public interface LikeCommentReplyListner {
        void onLikeComment(CommentReplyData.DataBean clickPostLike);
    }
}
