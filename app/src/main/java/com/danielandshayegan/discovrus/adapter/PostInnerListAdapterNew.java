package com.danielandshayegan.discovrus.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CustomHtml;
import com.danielandshayegan.discovrus.custome_veiws.autoplayvideos.AAH_CustomViewHolder;
import com.danielandshayegan.discovrus.custome_veiws.autoplayvideos.AAH_VideosAdapter;
import com.danielandshayegan.discovrus.databinding.ItemPostInnerListNewBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.fragment.PostManagementFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

public class PostInnerListAdapterNew extends AAH_VideosAdapter {

    private final List<PostListData.Post> list;
    //    private final Picasso picasso;
    ItemPostInnerListNewBinding mBinder;
    PostManagementFragment mContext;
    private static final int TYPE_VIDEO = 0, TYPE_TEXT = 1;

    public class MyViewHolder extends AAH_CustomViewHolder {
        ItemPostInnerListNewBinding mBinder;
        //to mute/un-mute video (optional)
        boolean isMuted;

        public MyViewHolder(View x) {
            super(x);
        }

        public MyViewHolder(ItemPostInnerListNewBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        //override this method to get callback when video starts to play
        @Override
        public void videoStarted() {
            super.videoStarted();
            mBinder.videoPlayIv.setVisibility(View.GONE);
            mBinder.imgPlayback.setVisibility(View.VISIBLE);
            if (isMuted) {
                muteVideo();
//                mBinder.imgVol.setImageResource(R.drawable.ic_mute);
                mBinder.imgVol.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_mute));
            } else {
                unmuteVideo();
//                mBinder.imgVol.setImageResource(R.drawable.ic_unmute);
                mBinder.imgVol.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_unmute));
            }
        }

        @Override
        public void pauseVideo() {
            super.pauseVideo();
//            img_playback.setImageResource(R.drawable.ic_play);
            mBinder.videoPlayIv.setVisibility(View.VISIBLE);
            mBinder.imgPlayback.setVisibility(View.GONE);
        }
    }

    public PostInnerListAdapterNew(List<PostListData.Post> list_urls, PostManagementFragment mContext) {
        this.list = list_urls;
        this.mContext = mContext;
    }

    @Override
    public AAH_CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*if (viewType==TYPE_TEXT) {
            View itemView1 = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_text, parent, false);
            return new MyTextViewHolder(itemView1);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.single_card, parent, false);
            return new MyViewHolder(itemView);
        }*/

        mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_post_inner_list_new, parent, false);

        return new MyViewHolder(mBinder);

    }

    @Override
    public void onBindViewHolder(final AAH_CustomViewHolder holder, int position) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext.getActivity()).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 2;

        //if you need 4-5-6 anything fix imageview in height
//            int deviceheight = displaymetrics.heightPixels / 4;

//        holder.mBinder.mainCard.getLayoutParams().width = devicewidth - dpToPx(20);

        //if you need same height as width you can set devicewidth in holder.image_view.getLayoutParams().height
//        card.getLayoutParams().height = devicewidth - 50;

        int deviceHalfWidth = (int) (devicewidth - mContext.getResources().getDimension(R.dimen._3sdp));
        int deviceFullWidth = (int) (displaymetrics.widthPixels);

        PostListData.Post postData = list.get(position);

        String like = Utils.convertLikes(postData.getLikeCount());
        String commentCount = Utils.convertLikes(postData.getCommentCount());
        String viewCount = Utils.convertLikes(postData.getViewCount());

        mBinder.commentsCountTv.setText(commentCount);
        mBinder.favoritesCountTv.setText(like);
        mBinder.viewsCountTv.setText(viewCount);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.placeholder_image)
                .error(R.drawable.placeholder_image);

        int userId = App_pref.getAuthorizedUser(mContext.getActivity()).getData().getUserId();
        if (postData.getUserID() == userId)
            mBinder.imgOptions.setVisibility(View.VISIBLE);
        else
            mBinder.imgOptions.setVisibility(View.GONE);

        mBinder.imgOptions.setOnClickListener(view -> {
            //Creating the instance of PopupMenu
            PopupMenu popup = new PopupMenu(mContext.getActivity(), mBinder.imgOptions);
            //Inflating the Popup using xml file
            popup.getMenuInflater().inflate(R.menu.delete_post_menu, popup.getMenu());

            //registering popup with OnMenuItemClickListener
            popup.setOnMenuItemClickListener(item -> {
//                Toast.makeText(mContext.getActivity(), "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                switch (item.getItemId()) {
                    case R.id.delete_post:
                        PostManagementFragment.managementFragment.deletePost(postData);
                        break;
                }
                return true;
            });

            popup.show();//showing popup menu
        });

        switch (postData.getType()) {
            case "Photo":
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                layoutParams.width = deviceHalfWidth;
                if (position == 0)
                    layoutParams.setMargins(0, (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                else
                    layoutParams.setMargins((int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            0, (int) mContext.getResources().getDimension(R.dimen._5sdp));
                mBinder.mainCard.setLayoutParams(layoutParams);
                ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                lp.height = devicewidth;
                mBinder.postViewConstraint.setLayoutParams(lp);
//mBinder.onlyImageIv.setScaleType(ImageView.ScaleType.FIT_XY);
                mBinder.videoConstraint.setVisibility(View.GONE);
                mBinder.imageConstraint.setVisibility(View.VISIBLE);
                mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                mBinder.textConstraint.setVisibility(View.GONE);
                Glide.with(mContext)
                        .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                        .apply(options)
                        .into(mBinder.onlyImageIv);

                break;
            case "Video":
                ConstraintLayout.LayoutParams layoutParams1 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                layoutParams1.width = deviceFullWidth;
                if (position == 0)
                    layoutParams1.setMargins(0, (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                else
                    layoutParams1.setMargins((int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            0, (int) mContext.getResources().getDimension(R.dimen._5sdp));
                mBinder.mainCard.setLayoutParams(layoutParams1);
                ConstraintLayout.LayoutParams lp1 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                lp1.height = devicewidth;
                mBinder.postViewConstraint.setLayoutParams(lp1);
                mBinder.videoConstraint.setVisibility(View.VISIBLE);
                mBinder.imageConstraint.setVisibility(View.GONE);
                mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                mBinder.textConstraint.setVisibility(View.GONE);

                RequestOptions options1 = new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder_video)
                        .error(R.drawable.placeholder_video);


                //todo
                holder.setImageUrl(ApiClient.WebService.imageUrl + postData.getThumbFileName());
                holder.setVideoUrl(ApplicationClass.getProxy().getProxyUrl(ApiClient.WebService.imageUrl + postData.getPostPath()));

                //load image into imageview
                if (postData.getThumbFileName() != null && !postData.getThumbFileName().isEmpty()) {
//                    picasso.load(holder.getImageUrl()).config(Bitmap.Config.RGB_565).into(holder.getAAH_ImageView());
                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getThumbFileName())
                            .apply(options1)
                            .into(holder.getAAH_ImageView());
                }

                holder.setLooping(true); //optional - true by default

                //to play pause videos manually (optional)
                mBinder.imgPlayback.setOnClickListener(v -> {
                    if (holder.isPlaying()) {
                        holder.pauseVideo();
                        holder.setPaused(true);
                    } else {
                        holder.playVideo();
                        holder.setPaused(false);
                    }
                });

                mBinder.videoPlayIv.setOnClickListener(view -> {
                    if (holder.isPlaying()) {
                        holder.pauseVideo();
                        holder.setPaused(true);
                    } else {
                        holder.playVideo();
                        holder.setPaused(false);
                    }
                });

                //to mute/un-mute video (optional)
                mBinder.imgVol.setOnClickListener(v -> {
                    if (((MyViewHolder) holder).isMuted) {
                        holder.unmuteVideo();
                        mBinder.imgVol.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_unmute));
//                        mBinder.imgVol.setImageResource(R.drawable.ic_unmute);
                    } else {
                        holder.muteVideo();
//                        mBinder.imgVol.setImageResource(R.drawable.ic_mute);
                        mBinder.imgVol.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_mute));
                    }
                    ((MyViewHolder) holder).isMuted = !((MyViewHolder) holder).isMuted;
                });

                if (postData.getPostPath() == null) {
                    mBinder.imgVol.setVisibility(View.GONE);
                    mBinder.imgPlayback.setVisibility(View.GONE);
                } else {
                    mBinder.imgVol.setVisibility(View.VISIBLE);
                    mBinder.imgPlayback.setVisibility(View.GONE);
                    mBinder.videoPlayIv.setVisibility(View.VISIBLE);
                }

                break;
            case "Text":
                ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                if (postData.isPaid()) {
                    if (position == 0)
                        layoutParams2.width = deviceFullWidth;
                    else
                        layoutParams2.width = deviceHalfWidth;
                } else
                    layoutParams2.width = deviceHalfWidth;
                if (position == 0)
                    layoutParams2.setMargins(0, (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                else
                    layoutParams2.setMargins((int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            0, (int) mContext.getResources().getDimension(R.dimen._5sdp));
                mBinder.mainCard.setLayoutParams(layoutParams2);

                if (postData.isPaid()) {
//                    mBinder.textConstraint.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    ConstraintLayout.LayoutParams imageParams = (ConstraintLayout.LayoutParams) mBinder.textConstraint.getLayoutParams();
                    imageParams.setMargins((int) mContext.getResources().getDimension(R.dimen._9sdp), (int) mContext.getResources().getDimension(R.dimen._9sdp),
                            (int) mContext.getResources().getDimension(R.dimen._9sdp), (int) mContext.getResources().getDimension(R.dimen._9sdp));
                    mBinder.textConstraint.setLayoutParams(imageParams);
                    mBinder.textConstraint.setBackground(mContext.getResources().getDrawable(R.drawable.text_bubble));
                } else {
//                    mBinder.textConstraint.setBackgroundColor(mContext.getResources().getColor(R.color.bubble_bg));
                    ConstraintLayout.LayoutParams imageParams = (ConstraintLayout.LayoutParams) mBinder.textConstraint.getLayoutParams();
                    imageParams.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._3sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._3sdp));
                    mBinder.textConstraint.setLayoutParams(imageParams);
                    mBinder.textConstraint.setBackground(mContext.getResources().getDrawable(R.drawable.new_half_post_bg));
                }
                if (!postData.isPaid()) {
                    ConstraintLayout.LayoutParams lp2 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                    lp2.height = devicewidth;
                    mBinder.postViewConstraint.setLayoutParams(lp2);
                } else {
                    if (position == 0) {
                        ConstraintLayout.LayoutParams lp2 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                        lp2.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        mBinder.postViewConstraint.setLayoutParams(lp2);
                    } else {
                        ConstraintLayout.LayoutParams lp2 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                        lp2.height = devicewidth;
                        mBinder.postViewConstraint.setLayoutParams(lp2);
                    }
                }

                mBinder.videoConstraint.setVisibility(View.GONE);
                mBinder.imageConstraint.setVisibility(View.GONE);
                mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                mBinder.textConstraint.setVisibility(View.VISIBLE);
                String title = "";
                try {
                    title = URLDecoder.decode(postData.getTitle(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String desc = "";
                try {
                    desc = URLDecoder.decode(postData.getDescription(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                final CaseInsensitiveAssetFontLoader fontLoader = new CaseInsensitiveAssetFontLoader(mContext.mActivity.getApplicationContext(), "fonts");
//                Spanned titleFromHTML = CustomHtml.fromHtml(postData.getTitle(), fontLoader);
//                Spanned descFromHTML = CustomHtml.fromHtml(postData.getDescription(), fontLoader);
                mBinder.titleTv.setText(title);
                mBinder.descTv.setText(desc);

                if (desc.contains("<ul>")) {
                    mBinder.descTv.setText(Html.fromHtml(desc));
                    switch (Utils.fontName) {
                        case "dancing_script_bold":
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                            break;
                        case "hanaleifill_regular":
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                            break;
                        case "merienda_bold":
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                            break;
                        case "opificio_light_rounded":
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                            break;
                        default:
                            mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                            break;
                    }
                }
//                mBinder.titleTv.setText(postData.getTitle());
//                mBinder.descTv.setText(postData.getDescription());

                break;
            case "PhotoAndText":
                ConstraintLayout.LayoutParams layoutParams3 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                layoutParams3.width = deviceHalfWidth;
                if (position == 0)
                    layoutParams3.setMargins(0, (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            (int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                else
                    layoutParams3.setMargins((int) mContext.getResources().getDimension(R.dimen._3sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp),
                            0, (int) mContext.getResources().getDimension(R.dimen._5sdp));
                mBinder.mainCard.setLayoutParams(layoutParams3);
                ConstraintLayout.LayoutParams lp3 = (ConstraintLayout.LayoutParams) mBinder.postViewConstraint.getLayoutParams();
                lp3.height = devicewidth;
                mBinder.postViewConstraint.setLayoutParams(lp3);
                mBinder.videoConstraint.setVisibility(View.GONE);
                mBinder.imageConstraint.setVisibility(View.GONE);
                mBinder.imageWithTextConstraint.setVisibility(View.VISIBLE);
                mBinder.textConstraint.setVisibility(View.GONE);

                Glide.with(mContext)
                        .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                        .apply(options)
                        .into(mBinder.imageWithTextIv);

                String ttl = "";
                try {
                    ttl = URLDecoder.decode(postData.getTitle(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String dsc = "";
                try {
                    dsc = URLDecoder.decode(postData.getDescription(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                final CaseInsensitiveAssetFontLoader fontLoader1 = new CaseInsensitiveAssetFontLoader(mContext.mActivity.getApplicationContext(), "fonts");
                mBinder.imgTitleTv.setText(CustomHtml.fromHtml(ttl, fontLoader1));
                mBinder.imgDescTv.setText(CustomHtml.fromHtml(dsc, fontLoader1));
                if (dsc.contains("<ul>")) {
                    mBinder.imgDescTv.setText(Html.fromHtml(dsc));
                    switch (Utils.fontName) {
                        case "dancing_script_bold":
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                            break;
                        case "hanaleifill_regular":
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                            break;
                        case "merienda_bold":
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                            break;
                        case "opificio_light_rounded":
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                            break;
                        default:
                            mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                            break;
                    }
                }

//                mBinder.imgTitleTv.setText(postData.getTitle());
//                mBinder.imgDescTv.setText(postData.getDescription());

                break;
        }

        if (postData.isLiked())
            mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
        else
            mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));

        if (postData.isCommented())
            mBinder.commentIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
        else
            mBinder.commentIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));

        if (postData.isViewed())
            mBinder.viewIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_view));
        else
            mBinder.viewIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_view_icon));


        mBinder.favoriteIv.setOnClickListener(v -> {
            if (!PostManagementFragment.isDisable) {
                boolean isActive = !postData.isLiked();
                if (isActive) {
                    // int likeCount = Integer.parseInt(mBinder.favoritesCountTv.getText().toString());
                    int likeCount = Integer.parseInt(Utils.convertToOriginalCount(((MyViewHolder) holder).mBinder.favoritesCountTv.getText().toString()));
                    int addCount = likeCount + 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    //mBinder.favoritesCountTv.setText(countToPrint);
                    ((MyViewHolder) holder).mBinder.favoritesCountTv.setText(countToPrint);
                    postData.setLikeCount(Integer.valueOf(countToPrint));
                    postData.setLiked(isActive);
//                    mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
                    ((MyViewHolder) holder).mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
                } else {
                    // int likeCount = Integer.parseInt(mBinder.favoritesCountTv.getText().toString());
                    int likeCount = Integer.parseInt(Utils.convertToOriginalCount(String.valueOf(postData.getLikeCount())));
                    if (likeCount != 0) {
                        int minusLikeCount = likeCount - 1;
                        String countToPrint = Utils.convertLikes(minusLikeCount);
                        ((MyViewHolder) holder).mBinder.favoritesCountTv.setText(countToPrint);
                        postData.setLikeCount(Integer.valueOf(countToPrint));
                        postData.setLiked(isActive);
                        ((MyViewHolder) holder).mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));
                    }
                }
                PostManagementFragment.managementFragment.saveLikePost(postData.getPostId(), isActive, postData.getUserID());
            } else if (PostManagementFragment.isDisable) {
                PostManagementFragment.managementFragment.trendinUi();
            }
        });

        mBinder.mainCard.setOnClickListener(v -> {
            if (!PostManagementFragment.isDisable) {
                int count = Integer.parseInt(Utils.convertToOriginalCount(((MyViewHolder) holder).mBinder.viewsCountTv.getText().toString()));
                int addCount = count + 1;
                String countToPrint = Utils.convertLikes(addCount);
                ((MyViewHolder) holder).mBinder.viewsCountTv.setText(countToPrint);
                postData.setViewCount(Integer.valueOf(countToPrint));
                postData.setViewed(true);
                ((MyViewHolder) holder).mBinder.viewIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_view));
                PostManagementFragment.managementFragment.savePostView(postData.getPostId());
                String postType = postData.getType();
                switch (postType) {
                    case "Video":
                        PostManagementFragment.managementFragment.getPostVideoDetails(postData);
                        break;
                    case "Photo":
                        PostManagementFragment.managementFragment.getPostImageDetails(postData);
                        break;
                    case "Text":
                        PostManagementFragment.managementFragment.getPostTextDetails(postData, position);
                        break;
                    case "PhotoAndText":
                        PostManagementFragment.managementFragment.getPostImageDetails(postData);
                        break;


                }
                   /* if (postData.getType().equalsIgnoreCase("Video")) {
                        mContext.startActivity(new Intent(mContext, VideoDetailsActivity.class).putExtra("videoUrl", postData.getPostPath()));
                    }*/

            } else if (PostManagementFragment.isDisable) {
                PostManagementFragment.managementFragment.trendinUi();
            }
        });

       /* mBinder.viewIv.setOnClickListener(v -> {
            if (!PostManagementFragment.isDisable) {
                *//*int viewCount = Integer.parseInt(mBinder.viewsCountTv.getText().toString());
                postData.setViewCount(postData.getViewCount() + 1);
                mBinder.viewsCountTv.setText(String.valueOf(postData.getViewCount()));
                PostManagementFragment.businessProfileFragment.savePostView(postData.getPostId());*//*
                int count = Integer.parseInt(Utils.convertToOriginalCount(((MyViewHolder) holder).mBinder.viewsCountTv.getText().toString()));
                int addCount = count + 1;
                String countToPrint = Utils.convertLikes(addCount);
                ((MyViewHolder) holder).mBinder.viewsCountTv.setText(countToPrint);
                postData.setViewCount(Integer.valueOf(countToPrint));
                postData.setViewed(true);
                ((MyViewHolder) holder).mBinder.viewIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_view));
                PostManagementFragment.managementFragment.savePostView(postData.getPostId());

            } else if (PostManagementFragment.isDisable) {
                PostManagementFragment.managementFragment.trendinUi();
            }
        });*/

    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public int getItemViewType(int position) {
        if (list.get(position).getType().equalsIgnoreCase("Video")) {
            return TYPE_VIDEO;
        } else return TYPE_TEXT;
    }


}