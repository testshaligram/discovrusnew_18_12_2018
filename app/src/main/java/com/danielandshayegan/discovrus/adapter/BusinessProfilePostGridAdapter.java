package com.danielandshayegan.discovrus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CustomHtml;
import com.danielandshayegan.discovrus.databinding.AdapterFooterBinding;
import com.danielandshayegan.discovrus.databinding.ItemPostInnerListBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.fragment.BusinessProfileFragment;
import com.danielandshayegan.discovrus.ui.fragment.IndividualProfileFragment;
import com.danielandshayegan.discovrus.ui.fragment.PostManagementFragment;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class BusinessProfilePostGridAdapter extends BaseAdapter<PostListData.Post> {

    // region Member Variables
    private FooterViewHolder footerViewHolder;
    static Context mContext;
    private int selectedPosition = 0;
    // endregion

    // region Constructors
    public BusinessProfilePostGridAdapter() {
        super();
    }

    public BusinessProfilePostGridAdapter(Context context) {
        this.mContext = context;
    }
    // endregion

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        ItemPostInnerListBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_post_inner_list, parent, false);

        final PostInnerViewHolder holder = new PostInnerViewHolder(mBinder);

        holder.itemView.setOnClickListener(v -> {
            int adapterPos = holder.getAdapterPosition();
            if (adapterPos != RecyclerView.NO_POSITION) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        AdapterFooterBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(mBinder);
        holder.itemView.setOnClickListener(v -> {
            if (onReloadClickListener != null) {
                onReloadClickListener.onReloadClick();
            }
        });
        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final PostInnerViewHolder holder = (PostInnerViewHolder) viewHolder;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int devicewidth = displaymetrics.widthPixels / 2;

        final PostListData.Post post = getItem(position);
        if (post != null) {
            holder.bind(post, (int) (devicewidth - mContext.getResources().getDimension(R.dimen._5sdp)));
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
    }

    @Override
    protected void displayLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.errorRl.setVisibility(View.GONE);
            footerViewHolder.mBinder.loadingFl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void displayErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.mBinder.loadingFl.setVisibility(View.GONE);
            footerViewHolder.mBinder.errorRl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new PostListData.Post());
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
        notifyDataSetChanged();
    }

    // region Inner Classes
    public static class PostInnerViewHolder extends RecyclerView.ViewHolder {
        ItemPostInnerListBinding mBinder;
        private DefaultTrackSelector trackSelector;

        public PostInnerViewHolder(ItemPostInnerListBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }

        // region Helper Methods
        private void bind(PostListData.Post postData, int deviceHalfWidth) {

            mBinder.commentsCountTv.setText(String.valueOf(postData.getCommentCount()));
            mBinder.favoritesCountTv.setText(String.valueOf(postData.getLikeCount()));
            mBinder.viewsCountTv.setText(String.valueOf(postData.getViewCount()));
            RequestOptions options = new RequestOptions()
                    .fitCenter()
                    .placeholder(R.drawable.placeholder_image)
                    .error(R.drawable.placeholder_image);

            if (BusinessProfileFragment.businessProfileFragment.logInUserId == BusinessProfileFragment.businessProfileFragment.userId)
                mBinder.imgOptions.setVisibility(View.VISIBLE);
            else
                mBinder.imgOptions.setVisibility(View.GONE);

            mBinder.imgOptions.setOnClickListener(view -> {
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(mContext, mBinder.imgOptions);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.delete_post_menu, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(item -> {
//                    Toast.makeText(mContext, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                    switch (item.getItemId()) {
                        case R.id.delete_post:
                            BusinessProfileFragment.businessProfileFragment.deletePost(postData);
                            break;
                    }
                    return true;
                });

                popup.show();//showing popup menu
            });

            switch (postData.getType()) {
                case "Photo":
                    ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams.width = deviceHalfWidth;
                    layoutParams.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._7sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.VISIBLE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.GONE);
                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.onlyImageIv);

                    break;
                case "Video":
                    ConstraintLayout.LayoutParams layoutParams1 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams1.width = deviceHalfWidth;
                    layoutParams1.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._7sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams1);

                    mBinder.videoConstraint.setVisibility(View.VISIBLE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.GONE);

                    RequestOptions options1 = new RequestOptions()
                            .centerCrop()
                            .placeholder(R.drawable.placeholder_video)
                            .error(R.drawable.placeholder_video);

                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getThumbFileName())
                            .apply(options1)
                            .into(mBinder.videoThumbIv);

                    break;
                case "Text":
                    ConstraintLayout.LayoutParams layoutParams2 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams2.width = deviceHalfWidth;
                    layoutParams2.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._7sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams2);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.GONE);
                    mBinder.textConstraint.setVisibility(View.VISIBLE);

//                    mBinder.titleTv.setText(postData.getTitle());
//                    mBinder.descTv.setText(postData.getDescription());
                    String title = "";
                    try {
                        title = URLDecoder.decode(postData.getTitle(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String desc = "";
                    try {
                        desc = URLDecoder.decode(postData.getDescription(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    final CaseInsensitiveAssetFontLoader fontLoader = new CaseInsensitiveAssetFontLoader(mContext.getApplicationContext(), "fonts");
                    mBinder.titleTv.setText(CustomHtml.fromHtml(title, fontLoader));
                    mBinder.descTv.setText(CustomHtml.fromHtml(desc, fontLoader));
                    if (desc.contains("<ul>")) {
                        mBinder.descTv.setText(Html.fromHtml(desc));
                        switch (Utils.fontName) {
                            case "dancing_script_bold":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/dancing_script_bold.ttf"));
                                break;
                            case "hanaleifill_regular":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/hanaleifill_regular.ttf"));
                                break;
                            case "merienda_bold":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/merienda_bold.ttf"));
                                break;
                            case "opificio_light_rounded":
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/opificio_light_rounded.ttf"));
                                break;
                            default:
                                mBinder.descTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/avenir_roman.ttf"));
                                break;
                        }
                    }

                    break;
                case "PhotoAndText":
                    ConstraintLayout.LayoutParams layoutParams3 = (ConstraintLayout.LayoutParams) mBinder.mainCard.getLayoutParams();
                    layoutParams3.width = deviceHalfWidth;
                    layoutParams3.setMargins((int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._7sdp),
                            (int) mContext.getResources().getDimension(R.dimen._5sdp), (int) mContext.getResources().getDimension(R.dimen._5sdp));
                    mBinder.mainCard.setLayoutParams(layoutParams3);

                    mBinder.videoConstraint.setVisibility(View.GONE);
                    mBinder.imageConstraint.setVisibility(View.GONE);
                    mBinder.imageWithTextConstraint.setVisibility(View.VISIBLE);
                    mBinder.textConstraint.setVisibility(View.GONE);

                    Glide.with(mContext)
                            .load(ApiClient.WebService.imageUrl + postData.getPostPath())
                            .apply(options)
                            .into(mBinder.imageWithTextIv);
//                    mBinder.imgTitleTv.setText(postData.getTitle());
//                    mBinder.imgDescTv.setText(postData.getDescription());
                    String ttl = "";
                    try {
                        ttl = URLDecoder.decode(postData.getTitle(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String dsc = "";
                    try {
                        dsc = URLDecoder.decode(postData.getDescription(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    final CaseInsensitiveAssetFontLoader fontLoader1 = new CaseInsensitiveAssetFontLoader(mContext.getApplicationContext(), "fonts");
                    mBinder.imgTitleTv.setText(CustomHtml.fromHtml(ttl, fontLoader1));
                    mBinder.imgDescTv.setText(CustomHtml.fromHtml(dsc, fontLoader1));
                    if (dsc.contains("<ul>")) {
                        mBinder.imgDescTv.setText(Html.fromHtml(dsc));
                        switch (Utils.fontName) {
                            case "dancing_script_bold":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/dancing_script_bold.ttf"));
                                break;
                            case "hanaleifill_regular":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/hanaleifill_regular.ttf"));
                                break;
                            case "merienda_bold":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/merienda_bold.ttf"));
                                break;
                            case "opificio_light_rounded":
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/opificio_light_rounded.ttf"));
                                break;
                            default:
                                mBinder.imgDescTv.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fonts/avenir_roman.ttf"));
                                break;
                        }
                    }

                    break;
            }

            if (postData.isLiked())
                mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
            else
                mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));

            if (postData.isViewed())
                mBinder.viewIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_view));
            else
                mBinder.viewIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_view_icon));

            if (postData.isCommented())
                mBinder.commentIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
            else
                mBinder.commentIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));


            mBinder.favoriteIv.setOnClickListener(v -> {
                boolean isActive = !postData.isLiked();
                if (isActive) {
                    // int likeCount = Integer.parseInt(mBinder.favoritesCountTv.getText().toString());
                    int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinder.favoritesCountTv.getText().toString()));
                    int addCount = likeCount + 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    //mBinder.favoritesCountTv.setText(countToPrint);
                    mBinder.favoritesCountTv.setText(countToPrint);
                    postData.setLikeCount(Integer.valueOf(countToPrint));
                    postData.setLiked(isActive);
//                    mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
                    mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
                } else {
                    // int likeCount = Integer.parseInt(mBinder.favoritesCountTv.getText().toString());
                    int likeCount = Integer.parseInt(Utils.convertToOriginalCount(String.valueOf(postData.getLikeCount())));
                    if (likeCount != 0) {
                        int minusLikeCount = likeCount - 1;
                        String countToPrint = Utils.convertLikes(minusLikeCount);
                        mBinder.favoritesCountTv.setText(countToPrint);
                        postData.setLikeCount(Integer.valueOf(countToPrint));
                        postData.setLiked(isActive);
                        mBinder.favoriteIv.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));
                    }
                }
                BusinessProfileFragment.businessProfileFragment.saveLikePost(postData.getPostId(), isActive);
            });

            mBinder.mainCard.setOnClickListener(v -> {
                int count = Integer.parseInt(Utils.convertToOriginalCount(mBinder.viewsCountTv.getText().toString()));
                int addCount = count + 1;
                String countToPrint = Utils.convertLikes(addCount);
                mBinder.viewsCountTv.setText(countToPrint);
                postData.setViewCount(Integer.valueOf(countToPrint));
                BusinessProfileFragment.businessProfileFragment.savePostView(postData.getPostId());
                String postType = postData.getType();
                switch (postType) {
                    case "Video":
                        BusinessProfileFragment.businessProfileFragment.getPostVideoDetails(postData);
                        break;
                    case "Photo":
                        BusinessProfileFragment.businessProfileFragment.getPostImageDetails(postData);
                        break;
                    case "Text":
                        BusinessProfileFragment.businessProfileFragment.getPostTextDetails(postData, getAdapterPosition());
                        break;
                    case "PhotoAndText":
                        BusinessProfileFragment.businessProfileFragment.getPostImageDetails(postData);
                        break;


                }

            });

            mBinder.viewIv.setOnClickListener(v -> {
                int count = Integer.parseInt(Utils.convertToOriginalCount(mBinder.viewsCountTv.getText().toString()));
                int addCount = count + 1;
                String countToPrint = Utils.convertLikes(addCount);
                mBinder.viewsCountTv.setText(countToPrint);
                postData.setViewCount(Integer.valueOf(countToPrint));
                BusinessProfileFragment.businessProfileFragment.savePostView(postData.getPostId());
            });
        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        AdapterFooterBinding mBinder;

        public FooterViewHolder(AdapterFooterBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }

}
