package com.danielandshayegan.discovrus.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.utils.SideSelector;
import com.danielandshayegan.discovrus.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Prakash on 11-08-2016.
 */
public class CategoryItemAdapter extends BaseAdapter implements SectionIndexer {

    Context mContext;
    List<CategoriesTableModel> data = null;
    public static String IMAGE_PATH = ApiClient.WebService.imageUrl;
    int index = 0;

    public CategoryItemAdapter(Context mContext, List<CategoriesTableModel> data) {
        this.mContext = mContext;
        this.data = data;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(R.layout.item_category_row, parent, false);

        ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.imageViewIcon);
        TextView textViewName = (TextView) listItem.findViewById(R.id.textViewName);
        TextView tvDescription = (TextView) listItem.findViewById(R.id.tvDescription);

        if(data.get(position).getImagePath()!=null) {
            Picasso.with(mContext).
                    load(IMAGE_PATH + data.get(position).getImagePath()).
                    into(imageViewIcon);
        }
        /*textViewName.setText(data.get(position).getCategoryName());
        tvDescription.setText(data.get(position).getDescription());*/

        textViewName.setText(data.get(position).getDescription());
        tvDescription.setText(data.get(position).getServices());

        /*imageViewIcon.setImageResource(data.get(position).icon);
        textViewName.setText(data.get(position).name);
        if (selectItem == position) {
            imageViewIcon.setColorFilter(ContextCompat.getColor(mContext, R.color.colorAccent));
            textViewName.setTextColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        }*/

        return listItem;
    }

    public Object[] getSections() {
        String[] chars = new String[SideSelector.ALPHABET.length];
        for (int i = 0; i < SideSelector.ALPHABET.length; i++) {
            chars[i] = String.valueOf(SideSelector.ALPHABET[i]);
        }
        return chars;
    }

    public int getPositionForSection(int i) {
        for (CategoriesTableModel string : data) {
            if (string.getCategoryName().startsWith(String.valueOf(SideSelector.ALPHABET[i]))) {
                index = data.indexOf(string);
                break;
            }
        }
//        return (int) (getCount() * ((float) i / (float) getSections().length));
        return index;
    }

    public int getSectionForPosition(int i) {
        return 0;
    }
}
