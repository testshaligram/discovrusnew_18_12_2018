
package com.danielandshayegan.discovrus.db.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.danielandshayegan.discovrus.db.AppDatabase;
import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;
import com.danielandshayegan.discovrus.interfaces.OnAsyncTaskEventListener;

import java.util.List;

public class DatabaseInitializer {

    public static void populateAsync(final AppDatabase db, List<CategoriesTableModel> categoriesList, OnAsyncTaskEventListener<String> listener) {

        PopulateDbAsync task = new PopulateDbAsync(db, categoriesList, listener);

        task.execute();
    }

    private static void addCategory(final AppDatabase db, final CategoriesTableModel categoriesModel) {

        long l = db.categoryDao().insert(categoriesModel);
    }

    private static void populateInitialData(AppDatabase db, List<CategoriesTableModel> categoriesArray) {
        db.categoryDao().deleteAll();

        // Insert Data in Database
        for (int i = 0; i < categoriesArray.size(); i++) {

            CategoriesTableModel categoriesModel = categoriesArray.get(i);
            addCategory(db, categoriesModel);
        }
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;
        List<CategoriesTableModel> mCategoriesData;
        OnAsyncTaskEventListener<String> mCallBack;

        PopulateDbAsync(AppDatabase db, List<CategoriesTableModel> daysArray, OnAsyncTaskEventListener<String> listener) {
            mDb = db;
            mCategoriesData = daysArray;
            mCallBack = listener;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateInitialData(mDb, mCategoriesData);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (mCallBack != null) {
                mCallBack.onSuccess("success");
            }
            Log.e("PopulateDbAsync : ", "Data Inserted");
        }
    }
}
