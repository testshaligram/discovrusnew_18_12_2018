package com.danielandshayegan.discovrus.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "category_master")
public class CategoriesTableModel {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("CategoryId")
    @Expose
    @ColumnInfo(name = "CategoryId")
    private String CategoryId = "";

    @SerializedName("CategoryName")
    @Expose
    @ColumnInfo(name = "CategoryName")
    private String CategoryName;

    @SerializedName("Description")
    @Expose
    @ColumnInfo(name = "Description")
    private String Description = "";

    @SerializedName("Services")
    @Expose
    @ColumnInfo(name = "Services")
    private String Services = "";

    @SerializedName("ImagePath")
    @Expose
    @ColumnInfo(name = "ImagePath")
    private String ImagePath;

    private String IsSelected = "1";

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIsSelected() {
        return IsSelected;
    }

    public void setIsSelected(String isSelected) {
        IsSelected = isSelected;
    }

    public String getServices() {
        return Services;
    }

    public void setServices(String services) {
        Services = services;
    }

    @Override
    public String toString() {
        return "CategoriesTableModel{" +
                "id=" + id +
                ", CategoryId='" + CategoryId + '\'' +
                ", CategoryName='" + CategoryName + '\'' +
                ", Description='" + Description + '\'' +
                ", Services='" + Services + '\'' +
                ", ImagePath='" + ImagePath + '\'' +
                ", IsSelected='" + IsSelected + '\'' +
                '}';
    }
}