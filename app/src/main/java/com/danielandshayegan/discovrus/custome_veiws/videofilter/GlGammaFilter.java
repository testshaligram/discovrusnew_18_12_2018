package com.danielandshayegan.discovrus.custome_veiws.videofilter;

import com.daasuu.mp4compose.filter.GlFilter;
import com.daasuu.mp4compose.utils.GlUtils;

public class GlGammaFilter extends GlFilter {
    private static float gammaValue=1.8f;
    private static final String FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n"
                    + "precision mediump float;\n"
                    + "varying vec2 vTextureCoord;\n"
                    + "uniform samplerExternalOES sTexture;\n"
                    + "float gamma=1.8;\n"
                    + "void main() {\n"
                    + "vec4 textureColor = texture2D(sTexture, vTextureCoord);\n"
                    + "gl_FragColor = vec4(pow(textureColor.rgb, vec3(gamma)), textureColor.w);\n"
                    + "}\n";


    public GlGammaFilter() {
        super(GlUtils.DEFAULT_VERTEX_SHADER, FRAGMENT_SHADER);
    }
}
