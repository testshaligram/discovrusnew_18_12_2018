package com.danielandshayegan.discovrus.custome_veiws.videofilter;

import com.daasuu.mp4compose.filter.GlFilter;
import com.daasuu.mp4compose.utils.GlUtils;

public class GlSaturationFilter extends GlFilter {
    private static float contrast;

    /**
     * Initialize Effect
     *
     * @param contrast Range should be between 0.1- 2.0 with 1.0 being normal.
     */


    private static final String FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n"
                    + "precision mediump float;\n"
                    + "uniform samplerExternalOES sTexture;\n"
                    + "const vec3 W = vec3(0.2125, 0.7154, 0.0721);\n"
                    + "varying vec2 vTextureCoord;\n"
                    + "void main() {\n"
                    + "float T = 1.0;\n"
                    + "vec2 st = vTextureCoord.st;\n"
                    + "vec3 irgb  = texture2D(sTexture, st).rgb;\n"
                    + "float luminance = dot(irgb, W);\n"
                    + "vec3 target  = vec3(luminance, luminance, luminance);\n"
                    + "gl_FragColor = vec4(mix(target, irgb, T), 1.);\n" + "}\n";


    public GlSaturationFilter() {
        super(GlUtils.DEFAULT_VERTEX_SHADER, FRAGMENT_SHADER);
    }

}
