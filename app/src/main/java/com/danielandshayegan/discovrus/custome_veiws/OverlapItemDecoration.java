package com.danielandshayegan.discovrus.custome_veiws;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class OverlapItemDecoration extends RecyclerView.ItemDecoration {
    private final static int vertOverlap = -30;

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        final int itemPosition = parent.getChildAdapterPosition(view);
        if (itemPosition == 0) {
            return;
        }
        outRect.set(vertOverlap, 0, 0, 0);
    }
}