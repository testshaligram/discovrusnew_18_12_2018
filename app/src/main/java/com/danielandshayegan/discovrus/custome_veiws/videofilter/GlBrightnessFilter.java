package com.danielandshayegan.discovrus.custome_veiws.videofilter;

import com.daasuu.mp4compose.filter.GlFilter;
import com.daasuu.mp4compose.utils.GlUtils;

public class GlBrightnessFilter extends GlFilter {
    private static float brightnessValue;

    public GlBrightnessFilter(float brightnessValue) {
        if (brightnessValue < 0.1f)
            brightnessValue = 0.1f;
        if (brightnessValue > 2.0f)
            brightnessValue = 2.0f;

        this.brightnessValue = brightnessValue;
    }

    private static final String FRAGMENT_SHADER =
            "#extension GL_OES_EGL_image_external : require\n"
                    + "precision mediump float;\n"
                    + "uniform samplerExternalOES sTexture;\n"
                    + "float brightness ;\n"
                    + "varying vec2 vTextureCoord;\n"
                    + "void main() {\n"
                    + "float T = 1.5;"
                    + "vec2 st = vTextureCoord.st;\n"
                    + "vec3 irgb  = texture2D(sTexture, st).rgb;\n"
                    + "vec3 black = vec3(0., 0., 0.);\n"
                    + "gl_FragColor = vec4(mix(black, irgb, T), 1.);\n" + "}\n";


    public GlBrightnessFilter() {
        super(GlUtils.DEFAULT_VERTEX_SHADER, FRAGMENT_SHADER);
    }
}
