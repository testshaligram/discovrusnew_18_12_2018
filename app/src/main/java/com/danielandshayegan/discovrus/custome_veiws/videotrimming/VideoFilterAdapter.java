package com.danielandshayegan.discovrus.custome_veiws.videotrimming;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ThumbnailListItemBinding;

import java.util.List;

public class VideoFilterAdapter extends RecyclerView.Adapter<VideoFilterAdapter.MyViewHolder> {


    private List<VideoFilterNameModel> videoFilterNameModels;
    private VideoFilterListener listener;
    private Context mContext;
    private int selectedIndex = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ThumbnailListItemBinding mBinder;

        public MyViewHolder(ThumbnailListItemBinding mBinder) {
            super(mBinder.getRoot());
            this.mBinder = mBinder;
        }
    }

    public VideoFilterAdapter(List<VideoFilterNameModel> videoFilterNameModels, VideoFilterListener listener, Context mContext) {
        this.videoFilterNameModels = videoFilterNameModels;
        this.listener = listener;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ThumbnailListItemBinding mBinder = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.thumbnail_list_item, parent, false);
        return new MyViewHolder(mBinder);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final VideoFilterNameModel videoFilterNameModel = videoFilterNameModels.get(position);

        //     holder.mBinder.thumbnail.setImageBitmap(thumbnailItem.image);

        holder.mBinder.filterName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onFilterSelected(videoFilterNameModel);
                selectedIndex = position;
                notifyDataSetChanged();
            }
        });

        holder.mBinder.filterName.setText(videoFilterNameModel.filterName);

    }

    @Override
    public int getItemCount() {
        return videoFilterNameModels.size();
    }

    public interface VideoFilterListener {
        void onFilterSelected(VideoFilterNameModel videoFilterNameModel);
    }
}
