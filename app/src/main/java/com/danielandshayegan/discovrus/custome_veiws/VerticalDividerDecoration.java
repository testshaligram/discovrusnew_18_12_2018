package com.danielandshayegan.discovrus.custome_veiws;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class VerticalDividerDecoration extends RecyclerView.ItemDecoration {
    public final static int vertOverlap = 8;

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view);
        // hide the divider for the last child
        if (position == parent.getAdapter().getItemCount() - 1) {
        } else {
            outRect.right = vertOverlap;
        }
    }
}