package com.danielandshayegan.discovrus.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.ui.fragment.LikesFragment;

public class LikesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_likes);

        Bundle b = new Bundle();
        b.putParcelable("postDetails", getIntent().hasExtra("postDetails") ? getIntent().getParcelableExtra("postDetails") : null);

        Fragment f = new LikesFragment();
        f.setArguments(b);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, f, "General Profile")
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}