package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daasuu.mp4compose.FillMode;
import com.daasuu.mp4compose.composer.Mp4Composer;
import com.daasuu.mp4compose.filter.GlFilter;
import com.daasuu.mp4compose.filter.GlGrayScaleFilter;
import com.daasuu.mp4compose.filter.GlInvertFilter;
import com.daasuu.mp4compose.filter.GlSepiaFilter;
import com.daasuu.mp4compose.filter.GlVignetteFilter;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.UserPostListAdapter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GLContrastFilter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GlBrightnessFilter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GlGammaFilter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GlHueFilter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GlPostrizedFilter;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.DetailPostingVideoFilterAdapter;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.VideoFilterNameModel;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.utils.FileUtils;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostingVideoBinding;
import com.danielandshayegan.discovrus.models.ConnectPeopleData;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.VideoPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.DetailPostConnectPeopleActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostPublishActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.ui.activity.VideoTrimActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.sherazkhilji.videffects.BlackAndWhiteEffect;
import com.sherazkhilji.videffects.BrightnessEffect;
import com.sherazkhilji.videffects.ContrastEffect;
import com.sherazkhilji.videffects.GammaEffect;
import com.sherazkhilji.videffects.HueEffect;
import com.sherazkhilji.videffects.InvertColorsEffect;
import com.sherazkhilji.videffects.NoEffect;
import com.sherazkhilji.videffects.PosterizeEffect;
import com.sherazkhilji.videffects.SepiaEffect;
import com.sherazkhilji.videffects.VignetteEffect;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import io.reactivex.disposables.CompositeDisposable;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;
import static com.danielandshayegan.discovrus.ui.fragment.RegisterFragment.SELECT_CATEGORY;
import static com.google.android.gms.common.util.IOUtils.copyStream;


public class DetailPostingVideoFragment extends BaseFragment implements DetailPostingVideoFilterAdapter.VideoFilterListener, SingleCallback {

    View view;
    FragmentDetailPostingVideoBinding mBinding;
    int postType;
    String videoUri;
    VideoFilterListFragment videoFilterListFragment;
    MediaPlayer mediaPlayer;
    private GlFilter filter;
    private ProgressDialog mProgressDialog;
    List<VideoFilterNameModel> videoFilterNameModelList;
    private Mp4Composer mp4Composer;
    String destPath;
    DetailPostingVideoFilterListFragment.FilterVideoList listener;
    DetailPostingVideoFilterAdapter mAdapter;
    Uri filterUril;
    private String pathToStoredVideo;
    File fileForCompress;
    int userId;
    byte[] byteArrayThumbnile;
    Bitmap thumb;
    String storedFilePath;
    private String filePath;
    private FFmpeg ffmpeg;
    File destCompress;
    File fileFiler;
    Activity activityMain;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<PostListData.Post> postDataList = new ArrayList<>();
    UserPostListAdapter userPostListAdapter;
    public HashMap<String, ConnectPeopleData.DataBean> selectedPeopleList = new HashMap<>();

    private static final int REQUEST_VIDEO_TRIMMER = 0x01;
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    static final String VIDEO_TOTAL_DURATION = "VIDEO_TOTAL_DURATION";

    public DetailPostingVideoFragment() {
        // Required empty public constructor
    }

    public static DetailPostingVideoFragment newInstance() {
        return new DetailPostingVideoFragment();
    }

    public static DetailPostingVideoFragment newInstance(Bundle extras) {
        DetailPostingVideoFragment fragment = new DetailPostingVideoFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_posting_video, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        disableScreen(false);
        activityMain = this.getActivity();

        videoFilterNameModelList = new ArrayList<>();
        VideoFilterNameModel videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Normal");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Black & White");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Contrast");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Sepia effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Invert color effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Vignette effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Hue effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Brightness effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Gamma effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Posterized effect");
        videoFilterNameModelList.add(videoFilterNameModel);

       /* DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBinding.previewCard.getLayoutParams();
        layoutParams.width = (int) (devicewidth - getResources().getDimension(R.dimen._15sdp));
        layoutParams.setMargins((int) getResources().getDimension(R.dimen._5sdp), (int) getResources().getDimension(R.dimen._7sdp),
                (int) getResources().getDimension(R.dimen._7sdp), (int) getResources().getDimension(R.dimen._5sdp));
        mBinding.previewCard.setLayoutParams(layoutParams);

        mBinding.recyclerPostData.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        mBinding.recyclerPostData.setOnFlingListener(null);

        userPostListAdapter = new UserPostListAdapter(mContext);

        mBinding.recyclerPostData.setItemAnimator(new SlideInUpAnimator());
       // mBinding.recyclerPostData.setAdapter(userPostListAdapter);*/

        mAdapter = new DetailPostingVideoFilterAdapter(videoFilterNameModelList, this, getActivity());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.uiVideoDetailPosting.recyclerView.setLayoutManager(mLayoutManager);
        //  showProgressDialog();
//        callAPI();
        mBinding.uiVideoDetailPosting.recyclerView.setItemAnimator(new DefaultItemAnimator());
      /*  int space = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8,
                getResources().getDisplayMetrics());
        mBinding.recyclerView.addItemDecoration(new SpacesItemDecoration(space));*/
        mBinding.uiVideoDetailPosting.recyclerView.setAdapter(mAdapter);

        /*if (getArguments() != null) {

            postData();
        }*/

        setClicks();

        return view;
    }

    public void setClicks() {
        mBinding.uiVideoDetailPosting.btnNext.setOnClickListener(v -> {
            if (mBinding.uiVideoDetailPosting.imgSelect.getVisibility() == GONE)
                saveVideo();
            else
                Toast.makeText(getActivity(), "Please select video.", Toast.LENGTH_LONG).show();
        });

        mBinding.uiVideoDetailPosting.imgBack.setOnClickListener(v -> {
            mediaPlayer.pause();
            DetailPostingActivity.postingActivity.onBackPressed();
        });

        /*mBinding.uiVideoDetailPosting.txtAddClickie.setOnClickListener(view -> {
            if (mBinding.uiVideoDetailPosting.imgSelect.getVisibility() == GONE)
                mActivity.startActivityForResult(new Intent(mActivity, ClickiesActivity.class), 201);
            else
                Toast.makeText(mActivity, "Please select image.", Toast.LENGTH_LONG).show();
        });*/

        mBinding.uiVideoDetailPosting.txtConnectPeople.setOnClickListener(view -> {
//            if (finalImage != null)
            if (selectedPeopleList.size() > 0)
                mActivity.startActivityForResult(new Intent(mActivity, DetailPostConnectPeopleActivity.class).putExtra("peopleList", selectedPeopleList), 601);
            else
                mActivity.startActivityForResult(new Intent(mActivity, DetailPostConnectPeopleActivity.class), 601);
//            else
//                Toast.makeText(mActivity, "Please select image.", Toast.LENGTH_LONG).show();
        });

        mBinding.uiVideoDetailPosting.imgSelect.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= 23)
                getPermission();
            else
                showDialogForVideo();
        });

        mBinding.uiVideoDetailPosting.layoutSelectType.setOnClickListener(view -> {
            mBinding.uiVideoDetailPosting.layoutSelectType.setVisibility(View.GONE);
            mBinding.uiVideoDetailPosting.strip.setVisibility(VISIBLE);
        });

        mBinding.uiVideoDetailPosting.layoutExtend.setOnClickListener(view1 -> {
            mBinding.uiVideoDetailPosting.layoutSelectType.setVisibility(VISIBLE);
            mBinding.uiVideoDetailPosting.strip.setVisibility(GONE);
        });

        mBinding.uiVideoDetailPosting.imgTypeImage.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingImageFragment.newInstance(), "DetailPostingImageFragment")
                    .commit();
        });

        mBinding.uiVideoDetailPosting.imgTypeImageWithText.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingTextFragment.newInstance(), "DetailPostingImageWithTextFragment")
                    .commit();
        });

        mBinding.uiVideoDetailPosting.imgTypeText.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingTextFragment.newInstance(), "DetailPostingTextFragment")
                    .commit();
        });

        mBinding.uiVideoDetailPosting.imgTypeVideo.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingVideoFragment.newInstance(), "DetailPostingVideoFragment")
                    .commit();
        });

    }

    @Override
    public void onStop() {
        super.onStop();
        if (mediaPlayer != null)
            mediaPlayer.pause();
    }

    private void postData() {
        if (getArguments() != null) {

            postType = getArguments().getInt("postType");
            videoUri = getArguments().getString("videoUri");


            switch (postType) {

                case 4:

                    loadVideo();

            }
            //  Bitmap bitmap = BitmapUtils.getBitmapFromGallery(this, data.getData(), 800, 800);

        }
    }

    private void loadVideo() {
        try {
            mediaPlayer.setDataSource(videoUri);

            mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                    new NoEffect());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
    }

    @Override
    public void onFilterSelected(VideoFilterNameModel videoFilterNameModel) {
        switch (videoFilterNameModel.getFilterName()) {
            case "Normal":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                        new NoEffect());
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                break;
            case "Black & White":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                        new BlackAndWhiteEffect());
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GlGrayScaleFilter();
                break;
            case "Contrast":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                        new ContrastEffect(1.5f));
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GLContrastFilter();
                break;

            case "Sepia effect":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                        new SepiaEffect());
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GlSepiaFilter();
                break;
            case "Invert color effect":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                        new InvertColorsEffect());
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GlInvertFilter();
                break;
            case "Vignette effect":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                        new VignetteEffect(0.2f));
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GlVignetteFilter();
                break;
            case "Hue effect":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                        new HueEffect(90));
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GlHueFilter();
                break;
            case "Brightness effect":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer,
                        new BrightnessEffect(90));
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GlBrightnessFilter();
                break;
            case "Gamma effect":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer, new GammaEffect(1.8f));
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GlGammaFilter();
                break;
            case "Posterized effect":
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.init(mediaPlayer, new PosterizeEffect());
                mBinding.uiVideoDetailPosting.mVideoSurfaceView.onResume();
                filter = new GlPostrizedFilter();
                break;


        }
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(activityMain);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Uploading video...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    public String getVideoFilePath() {
        return getAndroidMoviesFolder().getAbsolutePath() + "/" + new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date()) + "filter_apply.mp4";
    }

    public File getAndroidMoviesFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
    }

    private void saveVideo() {
        showProgressDialog();
        disableScreen(true);

        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        final String fileName = "MP4_" + timeStamp + ".mp4";

        File folder = Environment.getExternalStorageDirectory();
        String mFinalPath = folder.getPath() + File.separator;

        final String filePath = mFinalPath + "DiscovrusTrim/" + fileName;
        fileFiler = new File(filePath);
        fileFiler.getParentFile().mkdirs();


        destPath = getVideoFilePath();
        mp4Composer = null;
        mp4Composer = new Mp4Composer(videoUri, fileFiler.getPath())
                // .rotation(Rotation.ROTATION_270)
                .fillMode(FillMode.PRESERVE_ASPECT_FIT)
                .filter(filter)
                .listener(new Mp4Composer.Listener() {
                    @Override
                    public void onProgress(double progress) {

                        //runOnUiThread(() -> progressBar.setProgress((int) (progress * 100)));
                        Log.i("videoSaving", videoUri);
                    }

                    @Override
                    public void onCompleted() {
                        Log.i("videoSaving", fileFiler.getPath());
                        new filteredVideoSaving().execute();
                    }

                    @Override
                    public void onCanceled() {
                        Log.i("videoSaving", "cancel");
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                hideProgressDialog();
                                disableScreen(false);

                            }
                        });

                    }

                    @Override
                    public void onFailed(Exception exception) {

                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                hideProgressDialog();
                                disableScreen(false);

                            }
                        });

                        Log.i("videoSaving", exception.toString());

                    }
                })
                .start();
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void videoUpload(String file) {

//        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayThumbnile);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("UploadThumbnail", userId + "thumbnile.png", requestFile);
//        RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), file);
//        MultipartBody.Part vFile = MultipartBody.Part.createFormData("UploadVideo", file.getName(), videoBody);
        if (Double.valueOf(getAvailableInternalMemorySize().split(" ")[0]) > 5) {
            String thumbUri = storeImage(thumb);
//            Uri uri = Uri.fromFile(file);

            String tagUserID = "";
            if (selectedPeopleList.size() > 0) {
                Iterator myVeryOwnIterator = selectedPeopleList.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    String key = (String) myVeryOwnIterator.next();
                    ConnectPeopleData.DataBean value = selectedPeopleList.get(key);
                    if (tagUserID.equalsIgnoreCase(""))
                        tagUserID = "" + value.getUserId();
                    else
                        tagUserID = tagUserID + "," + value.getUserId();
                }
            }
            hideProgressDialog();
            disableScreen(false);

            startActivity(new Intent(activityMain, DetailPostPublishActivity.class)
                    .putExtra("Type", "Video")
                    .putExtra("tagUserID", tagUserID)
                    .putExtra("caption", mBinding.uiVideoDetailPosting.edtAddCaption.getText().toString())
                    .putExtra("Thumb", thumbUri)
                    .putExtra("VideoFile", file));
        } else {
            Toast.makeText(activityMain, "Please free some space to continue.", Toast.LENGTH_LONG).show();
        }

        /* ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                uploadVideoPostData("", userId, vFile, body)
                        , getCompositeDisposable(), postVideo, this);*/
    }

    public static String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return formatSize(availableBlocks * blockSize);
    }

    public static String formatSize(long size) {
        String suffix = null;
        if (size >= 1024) {
            suffix = " KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = " MB";
                size /= 1024;
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        /*int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }*/

        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    private String storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Toast.makeText(activityMain, "Error creating media file, check storage permissions.", Toast.LENGTH_LONG).show();// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            return storedFilePath;
//            Toast.makeText(getActivity(), "bitmap store", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            Log.d("TAG", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("TAG", "Error accessing file: " + e.getMessage());
        }
        return "";
    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getActivity().getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        storedFilePath = mediaStorageDir.getPath() + File.separator + mImageName;
        return mediaFile;
    }

    private void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {

                ffmpeg = FFmpeg.getInstance(getActivity());
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    Log.d("videoSaving", "ffmpeg : correct Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        } catch (Exception e) {
            Log.d("videoSaving", "EXception no controlada : " + e);
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Supported")
                .setMessage("Device Not Supported")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                })
                .create()
                .show();

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case postVideo:
                hideProgressDialog();
                disableScreen(false);
                VideoPostData videoPostData = (VideoPostData) o;
                if (videoPostData.isSuccess()) {
                  /*  File moviesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
                    if (moviesDir.isDirectory()) {
                        String[] children = moviesDir.list();
                        for (int i = 0; i < children.length; i++) {
                            new File(moviesDir, children[i]).delete();
                        }
                        moviesDir.delete();


                    }

                    File folder = Environment.getExternalStorageDirectory();
                    String mFinalPath = folder.getPath() + File.separator + "DiscovrusTrim";
                    File trimFolder = new File(mFinalPath);
                    if (trimFolder.isDirectory()) {
                        String[] children = trimFolder.list();
                        for (int i = 0; i < children.length; i++) {
                            new File(trimFolder, children[i]).delete();
                        }
                        trimFolder.delete();
                    }*/
                    mediaPlayer.pause();
//                    PostManagementFragment.managementFragment.callAPI(false);
//                    activityMain.onBackPressed();
                  /*  getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.relative_filter, PostManagementFragment.newInstance(), "")
                            .commit();*/
                    Intent intent = new Intent(getActivity(), MenuActivity.class);
                    intent.putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    getActivity().startActivity(intent);
                    getActivity().finish();

                    getActivity().overridePendingTransition(0, 0);
                    Log.i("videoResponse", "" + videoPostData);

                    Toast.makeText(getActivity(), videoPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(), videoPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        hideProgressDialog();
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }

    }

    class filteredVideoSaving extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... voids) {

            final ContentValues values = new ContentValues(2);
            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
            values.put(MediaStore.Video.Media.DATA, fileFiler.getPath());
            // MediaStoreに登録
            getActivity().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    values);
            getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                    Uri.parse("file://" + fileFiler.getPath())));

            Log.e("videoSavingFIlter", fileFiler.getPath());


            return "file:" + fileFiler.getPath();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("videoSavingFIlter", s);
            if (s != null) {
                filterUril = Uri.parse(s);

                if (filterUril != null) {
                    pathToStoredVideo = getRealPathFromURIPath(filterUril, getActivity());
                    fileForCompress = new File(pathToStoredVideo);
                    thumb = ThumbnailUtils.createVideoThumbnail(fileForCompress.getPath(),
                            MediaStore.Images.Thumbnails.MINI_KIND);
                    //  new VideoCompressor().execute();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    thumb.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byteArrayThumbnile = stream.toByteArray();
//                    thumb.recycle();
                    if (fileForCompress != null && byteArrayThumbnile != null) {

                        executeCompressCommand();

                    }
                }

            }
        }
    }

    private void executeCompressCommand() {


        File cacheFile = new File(Environment.getExternalStorageDirectory() + "/Discovrus");
        Log.e("videoSaving", "" + cacheFile);
        if (!cacheFile.exists()) {
            cacheFile.mkdirs();

        }

        String filePrefix = userId + "video_discovrus" + System.currentTimeMillis();
        String fileExtn = ".mp4";
        String yourRealPath = getPath(getActivity(), filterUril);


        destCompress = new File(cacheFile, filePrefix + fileExtn);
        int fileNo = 0;
        while (destCompress.exists()) {
            fileNo++;
            destCompress = new File(cacheFile, filePrefix + fileNo + fileExtn);
        }

        Log.d("videoSaving", "startTrim: src: " + yourRealPath);
        Log.d("videoSaving", "startTrim: dest: " + destCompress.getAbsolutePath());
        filePath = destCompress.getAbsolutePath();
        String[] complexCommand = {"-y", "-i", yourRealPath, "-s", "480x320", "-r", "25", "-vcodec", "mpeg4", "-b:v", "900k", "-b:a", "48000", "-ac", "2", "-ar", "22050", filePath};
        execFFmpegBinary(complexCommand);

    }

    private void execFFmpegBinary(final String[] command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.d("videoSaving", "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {

                    Log.i("videoSavingTrimFIle", s);

                    pathToStoredVideo = getRealPathFromURIPath(Uri.parse(filePath), getActivity());
                    File fileForUpload = new File(pathToStoredVideo);
                    videoUpload(pathToStoredVideo);


                }

                @Override
                public void onProgress(String s) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {


                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }

    private String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content_navigation://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content_navigation".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private void getPermission() {
        String[] params = null;
        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
        String camera = Manifest.permission.CAMERA;

        int hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(activityMain, writeExternalStorage);
        int hasReadExternalStoragePermission = ActivityCompat.checkSelfPermission(activityMain, readExternalStorage);
        int hasCameraPermission = ActivityCompat.checkSelfPermission(activityMain, camera);
        List<String> permissions = new ArrayList<String>();

        if (hasWriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(writeExternalStorage);
        if (hasReadExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(readExternalStorage);
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(camera);

        if (!permissions.isEmpty()) {
            params = permissions.toArray(new String[permissions.size()]);
        }
        if (params != null && params.length > 0) {
            ActivityCompat.requestPermissions(activityMain,
                    params,
                    100);
        } else
            showDialogForVideo();
    }

    private void showDialogForVideo() {

        new AlertDialog.Builder(activityMain)
                .setTitle("Select video")
                .setMessage("Please choose video selection type")
                .setPositiveButton("CAMERA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        openVideoCapture();
                    }
                })
                .setNegativeButton("GALLERY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickFromGallery();
                    }
                })
                .show();
    }

    private void openVideoCapture() {
        Intent videoCapture = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(videoCapture, REQUEST_VIDEO_TRIMMER);
    }

    private void pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(activityMain, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.permission_read_storage_rationale), REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setTypeAndNormalize("video/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_video)), REQUEST_VIDEO_TRIMMER);
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activityMain, permission)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activityMain);
            builder.setTitle(getString(R.string.permission_title_rationale));
            builder.setMessage(rationale);
            builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(activityMain, new String[]{permission}, requestCode);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), null);
            builder.show();
        } else {
            ActivityCompat.requestPermissions(activityMain, new String[]{permission}, requestCode);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        Log.e("onactivityResult", "done");

        if (reqCode == REQUEST_VIDEO_TRIMMER && resultCode == RESULT_OK) {
            final Uri selectedUri = data.getData();
            if (selectedUri != null) {
                videoUri = String.valueOf(selectedUri);
                try {
                    GeneralSettingsData nsData = Utils.getGeneralSettingsData(getActivity());

                    if (nsData != null && nsData.isSuccess() && nsData.getData() != null) {
                        if(nsData.getData().get(0).isSaveVideos())
                            Utils.saveVideoToExternal(getActivity(), selectedUri);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                startTrimActivity(selectedUri);
            } else {
                Toast.makeText(activityMain, R.string.toast_cannot_retrieve_selected_video, Toast.LENGTH_SHORT).show();
            }
        }

        if (reqCode == SELECT_CATEGORY && resultCode == RESULT_OK && data != null) {
            //* mBinding.uiBusiness.edtCategory.setText(data.getStringExtra(CATEGORY_NAME));
            // bussinessCategoryId = data.getIntExtra(CATEGORY_ID, 0);*//*
        }

        if (reqCode == 3000 && resultCode == RESULT_OK && data != null) {
            /*Bundle bundle = new Bundle();
            bundle.putInt("postType", 4);
            bundle.putString("imageUri", "");
            bundle.putString("videoUri", VideoTrimActivity.VIDEO_URL);*/

            /*Intent intent = new Intent(activityMain, PostFilteringActivity.class);
            intent.putExtra("filterFragment", "videoFilter");
            intent.putExtras(bundle);
            startActivity(intent);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            activityMain.overridePendingTransition(0, 0);*/
            mBinding.uiVideoDetailPosting.imagePreview.setVisibility(View.GONE);
            mBinding.uiVideoDetailPosting.imgSelect.setVisibility(View.GONE);
            mBinding.uiVideoDetailPosting.mVideoSurfaceView.setVisibility(VISIBLE);

            loadFFMpegBinary();
            mediaPlayer = new MediaPlayer();

            videoUri = VideoTrimActivity.VIDEO_URL;

            loadVideo();

        }
        if (reqCode == 601 && resultCode == RESULT_OK) {
            selectedPeopleList = (HashMap<String, ConnectPeopleData.DataBean>) data.getSerializableExtra("peopleList");
            mBinding.uiVideoDetailPosting.txtConnectPeople.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_post_connect_people_selected, 0);
        }

    }

    private void startTrimActivity(@NonNull Uri uri) {
        Intent intent = new Intent(activityMain, VideoTrimActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATH, FileUtils.getPath(activityMain, uri));
        intent.putExtra(VIDEO_TOTAL_DURATION, getMediaDuration(uri));
        startActivityForResult(intent, 3000);
    }

    private int getMediaDuration(Uri uriOfFile) {
        MediaPlayer mp = MediaPlayer.create(activityMain, uriOfFile);
        int duration = mp.getDuration();
        return duration;
    }
}
