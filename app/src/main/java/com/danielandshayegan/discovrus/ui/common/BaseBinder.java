package com.danielandshayegan.discovrus.ui.common;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.danielandshayegan.discovrus.R;
import com.google.android.exoplayer2.ui.PlayerView;

import java.util.HashMap;

import static com.danielandshayegan.discovrus.webservice.APIs.BASE_IMAGE_PATH;

public class BaseBinder {
    public static final int IMAGE_CORNER_RADIUS = 18;

    @BindingAdapter({"app:setImageUri", "app:setImageUrl"})
    public static void setImage(ImageView iv, Uri imageUri, String Url) {
        if (Url == null || Url.length() == 0) {
            setImageUri(iv, imageUri);
        } else {
            setImageUrl(iv, Url);
        }
    }

    @BindingAdapter({"app:setImageUri"})
    public static void setImageUri(ImageView iv, Uri imageUri) {
        if (imageUri != null)
            Glide.with(iv.getContext())
                    .load(imageUri.getPath())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.placeholder_image)
                            .error(R.drawable.placeholder_image)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .transforms(new CenterCrop(), new RoundedCorners(IMAGE_CORNER_RADIUS)))
                    .into(iv);
        else
            iv.setImageResource(R.drawable.placeholder_image);
    }

    @BindingAdapter({"app:setImageUrl"})
    public static void setImageUrl(ImageView iv, String imageUrl) {
        if (imageUrl != null)
            Glide.with(iv.getContext())
                    .load(BASE_IMAGE_PATH + imageUrl)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.placeholder_image)
                            .error(R.drawable.placeholder_image)
                            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                            .transforms(new CenterCrop(), new RoundedCorners(IMAGE_CORNER_RADIUS)))
                    .into(iv);
        else
            iv.setImageResource(R.drawable.placeholder_image);
    }

    @BindingAdapter({"app:setUserImageUri", "app:setUserImageUrl"})
    public static void setUserImage(ImageView iv, Uri imageUri, String Url) {
        if (Url == null || Url.length() == 0) {
            setUserImageUri(iv, imageUri);
        } else {
            setUserImageUrl(iv, Url);
        }
    }

    @BindingAdapter({"app:setUserImageUri"})
    public static void setUserImageUri(ImageView iv, Uri imageUri) {
        if (imageUri != null)
            Glide.with(iv.getContext())
                    .load(imageUri.getPath())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.user_placeholder)
                            .error(R.drawable.user_placeholder))
                    .into(iv);
        else
            iv.setImageResource(R.drawable.user_placeholder);
    }

    @BindingAdapter({"app:setUserImageUrl"})
    public static void setUserImageUrl(ImageView iv, String imageUrl) {
        if (imageUrl != null)
            Glide.with(iv.getContext())
                    .load(BASE_IMAGE_PATH + imageUrl)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.user_placeholder)
                            .error(R.drawable.user_placeholder))
                    .into(iv);
        else
            iv.setImageResource(R.drawable.user_placeholder);
    }

    public static void loadThumbnail(Context context, PlayerView playerView, String videoThumbnail, int video_width, int video_height) {
        if (video_width != 0 && video_height != 0) {
            Glide.with(context)
                    .asBitmap()
                    .apply(RequestOptions.overrideOf(video_width, video_height))
                    .load(videoThumbnail)
                    .thumbnail(.05f)
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            if (playerView != null) {
                                playerView.setUseArtwork(true);
                                playerView.setDefaultArtwork(resource);
                            }
                        }
                    });
        } else {
            Glide.with(context)
                    .asBitmap()
                    .load(videoThumbnail)
                    .thumbnail(.05f)
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            if (playerView != null) {
                                playerView.setUseArtwork(true);
                                playerView.setDefaultArtwork(resource);
                            }
                        }
                    });
        }
    }
}