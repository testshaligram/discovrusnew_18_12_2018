package com.danielandshayegan.discovrus.ui.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivitySplashBinding;
import com.danielandshayegan.discovrus.db.AppDatabase;
import com.danielandshayegan.discovrus.db.utils.DatabaseInitializer;
import com.danielandshayegan.discovrus.interfaces.OnAsyncTaskEventListener;
import com.danielandshayegan.discovrus.models.CategoryList;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getCategoryList;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

public class SplashActivity extends BaseActivity implements SingleCallback {

    ActivitySplashBinding mBinding;
    private static final String ACTIVITY_ANIMATION = "animation";
    Uri deepLink;
    String trendingId = "", postId = "", type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        FirebaseApp.initializeApp(this);
        int SPLASH_TIME_OUT = 2000;

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.danielandshayegan.discovrus",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)

                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                            //     Toast.makeText(SplashActivity.this, deepLink.toString(), Toast.LENGTH_SHORT).show();
                            Intent intent = getIntent();
                            if (null != intent) {
                                Uri uri = intent.getData();
                                if (uri.getQueryParameterNames().contains("dataId"))
                                    trendingId = uri.getQueryParameter("dataId");
                                if (uri.getQueryParameterNames().contains("postId"))
                                    postId = uri.getQueryParameter("postId");
                                if (uri.getQueryParameterNames().contains("type"))
                                    type = uri.getQueryParameter("type");
                                //   Toast.makeText(SplashActivity.this, trendingId, Toast.LENGTH_SHORT).show();
                                Utils.FROM_SPLASH = true;

                            }
                        } else {
                            //  Toast.makeText(SplashActivity.this, "null", Toast.LENGTH_SHORT).show();
                        }

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("link", "getDynamicLink:onFailure", e);
                    }
                });

        mBinding.btnReload.setOnClickListener(view -> {
            mBinding.btnReload.setVisibility(View.GONE);
            callAPI();
        });
        if (Utils.isNetworkAvailable(SplashActivity.this)) {
            generateHashkey();
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(SplashActivity.this, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }

    }


    public void generateHashkey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.danielandshayegan.discovrus",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash", e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash", e.getMessage(), e);
        }
    }

    private void callAPI() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(SplashActivity.this).
                                create(WebserviceBuilder.class).
                                getCategoryList()
                        , getCompositeDisposable(), getCategoryList, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getCategoryList:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CategoryList categoryList = (CategoryList) o;
                if (categoryList.isSuccess()) {
                    DatabaseInitializer.populateAsync(AppDatabase.getAppDatabase(SplashActivity.this), categoryList.getData(), new OnAsyncTaskEventListener<String>() {
                        @Override
                        public void onSuccess(String object) {
                            if (App_pref.getAuthorizedUser(getApplicationContext()) != null) {

                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent(getApplicationContext(), MenuActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                                        intent.putExtra("email", App_pref.getAuthorizedUser(getApplicationContext()).getData().getEmail());
                                        if (!trendingId.equalsIgnoreCase(""))
                                            intent.putExtra("trendingId", trendingId);
                                        if (!postId.equalsIgnoreCase("")) {
                                            intent.putExtra("postId", postId);
                                            intent.putExtra("type", type);
                                        }
                                        startActivity(intent);
                                        finish();
                                    }
                                }, 1500);


                            } else {
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra(ACTIVITY_ANIMATION, "splash");
                                overridePendingTransition(R.anim.fade_out, R.anim.fade_in);
                                startActivity(i);
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Toast.makeText(SplashActivity.this, "Insert data fail", Toast.LENGTH_LONG).show();
                            mBinding.btnReload.setVisibility(View.VISIBLE);
                        }
                    });

                } else {
                    Toast.makeText(SplashActivity.this, categoryList.getMessage().get(0), Toast.LENGTH_LONG).show();
                    mBinding.btnReload.setVisibility(View.VISIBLE);
                }
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getApplicationContext(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
}
