package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StatFs;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.system.ErrnoException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.daasuu.mp4compose.filter.GlFilter;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.BitmapUtils;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostingImageBinding;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostingImageWithTextBinding;
import com.danielandshayegan.discovrus.models.ConnectPeopleData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.ClickiesActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostConnectPeopleActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingImageWithTextActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.utils.LayoutToImage;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postPhoto;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;
import static com.danielandshayegan.discovrus.ui.fragment.RegisterFragment.SELECT_CATEGORY;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPostingImageWithTextFragment extends BaseFragment implements SingleCallback, DetailPostingFiltersListFragment.FiltersListFragmentListener, EditImageFragment.EditImageFragmentListener {

    FragmentDetailPostingImageWithTextBinding mBinding;
    View view;
    int userId;
    int postType;
    DetailPostingFiltersListFragment filtersListFragment;
    EditImageFragment editImageFragment;
    public Uri mImageUri;
    File fileImage;
    String videoUri;
    Bitmap bitmap;
    MultipartBody.Part body;
    public static final String IMAGE_NAME = "cap_top.png";

    private ProgressDialog mProgressDialog;
    Bitmap originalImage;
    // to backup image with filter applied
    Bitmap filteredImage;
    private GlFilter filter;
    // the final image after applying
    // brightness, saturation, contrast
    Bitmap finalImage;
    int brightnessFinal = 0;
    float saturationFinal = 1.0f;
    float contrastFinal = 1.0f;
    Activity activityMain;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<PostListData.Post> postDataList = new ArrayList<>();

    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    boolean doubleClick = false;
    private Handler handler = new Handler();
    public HashMap<String, ConnectPeopleData.DataBean> selectedPeopleList = new HashMap<>();
    public static DetailPostingImageWithTextFragment postingImageWithTextFragment;
    String storedFilePath;
    // load native image filters library
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    public DetailPostingImageWithTextFragment() {
        // Required empty public constructor
    }

    public static DetailPostingImageWithTextFragment newInstance() {
        return new DetailPostingImageWithTextFragment();
    }

    public static DetailPostingImageWithTextFragment newInstance(Bundle extras) {
        DetailPostingImageWithTextFragment fragment = new DetailPostingImageWithTextFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_posting_image_with_text, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        postingImageWithTextFragment = this;

        setClicks();
        setTouch();

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void setTouch() {
        mBinding.uiImageDetailPosting.imgClickie.setOnTouchListener(new View.OnTouchListener() {

            FrameLayout.LayoutParams parms;
            int startwidth;
            int startheight;
            float dx = 0, dy = 0, x = 0, y = 0;
            float angle = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final FrameLayout view = mBinding.uiImageDetailPosting.movableFrame;

//                ((BitmapDrawable) view.getDrawable()).setAntiAlias(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        mBinding.uiImageDetailPosting.nestedScrollView.requestDisallowInterceptTouchEvent(true);
                        parms = (FrameLayout.LayoutParams) view.getLayoutParams();
                        startwidth = parms.width;
                        startheight = parms.height;
                        dx = event.getRawX() - parms.leftMargin;
                        dy = event.getRawY() - parms.topMargin;
                        mode = DRAG;


                        break;

                    case MotionEvent.ACTION_POINTER_DOWN:
                        oldDist = spacing(event);
                        if (oldDist > 10f) {
                            mode = ZOOM;
                        }

                        d = rotation(event);


                        break;
                    case MotionEvent.ACTION_UP:
                        mBinding.uiImageDetailPosting.nestedScrollView.requestDisallowInterceptTouchEvent(false);
                        mBinding.uiImageDetailPosting.imgClickie.performClick();
                        break;

                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;


                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode == DRAG) {

                            x = event.getRawX();
                            y = event.getRawY();

                            parms.leftMargin = (int) (x - dx);
                            parms.topMargin = (int) (y - dy);

                          /*  parms.rightMargin = 0;
                            parms.bottomMargin = 0;
                            parms.rightMargin = parms.leftMargin + (5 * parms.width);
                            parms.bottomMargin = parms.topMargin + (10 * parms.height);
*/
                            view.setLayoutParams(parms);


                        } else if (mode == ZOOM) {

                            if (event.getPointerCount() == 2) {

                                newRot = rotation(event);
                                float r = newRot - d;
                                angle = r;

                                x = event.getRawX();
                                y = event.getRawY();

                              /*  float newDist = spacing(event);
                                if (newDist > 10f) {
                                    float scale = newDist / oldDist * view.getScaleX();
                                    if (scale > 0.6) {
                                        scalediff = scale;
                                        view.setScaleX(scale);
                                        view.setScaleY(scale);

                                    }
                                }*/

                                view.animate().rotationBy(angle).setDuration(0).setInterpolator(new LinearInterpolator()).start();

                                x = event.getRawX();
                                y = event.getRawY();

                             /*   parms.leftMargin = (int) ((x - dx) + scalediff);
                                parms.topMargin = (int) ((y - dy) + scalediff);

                                parms.rightMargin = 0;
                                parms.bottomMargin = 0;
                                parms.rightMargin = parms.leftMargin + (5 * parms.width);
                                parms.bottomMargin = parms.topMargin + (10 * parms.height);

                                view.setLayoutParams(parms);*/


                            }
                        }
                        break;
                }

                return true;

            }

        });
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }

    public void setClicks() {
        mBinding.uiImageDetailPosting.imgBack.setOnClickListener(v -> {
            DetailPostingActivity.postingActivity.onBackPressed();
        });

        mBinding.uiImageDetailPosting.btnNext.setOnClickListener(v -> {
            if (finalImage != null) {
                mBinding.uiImageDetailPosting.imgRemoveClickie.setVisibility(View.GONE);
                mBinding.uiImageDetailPosting.btnNext.setEnabled(false);
                mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                mBinding.uiImageDetailPosting.btnNext.setEnabled(true);
                saveImageToGallery();
            } else {
                Toast.makeText(getActivity(), "Please select image.", Toast.LENGTH_LONG).show();
            }
        });

        mBinding.uiImageDetailPosting.imgClickie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        doubleClick = false;
                    }
                };
                if (!doubleClick) {
                    //your logic for double click action
                    doubleClick = true;
                    handler.postDelayed(r, 200);
                } else {
                    doubleClick = false;
                    mBinding.uiImageDetailPosting.imgRemoveClickie.setVisibility(View.VISIBLE);
                }
            }
        });

        mBinding.uiImageDetailPosting.imgRemoveClickie.setOnClickListener(v -> {
            mBinding.uiImageDetailPosting.imgRemoveClickie.setVisibility(View.GONE);
            mBinding.uiImageDetailPosting.imgClickie.setVisibility(View.GONE);
        });

        mBinding.uiImageDetailPosting.txtAddClickie.setOnClickListener(view -> {
            if (finalImage != null)
                mActivity.startActivityForResult(new Intent(mActivity, ClickiesActivity.class), 201);
            else
                Toast.makeText(mActivity, "Please select image.", Toast.LENGTH_LONG).show();
        });

        mBinding.uiImageDetailPosting.txtConnectPeople.setOnClickListener(view -> {
//            if (finalImage != null)
            if (selectedPeopleList.size() > 0)
                mActivity.startActivityForResult(new Intent(mActivity, DetailPostConnectPeopleActivity.class).putExtra("peopleList", selectedPeopleList), 601);
            else
                mActivity.startActivityForResult(new Intent(mActivity, DetailPostConnectPeopleActivity.class), 601);
//            else
//                Toast.makeText(mActivity, "Please select image.", Toast.LENGTH_LONG).show();
        });

        mBinding.uiImageDetailPosting.imgSelect.setOnClickListener(view -> {
            imgPick();
        });

        mBinding.uiImageDetailPosting.layoutSelectType.setOnClickListener(view -> {
            mBinding.uiImageDetailPosting.layoutSelectType.setVisibility(View.GONE);
            mBinding.uiImageDetailPosting.strip.setVisibility(VISIBLE);
        });

        mBinding.uiImageDetailPosting.layoutExtend.setOnClickListener(view1 -> {
            mBinding.uiImageDetailPosting.layoutSelectType.setVisibility(VISIBLE);
            mBinding.uiImageDetailPosting.strip.setVisibility(View.GONE);
        });

        mBinding.uiImageDetailPosting.imgTypeImage.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingImageFragment.newInstance(), "DetailPostingImageFragment")
                    .commit();
        });

        mBinding.uiImageDetailPosting.imgTypeImageWithText.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingImageWithTextFragment.newInstance(), "DetailPostingImageWithTextFragment")
                    .commit();
        });

        mBinding.uiImageDetailPosting.imgTypeText.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingTextFragment.newInstance(), "DetailPostingTextFragment")
                    .commit();
        });

        mBinding.uiImageDetailPosting.imgTypeVideo.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingVideoFragment.newInstance(), "DetailPostingVideoFragment")
                    .commit();
        });

    }

    private void setupViewPager(ViewPager viewPager) {
        viewPager.setVisibility(VISIBLE);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        // adding filter list fragment
        filtersListFragment = new DetailPostingFiltersListFragment();
        filtersListFragment.setListener(this);

        // adding edit image fragment
        editImageFragment = new EditImageFragment();
        editImageFragment.setListener(this);

        adapter.addFragment(filtersListFragment, "");
        adapter.addFragment(editImageFragment, getString(R.string.tab_edit));

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onFilterSelected(Filter filter) {
        // reset image controls
        showProgressDialog();
        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        resetControls();

        // applying the selected filter
        filteredImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        // preview filtered image
        mBinding.uiImageDetailPosting.imagePreview.setImageBitmap(filter.processFilter(filteredImage));

        finalImage = filteredImage.copy(Bitmap.Config.ARGB_8888, true);
        hideProgressDialog();
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onBrightnessChanged(final int brightness) {
        brightnessFinal = brightness;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightness));
        mBinding.uiImageDetailPosting.imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onSaturationChanged(final float saturation) {
        saturationFinal = saturation;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new SaturationSubfilter(saturation));
        mBinding.uiImageDetailPosting.imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onContrastChanged(final float contrast) {
        contrastFinal = contrast;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new ContrastSubFilter(contrast));
        mBinding.uiImageDetailPosting.imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onEditCompleted() {
        // once the editing is done i.e seekbar is drag is completed,
        // apply the values on to filtered image
        final Bitmap bitmap = filteredImage.copy(Bitmap.Config.ARGB_8888, true);

        Filter myFilter = new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightnessFinal));
        myFilter.addSubFilter(new ContrastSubFilter(contrastFinal));
        myFilter.addSubFilter(new SaturationSubfilter(saturationFinal));
        finalImage = myFilter.processFilter(bitmap);
    }

    private void resetControls() {
        if (editImageFragment != null) {
            editImageFragment.resetControls();
        }
        brightnessFinal = 0;
        saturationFinal = 1.0f;
        contrastFinal = 1.0f;
    }

   /* public boolean validation() {

        boolean value = true;
        if (mBinding.uiImageFilter.edtTitle.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter title", Toast.LENGTH_SHORT).show();
            value = false;
        }
        if (mBinding.uiImageFilter.edtDesc.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter description", Toast.LENGTH_SHORT).show();
            value = false;
        }

        return value;

    }*/

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public Fragment getFragment(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    // load the default image from assets on app launch
    private void loadImage() {
        originalImage = BitmapUtils.getBitmapFromAssets(getActivity(), IMAGE_NAME, 300, 300);
        filteredImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        finalImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        mBinding.uiImageDetailPosting.imagePreview.setImageBitmap(originalImage);
    }

    @Override
    public void onEditStarted() {

    }

    private void saveImageToGallery() {
        Dexter.withActivity(getActivity()).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            mBinding.loading.progressBar.setVisibility(VISIBLE);
                            disableScreen(true);
                            String tagUserID = "";
                            if (selectedPeopleList.size() > 0) {
                                Iterator myVeryOwnIterator = selectedPeopleList.keySet().iterator();
                                while (myVeryOwnIterator.hasNext()) {
                                    String key = (String) myVeryOwnIterator.next();
                                    ConnectPeopleData.DataBean value = selectedPeopleList.get(key);
                                    if (tagUserID.equalsIgnoreCase(""))
                                        tagUserID = "" + value.getUserId();
                                    else
                                        tagUserID = tagUserID + "," + value.getUserId();
                                }
                            }
                            Intent intent = new Intent(activityMain, DetailPostingImageWithTextActivity.class);
                            intent.putExtra("Type", "ImageWithText");
                            intent.putExtra("tagUserID", tagUserID);
                            if (mBinding.uiImageDetailPosting.imgClickie.getVisibility() == VISIBLE) {
                                LayoutToImage layoutToImage = new LayoutToImage(activityMain, mBinding.uiImageDetailPosting.mainImageFrame);
                                bitmap = layoutToImage.convert_layout();
                                Log.e("Free internal memory:", "------>" + getAvailableInternalMemorySize());
                                if (Double.valueOf(getAvailableInternalMemorySize().split(" ")[0]) > 10) {
                                    String clickieImgUri = storeImage(bitmap);
                                    new Handler().postDelayed(() -> {
                                        String originalImgUri = storeImage(finalImage);
                                        intent.putExtra("uploadImgUri", clickieImgUri);
                                        intent.putExtra("originalImgUri", originalImgUri);
                                        mBinding.loading.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                        startActivity(intent);
                                    }, 200);
                                } else {
                                    Toast.makeText(activityMain, "Please free some space to continue.", Toast.LENGTH_LONG).show();
                                }

                            } else {
                                if (Double.valueOf(getAvailableInternalMemorySize().split(" ")[0]) > 10) {
                                    String originalImgUri = storeImage(finalImage);
                                    intent.putExtra("uploadImgUri", originalImgUri);
                                    mBinding.loading.progressBar.setVisibility(View.GONE);
                                    disableScreen(false);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(activityMain, "Please free some space to continue.", Toast.LENGTH_LONG).show();
                                }

                            }
                        } else {
                            Toast.makeText(getActivity(), "Permissions are not granted!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public static String getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSizeLong();
        long availableBlocks = stat.getAvailableBlocksLong();
        return formatSize(availableBlocks * blockSize);
    }

    public static String formatSize(long size) {
        String suffix = null;
        if (size >= 1024) {
            suffix = " KB";
            size /= 1024;
            if (size >= 1024) {
                suffix = " MB";
                size /= 1024;
            }
        }
        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        /*int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
            resultBuffer.insert(commaOffset, ',');
            commaOffset -= 3;
        }*/

        if (suffix != null) resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

    private String storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Toast.makeText(activityMain, "Error creating media file, check storage permissions.", Toast.LENGTH_LONG).show();// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
            return storedFilePath;
//            Toast.makeText(getActivity(), "bitmap store", Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            Log.d("TAG", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("TAG", "Error accessing file: " + e.getMessage());
        }
        return "";
    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getActivity().getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        storedFilePath = mediaStorageDir.getPath() + File.separator + mImageName;
        return mediaFile;
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        Log.e("onactivityResult", "done");
        if (reqCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        }

        if (reqCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {
                    loadImage();
                    mBinding.uiImageDetailPosting.imgSelect.setVisibility(View.GONE);
                    mBinding.uiImageDetailPosting.viewpager.setVisibility(View.VISIBLE);
                    setupViewPager(mBinding.uiImageDetailPosting.viewpager);
                    mBinding.uiImageDetailPosting.tabs.setupWithViewPager(mBinding.uiImageDetailPosting.viewpager);

                    imageData();

                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
                Log.e("Crop error", result.getError().toString());
            }
        }
        if (reqCode == SELECT_CATEGORY && resultCode == RESULT_OK && data != null) {
            //* mBinding.uiBusiness.edtCategory.setText(data.getStringExtra(CATEGORY_NAME));
            // bussinessCategoryId = data.getIntExtra(CATEGORY_ID, 0);*//*
        }
        if (reqCode == 201 && resultCode == RESULT_OK) {
            /*String uri = data.getStringExtra("imageByteArray");
            Uri imageUri = Uri.parse(uri);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), imageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            byte[] byteArray = data.getByteArrayExtra("imageByteArray");
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            mBinding.uiImageDetailPosting.imgClickie.setImageBitmap(bmp);
            mBinding.uiImageDetailPosting.imgClickie.setVisibility(VISIBLE);
        }
        if (reqCode == 601 && resultCode == RESULT_OK) {
            selectedPeopleList = (HashMap<String, ConnectPeopleData.DataBean>) data.getSerializableExtra("peopleList");
            mBinding.uiImageDetailPosting.txtConnectPeople.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_post_connect_people_selected, 0);
        }

    }

    private void callApiSaveData() {
        String title = null, description = null;
        if (postType == 1) {
            title = null;
            description = null;
            mBinding.loading.progressBar.setVisibility(VISIBLE);
            disableScreen(true);
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    uploadPostData(description, title, userId, body)
                            , getCompositeDisposable(), postPhoto, this);
        }
        /*if (postType == 2 || postType == 3) {
            if (validation()) {
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                title = mBinding.uiImageFilter.edtTitle.getText().toString();
                description = mBinding.uiImageFilter.edtDesc.getText().toString();
                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(getActivity()).
                                        create(WebserviceBuilder.class).
                                        uploadPostData(description, title, userId, body)
                                , getCompositeDisposable(), postPhoto, this);
            } else {
                Toast.makeText(getActivity(), "Please Enter Title", Toast.LENGTH_LONG).show();
            }

        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void imageData() {

        String imageUri = (String.valueOf(mImageUri)).split("file://")[1];
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageUri, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 800, 800);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(imageUri, options);

        // clear bitmap memory
        originalImage.recycle();
        finalImage.recycle();
        finalImage.recycle();

        originalImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        filteredImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        finalImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        mBinding.uiImageDetailPosting.imagePreview.setImageBitmap(originalImage);
        bitmap.recycle();

        filtersListFragment.prepareThumbnail(originalImage);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case postPhoto:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                body = null;

                UploadPostData uploadPostData = (UploadPostData) o;
                if (uploadPostData.isSuccess()) {
                    Toast.makeText(getActivity(), uploadPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
//                    PostManagementFragment.managementFragment.callAPI(false);
                  /*  getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.frame_replace, PostManagementFragment.newInstance(), "")
                            .commit();*/
                    Intent intent = new Intent(getActivity(), MenuActivity.class);
                    intent.putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                    getActivity().overridePendingTransition(0, 0);
//                    activityMain.onBackPressed();
                } else {
                    Toast.makeText(getActivity(), uploadPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(activityMain);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Applying filter...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }


    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
