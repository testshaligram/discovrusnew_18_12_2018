package com.danielandshayegan.discovrus.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivitySettingBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.LaunchpadFragment;

public class LaunchpadActivity extends BaseActivity {
    ActivitySettingBinding mBinding;
    LaunchpadFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setting);

        fragment = LaunchpadFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, fragment, "launchpadFragment")
                .commit();
    }

    public void handleClicks(View v) {
        fragment.handleClicks(v);
    }
}