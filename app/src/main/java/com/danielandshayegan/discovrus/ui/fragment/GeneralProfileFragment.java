package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentGeneralProfileBinding;
import com.danielandshayegan.discovrus.models.UserProfileData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.SplashActivity;
import com.danielandshayegan.discovrus.utils.Utils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getCategoryList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GeneralProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GeneralProfileFragment extends BaseFragment implements SingleCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentGeneralProfileBinding mBinding;
    public int userId, logInUserId;
    int currentPagePost = 1, pageSize = 1;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;


    public GeneralProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GeneralProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GeneralProfileFragment newInstance() {
        GeneralProfileFragment fragment = new GeneralProfileFragment();
      /*  Bundle args = new Bundle();
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_general_profile, container, false);
        mBinding.uiSetting.toolbar.txtTitle.setText(R.string.str_profile);
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        //   userId = getArguments().getInt("userId");
        logInUserId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();

        mBinding.uiSetting.toolbar.imgBack.setOnClickListener(view -> {
            getActivity().finish();
        });
        getProfileData();

        return mBinding.getRoot();
    }

    private void getProfileData() {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void callAPI() {
        disposable.add(apiService.getProfile(logInUserId, logInUserId, currentPagePost, pageSize).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<UserProfileData>() {
                    @Override
                    public void onSuccess(UserProfileData value) {
                        if (value.isSuccess()) {
                            mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            UserProfileData.DataBean profileData = value.getData().get(0);
                            setData(profileData);

//                            disableScreen(false);
                        } else {
                            mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    private void setData(UserProfileData.DataBean profileData) {
        mBinding.uiSetting.txtName.setText(profileData.getUserName());
        if (profileData.getUserType().equals("Business")) {
            mBinding.uiSetting.txtBusinessNameContent.setText(profileData.getBusinessName());
        } else {
            if (profileData.getWorkingAt() != null)
                mBinding.uiSetting.txtBusinessNameContent.setText(profileData.getWorkingAt());

        }
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_placeholder)
                .error(R.drawable.user_placeholder);

        Glide.with(mContext)
                .load(ApiClient.WebService.imageUrl + profileData.getUserImagePath())
                .apply(options)
                .into(mBinding.uiSetting.imgProfile);

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }
}
