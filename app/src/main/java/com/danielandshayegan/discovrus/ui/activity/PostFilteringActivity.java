package com.danielandshayegan.discovrus.ui.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.system.ErrnoException;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.utils.FileUtils;
import com.danielandshayegan.discovrus.databinding.ActivityPostFilteringBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.ImageFilterFragment;
import com.danielandshayegan.discovrus.ui.fragment.TextPostFragment;
import com.danielandshayegan.discovrus.ui.fragment.VideoFilterFrgament;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;
import static com.danielandshayegan.discovrus.ui.fragment.RegisterFragment.SELECT_CATEGORY;

public class PostFilteringActivity extends BaseActivity {


    ActivityPostFilteringBinding mBinding;
    Animator translationAnimator;
    int fragmentView;
    String fragmentName;
    private static final int REQUEST_VIDEO_TRIMMER = 0x01;
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    static final String VIDEO_TOTAL_DURATION = "VIDEO_TOTAL_DURATION";
    Bundle bundle;
    public Uri mImageUri;
    File fileImage;
    public static String videoUri;
    int postType;
    public static boolean isFromFilterFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_post_filtering);
        bottomSheetClick();
        defaultViewTranslation();
        mBinding.uiNavigation.discovrusNews.setImageDrawable(getResources().getDrawable(R.drawable.newsfeed_selected));
        Intent intent = getIntent();
        if (null != intent) {


            fragmentName = intent.getExtras().getString("filterFragment");
            bundle = intent.getExtras();
            /*if (bundle != null) {
                bundle = bundle.getParcelable("postDetails");
            }*/
            switch (fragmentName) {
                case "imageFilter":
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_replace, ImageFilterFragment.newInstance(bundle), "")
                            .commit();

                    break;
                case "videoFilter":
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fragment_replace, VideoFilterFrgament.newInstance(bundle), "")
                            .commit();
                    break;

                case "textAddingPost":
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, TextPostFragment.newInstance(bundle), "TextPostFragment")
                            .commit();

                    break;

            }

        }
        bottomMenuClick();


    }

    public void bottomMenuClick() {
        mBinding.uiNavigation.discovrusChat.setOnClickListener(v -> {
            /*getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, ChatFragment.newInstance(), "")
                    .commit();*/

            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "chatFragment");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);


        });

        mBinding.uiNavigation.discovrusNavigation.setOnClickListener(v -> {
         /*   getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, FragmentNavigation.newInstance(), "")
                    .commit();*/

            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "navigationFragment");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });


        mBinding.uiNavigation.discovrusProfile.setOnClickListener(v -> {
           /* getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, ProfileFragment.newInstance(), "")
                    .commit();*/

            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "profileFragment");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });

        /* mBinding.uiNavigation.discovrusNews.setOnClickListener(v -> {
         *//*  getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, PostManagementFragment.newInstance(), "")
                    .commit();*//*

            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra("menuActivityFragment", "chatFragment");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            overridePendingTransition(0,0);

        });*/

        mBinding.btnAddPost.setOnClickListener(v -> {

            if (mBinding.uiNavigation.constraintBottomMenu.getVisibility() == View.GONE) {
                openBottomMenu();
                mBinding.btnAddPost.setRotation(45.0f);
            } else {
                closeBottomMenu();
                mBinding.btnAddPost.setRotation(0.0f);
            }

        });


    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        Log.e("onactivityResult", "done");
        if (reqCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        } else if (reqCode == REQUEST_VIDEO_TRIMMER && resultCode == RESULT_OK) {
            final Uri selectedUri = data.getData();
            if (selectedUri != null) {
                videoUri = String.valueOf(selectedUri);
                startTrimActivity(selectedUri);
            } else {
                Toast.makeText(PostFilteringActivity.this, R.string.toast_cannot_retrieve_selected_video, Toast.LENGTH_SHORT).show();
            }
        } else if (reqCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {
                    if (isFromFilterFragment) {
                        ImageFilterFragment.imageFilterFragment.mImageUri = mImageUri;
                        ImageFilterFragment.imageFilterFragment.imageData();
                    } else {
                        closeBottomMenu();
                        mBinding.btnAddPost.setRotation(0.0f);

                        int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
                        translationAnimator = ObjectAnimator
                                .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                                .setDuration(400);
                        translationAnimator.start();
                        translationAnimator.addListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);

                                Bundle bundle = new Bundle();
                                bundle.putInt("postType", postType);
                                bundle.putString("imageUri", String.valueOf(mImageUri));
                                bundle.putString("videoUri", "");

                                getSupportFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                        .replace(R.id.fragment_replace, ImageFilterFragment.newInstance(bundle), "ImageFilterFragment")
                                        .commit();

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        });

                    }
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    // showError("Cropping failed: " + result.getError());
                }
            }
        } else if (reqCode == SELECT_CATEGORY && resultCode == RESULT_OK && data != null) {
            //* mBinding.uiBusiness.edtCategory.setText(data.getStringExtra(CATEGORY_NAME));
            // bussinessCategoryId = data.getIntExtra(CATEGORY_ID, 0);*//*
        } else if (reqCode == 3000 && resultCode == RESULT_OK && data != null) {
//            closeBottomMenu();
            mBinding.btnAddPost.setRotation(0.0f);

            int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                    .setDuration(400);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
                    Bundle bundle = new Bundle();
                    bundle.putInt("postType", 4);
                    bundle.putString("imageUri", "");
                    bundle.putString("videoUri", VideoTrimActivity.VIDEO_URL);

                   /* Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);
                    if (f instanceof ImageFilterFragment) {
                        getSupportFragmentManager().popBackStack();
                    } else if (f instanceof VideoFilterFrgament) {
                        getSupportFragmentManager().popBackStack();
                    } else if (f instanceof TextPostFragment) {
                        getSupportFragmentManager().popBackStack();
                    }*/

                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, VideoFilterFrgament.newInstance(bundle), "VideoFilterFrgament")
                            .commit();

//                    fragmentView = 2;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });


        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);
            if (fragment != null) {
                fragment.onActivityResult(reqCode, resultCode, data);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void bottomSheetClick() {
        mBinding.uiNavigation.uiBottomsheet.imgCamera.setOnClickListener(v -> {
            Log.e("camera Clicked", "-----------------");
            imgPick();
            //  openImageFromGallery();
            postType = 1;

        });
        mBinding.uiNavigation.uiBottomsheet.imgCamText.setOnClickListener(v -> {
            Log.e("text image Clicked", "-----------------");
            imgPick();
            // openImageFromGallery();
            postType = 2;

        });
        mBinding.uiNavigation.uiBottomsheet.imgVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("video Clicked", "-----------------");
                postType = 4;

                if (Build.VERSION.SDK_INT >= 23)
                    getPermission();
                else
                    showDialogForVideo();
            }
        });
        mBinding.uiNavigation.uiBottomsheet.imgText.setOnClickListener(v -> {
            Log.e("text Clicked", "-----------------");
            postType = 3;
//            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBinding.btnAddPost.setRotation(0.0f);
            int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                    .setDuration(400);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
                    Bundle bundle = new Bundle();
                    bundle.putInt("postType", postType);
                    bundle.putString("imageUri", "");
                    bundle.putString("videoUri", "");

                    Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);

                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, TextPostFragment.newInstance(bundle), "TextPostFragment")
//                        .addToBackStack(null)
                            .commit();
//            }
//            fragmentView = 2;

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

        });
    }

    private void getPermission() {
        String[] params = null;
        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;

        int hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(this, writeExternalStorage);
        int hasReadExternalStoragePermission = ActivityCompat.checkSelfPermission(this, readExternalStorage);
        List<String> permissions = new ArrayList<String>();

        if (hasWriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(writeExternalStorage);
        if (hasReadExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(readExternalStorage);

        if (!permissions.isEmpty()) {
            params = permissions.toArray(new String[permissions.size()]);
        }
        if (params != null && params.length > 0) {
            ActivityCompat.requestPermissions(PostFilteringActivity.this,
                    params,
                    100);
        } else
            showDialogForVideo();
    }

    private void showDialogForVideo() {

        new AlertDialog.Builder(PostFilteringActivity.this)
                .setTitle("Select video")
                .setMessage("Please choose video selection type")
                .setPositiveButton("CAMERA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        openVideoCapture();
                    }
                })
                .setNegativeButton("GALLERY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        pickFromGallery();
                    }
                })
                .show();
    }

    private void openVideoCapture() {
        Intent videoCapture = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(videoCapture, REQUEST_VIDEO_TRIMMER);
    }

    private void pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getString(R.string.permission_read_storage_rationale), REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {
            Intent intent = new Intent();
            intent.setTypeAndNormalize("video/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_video)), REQUEST_VIDEO_TRIMMER);
        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.permission_title_rationale));
            builder.setMessage(rationale);
            builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(PostFilteringActivity.this, new String[]{permission}, requestCode);
                }
            });
            builder.setNegativeButton(getString(R.string.cancel), null);
            builder.show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void closeBottomMenu() {
        int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
        translationAnimator = ObjectAnimator
                .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                .setDuration(400);
        translationAnimator.start();
        translationAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    public void openBottomMenu() {
        mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.VISIBLE);
        int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
        translationAnimator = ObjectAnimator
                .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, height, 0f)
                .setDuration(400);
        translationAnimator.start();
    }

    private void defaultViewTranslation() {
        if (mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight() == 0) {
            mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.INVISIBLE);
            ViewTreeObserver viewTreeObserverList = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getViewTreeObserver();
            viewTreeObserverList.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int width = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getMeasuredWidth();
                    int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getMeasuredHeight();
                    mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
                    translationAnimator = ObjectAnimator
                            .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                            .setDuration(0);
                    translationAnimator.start();
                }
            });
        } else {
            mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight())
                    .setDuration(0);
            translationAnimator.start();
        }
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    private void startTrimActivity(@NonNull Uri uri) {
        Intent intent = new Intent(this, VideoTrimActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATH, FileUtils.getPath(this, uri));
        intent.putExtra(VIDEO_TOTAL_DURATION, getMediaDuration(uri));
        startActivityForResult(intent, 3000);
    }


    private int getMediaDuration(Uri uriOfFile) {
        MediaPlayer mp = MediaPlayer.create(this, uriOfFile);
        int duration = mp.getDuration();
        return duration;
    }


}
