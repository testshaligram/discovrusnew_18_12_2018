package com.danielandshayegan.discovrus.ui.profile;


import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.BusinessProfileReviewAdapter;
import com.danielandshayegan.discovrus.adapter.ProfileHighlightsAdapter;
import com.danielandshayegan.discovrus.adapter.ProfileNewsFeedAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentProfileBinding;
import com.danielandshayegan.discovrus.enums.NewsFeedTypeEnum;
import com.danielandshayegan.discovrus.models.ChartDataList;
import com.danielandshayegan.discovrus.models.NewsFeedData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.ReferenceListByUserId;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.MyMarkerView;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.webservice.APIs;
import com.danielandshayegan.discovrus.webservice.JSONCallback;
import com.danielandshayegan.discovrus.webservice.Retrofit;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener, OnChartGestureListener, OnChartValueSelectedListener {

    FragmentProfileBinding mBinding;
    private ArrayList<NewsFeedData> newsFeedList = new ArrayList<>();
    private ArrayList<ChartDataList.DataBean> chartDataList = new ArrayList<>();
    private ArrayList<ReferenceListByUserId.DataBean> referencesList = new ArrayList<>();
    List<PostListData.PostList> highlightList = new ArrayList<>();
    private String typeDetail = "posts";
    private String monthIndex = "";
    private List<String> tempMonthList = new ArrayList<>();
    private String UserID = "2", LoginID = "2", Type = "i";
    private int PageNumber = 1, PageSize = 10;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    public static ProfileFragment newInstance(Bundle extras) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_profile, container, false);
        mappingWidgets();
        mBinding.swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        mBinding.swipeRefreshLayout.setOnRefreshListener(() -> {
            setSelectorView(typeDetail);
        });
        return mBinding.getRoot();
    }

    private void setViewVisible() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM yy");
        mBinding.uiProfileFragment.txtMonthThree.setText(sdf.format(calendar.getTime()));
        calendar.add(Calendar.MONTH, -1);
        mBinding.uiProfileFragment.txtMonthTwo.setText(sdf.format(calendar.getTime()));
        calendar.add(Calendar.MONTH, -1);
        mBinding.uiProfileFragment.txtMonthOne.setText(sdf.format(calendar.getTime()));
    }

    private void setClickListener() {
        mBinding.uiProfileFragment.imgBack.setOnClickListener(this);
        mBinding.uiProfileFragment.imgFollow.setOnClickListener(this);
        mBinding.uiProfileFragment.imgMessage.setOnClickListener(this);
        mBinding.uiProfileFragment.imgLocation.setOnClickListener(this);
    }

    private void mappingWidgets() {
        //  ((BaseActivityNew) mActivity).baseBinding.mToolbar.setVisibility(View.GONE);//   TODO: need to change when include in other activity
        mBinding.uiProfileFragment.tabLayout.removeAllTabs();
        mBinding.uiProfileFragment.tabLayout.addTab(mBinding.uiProfileFragment.tabLayout.newTab().setText("Posts"));
        mBinding.uiProfileFragment.tabLayout.addTab(mBinding.uiProfileFragment.tabLayout.newTab().setText("References"));
        mBinding.uiProfileFragment.tabLayout.addTab(mBinding.uiProfileFragment.tabLayout.newTab().setText("Highlights"));
        mBinding.uiProfileFragment.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setSelectorView(tab.getText().toString());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        getNewsFeedData();
        getChartData();
        setClickListener();
        setViewVisible();
    }


    private void setSelectorView(String title) {
        typeDetail = title;
        switch (title) {
            case "Posts":
                mBinding.uiProfileFragment.selector1.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.chart_blue_selected));
                mBinding.uiProfileFragment.selector2.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
                mBinding.uiProfileFragment.selector3.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
                getNewsFeedData();
                break;
            case "References":
                mBinding.uiProfileFragment.selector1.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
                mBinding.uiProfileFragment.selector2.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.chart_blue_selected));
                mBinding.uiProfileFragment.selector3.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
                getReferenceData();
                break;
            case "Highlights":
                mBinding.uiProfileFragment.selector1.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
                mBinding.uiProfileFragment.selector2.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.colorAccent));
                mBinding.uiProfileFragment.selector3.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.chart_blue_selected));
                callHighlightsAPI();
                break;
        }
    }

    private void getNewsFeedData() {
        try {
            Utils.showProgressBar(me);
            mBinding.uiProfileFragment.rvNewsFeed.showShimmerAdapter();

            HashMap<String, String> params = new HashMap<>();
            params.put("UserID", UserID); //   TODO: change static id
            params.put("LoginID", LoginID); //   TODO: change static id
            params.put("PageNumber", "" + PageNumber);//   TODO: change
            params.put("PageSize", "" + PageSize);
            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.GET_PROFILE)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Utils.dismissProgressBar();
                                mBinding.uiProfileFragment.rvNewsFeed.hideShimmerAdapter();
                                mBinding.swipeRefreshLayout.setRefreshing(false);
                            }

                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                Utils.dismissProgressBar();
                                mBinding.swipeRefreshLayout.setRefreshing(false);
                                mBinding.uiProfileFragment.rvNewsFeed.hideShimmerAdapter();
                                Gson gson = new Gson();
                                newsFeedList = gson.fromJson(jsonObject.getJSONArray("Data").toString(),
                                        new TypeToken<ArrayList<NewsFeedData>>() {
                                        }.getType());

                                for (int i = 0; i < newsFeedList.size(); i++) {
                                    for (int j = 0; j < newsFeedList.get(i).getPostData().size(); j++) {
                                        newsFeedList.get(i).getPostData().get(j).setNewsFeedTypeEnum(NewsFeedTypeEnum.valueOf(newsFeedList.get(i).getPostData().get(j).getType()).getIntType());
                                    }
                                }
                                GridLayoutManager gridLayoutManager = new GridLayoutManager(me, 2);
                                mBinding.uiProfileFragment.rvNewsFeed.setLayoutManager(gridLayoutManager);
                                mBinding.uiProfileFragment.rvNewsFeed.setAdapter(new ProfileNewsFeedAdapter(me, newsFeedList.get(0)));

                                mBinding.setItem(newsFeedList.get(0));
                                mBinding.uiProfileFragment.setItem(newsFeedList.get(0));

                                if (newsFeedList.get(0).getUserType().equalsIgnoreCase("Individual")) {
                                    mBinding.uiProfileFragment.imgCopy.setVisibility(View.VISIBLE);
                                    mBinding.uiProfileFragment.txtUserAddress.setText(newsFeedList.get(0).getWorkingAt());
                                    mBinding.uiProfileFragment.txtUserName.setText(newsFeedList.get(0).getUserName());
                                } else {
                                    mBinding.uiProfileFragment.imgCopy.setVisibility(View.GONE);
                                    mBinding.uiProfileFragment.txtUserAddress.setText(newsFeedList.get(0).getLocation());
                                    mBinding.uiProfileFragment.txtUserName.setText(newsFeedList.get(0).getBusinessName());
                                }
                                Toast.makeText(me, "SUCCESS GOT == " + newsFeedList.size(), Toast.LENGTH_LONG).show();
                            }
                        });
            } catch (Exception e) {
                Utils.dismissProgressBar();
                mBinding.uiProfileFragment.rvNewsFeed.hideShimmerAdapter();
                mBinding.swipeRefreshLayout.setRefreshing(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Utils.dismissProgressBar();
            mBinding.uiProfileFragment.rvNewsFeed.hideShimmerAdapter();
            mBinding.swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void getChartData() {
        mBinding.uiProfileFragment.mChart.clear();
        try {
            Utils.showProgressBar(me);

            HashMap<String, String> params = new HashMap<>();
            params.put("UserID", UserID); //   TODO: change static id
            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.GET_CHART_LIST)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                Utils.dismissProgressBar();
                            }

                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                Utils.dismissProgressBar();
                                Gson gson = new Gson();
                                chartDataList = gson.fromJson(jsonObject.getJSONArray("Data").toString(),
                                        new TypeToken<ArrayList<ChartDataList.DataBean>>() {
                                        }.getType());
                                setUpChart();
                            }
                        });
            } catch (Exception e) {
                Utils.dismissProgressBar();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getReferenceData() {
        try {
            showProgress();

            HashMap<String, String> params = new HashMap<>();
            params.put("UserID", UserID); //   TODO: change static id
            params.put("LoginID", LoginID); //   TODO: change static id
            params.put("Type", Type); //   TODO: change static id
            params.put("PageNumber", "" + PageNumber);//   TODO: change
            params.put("PageSize", "" + PageSize);
            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.GET_REFERENCES_USERID)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                hideProgress();
                            }

                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                hideProgress();
                                Gson gson = new Gson();
                                referencesList = gson.fromJson(jsonObject.getJSONArray("Data").toString(),
                                        new TypeToken<ArrayList<ReferenceListByUserId.DataBean>>() {
                                        }.getType());


                                LinearLayoutManager layoutManagerReference = new LinearLayoutManager(mContext);
                                layoutManagerReference.setOrientation(LinearLayoutManager.VERTICAL);
                                mBinding.uiProfileFragment.rvNewsFeed.setLayoutManager(layoutManagerReference);
                                BusinessProfileReviewAdapter adapter = new BusinessProfileReviewAdapter(mActivity);
                                adapter.addAll(referencesList);
                                mBinding.uiProfileFragment.rvNewsFeed.setAdapter(adapter);
                                Toast.makeText(me, "SUCCESS GOT == " + referencesList.size(), Toast.LENGTH_LONG).show();
                            }
                        });
            } catch (Exception e) {
                hideProgress();
            }
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    private void callHighlightsAPI() {
        try {
            showProgress();

            HashMap<String, String> params = new HashMap<>();
            params.put("UserID", UserID); //   TODO: change static id
            params.put("LoginID", LoginID); //   TODO: change static id
            params.put("Type", Type); //   TODO: change static id
            params.put("PageNumber", "" + PageNumber);//   TODO: change
            params.put("PageSize", "" + PageSize);
            Log.e("HashMap Params", params.toString());

            try {
                Retrofit.with(me)
                        .setAPI(APIs.GET_HIGHLIGHTED_POST)
                        .setGetParameters(params)
                        .setCallBackListener(new JSONCallback(me) {
                            @Override
                            protected void onFailed(int statusCode, String message) {
                                hideProgress();
                            }

                            @Override
                            protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                hideProgress();
                                Gson gson = new Gson();
                                highlightList = gson.fromJson(jsonObject.getJSONArray("Data").toString(),
                                        new TypeToken<ArrayList<PostListData.PostList>>() {
                                        }.getType());


                                LinearLayoutManager layoutManagerReference = new LinearLayoutManager(mContext);
                                layoutManagerReference.setOrientation(LinearLayoutManager.VERTICAL);
                                mBinding.uiProfileFragment.rvNewsFeed.setLayoutManager(layoutManagerReference);
                                ProfileHighlightsAdapter adapter = new ProfileHighlightsAdapter(mActivity);
                                adapter.addAll(highlightList.get(0).getPost());
                                mBinding.uiProfileFragment.rvNewsFeed.setAdapter(adapter);
                                Toast.makeText(me, "SUCCESS GOT == " + highlightList.size(), Toast.LENGTH_LONG).show();
                            }
                        });
            } catch (Exception e) {
                hideProgress();
            }
        } catch (Exception e) {
            e.printStackTrace();
            hideProgress();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void showProgress() {
        Utils.showProgressBar(me);
        mBinding.uiProfileFragment.rvNewsFeed.showShimmerAdapter();
    }

    private void hideProgress() {
        Utils.dismissProgressBar();
        mBinding.uiProfileFragment.rvNewsFeed.hideShimmerAdapter();
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgBack:
                break;
            case R.id.imgFollow:
                break;
            case R.id.imgMessage:
                break;
            case R.id.imgLocation:
                break;
        }
    }

    private void setUpChart() {
        mBinding.uiProfileFragment.mChart.setOnChartGestureListener(this);
        mBinding.uiProfileFragment.mChart.setOnChartValueSelectedListener(this);
        mBinding.uiProfileFragment.mChart.setDrawGridBackground(false);
        // no description text
        mBinding.uiProfileFragment.mChart.getDescription().setEnabled(false);
        // enable touch gestures
        mBinding.uiProfileFragment.mChart.setTouchEnabled(true);
        // enable scaling and dragging
        mBinding.uiProfileFragment.mChart.setDragEnabled(true);
        mBinding.uiProfileFragment.mChart.setScaleEnabled(true);
        mBinding.uiProfileFragment.mChart.setNoDataTextColor(ContextCompat.getColor(mContext, R.color.white));
        // if disabled, scaling can be done on x- and y-axis separately
        mBinding.uiProfileFragment.mChart.setPinchZoom(true);
        Typeface tf = ResourcesCompat.getFont(mActivity, R.font.avenir_next_medium);
        // set an alternative background color
        // mBinding.mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
        mv.setChartView(mBinding.uiProfileFragment.mChart); // For bounds control
        mBinding.uiProfileFragment.mChart.setMarker(mv); // Set the marker to the chart

        XAxis xAxis = mBinding.uiProfileFragment.mChart.getXAxis();
        xAxis.setTextColor(ContextCompat.getColor(mContext, R.color.black_translucent));
        xAxis.setAxisLineColor(ContextCompat.getColor(mContext, R.color.white));
        xAxis.setGridColor(ContextCompat.getColor(mContext, R.color.white_opacity_25));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawLabels(false);
        xAxis.setTypeface(tf);
        YAxis leftAxis = mBinding.uiProfileFragment.mChart.getAxisLeft();
        leftAxis.setTextColor(ContextCompat.getColor(mContext, R.color.black_translucent));
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
        leftAxis.setAxisMaximum(500f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setTypeface(tf);
        leftAxis.setDrawLabels(false);
        leftAxis.enableGridDashedLine(0f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setGridColor(ContextCompat.getColor(mContext, R.color.white_opacity_25));
        leftAxis.setAxisLineColor(ContextCompat.getColor(mContext, R.color.white_opacity_25));
        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(false);
        mBinding.uiProfileFragment.mChart.getAxisRight().setEnabled(false);
        setData(60, chartDataList);


    }

    private void setData(int count, List<ChartDataList.DataBean> dataList) {

        monthIndex = "";
        ArrayList<Entry> values = new ArrayList<Entry>();
        String month = "";
        String targetDateValue = "";
        if (dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                ChartDataList.DataBean data = dataList.get(i);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                Date sourceDate = null;
                try {
                    sourceDate = dateFormat.parse(data.getDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                @SuppressLint("SimpleDateFormat") SimpleDateFormat targetFormat = new SimpleDateFormat("MMM yy");
                targetDateValue = targetFormat.format(sourceDate);

                float val = data.getViewCount();
                if (i == 0) {
                    month = targetDateValue.split(" ")[0];
                    tempMonthList.add(targetDateValue);
                    values.add(new Entry(i, val, getResources().getDrawable(R.drawable.ic_chart_marker)));
                    monthIndex = String.valueOf(i);

                } else if (!month.equalsIgnoreCase(targetDateValue.split(" ")[0])) {
                    month = targetDateValue.split(" ")[0];
                    tempMonthList.add(targetDateValue);
                    values.add(new Entry(i, val, getResources().getDrawable(R.drawable.ic_chart_marker)));
                    monthIndex = String.format("%s,%d", monthIndex, i);
                } else
                    values.add(new Entry(i, val, null));
            }
            if (tempMonthList.size() == 1) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yy");
                Date sourceDate = null;
                try {
                    sourceDate = dateFormat.parse(targetDateValue);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                @SuppressLint("SimpleDateFormat") SimpleDateFormat targetFormat = new SimpleDateFormat("MM yyyy");
                String dateValue = targetFormat.format(sourceDate);
                Calendar date = Calendar.getInstance();
                date.set(Integer.valueOf(dateValue.split(" ")[1]), Integer.valueOf(dateValue.split(" ")[0]) - 1, 1);
                date.add(Calendar.MONTH, 1);

                targetDateValue = dateFormat.format(date.getTime());
                tempMonthList.add(targetDateValue);
                values.add(new Entry(1, 0, getResources().getDrawable(R.drawable.ic_chart_marker)));
                monthIndex = monthIndex + "," + 1;

                date.add(Calendar.MONTH, 1);
                targetDateValue = dateFormat.format(date.getTime());
                tempMonthList.add(targetDateValue);
                values.add(new Entry(2, 0, getResources().getDrawable(R.drawable.ic_chart_marker)));
                monthIndex = monthIndex + "," + 2;
            } else if (tempMonthList.size() == 2) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yy");
                Date sourceDate = null;
                try {
                    sourceDate = dateFormat.parse(targetDateValue);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                @SuppressLint("SimpleDateFormat") SimpleDateFormat targetFormat = new SimpleDateFormat("MM yyyy");
                String dateValue = targetFormat.format(sourceDate);
                Calendar date = Calendar.getInstance();
                date.set(Integer.valueOf(dateValue.split(" ")[1]), Integer.valueOf(dateValue.split(" ")[0]) - 1, 1);
                date.add(Calendar.MONTH, 1);

                targetDateValue = dateFormat.format(date.getTime());
                tempMonthList.add(targetDateValue);
                values.add(new Entry(2, 0, getResources().getDrawable(R.drawable.ic_chart_marker)));
                monthIndex = monthIndex + "," + 2;
            }
        } else {
            for (int i = 0; i < count; i++) {

//                float val = (float) (Math.random() * range) + 3;
                float val = 0;
                if (i == 0 || i == 30 || i == 59) {
                    values.add(new Entry(i, val, getResources().getDrawable(R.drawable.ic_chart_marker)));
                    if (monthIndex.equalsIgnoreCase(""))
                        monthIndex = String.valueOf(i);
                    else
                        monthIndex = String.format("%s,%d", monthIndex, i);
                    tempMonthList.add("");
                } else
                    values.add(new Entry(i, val, null));
            }
        }

        mBinding.uiProfileFragment.txtMonthOne.setText(tempMonthList.get(0));
        mBinding.uiProfileFragment.txtMonthTwo.setText(tempMonthList.get(1));
        mBinding.uiProfileFragment.txtMonthThree.setText(tempMonthList.get(2));

        LineDataSet set1;

        if (mBinding.uiProfileFragment.mChart.getData() != null &&
                mBinding.uiProfileFragment.mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mBinding.uiProfileFragment.mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);

            mBinding.uiProfileFragment.mChart.getData().notifyDataChanged();
            mBinding.uiProfileFragment.mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.4f);
            set1.setDrawIcons(true);
            set1.setValueTextColor(Color.WHITE);
            set1.setHighlightEnabled(true);
            set1.setDrawHighlightIndicators(false);


            set1.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

            set1.setCircleColor(Color.TRANSPARENT);
            set1.setLineWidth(2f);
            set1.setCircleRadius(2f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(0f);
            set1.setDrawFilled(false);
            set1.setFormLineWidth(1.5f);
            set1.setFormSize(15.f);


            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            mBinding.uiProfileFragment.mChart.setData(data);
            mBinding.uiProfileFragment.mChart.invalidate();


            mBinding.uiProfileFragment.mChart.animateX(2500);

            // get the legend (only possible after setting data)
            Legend l = mBinding.uiProfileFragment.mChart.getLegend();

            // modify the legend ...
            l.setForm(Legend.LegendForm.LINE);
            l.setEnabled(false);
        }
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            mBinding.uiProfileFragment.mChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }

    @Override
    public void onStart() {
        super.onStart();
        getView().post(() -> setupGradient(mBinding.uiProfileFragment.mChart));
    }

    private void setupGradient(LineChart mChart) {
        Paint paint = mChart.getRenderer().getPaintRender();
        int height = mChart.getHeight();

        LinearGradient linGrad = new LinearGradient(0, 0, 0, height,
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorSecondary),
                Shader.TileMode.REPEAT);
        paint.setShader(linGrad);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.e("Entry selected", e.toString());
        String[] splits = monthIndex.split(",");
        String selectedMonth = "";
        if (e.getX() < Float.valueOf(splits[1])) {
            Log.e("month selected", mBinding.uiProfileFragment.txtMonthOne.getText().toString());
            selectedMonth = mBinding.uiProfileFragment.txtMonthOne.getText().toString();
        } else if (e.getX() < Float.valueOf(splits[2])) {
            Log.e("month selected", mBinding.uiProfileFragment.txtMonthTwo.getText().toString());
            selectedMonth = mBinding.uiProfileFragment.txtMonthTwo.getText().toString();
        } else if (e.getX() == Float.valueOf(splits[2])) {
            Log.e("month selected", mBinding.uiProfileFragment.txtMonthThree.getText().toString());
            selectedMonth = mBinding.uiProfileFragment.txtMonthThree.getText().toString();
        }
        Log.i("LOWHIGH", "low: " + mBinding.uiProfileFragment.mChart.getLowestVisibleX() + ", high: " + mBinding.uiProfileFragment.mChart.getHighestVisibleX());
        Log.i("MIN MAX", "xmin: " + mBinding.uiProfileFragment.mChart.getXChartMin() + ", xmax: " + mBinding.uiProfileFragment.mChart.getXChartMax() + ", ymin: " + mBinding.uiProfileFragment.mChart.getYChartMin() + ", ymax: " + mBinding.uiProfileFragment.mChart.getYChartMax());
        if (newsFeedList.get(0) != null) {
            int sizeOfPostData = newsFeedList.get(0).getPostData().size();
            if (sizeOfPostData > 0) {
                for (int i = 0; i < sizeOfPostData - 1; i++) {
                    String monthOfData = newsFeedList.get(0).getPostData().get(i).getMonth();
                    if (monthOfData.equalsIgnoreCase(selectedMonth)) {
                        float yFirst = mBinding.uiProfileFragment.rvNewsFeed.getY() + mBinding.uiProfileFragment.rvNewsFeed.getChildAt(0).getY();
                        mBinding.uiProfileFragment.nestedScrollView.smoothScrollTo(0, (int) yFirst);
                        float y = mBinding.uiProfileFragment.rvNewsFeed.getY() + mBinding.uiProfileFragment.rvNewsFeed.getChildAt(i).getY();
                        mBinding.uiProfileFragment.nestedScrollView.smoothScrollTo(0, (int) y);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void onNothingSelected() {

    }
}
