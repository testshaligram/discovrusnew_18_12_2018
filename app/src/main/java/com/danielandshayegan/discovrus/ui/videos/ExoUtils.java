package com.danielandshayegan.discovrus.ui.videos;

import android.app.Activity;
import android.content.Context;

import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;

import java.io.File;

import im.ene.toro.exoplayer.Config;
import im.ene.toro.exoplayer.ExoCreator;
import im.ene.toro.exoplayer.MediaSourceBuilder;
import im.ene.toro.exoplayer.ToroExo;

/**
 * Created by cisner-2 on 20/2/18.
 */

public class ExoUtils {
    public static ExoCreator exoCreator;
    private static CacheDataSourceFactory cacheDataSourceFactory;
    private static long catchFile = 100 * 1024 * 1024;
    private static SimpleCache cache;

   /* public static CacheDataSourceFactory getCacheData(Activity mActivity) {
        if (cacheDataSourceFactory == null)
            cacheDataSourceFactory = new CacheDataSourceFactory(mActivity, 100 * 1024 * 1024, 5 * 1024 * 1024);
        return cacheDataSourceFactory;
    }
*/
    public static ExoCreator createExo(Context context) {
        if (exoCreator == null) {
            cache = new SimpleCache(new File(context.getCacheDir(), "discovrus_video"), new LeastRecentlyUsedCacheEvictor(catchFile));
            Config.Builder config2 = new Config.Builder();
            config2.setMediaSourceBuilder(MediaSourceBuilder.DEFAULT);
            config2.setCache(cache);
            exoCreator = ToroExo.with(context).getCreator(config2.build());
            return exoCreator;
        }
        else
            return exoCreator;
    }
}