package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.applozic.mobicomkit.uiwidgets.people.contact.DeviceContactSyncService;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.IndividualProfileHighlightsAdapter;
import com.danielandshayegan.discovrus.adapter.IndividualReferencesAdapter;
import com.danielandshayegan.discovrus.adapter.ProfilePostGridAdapter;
import com.danielandshayegan.discovrus.adapter.WorkingDataAdapter;
import com.danielandshayegan.discovrus.custome_veiws.OnSwipeTouchListener;
import com.danielandshayegan.discovrus.databinding.FragmentBusinessProfileBinding;
import com.danielandshayegan.discovrus.dialog.LogoutDialog;
import com.danielandshayegan.discovrus.models.ChartDataList;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.FollowPeopleData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.RefenrenceModel;
import com.danielandshayegan.discovrus.models.ReferenceListByUserId;
import com.danielandshayegan.discovrus.models.SaveLikeData;
import com.danielandshayegan.discovrus.models.SavePost;
import com.danielandshayegan.discovrus.models.SavePostViewData;
import com.danielandshayegan.discovrus.models.SaveReferenceData;
import com.danielandshayegan.discovrus.models.UserProfileData;
import com.danielandshayegan.discovrus.models.WorkingAtData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailActivity;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailVideoActivity;
import com.danielandshayegan.discovrus.ui.activity.FollowingActivity;
import com.danielandshayegan.discovrus.utils.MyMarkerView;
import com.danielandshayegan.discovrus.utils.Utils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deletePost;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.followPeople;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.referenceDataList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveLike;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePostView;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveReferenceData;

public class IndividualProfileFragment extends BaseFragment implements OnMapReadyCallback, OnChartGestureListener, OnChartValueSelectedListener, SingleCallback {

    FragmentBusinessProfileBinding mBinding;
    View view;
    private WebserviceBuilder apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    public int userId, logInUserId;
    public static IndividualProfileFragment individualProfileFragment;
    private GoogleMap map;
    List<ReferenceListByUserId.DataBean> referenceList = new ArrayList<>();
    List<PostListData.Post> postDataList = new ArrayList<>();
    List<PostListData.Post> highlightList = new ArrayList<>();

    List<String> tempMonthList = new ArrayList<>();
    List<ChartDataList.DataBean> chartDataList = new ArrayList<>();
    String monthIndex = "";
    Animator translationAnimator;
    boolean isFollow = false;
    int likeCountFollower;
    ProfilePostGridAdapter postGridAdapter;
    int currentPagePost = 1, pageSize = 10;
    int currentPageReference = 1;
    int currentPageHightLight = 1;
    public boolean isLoading = true;
    public boolean isLastPage = true;
    IndividualReferencesAdapter individualReferencesAdapter;
    public static final int PAGE_SIZE = 1;
    LinearLayoutManager layoutManagerReference;
    LinearLayoutManager layoutManagerHighLight;
    IndividualProfileHighlightsAdapter profileHighlightsAdapter;
    GridLayoutManager layoutManagerPost;
    boolean isReferenceFromList = false;
    private ArrayList<String> referenceData = new ArrayList<>();
    private WorkingDataAdapter mAdapter;
    private Float latitude = 23.0225f, longitude = 72.5714f;
    HashMap<Integer, String> spinnerMap;
    String[] spinnerArray;
    int idOfReference;
    public boolean fromHighlight = false;
    UserProfileData.DataBean profileData;

    public IndividualProfileFragment() {
        // Required empty public constructor
    }

    public static IndividualProfileFragment newInstance() {
        return new IndividualProfileFragment();
    }

    public static IndividualProfileFragment newInstance(Bundle extras) {
        IndividualProfileFragment fragment = new IndividualProfileFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mBinding == null)
            mBinding = DataBindingUtil.inflate(
                    inflater, R.layout.fragment_business_profile, container, false);
        if (view == null)
            view = mBinding.getRoot();


        userId = getArguments().getInt("userId");

        String from = getArguments().getString("from");
        if (from != null && from.equalsIgnoreCase("footer")) {
            mBinding.uiBusinessProfile.imgBack.setVisibility(View.GONE);
        } else {
            mBinding.uiBusinessProfile.imgBack.setVisibility(View.VISIBLE);
        }

        logInUserId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        Log.e("UserId", "------" + userId);
        Log.e("logInUserId", "------" + logInUserId);
        individualProfileFragment = this;
        mBinding.uiBusinessProfile.recyclerProfilePost.setVisibility(View.VISIBLE);
        mBinding.uiBusinessProfile.recyclerProfileReference.setVisibility(View.GONE);
        mBinding.uiBusinessProfile.recyclerProfileHighlights.setVisibility(View.GONE);
        mBinding.uiBusinessProfile.imgAdd.setImageDrawable(getResources().getDrawable(R.drawable.profile_plus_icon));

        if (userId != logInUserId) {
            mBinding.uiBusinessProfile.imgAdd.setVisibility(View.VISIBLE);
            mBinding.uiBusinessProfile.imgMsg.setVisibility(View.VISIBLE);
            mBinding.uiBusinessProfile.txtLogout.setVisibility(View.GONE);

        } else {
            mBinding.uiBusinessProfile.txtLogout.setVisibility(View.VISIBLE);
            mBinding.uiBusinessProfile.imgAdd.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.imgMsg.setVisibility(View.INVISIBLE);
        }

        setUpChart();

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
            Log.e("ask for permission", "true");
        }

       /* setupViewPager(mBinding.uiBusinessProfile.viewPager);
        mBinding.uiBusinessProfile.tabLayout.setupWithViewPager(mBinding.uiBusinessProfile.viewPager);*/


      /*  mBinding.uiBusinessProfile.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    mBinding.uiBusinessProfile.indicatorZero.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));
                    mBinding.uiBusinessProfile.indicatorOne.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    mBinding.uiBusinessProfile.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                } else if (tab.getPosition() == 1) {
                    mBinding.uiBusinessProfile.indicatorZero.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    mBinding.uiBusinessProfile.indicatorOne.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));
                    mBinding.uiBusinessProfile.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                } else if (tab.getPosition() == 2) {
                    mBinding.uiBusinessProfile.indicatorZero.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    mBinding.uiBusinessProfile.indicatorOne.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    mBinding.uiBusinessProfile.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });*/

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        tabClick();

        mBinding.uiBusinessProfile.transparantRelative.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                mBinding.uiBusinessProfile.mapFrame.setVisibility(View.GONE);
            }
        });
        setClick();
        setReferenceView();

        mBinding.uiBusinessProfile.recyclerProfilePost.setNestedScrollingEnabled(false);
        mBinding.uiBusinessProfile.recyclerProfileReference.setNestedScrollingEnabled(false);
        mBinding.uiBusinessProfile.recyclerProfileHighlights.setNestedScrollingEnabled(false);

        mBinding.uiBusinessProfile.nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) mBinding.uiBusinessProfile.nestedScrollView.getChildAt(mBinding.uiBusinessProfile.nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (mBinding.uiBusinessProfile.nestedScrollView.getHeight() + mBinding.uiBusinessProfile.nestedScrollView
                        .getScrollY()));

                if (diff == 0) {
                    // your pagination code
                    if (mBinding.uiBusinessProfile.recyclerProfilePost.getVisibility() == VISIBLE) {
                        isLoading = true;
                        currentPagePost += 1;
                        callAPIPost();
                    } else if (mBinding.uiBusinessProfile.recyclerProfileReference.getVisibility() == VISIBLE) {
                        loadMoreItemsReference();
                    } else if (mBinding.uiBusinessProfile.recyclerProfileHighlights.getVisibility() == VISIBLE) {
                        loadMoreItemsHightLight();
                    }
                }
            }
        });


//        referenceDataCall();
        return view;
    }

    private void tabClick() {

        mBinding.uiBusinessProfile.tabZero.setOnClickListener(v -> {
            mBinding.uiBusinessProfile.indicatorZero.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));
            mBinding.uiBusinessProfile.indicatorOne.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mBinding.uiBusinessProfile.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mBinding.uiBusinessProfile.recyclerProfilePost.setVisibility(View.VISIBLE);
            mBinding.uiBusinessProfile.recyclerProfileReference.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.recyclerProfileHighlights.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.cardReferences.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.tabZero.setTextColor(getResources().getColor(R.color.colorAccent));
            mBinding.uiBusinessProfile.tabOne.setTextColor(getResources().getColor(R.color.post_title));
            mBinding.uiBusinessProfile.tabTwo.setTextColor(getResources().getColor(R.color.post_title));


        });

        mBinding.uiBusinessProfile.tabOne.setOnClickListener(v -> {

            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mBinding.uiBusinessProfile.spinnerReference.setAdapter(adapter);
            mBinding.uiBusinessProfile.indicatorZero.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mBinding.uiBusinessProfile.indicatorOne.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));
            mBinding.uiBusinessProfile.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mBinding.uiBusinessProfile.recyclerProfilePost.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.recyclerProfileReference.setVisibility(View.VISIBLE);
            mBinding.uiBusinessProfile.recyclerProfileHighlights.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.cardReferences.setVisibility(View.VISIBLE);
            mBinding.uiBusinessProfile.tabZero.setTextColor(getResources().getColor(R.color.post_title));
            mBinding.uiBusinessProfile.tabOne.setTextColor(getResources().getColor(R.color.colorAccent));
            mBinding.uiBusinessProfile.tabTwo.setTextColor(getResources().getColor(R.color.post_title));
            mBinding.uiBusinessProfile.spinnerReference.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItem = parent.getItemAtPosition(position).toString();
                    idOfReference = Integer.parseInt(spinnerMap.get(mBinding.uiBusinessProfile.spinnerReference.getSelectedItemPosition()));
                    //   Toast.makeText(getActivity(), selectedItem + idOfReference, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            referencesPost();
        });
        mBinding.uiBusinessProfile.tabTwo.setOnClickListener(v -> {
            mBinding.uiBusinessProfile.indicatorZero.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mBinding.uiBusinessProfile.indicatorOne.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            mBinding.uiBusinessProfile.indicatorTwo.setBackgroundColor(getResources().getColor(R.color.profile_tab_indicator));
            mBinding.uiBusinessProfile.recyclerProfilePost.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.recyclerProfileReference.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.recyclerProfileHighlights.setVisibility(View.VISIBLE);
            mBinding.uiBusinessProfile.cardReferences.setVisibility(View.GONE);
            mBinding.uiBusinessProfile.tabZero.setTextColor(getResources().getColor(R.color.post_title));
            mBinding.uiBusinessProfile.tabOne.setTextColor(getResources().getColor(R.color.post_title));
            mBinding.uiBusinessProfile.tabTwo.setTextColor(getResources().getColor(R.color.colorAccent));
            highlightPost();
        });

    }

    private void referenceDataCall() {

        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getReferencesSpinner("i")
                        , getCompositeDisposable(), referenceDataList, this);

    }


    private void profilePost() {

        layoutManagerPost = new GridLayoutManager(getActivity(), 2);
        layoutManagerPost.setOrientation(LinearLayoutManager.VERTICAL);
      /*  linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mBinding.recyclerBusinessProfile.setLayoutManager(linearLayoutManager);
*/
        mBinding.uiBusinessProfile.recyclerProfilePost.setLayoutManager(layoutManagerPost);
        postGridAdapter = new ProfilePostGridAdapter(getActivity());

        mBinding.uiBusinessProfile.recyclerProfilePost.setItemAnimator(new SlideInUpAnimator());
        mBinding.uiBusinessProfile.recyclerProfilePost.setAdapter(postGridAdapter);
//        mBinding.recyclerBusinessProfile.addOnScrollListener(recycleReload);
        if (postDataList != null) {
            currentPagePost = 1;
            callAPIPost();
        } else {
            postGridAdapter.addAll(postDataList);
        }
//        if (IndividualProfileFragment.individualProfileFragment.postDataList.size()
    }

    private void referencesPost() {
        layoutManagerReference = new LinearLayoutManager(mContext);
        layoutManagerReference.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.uiBusinessProfile.recyclerProfileReference.setLayoutManager(layoutManagerReference);

        individualReferencesAdapter = new IndividualReferencesAdapter(individualProfileFragment);

        mBinding.uiBusinessProfile.recyclerProfileReference.setItemAnimator(new SlideInUpAnimator());
        mBinding.uiBusinessProfile.recyclerProfileReference.setAdapter(individualReferencesAdapter);
        mBinding.uiBusinessProfile.recyclerProfileReference.addOnScrollListener(recycleReloadReference);


        if (referenceList != null) {
            currentPageReference = 1;
            callAPIReferences();
        } else {
            individualReferencesAdapter.addAll(referenceList);
        }
    }

    private void highlightPost() {
        layoutManagerHighLight = new LinearLayoutManager(getActivity());
        layoutManagerHighLight.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.uiBusinessProfile.recyclerProfileHighlights.setLayoutManager(layoutManagerHighLight);

        profileHighlightsAdapter = new IndividualProfileHighlightsAdapter(getActivity());

        mBinding.uiBusinessProfile.recyclerProfileHighlights.setItemAnimator(new SlideInUpAnimator());
        mBinding.uiBusinessProfile.recyclerProfileHighlights.setAdapter(profileHighlightsAdapter);
        mBinding.uiBusinessProfile.recyclerProfileHighlights.addOnScrollListener(recycleReloadHightlight);


        if (highlightList != null) {
            currentPageHightLight = 1;
            callAPIHightlight();
        } else {
            profileHighlightsAdapter.addAll(highlightList);
        }
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {

        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    public void setData(UserProfileData.DataBean data) {
        try {
            mBinding.uiBusinessProfile.txtUserName.setText(data.getUserName());
            mBinding.uiBusinessProfile.txtUserAddress.setText(data.getLocation());
            /*   mBinding.uiBusinessProfile.txtFollowers.setText(withSuffix(data.getFollowers()));*/
            /*  mBinding.uiBusinessProfile.txtFollowing.setText(withSuffix(data.getFollowing()));*/
            mBinding.uiBusinessProfile.imgCopySelected.setVisibility(View.VISIBLE);
            Log.i("followData", " " + data.isIsFollow() + data.getUserID());

            likeCountFollower = Integer.parseInt(Utils.convertToOriginalCount(String.valueOf(data.getFollowers())));
            //    int addCountFollower = likeCountFollower + 1;
            String countToPrint = Utils.convertLikes(likeCountFollower);
            mBinding.uiBusinessProfile.txtFollowers.setText(countToPrint);

            int likeCountFollowing = Integer.parseInt(Utils.convertToOriginalCount(String.valueOf(data.getFollowing())));
            //   int addCountFollowing = likeCountFollowing + 1;
            String countToPrintFollowing = Utils.convertLikes(likeCountFollowing);
            mBinding.uiBusinessProfile.txtFollowing.setText(countToPrintFollowing);

            if (map != null) {
                if (data.getLat().equalsIgnoreCase("") && data.getLong().equalsIgnoreCase("")) {
                    map.addMarker(new MarkerOptions().position(new LatLng(23, 72)).title("Location"));
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23, 72), 10));
                } else {
                    LatLng latLng = new LatLng(Double.valueOf(data.getLat()), Double.valueOf(data.getLong()));
                    map.addMarker(new MarkerOptions().position(latLng).title("Location"));
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                }
            }

            isFollow = data.isIsFollow();
            if (data.isIsFollow())
                mBinding.uiBusinessProfile.imgAdd.setImageDrawable(getResources().getDrawable(R.drawable.ic_hide_icon));
            else
                mBinding.uiBusinessProfile.imgAdd.setImageDrawable(getResources().getDrawable(R.drawable.profile_plus_icon));

            if (userId == logInUserId)
                mBinding.uiBusinessProfile.imgAdd.setImageDrawable(getResources().getDrawable(R.drawable.profile_plus_icon));

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.user_placeholder)
                    .error(R.drawable.user_placeholder);

            Glide.with(mContext)
                    .load(ApiClient.WebService.imageUrl + data.getUserImagePath())
                    .apply(options)
                    .into(mBinding.uiBusinessProfile.imgUserProfile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public void setClick() {
        mBinding.uiBusinessProfile.imgLocation.setOnClickListener(view -> {
            mapVisibility();
        });

        mBinding.uiBusinessProfile.imgBack.setOnClickListener(view1 -> {
            getActivity().finish();
        });

       /* mBinding.uiBusinessProfile.txtOpenInMap.setOnClickListener(view -> {
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(mActivity.getPackageManager()) != null) {
                startActivity(mapIntent);
            }
        });*/
        mBinding.uiBusinessProfile.txtSend.setOnClickListener(v -> {

            if (idOfReference == 0)
                Toast.makeText(getActivity(), "Please select reference", Toast.LENGTH_SHORT).show();
            else {
                mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                disableScreen(false);
                saveReference();
            }
        });

        mBinding.uiBusinessProfile.txtLogout.setOnClickListener(v -> {
            LogoutDialog dialogFragment = new LogoutDialog();
            dialogFragment.show(getActivity().getFragmentManager(), "Dialog Logout");
        });


     /*   mBinding.uiBusinessProfile.imgMsg.setOnClickListener(view -> {
            Intent intent1 = new Intent(getActivity(), ConversationActivity.class);
            intent1.putExtra(ConversationUIService.USER_ID, String.valueOf(userId));
            if (profileData != null) {
                intent1.putExtra(ConversationUIService.DISPLAY_NAME, profileData.getUserName()); //put it for displaying the title.
            }
            intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
            getActivity().startActivity(intent1);
        });*/
       /* mBinding.uiBusinessProfile.cardReferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mBinding.uiBusinessProfile.spinnerReference.performClick()

            }
        });*/


        mBinding.uiBusinessProfile.imgAdd.setOnClickListener(v -> {
            if (userId != logInUserId) {
                followPeople(userId);
            }

        });
        mBinding.uiBusinessProfile.imgMsg.setOnClickListener(v -> {
            Intent intentDeviceSync = new Intent(mActivity, DeviceContactSyncService.class);
            DeviceContactSyncService.enqueueWork(mActivity, intentDeviceSync);

            Applozic.getInstance(mActivity).enableDeviceContactSync(true);

            Intent intent1 = new Intent(getActivity(), ConversationActivity.class);
            intent1.putExtra(ConversationUIService.USER_ID, String.valueOf(userId));
            if (profileData != null) {
                intent1.putExtra(ConversationUIService.DISPLAY_NAME, profileData.getUserName()); //put it for displaying the title.
            }
            intent1.putExtra(ConversationUIService.TAKE_ORDER, true); //Skip chat list for showing on back press
            getActivity().startActivity(intent1);
        /*    float yFirst = mBinding.uiBusinessProfile.recyclerProfilePost.getY() + mBinding.uiBusinessProfile.recyclerProfilePost.getChildAt(0).getY();
            mBinding.uiBusinessProfile.nestedScrollView.smoothScrollTo(0, (int) yFirst);

            float y = mBinding.uiBusinessProfile.recyclerProfilePost.getY() + mBinding.uiBusinessProfile.recyclerProfilePost.getChildAt(10).getY();
            mBinding.uiBusinessProfile.nestedScrollView.smoothScrollTo(0, (int) y);*/


        });
        mBinding.uiBusinessProfile.txtFollowers.setOnClickListener(v -> {
            if (!mBinding.uiBusinessProfile.txtFollowers.getText().toString().equals("0")) {
                Intent intent1 = new Intent(getActivity(), FollowingActivity.class);
                intent1.putExtra("USER_ID", userId);
                intent1.putExtra("LIST_TYPE", "Follower"); //Skip chat list for showing on back press
                getActivity().startActivity(intent1);
            }

        });

        mBinding.uiBusinessProfile.txtFollowing.setOnClickListener(v -> {
            if (!mBinding.uiBusinessProfile.txtFollowing.getText().toString().equals("0")) {
                Intent intent1 = new Intent(getActivity(), FollowingActivity.class);
                intent1.putExtra("USER_ID", userId);
                intent1.putExtra("LIST_TYPE", "Following"); //Skip chat list for showing on back press
                getActivity().startActivity(intent1);
            }


        });
        mBinding.uiBusinessProfile.mapFrame.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                mapVisibility();
            }
        });
/*
        mBinding.uiBusinessProfile.txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                */
/*if(App_pref.getAuthorizedUser(getActivity()) != null)
                {
                    ApplicationClass.OnlineUserListAPI(getActivity(), false);
                }

                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.clear();
                editor.apply();

                App_pref.signOut(getActivity());

                LoginManager.getInstance().logOut();
                Intent intentLogout = new Intent(getActivity(), SplashActivity.class);
                startActivity(intentLogout);
                getActivity().finish();*//*


                LogoutDialog dialogFragment = new LogoutDialog();
                dialogFragment.show(getActivity().getFragmentManager(), "Dialog Logout");
            }
        });
*/

    }

    private void mapVisibility() {
        if (mBinding.uiBusinessProfile.mapFrame.getVisibility() == View.VISIBLE) {

            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiBusinessProfile.mapFrame, View.TRANSLATION_Y, 0f, -1500f)
                    .setDuration(300);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiBusinessProfile.mapFrame.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            mBinding.uiBusinessProfile.mapFrame.setVisibility(View.GONE);
        } else {
            mBinding.uiBusinessProfile.mapFrame.setVisibility(View.VISIBLE);
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiBusinessProfile.mapFrame, View.TRANSLATION_Y, -1500f, 0f)
                    .setDuration(300);
            translationAnimator.start();
        }
    }

    private void saveReference() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveReference(logInUserId, userId, idOfReference)
                        , getCompositeDisposable(), saveReferenceData, this);
    }

    public void followPeople(int followUserId) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                followPeople(logInUserId, followUserId)
                        , getCompositeDisposable(), followPeople, this);

    }

  /*  private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new IndividualProfilePostsFragment(), "POSTS");
        adapter.addFragment(new IndividualProfileReferencesFragment(), "REFERENCES");
        adapter.addFragment(new IndividualProfileHighlightsFragment(), "HIGHLIGHTS");
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }*/

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e("onMapReady", "call");
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        if (profileData != null) {
            if (profileData.getLat().equalsIgnoreCase("") && profileData.getLong().equalsIgnoreCase("")) {
                map.addMarker(new MarkerOptions().position(new LatLng(23, 72)).title("Location"));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23, 72), 10));
            } else {
                LatLng latLng = new LatLng(Double.valueOf(profileData.getLat()), Double.valueOf(profileData.getLong()));
                map.addMarker(new MarkerOptions().position(latLng).title("Location"));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
            }
        } else {
            map.addMarker(new MarkerOptions().position(new LatLng(23, 72)).title("Location"));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23, 72), 10));
        }
        map.getUiSettings().setAllGesturesEnabled(false);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        try {
            switch (apiNames) {
                case followPeople:
                    FollowPeopleData followPeopleData = (FollowPeopleData) o;
                    if (followPeopleData.isSuccess()) {
                        if (isFollow && !mBinding.uiBusinessProfile.txtFollowers.getText().toString().equalsIgnoreCase("0")) {

                            mBinding.uiBusinessProfile.imgAdd.setImageDrawable(getResources().getDrawable(R.drawable.profile_plus_icon));
                            isFollow = false;

                            int likeCountFollower = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiBusinessProfile.txtFollowers.getText().toString()));
                            int addCountFollower = likeCountFollower - 1;
                            String countToPrint = Utils.convertLikes(addCountFollower);
                            mBinding.uiBusinessProfile.txtFollowers.setText(countToPrint);
//                    mBinding.loading.progressBar.setVisibility(View.GONE);
//                    disableScreen(false);
//                    callAPI(false);
                        } else {
                            mBinding.uiBusinessProfile.imgAdd.setImageDrawable(getResources().getDrawable(R.drawable.ic_hide_icon));
                            isFollow = true;

                            int likeCountFollower = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiBusinessProfile.txtFollowers.getText().toString()));
                            int addCountFollower = likeCountFollower + 1;
                            String countToPrint = Utils.convertLikes(addCountFollower);
                            mBinding.uiBusinessProfile.txtFollowers.setText(countToPrint);
                        }
                        Toast.makeText(mContext, followPeopleData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), followPeopleData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case saveLike:
                    SaveLikeData saveLikeData = (SaveLikeData) o;
                    if (saveLikeData.isSuccess()) {
//                    callAPI(false);
                    } else {
                        Toast.makeText(getActivity(), saveLikeData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case savePostView:
                    SavePostViewData savePostViewData = (SavePostViewData) o;
                    if (savePostViewData.isSuccess()) {
//                    Toast.makeText(getActivity(), savePostViewData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), savePostViewData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case referenceDataList:
                    RefenrenceModel refenrenceModel = (RefenrenceModel) o;
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    if (refenrenceModel.isSuccess()) {
                        spinnerArray = new String[refenrenceModel.getData().size()];
                        spinnerMap = new HashMap<Integer, String>();
                        for (int i = 0; i < refenrenceModel.getData().size(); i++) {
                            spinnerMap.put(i, String.valueOf(refenrenceModel.getData().get(i).getReferenceID()));
                            spinnerArray[i] = refenrenceModel.getData().get(i).getReferenceText();
                        }


                    }
                    break;
                case saveReferenceData:
                    SaveReferenceData saveReferenceData = (SaveReferenceData) o;
                    if (saveReferenceData.isSuccess()) {

                        disableScreen(false);
                        currentPageReference = 1;
                        callAPIReferences();
                   /* mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);*/
                   /* final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            disableScreen(false);
                            callAPIReferences();
                            //Do something after 100ms
                        }
                    }, 1000);*/

                        // Toast.makeText(getActivity(), saveReferenceData.getMessage().get(0), Toast.LENGTH_SHORT).show();

                    }

                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }

 /*   class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private int mCurrentPosition = -1;

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

     *//* @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                CustomViewPager pager = (CustomViewPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.measureCurrentView(fragment.getView());
                }

            }
        }*//*

    }*/

    @Override
    public void onResume() {
        super.onResume();
    }

    @SuppressLint("DefaultLocale")
    public static String withSuffix(long count) {
        if (count < 1000) return "" + count;
        int exp = (int) (Math.log(count) / Math.log(1000));
        return String.format("%.0f %c",
                count / Math.pow(1000, exp),
                "kMGTPE".charAt(exp - 1));
    }

   /* public void setUpChart() {
        // Tooltip
        mTip = new Tooltip(mContext, R.layout.linechart_tooltip, R.id.value);

        ((TextView) mTip.findViewById(R.id.value)).setTypeface(
                ResourcesCompat.getFont(mActivity, R.font.avenir_next_medium));

        mTip.setVerticalAlignment(Tooltip.Alignment.BOTTOM_TOP);
        mTip.setDimensions((int) Tools.fromDpToPx(50), (int) Tools.fromDpToPx(25));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {

            mTip.setEnterAnimation(PropertyValuesHolder.ofFloat(View.ALPHA, 1),
                    PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f),
                    PropertyValuesHolder.ofFloat(View.SCALE_X, 1f)).setDuration(200);

            mTip.setExitAnimation(PropertyValuesHolder.ofFloat(View.ALPHA, 0),
                    PropertyValuesHolder.ofFloat(View.SCALE_Y, 0f),
                    PropertyValuesHolder.ofFloat(View.SCALE_X, 0f)).setDuration(200);

            mTip.setPivotX(Tools.fromDpToPx(65) / 2);
            mTip.setPivotY(Tools.fromDpToPx(25));
        }

        // Data
        LineSet dataset = new LineSet(mLabels, mValues);
        *//*dataset.setColor(Color.parseColor("#000F62"))
//                .setFill(Color.parseColor("#2d374c"))
                .setDotsColor(Color.parseColor("#27CEC2"))
                .setThickness(8)
//                .setDashed(new float[]{10f, 10f})
                .setSmooth(true)
                .beginAt(1);
        mBinding.uiBusinessProfile.chart.addData(dataset);*//*

        dataset = new LineSet(mLabels, mValues);
        dataset.setColor(Color.parseColor("#000F62"))
//                .setFill(Color.parseColor("#2d374c"))
//                .setDotsColor(Color.parseColor("#27CEC2"))
                .setDotsDrawable(getResources().getDrawable(R.drawable.ic_chart_marker))
                .setThickness(8)
                .setSmooth(true)
                .endAt(4);
        mBinding.uiBusinessProfile.chart.addData(dataset);

//        mBaseAction = action;
        Runnable chartAction = new Runnable() {
            @Override
            public void run() {

//                mBaseAction.run();
                mTip.prepare(mBinding.uiBusinessProfile.chart.getEntriesArea(0).get(3), mValues[3]);
                mBinding.uiBusinessProfile.chart.showTooltip(mTip, true);
            }
        };

        mBinding.uiBusinessProfile.chart.setAxisBorderValues(0, 200)
                .setYLabels(AxisRenderer.LabelPosition.NONE)
                .setTooltips(mTip)
                .show(new Animation().setInterpolator(new BounceInterpolator())
                        .fromAlpha(0)
                        .withEndAction(chartAction));

    }*/

    private void setUpChart() {
        mBinding.uiBusinessProfile.mChart.setOnChartGestureListener(this);
        mBinding.uiBusinessProfile.mChart.setOnChartValueSelectedListener(this);
        mBinding.uiBusinessProfile.mChart.setDrawGridBackground(false);

        // no description text
        mBinding.uiBusinessProfile.mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mBinding.uiBusinessProfile.mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mBinding.uiBusinessProfile.mChart.setDragEnabled(true);
        mBinding.uiBusinessProfile.mChart.setScaleEnabled(true);
        // mBinding.mChart.setScaleXEnabled(true);
        // mBinding.mChart.setScaleYEnabled(true);

        mBinding.uiBusinessProfile.mChart.setNoDataTextColor(ContextCompat.getColor(mContext, R.color.white));

        // if disabled, scaling can be done on x- and y-axis separately
        mBinding.uiBusinessProfile.mChart.setPinchZoom(true);
        Typeface tf = ResourcesCompat.getFont(mActivity, R.font.avenir_next_medium);
        // set an alternative background color
        // mBinding.mChart.setBackgroundColor(Color.GRAY);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(mContext, R.layout.custom_marker_view);
        mv.setChartView(mBinding.uiBusinessProfile.mChart); // For bounds control
        mBinding.uiBusinessProfile.mChart.setMarker(mv); // Set the marker to the chart

        // x-axis limit line
        /*LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        llXAxis.setLineWidth(4f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);*/

        XAxis xAxis = mBinding.uiBusinessProfile.mChart.getXAxis();
        xAxis.setTextColor(ContextCompat.getColor(mContext, R.color.black_translucent));
        xAxis.setAxisLineColor(ContextCompat.getColor(mContext, R.color.white));
        xAxis.setGridColor(ContextCompat.getColor(mContext, R.color.white_opacity_25));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawLabels(false);
        xAxis.setTypeface(tf);
        /*xAxis.setValueFormatter(new IAxisValueFormatter() {

            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                return numMap.get((int)value);
            }

        });*/
//        xAxis.enableGridDashedLine(10f, 10f, 0f);
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line


//        Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "OpenSans-Regular.ttf");


        /*LimitLine ll1 = new LimitLine(150f, "Upper Limit");
        ll1.setLineWidth(4f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll1.setTextSize(10f);
        ll1.setTypeface(tf);*/

       /* LimitLine ll2 = new LimitLine(0f, null);
        ll2.setLineWidth(2f);
//        ll2.enableDashedLine(10f, 10f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);
//        ll2.setTypeface(tf);
        ll2.setLineColor(ContextCompat.getColor(mContext, R.color.white_opacity_80));
        ll2.setTextColor(ContextCompat.getColor(mContext, R.color.black));*/

        YAxis leftAxis = mBinding.uiBusinessProfile.mChart.getAxisLeft();
        leftAxis.setTextColor(ContextCompat.getColor(mContext, R.color.black_translucent));
        leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
//        leftAxis.addLimitLine(ll1);
//        leftAxis.addLimitLine(ll2);
        leftAxis.setAxisMaximum(500f);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setTypeface(tf);
        leftAxis.setDrawLabels(false);
        //leftAxis.setYOffset(20f);
        leftAxis.enableGridDashedLine(0f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setGridColor(ContextCompat.getColor(mContext, R.color.white_opacity_25));
        leftAxis.setAxisLineColor(ContextCompat.getColor(mContext, R.color.white_opacity_25));

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(false);

        mBinding.uiBusinessProfile.mChart.getAxisRight().setEnabled(false);

        //mBinding.mChart.getViewPortHandler().setMaximumScaleY(2f);
        //mBinding.mChart.getViewPortHandler().setMaximumScaleX(2f);


//        mBinding.mChart.setVisibleXRange(20);
//        mBinding.mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mBinding.mChart.centerViewTo(20, 50, AxisDependency.LEFT);

        getGraphData();

    }

    private void setData(int count, List<ChartDataList.DataBean> dataList) {

        monthIndex = "";
        ArrayList<Entry> values = new ArrayList<>();
        String month = "";
        String targetDateValue = "";
        if (dataList.size() > 0) {
            for (int i = 0; i < dataList.size(); i++) {
                ChartDataList.DataBean data = dataList.get(i);
                SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
                Date sourceDate = null;
                try {
                    sourceDate = dateFormat.parse(data.getDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat targetFormat = new SimpleDateFormat("MMM yy");
                targetDateValue = targetFormat.format(sourceDate);

                float val = data.getViewCount();
                if (i == 0) {
                    month = targetDateValue.split(" ")[0];
                    tempMonthList.add(targetDateValue);
                    values.add(new Entry(i, val, getResources().getDrawable(R.drawable.ic_chart_marker)));
                    monthIndex = String.valueOf(i);

                } else if (!month.equalsIgnoreCase(targetDateValue.split(" ")[0])) {
                    month = targetDateValue.split(" ")[0];
                    tempMonthList.add(targetDateValue);
                    values.add(new Entry(i, val, getResources().getDrawable(R.drawable.ic_chart_marker)));
                    monthIndex = monthIndex + "," + i;
                } else
                    values.add(new Entry(i, val, null));
            }
            if (tempMonthList.size() == 1) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yy");
                Date sourceDate = null;
                try {
                    sourceDate = dateFormat.parse(targetDateValue);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat targetFormat = new SimpleDateFormat("MM yyyy");
                String dateValue = targetFormat.format(sourceDate);
                Calendar date = Calendar.getInstance();
                date.set(Integer.valueOf(dateValue.split(" ")[1]), Integer.valueOf(dateValue.split(" ")[0]) - 1, 1);
                date.add(Calendar.MONTH, 1);

                targetDateValue = dateFormat.format(date.getTime());
                tempMonthList.add(targetDateValue);
                values.add(new Entry(1, 0, getResources().getDrawable(R.drawable.ic_chart_marker)));
                monthIndex = monthIndex + "," + 1;

                date.add(Calendar.MONTH, 1);
                targetDateValue = dateFormat.format(date.getTime());
                tempMonthList.add(targetDateValue);
                values.add(new Entry(2, 0, getResources().getDrawable(R.drawable.ic_chart_marker)));
                monthIndex = monthIndex + "," + 2;
            } else if (tempMonthList.size() == 2) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM yy");
                Date sourceDate = null;
                try {
                    sourceDate = dateFormat.parse(targetDateValue);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                SimpleDateFormat targetFormat = new SimpleDateFormat("MM yyyy");
                String dateValue = targetFormat.format(sourceDate);
                Calendar date = Calendar.getInstance();
                date.set(Integer.valueOf(dateValue.split(" ")[1]), Integer.valueOf(dateValue.split(" ")[0]) - 1, 1);
                date.add(Calendar.MONTH, 1);

                targetDateValue = dateFormat.format(date.getTime());
                tempMonthList.add(targetDateValue);
                values.add(new Entry(2, 0, getResources().getDrawable(R.drawable.ic_chart_marker)));
                monthIndex = monthIndex + "," + 2;
            }
        } else {
            for (int i = 0; i < count; i++) {

//                float val = (float) (Math.random() * range) + 3;
                float val = 0;
                if (i == 0 || i == 30 || i == 59) {
                    values.add(new Entry(i, val, getResources().getDrawable(R.drawable.ic_chart_marker)));
                    if (monthIndex.equalsIgnoreCase(""))
                        monthIndex = String.valueOf(i);
                    else
                        monthIndex = monthIndex + "," + i;
                    tempMonthList.add("");
                } else
                    values.add(new Entry(i, val, null));
            }
        }

        mBinding.uiBusinessProfile.txtMonthOne.setText(tempMonthList.get(0));
        mBinding.uiBusinessProfile.txtMonthTwo.setText(tempMonthList.get(1));
        mBinding.uiBusinessProfile.txtMonthThree.setText(tempMonthList.get(2));

        LineDataSet set1;

        if (mBinding.uiBusinessProfile.mChart.getData() != null &&
                mBinding.uiBusinessProfile.mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mBinding.uiBusinessProfile.mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);

            mBinding.uiBusinessProfile.mChart.getData().notifyDataChanged();
            mBinding.uiBusinessProfile.mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");
            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.4f);
            set1.setDrawIcons(true);
            set1.setValueTextColor(Color.WHITE);
            set1.setHighlightEnabled(true);
            set1.setDrawHighlightIndicators(false);


            // set the line to be drawn like this "- - - - - -"
//            set1.enableDashedLine(10f, 5f, 0f);
//            set1.enableDashedHighlightLine(10f, 5f, 0f);
            set1.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

           /* List<Integer> colorList = new ArrayList<>();
            colorList.add(ContextCompat.getColor(mContext, R.color.colorPrimary));
            colorList.add(ContextCompat.getColor(mContext, R.color.colorSecondary));
            colorList.add(ContextCompat.getColor(mContext, R.color.colorPrimary));
            colorList.add(ContextCompat.getColor(mContext, R.color.colorSecondary));
            colorList.add(ContextCompat.getColor(mContext, R.color.colorPrimary));
            colorList.add(ContextCompat.getColor(mContext, R.color.colorSecondary));
            set1.setColors(colorList);
            set1.setCircleColors(colorList);*/

            set1.setCircleColor(Color.TRANSPARENT);
            set1.setLineWidth(2f);
            set1.setCircleRadius(2f);
            set1.setDrawCircleHole(false);
            set1.setValueTextSize(0f);
            set1.setDrawFilled(false);
            set1.setFormLineWidth(1.5f);
//            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            /*if (Utils.getSDKInt() >= 18) {
                // fill drawable only supported on api level 18 and above
                Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.fade_blue);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(ContextCompat.getColor(mContext, R.color.colorSecondary));
            }*/

            ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(dataSets);

            // set data
            mBinding.uiBusinessProfile.mChart.setData(data);
        }
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START, x: " + me.getX() + ", y: " + me.getY());
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END, lastGesture: " + lastPerformedGesture);

        // un-highlight values after the gesture is finished and no single-tap
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP)
            mBinding.uiBusinessProfile.mChart.highlightValues(null); // or highlightTouch(null) for callback to onNothingSelected(...)
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.e("Entry selected", e.toString());
        String[] splits = monthIndex.split(",");
        String selectedMonth = "";
        Log.e("selected", "" + splits);
        if (e.getX() < Float.valueOf(splits[1])) {
            Log.e("month selected", mBinding.uiBusinessProfile.txtMonthOne.getText().toString());
            selectedMonth = mBinding.uiBusinessProfile.txtMonthOne.getText().toString();
        } else if (e.getX() < Float.valueOf(splits[2])) {
            Log.e("month selected", mBinding.uiBusinessProfile.txtMonthTwo.getText().toString());
            selectedMonth = mBinding.uiBusinessProfile.txtMonthTwo.getText().toString();
        } else if (e.getX() == Float.valueOf(splits[2])) {
            Log.e("month selected", mBinding.uiBusinessProfile.txtMonthThree.getText().toString());
            selectedMonth = mBinding.uiBusinessProfile.txtMonthThree.getText().toString();
        }
        Log.i("LOWHIGH", "low: " + mBinding.uiBusinessProfile.mChart.getLowestVisibleX() + ", high: " + mBinding.uiBusinessProfile.mChart.getHighestVisibleX());
        Log.i("MIN MAX", "xmin: " + mBinding.uiBusinessProfile.mChart.getXChartMin() + ", xmax: " + mBinding.uiBusinessProfile.mChart.getXChartMax() + ", ymin: " + mBinding.uiBusinessProfile.mChart.getYChartMin() + ", ymax: " + mBinding.uiBusinessProfile.mChart.getYChartMax());
        /*if (postDataList != null) {
            int sizeOfPostData = postDataList.size();
            if (sizeOfPostData > 0) {
                for (int i = 0; i < sizeOfPostData - 1; i++) {
                    String monthOfData = postDataList.get(i).getMonth();
                    if (monthOfData.equalsIgnoreCase(selectedMonth)) {
                        float yFirst = mBinding.uiBusinessProfile.recyclerProfilePost.getY() + mBinding.uiBusinessProfile.recyclerProfilePost.getChildAt(0).getY();
                        mBinding.uiBusinessProfile.nestedScrollView.smoothScrollTo(0, (int) yFirst);
                        float y = mBinding.uiBusinessProfile.recyclerProfilePost.getY() + mBinding.uiBusinessProfile.recyclerProfilePost.getChildAt(i).getY();
                        mBinding.uiBusinessProfile.nestedScrollView.smoothScrollTo(0, (int) y);
                        break;
                    }
                }
            }
        }*/
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    public void callAPIPost() {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
//            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getProfile(userId, logInUserId, currentPagePost, pageSize).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<UserProfileData>() {
                    @Override
                    public void onSuccess(UserProfileData value) {
                        try {
                            if (value.isSuccess()) {
                                isLoading = false;
                                UserProfileData.DataBean data = value.getData().get(0);
                                profileData = data;
                                IndividualProfileFragment.individualProfileFragment.setData(data);
                                if (data.isIsFollow())
                                    if (data.isIsFollow())
                                        mBinding.uiBusinessProfile.imgAdd.setImageDrawable(getResources().getDrawable(R.drawable.ic_hide_icon));
                                    else
                                        mBinding.uiBusinessProfile.imgAdd.setImageDrawable(getResources().getDrawable(R.drawable.profile_plus_icon));
                                postDataList.clear();
                                postDataList.addAll(data.getPost());

                                isLastPage = postDataList.size() <= 0;
                                if (currentPagePost == 1) {
                                    postGridAdapter.clear();
                                    referenceDataCall();
                                }
                                postGridAdapter.addAll(postDataList);
                                mBinding.loading.progressBar.setVisibility(View.GONE);
//                            postGridAdapter.notifyDataSetChanged();
//                            disableScreen(false);
                            } else {
                                //   Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                                mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
//                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    private RecyclerView.OnScrollListener recycleReloadReference = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManagerReference.getChildCount();
            int totalItemCount = layoutManagerReference.getItemCount();
            int firstVisibleItemPosition = layoutManagerReference.findFirstVisibleItemPosition();
            if (!isLoading && !isLastPage) {

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItemsReference();
                }
            }
        }
    };

    private void loadMoreItemsReference() {
        isLoading = true;
        currentPageReference += 1;
        callAPIReferences();
    }

    private RecyclerView.OnScrollListener recycleReloadHightlight = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManagerHighLight.getChildCount();
            int totalItemCount = layoutManagerHighLight.getItemCount();
            int firstVisibleItemPosition = layoutManagerHighLight.findFirstVisibleItemPosition();
            if (!isLoading && !isLastPage) {

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItemsHightLight();
                }
            }
        }
    };

    private void loadMoreItemsHightLight() {
        isLoading = true;
        currentPageHightLight += 1;
        callAPIHightlight();
    }


    public void callAPIReferences() {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
//            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getReferencesByUserId(userId, logInUserId, "i", currentPageReference, pageSize).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<ReferenceListByUserId>() {
                    @Override
                    public void onSuccess(ReferenceListByUserId value) {
                        try {
                            if (value.isSuccess()) {
                                isLoading = false;
                                referenceList.clear();
                                referenceList.addAll(value.getData());
                            /*if (userId == logInUserId) {
                                if (currentPagePost == 1)
                                    referenceList.remove(0);
                            }*/
                                isLastPage = referenceList.size() <= 0;
                                if (currentPageReference == 1)
                                    individualReferencesAdapter.clear();
                                individualReferencesAdapter.addAll(referenceList);
                                individualReferencesAdapter.notifyDataSetChanged();
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                if (referenceList.size() == 0 && currentPageReference == 1)
                                    mBinding.uiBusinessProfile.txtReferenceNoData.setVisibility(View.VISIBLE);
                                else
                                    mBinding.uiBusinessProfile.txtReferenceNoData.setVisibility(View.GONE);
//                            disableScreen(false);
                            } else {
                                //   Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                                mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            Log.d("fail", e.toString());
                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                        disableScreen(false);
                            Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                }));
    }

    public void saveUserReference(String referenceIds) {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
//            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.saveUserReference(logInUserId, userId, referenceIds).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<CommonApiResponse>() {
                    @Override
                    public void onSuccess(CommonApiResponse value) {
                        try {
                            if (value.isSuccess()) {
                                mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                                currentPageReference = 1;
                                callAPIReferences();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            Log.d("fail", e.toString());
                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                        disableScreen(false);
                            Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                }));

    }

    public void callAPIHightlight() {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
//            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getHighlightedPost(userId, logInUserId, currentPageHightLight, pageSize).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<UserProfileData>() {
                    @Override
                    public void onSuccess(UserProfileData value) {
                        try {
                            if (value.isSuccess()) {
                                isLoading = false;
                                UserProfileData.DataBean data = value.getData().get(0);
//                            IndividualProfileFragment.individualProfileFragment.highlightList.clear();
//                            IndividualProfileFragment.individualProfileFragment.highlightList.addAll(data.getPost());
                                postDataList.clear();
                                postDataList.addAll(data.getPost());
                                isLastPage = postDataList.size() <= 0;
                                if (currentPageHightLight == 1)
                                    profileHighlightsAdapter.clear();
                                profileHighlightsAdapter.addAll(postDataList);
                                profileHighlightsAdapter.notifyDataSetChanged();
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                if (postDataList.size() == 0 && currentPageHightLight == 1)
                                        mBinding.uiBusinessProfile.txtHighlightNoData.setVisibility(View.VISIBLE);
                                else
                                    mBinding.uiBusinessProfile.txtHighlightNoData.setVisibility(View.GONE);
//                            disableScreen(false);
                            } else {
                                //    Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                                mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            Log.d("fail", e.toString());
                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                        disableScreen(false);
                            Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }));
    }

    private void setupGradient(LineChart mChart) {
        Paint paint = mChart.getRenderer().getPaintRender();
        int height = mChart.getHeight();

        LinearGradient linGrad = new LinearGradient(0, 0, 0, height,
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorSecondary),
                Shader.TileMode.REPEAT);
        paint.setShader(linGrad);
    }

    @Override
    public void onStart() {
        super.onStart();
        getView().post(() -> setupGradient(mBinding.uiBusinessProfile.mChart));
    }

    public void getGraphData() {
        try {
//            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
//            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getChartList(userId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<ChartDataList>() {
                    @Override
                    public void onSuccess(ChartDataList value) {
                        try {
                            if (value.isSuccess()) {
                                chartDataList.clear();
                                chartDataList.addAll(value.getData());
                                if (chartDataList.size() > 0) {
                                    mBinding.uiBusinessProfile.layoutMonths.setVisibility(View.VISIBLE);
                                    // add data
                                    setData(60, chartDataList);

                                    mBinding.uiBusinessProfile.mChart.animateX(2500);

                                    // get the legend (only possible after setting data)
                                    Legend l = mBinding.uiBusinessProfile.mChart.getLegend();

                                    // modify the legend ...
                                    l.setForm(Legend.LegendForm.LINE);
                                    l.setEnabled(false);
                                    //  dont forget to refresh the drawing
                                    mBinding.uiBusinessProfile.mChart.invalidate();
                                    profilePost();
                                } else {
                                    mBinding.uiBusinessProfile.mChart.setNoDataTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                                    mBinding.uiBusinessProfile.layoutMonths.setVisibility(View.GONE);
                                    profilePost();
                                }

//                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                            } else {
                                //  Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                                mBinding.uiBusinessProfile.layoutMonths.setVisibility(View.GONE);
                                profilePost();
//                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            Log.d("fail", e.toString());
                            Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                            mBinding.uiBusinessProfile.layoutMonths.setVisibility(View.GONE);
                            profilePost();
//                        mBinding.loading.progressBar.setVisibility(View.GONE);
//                        disableScreen(false);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }));
    }

    public void setReferenceView() {
        mBinding.uiBusinessProfile.edtReferences.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isReferenceFromList = true;
            }
        });

       /* disposable.add(
                RxTextView.textChangeEvents(mBinding.uiBusinessProfile.edtReferences)
                        .skipInitialValue()
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(searchContactsTextWatcher()));*/
    }

    private DisposableObserver<TextViewTextChangeEvent> searchContactsTextWatcher() {

        return new DisposableObserver<TextViewTextChangeEvent>() {
            @Override
            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    Log.e("edtLengthBefore", "" + mBinding.uiBusinessProfile.edtReferences.getText().length());
                    if (mBinding.uiBusinessProfile.edtReferences.getText().length() > 2) {
                        referenceData.clear();
                        mAdapter = new WorkingDataAdapter(getActivity(), referenceData);
                        Log.e("edtLength", "" + mBinding.uiBusinessProfile.edtReferences.getText().length());
                        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(true);
                        disposable.add(apiService.workingAtData(mBinding.uiBusinessProfile.edtReferences.getText().toString()).
                                subscribeOn(Schedulers.io()).
                                observeOn(AndroidSchedulers.mainThread()).
                                subscribeWith(new DisposableSingleObserver<WorkingAtData>() {
                                    @Override
                                    public void onSuccess(WorkingAtData value) {
                                        try {
                                            if (value.isSuccess() == true && value.getData().size() > 0) {
                                                referenceData.clear();
                                                referenceData.addAll(value.getData());
                                                mAdapter.notifyDataSetChanged();
                                                mBinding.uiBusinessProfile.edtReferences.setThreshold(1);
                                                mBinding.uiBusinessProfile.edtReferences.setAdapter(mAdapter);
                                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                                disableScreen(false);
                                            } else {
                                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                                disableScreen(false);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        try {
                                            Log.d("fail", e.toString());
                                            mBinding.loading.progressBar.setVisibility(View.GONE);
                                            disableScreen(false);
                                        } catch (Exception e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                }));
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
        //... your stuff
    }

    public void getPostVideoDetails(PostListData.Post postListData) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("postDetails", postListData);
        Intent intent = new Intent(getActivity(), ClickPostDetailVideoActivity.class);
        intent.putExtra("clickFragmentName", "postVideo");
        intent.putExtras(bundle);
        startActivityForResult(intent, 510);
        getActivity().overridePendingTransition(0, 0);

    }

    public void getPostTextDetails(PostListData.Post postListData, int position) {
        int id = position;
        Bundle bundle = new Bundle();
        bundle.putParcelable("postDetails", postListData);
        Intent intent = new Intent(getActivity(), ClickPostDetailActivity.class);
        intent.putExtra("clickFragmentName", "postText");
        intent.putExtra("postDetails", postListData);
        startActivityForResult(intent, 510);
        getActivity().overridePendingTransition(0, 0);

    }

    public void getPostImageDetails(PostListData.Post postListData) {
        /* Utils.selectedPosition = position;*/
        Bundle bundle = new Bundle();
        bundle.putParcelable("postDetails", postListData);
        Intent intent = new Intent(getActivity(), ClickPostDetailActivity.class);
        intent.putExtra("clickFragmentName", "postImage");
        intent.putExtra("postDetails", postListData);
        startActivityForResult(intent, 510);
        getActivity().overridePendingTransition(0, 0);
    }

    public void deletePost(PostListData.Post post) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                deletePost(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), post.getPostId())
                        , getCompositeDisposable(), deletePost, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                SavePost savePostData = (SavePost) o;
                                Log.e("onSingleSuccess: ", "Success");
                                Toast.makeText(getActivity(), savePostData.getMessage().get(0), Toast.LENGTH_LONG).show();
                                currentPagePost = 1;
                                callAPIPost();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");

                            }
                        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("onActivityResult", "From News Feed" + requestCode);
        if (requestCode == 510 && resultCode == RESULT_OK) {
            boolean isRefresh = data.getBooleanExtra("isRefresh", false);
            if (isRefresh) {
                if (fromHighlight)
                    highlightPost();
                else
                    profilePost();
            }

        }
    }

    public void saveLikePost(int postId, boolean isActive) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveLike(postId, userId, isActive, logInUserId)
                        , getCompositeDisposable(), saveLike, this);

    }

    public void savePostView(int postId) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                savePostView(postId, userId)
                        , getCompositeDisposable(), savePostView, this);
    }

}
