package com.danielandshayegan.discovrus.ui.fragment;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.MediaRouteButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.models.VideoSavedState;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class VideoDetailsFragment extends BaseFragment {

    // ExoPlayer Guide
    // https://google.github.io/ExoPlayer/guide.html
    // https://google.github.io/ExoPlayer/doc/reference/com/google/android/exoplayer2/ui/SimpleExoPlayerView.html

    // region Constants

    // endregion
    static final String TAG = "Video Play";

    View view;
    // region Views

    SimpleExoPlayerView simpleExoPlayerView;
    ImageView thumbnailImageView;
    TextView castInfoTextView;
    View shutterView;
    SeekBar exoSeekBar;
    ImageButton exoPlayButton;
    ImageButton exoPauseButton;
    ImageButton replayImageButton;
    FrameLayout exoButtonsFrameLayout;
    LinearLayout controlViewLinearLayout;
    MediaRouteButton mediaRouteButton;
    AspectRatioFrameLayout aspectRatioFrameLayout;
    ProgressBar progressBar;
    // endregion

    // region Member Variables
    private String videoUrl;
    private VideoSavedState videoSavedState;
    private SimpleExoPlayer exoPlayer;
    private PlaybackState playbackState;
    private long currentPosition = 0;
    // endregion

    private ExoPlayer.EventListener exoPlayerEventListener = new ExoPlayer.EventListener() {

        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            switch (playbackState) {
                case ExoPlayer.STATE_BUFFERING:
                    progressBar.setVisibility(View.VISIBLE);
                    break;

                case ExoPlayer.STATE_READY:
                    replayImageButton.setVisibility(View.GONE);
                    exoButtonsFrameLayout.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    break;
                case ExoPlayer.STATE_ENDED:
                    exoButtonsFrameLayout.setVisibility(View.GONE);
                    replayImageButton.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {

        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {

        }

        @Override
        public void onPositionDiscontinuity(int reason) {

        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

        }

        @Override
        public void onSeekProcessed() {

        }

    };

    private PlaybackControlView.VisibilityListener playbackControlViewVisibilityListener = new PlaybackControlView.VisibilityListener() {
        @Override
        public void onVisibilityChange(int visibility) {
            int orientation = getContext().getResources().getConfiguration().orientation;
            switch (orientation) {
                case ORIENTATION_PORTRAIT:
                    if (visibility == View.GONE) {
                        hidePortraitSystemUI();
                    } else {
                        showPortraitSystemUI();
                    }
                    break;
                case ORIENTATION_LANDSCAPE:
                    if (visibility == View.GONE) {
                        hideLandscapeSystemUI();
                    } else {
                        showLandscapeSystemUI();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private SeekBar.OnSeekBarChangeListener seekBarOnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                currentPosition = (long) (exoPlayer.getDuration() * (progress / 1000.0D));
                updateLocalVideoPosition(currentPosition);


            } else {
                playbackState = PlaybackState.PLAYING;
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
    // endregion

    // region Constructors
    public VideoDetailsFragment() {
    }
    // endregion

    // region Factory Methods
    public static VideoDetailsFragment newInstance(Bundle extras) {
        VideoDetailsFragment fragment = new VideoDetailsFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    public static VideoDetailsFragment newInstance() {
        VideoDetailsFragment fragment = new VideoDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    // endregion

    // region Lifecycle Methods
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        // Retain this fragment across configuration changes.
        setRetainInstance(true);

        if (getArguments() != null) {
            videoUrl = ApiClient.WebService.imageUrl + getArguments().getString("videoUrl");
        }

        playbackState = PlaybackState.PLAYING;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_video_details, container, false);

        thumbnailImageView = view.findViewById(R.id.exo_thumbnail);
        shutterView = view.findViewById(R.id.exo_shutter);
        exoSeekBar = view.findViewById(R.id.exo_progress);
        exoPlayButton = view.findViewById(R.id.exo_play);
        exoPauseButton = view.findViewById(R.id.exo_pause);
        replayImageButton = view.findViewById(R.id.exo_replay);
        exoButtonsFrameLayout = view.findViewById(R.id.exo_btns_fl);
        controlViewLinearLayout = view.findViewById(R.id.control_view_ll);
        mediaRouteButton = view.findViewById(R.id.mrb);
        aspectRatioFrameLayout = view.findViewById(R.id.exo_content_frame);
        simpleExoPlayerView = view.findViewById(R.id.simple_exo_player_view);
        progressBar = view.findViewById(R.id.progress_bar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            exoSeekBar.setThumbTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
        }
        exoSeekBar.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY));

        setClickListeners();

        return view;
    }

    public void setClickListeners() {
        exoPlayButton.setOnClickListener(view -> {
            playbackState = PlaybackState.PLAYING;
            exoPlayButton.setVisibility(View.GONE);
            resumeLocalVideo();
        });

        exoPauseButton.setOnClickListener(view1 -> {
            playbackState = PlaybackState.PAUSED;
            exoPlayButton.setVisibility(View.GONE);
            pauseLocalVideo();
        });

        replayImageButton.setOnClickListener(view1 -> {
            replayImageButton.setVisibility(View.GONE);
            exoButtonsFrameLayout.setVisibility(View.VISIBLE);
            updateLocalVideoPosition(0);
        });

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (videoUrl != null) {
            setUpExoPlayer();
            setUpSimpleExoPlayerView();

            exoSeekBar.setOnSeekBarChangeListener(seekBarOnSeekBarChangeListener);
        }

        int orientation = view.getResources().getConfiguration().orientation;

        switch (orientation) {
            case ORIENTATION_PORTRAIT:
                showPortraitSystemUI();
                break;
            case ORIENTATION_LANDSCAPE:
                showLandscapeSystemUI();
                break;
            default:
                break;
        }

        VideoSavedState videoSavedState = getVideoSavedState();
        if (videoSavedState != null && !TextUtils.isEmpty(videoSavedState.getVideoUrl())) {
            long currentPosition = videoSavedState.getCurrentPosition();
            videoUrl = videoSavedState.getVideoUrl();
            playbackState = videoSavedState.getPlaybackState();

            boolean autoPlay = false;
            switch (playbackState) {
                case PLAYING:
                    autoPlay = true;
                    break;
                case PAUSED:
                    autoPlay = false;
                    break;
                default:
                    break;
            }

            playLocalVideo(currentPosition, autoPlay);
            updateLocalVideoVolume(videoSavedState.getCurrentVolume());
        }

        playLocalVideo(0, true);

    }


    @Override
    public void onResume() {
        super.onResume();
        resumeLocalVideo();
        Log.e(TAG, "onResume: ");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e(TAG, "onPause: ");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (!TextUtils.isEmpty(videoUrl)) {
            VideoSavedState videoSavedState = new VideoSavedState();
            videoSavedState.setVideoUrl(videoUrl);
            videoSavedState.setCurrentPosition(exoPlayer.getCurrentPosition());
            videoSavedState.setCurrentVolume(exoPlayer.getVolume());
            videoSavedState.setPlaybackState(playbackState);
            setVideoSavedState(videoSavedState);
        }
        removeListeners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        exoPlayer.release();
    }
    // endregion

    // region Helper Methods
    private void setUpExoPlayer() {
        // Create a default TrackSelector
        TrackSelector trackSelector = createTrackSelector();

        // Create a default LoadControl
        LoadControl loadControl = new DefaultLoadControl();

        // Create the player
        exoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector, loadControl);
        resumeLocalVideo();

        exoPlayer.addListener(exoPlayerEventListener);
    }

    private void setUpThumbnail() {
        String thumbnailUrl = "";

        if (!TextUtils.isEmpty(thumbnailUrl)) {
            Glide.with(getActivity())
                    .load(thumbnailUrl)
//                                .placeholder(R.drawable.ic_placeholder)
//                                .error(R.drawable.ic_error)
                    .into(thumbnailImageView);
        }
    }

    private void setUpSimpleExoPlayerView() {
        simpleExoPlayerView.setPlayer(exoPlayer);
        simpleExoPlayerView.setControllerVisibilityListener(playbackControlViewVisibilityListener);
    }

    // This snippet hides the system bars.
    private void hidePortraitSystemUI() {
        final View decorView = getActivity().getWindow().getDecorView();

        // Set the IMMERSIVE flag.
        // Set the content_navigation to appear under the system bars so that the content_navigation
        // doesn't resize when the system bars hide and show.
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN// hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showPortraitSystemUI() {
        final View decorView = getActivity().getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    // This snippet hides the system bars.
    private void hideLandscapeSystemUI() {
        final View decorView = getActivity().getWindow().getDecorView();

        // Set the IMMERSIVE flag.
        // Set the content_navigation to appear under the system bars so that the content_navigation
        // doesn't resize when the system bars hide and show.
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showLandscapeSystemUI() {
        final View decorView = getActivity().getWindow().getDecorView();

        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

    }

    public void setVideoSavedState(VideoSavedState videoSavedState) {
        this.videoSavedState = videoSavedState;
    }

    public VideoSavedState getVideoSavedState() {
        return videoSavedState;
    }

    private TrackSelector createTrackSelector() {
        // Create a default TrackSelector
        // Measures bandwidth during playback. Can be null if not required.
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        return trackSelector;
    }

    private MediaSource getMediaSource(String videoUrl) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
//        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), "VirtualManager"), bandwidthMeter);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(ApplicationClass.getInstance().getApplicationContext(), Util.getUserAgent(ApplicationClass.getInstance().getApplicationContext(), "VirtualManager"), bandwidthMeter);
        // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        // This is the MediaSource representing the media to be played.
        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(videoUrl),
                dataSourceFactory, extractorsFactory, null, null);
        // VirtualManagers the video indefinitely.
//        VirtualManageringMediaSource VirtualManageringSource = new VirtualManageringMediaSource(mediaSource);
        return mediaSource;
    }

    private void removeListeners() {
        exoPlayer.removeListener(exoPlayerEventListener);
        simpleExoPlayerView.setControllerVisibilityListener(null);
        exoSeekBar.setOnSeekBarChangeListener(null);
    }

    private void playLocalVideo(long position, boolean autoPlay) {
        updateLocalVideoPosition(position);
        // Prepare the player with the source.
        exoPlayer.prepare(getMediaSource(videoUrl));
        if (!autoPlay)
            pauseLocalVideo();
    }

    private void updateLocalVideoPosition(long position) {
        exoPlayer.seekTo(position);
    }


    private void resumeLocalVideo() {
        exoPlayer.setPlayWhenReady(true);
    }


    private void pauseLocalVideo() {
        exoPlayer.setPlayWhenReady(false);
    }


    private void updateLocalVideoVolume(float volume) {
        exoPlayer.setVolume(volume);
    }

    // endregion

    // region Inner Classes

    /**
     * indicates whether we are doing a local or a remote playback
     */
    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

    /**
     * List of various states that we can be in
     */
    public enum PlaybackState {
        PLAYING, PAUSED, BUFFERING, IDLE
    }

    // endregion

}
