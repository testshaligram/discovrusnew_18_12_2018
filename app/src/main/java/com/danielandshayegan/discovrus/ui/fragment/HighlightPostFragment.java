package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentLaunchpadBinding;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.AnalyticsActivity;
import com.danielandshayegan.discovrus.ui.activity.SavePostActivity;
import com.danielandshayegan.discovrus.ui.activity.SettingActivity;

public class HighlightPostFragment extends BaseFragment {

    FragmentLaunchpadBinding mBinding;
    private Activity me;
    private View view;
    private Handler handler;
    private final int DELAY = 1000;

    public static HighlightPostFragment newInstance() {
        HighlightPostFragment fragment = new HighlightPostFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_launchpad, container, false);
        view = mBinding.getRoot();
        handler = new Handler();
        me = getActivity();
        mBinding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return view;
    }

    public void handleClicks(View v) {
        Class className = null;
        String title = "";
        View imageView = null;

        //TODO: Change classnames as per navigation flow
        switch (v.getId()) {
            case R.id.ivAnalytics:
                className = AnalyticsActivity.class;
                title = me.getString(R.string.analytics);
                imageView = mBinding.ivAnalytics;
                break;

            case R.id.ivDiscovrCart:
                className = SettingActivity.class;
                title = me.getString(R.string.discovr_cart);
                imageView = mBinding.ivDiscovrCart;
                break;

            case R.id.ivDiscovrMe:
                className = SettingActivity.class;
                title = me.getString(R.string.discovr_me);
                imageView = mBinding.ivDiscovrMe;
                break;

            case R.id.ivDiscovrMeLive:
                className = SettingActivity.class;
                title = me.getString(R.string.discovr_me_live);
                imageView = mBinding.ivDiscovrMeLive;
                break;

            case R.id.ivDraftPosts:
                className = SettingActivity.class;
                title = me.getString(R.string.draft_posts);
                imageView = mBinding.ivDraftPosts;
                break;

            case R.id.ivProfileSettings:
                className = SettingActivity.class;
                title = me.getString(R.string.profile_settings);
                imageView = mBinding.ivProfileSettings;
                break;

            case R.id.ivSettings:
                className = SettingActivity.class;
                title = me.getString(R.string.settings);
                imageView = mBinding.ivSettings;
                break;

            case R.id.ivSavedPosts:
                className = SavePostActivity.class;
                title = me.getString(R.string.saved_posts);
                imageView = mBinding.ivSavedPosts;
                break;
        }
        callNextActivity(className, title, imageView);
    }

    private void callNextActivity(Class className, String title, View view) {
        mBinding.tvSelectedItemName.setVisibility(View.VISIBLE);
        mBinding.tvSelectedItemName.setText(title);
        view.setEnabled(false);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBinding.tvSelectedItemName.setVisibility(View.GONE);
                view.setEnabled(true);
                //TODO: Remove this condition when other classes has redirection

                if (title.equalsIgnoreCase("Settings") || title.equalsIgnoreCase("Analytics") || title.equalsIgnoreCase("Saved Posts") ) {
                    startActivity(new Intent(me, className));
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage("Comming Soon.");
                    builder.setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }
            }

        }, DELAY);
    }
}