package com.danielandshayegan.discovrus.ui.fragment;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentConfirmEmailBinding;
import com.danielandshayegan.discovrus.models.ConfirmMailData;
import com.danielandshayegan.discovrus.models.LoginData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.utils.Utils;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.confirmCode;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.single;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmEmailFragment extends BaseFragment implements SingleCallback {

    FragmentConfirmEmailBinding mBinding;
    View view;
    String emailAddress, resendCode;


    public ConfirmEmailFragment() {
        // Required empty public constructor
    }

    static ConfirmEmailFragment newInstance() {
        return new ConfirmEmailFragment();
    }

    public static ConfirmEmailFragment newInstance(Bundle extras) {
        ConfirmEmailFragment fragment = new ConfirmEmailFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_confirm_email, container, false);
        view = mBinding.getRoot();
        resendCode = "<font color=#4a90e2>Dont't receive the code? </font> <font color=#4be9eb>Send again</font>";
        mBinding.uiConfirmEmail.txtResendCode.setText(Html.fromHtml(resendCode));

        Intent intent = getActivity().getIntent();
        if (null != intent) {
            emailAddress = intent.getExtras().getString("email");
            mBinding.uiConfirmEmail.txtEmail.setText(emailAddress);
            if (emailAddress != null) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callAPI(null);
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        }
        mBinding.uiConfirmEmail.txtResendCode.setOnClickListener(v -> {
            if (emailAddress != null) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callAPI(null);
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        mBinding.uiConfirmEmail.btnConfirm.setOnClickListener(v -> {
            if (validation()) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callAPI(mBinding.uiConfirmEmail.edtVerificationCode.getText().toString());
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        });
        mBinding.uiConfirmEmail.imgBack.setOnClickListener(v -> {
            getActivity().overridePendingTransition(R.anim.exit, R.anim.stay);
            getActivity().finish();
        });


        return view;
    }

    private boolean validation() {
        boolean value = true;
        if (mBinding.uiConfirmEmail.edtVerificationCode.length() == 0) {
            mBinding.uiConfirmEmail.txtErrorCode.setText(R.string.null_code);
            mBinding.uiConfirmEmail.txtErrorCode.setVisibility(View.VISIBLE);
            value = false;
        }
        return value;
    }

    private void callAPI(String code) {
        if (code == null) {
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    confirmMail(mBinding.uiConfirmEmail.txtEmail.getText().toString(), code)
                            , getCompositeDisposable(), single, this);
        } else {
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    confirmCode(mBinding.uiConfirmEmail.txtEmail.getText().toString(), code)
                            , getCompositeDisposable(), confirmCode, this);
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case single:
                ConfirmMailData confirmMailData = (ConfirmMailData) o;
                if (confirmMailData.isSuccess()) {
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);

                }

                break;
            case confirmCode:
                LoginData loginModel = (LoginData) o;
                if (loginModel.isSuccess()) {
                    App_pref.saveAuthorizedUser(getActivity(), loginModel);
                    Toast.makeText(getActivity(), loginModel.getMessage().get(0), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), MenuActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.putExtra("email", loginModel.getData().getEmail());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.stay);
                    getActivity().finish();

                } else {

                    Toast.makeText(getContext(), loginModel.getMessage().get(0), Toast.LENGTH_LONG).show();
                }

        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
}
