package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.HashTagDataAdapter;
import com.danielandshayegan.discovrus.custome_veiws.SpaceTokenizer;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostPublishBinding;
import com.danielandshayegan.discovrus.dialog.LocationDialog;
import com.danielandshayegan.discovrus.dialog.LocationSelectDialog;
import com.danielandshayegan.discovrus.models.DetailPostPhotoData;
import com.danielandshayegan.discovrus.models.DetailPostVideoData;
import com.danielandshayegan.discovrus.models.HashTagListModel;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.DetailPostPaymentActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostPublishActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.utils.FusedLocationProviderClass;
import com.danielandshayegan.discovrus.utils.MyLocationChangeListner;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postDetailPhoto;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postDetailText;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postDetailVideo;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPostPublishFragment extends BaseFragment implements SingleCallback, PlaceSelectionListener, LocationSelectDialog.SelectLocationListner, MyLocationChangeListner {

    FragmentDetailPostPublishBinding mBinding;
    View view;
    int userId;
    double lat, lng;
    String postType;
    public Uri mImageUri;
    Bitmap bitmap;
    private HashTagDataAdapter mAdapter;
    Activity activityMain;
    private WebserviceBuilder apiService;
    List<PostListData.Post> postDataList = new ArrayList<>();
    private CompositeDisposable disposable = new CompositeDisposable();
    View thumbViewDistance, thumbViewTime;
    private List<HashTagListModel> workingData = new ArrayList<>();
    HashMap<Integer, String> spinnerMap;
    String[] spinnerArray;
    ArrayAdapter<String> adapter;
    ArrayList<String> hashtagName = new ArrayList<>();
    String selectedHashTag;
    int idOfHashTag;
    List<String> selectedTags = new ArrayList<String>();
    String singleTag = "";
    String distance = "400 M", time = "3 Hours";
    double amountToPay = 0;
    double timeAmount = 0;
    double distanceAmount = 0;
    private static final int REQUEST_SELECT_PLACE = 1000;
    DecimalFormat decimalFormat = new DecimalFormat("0.00");

    double Latitude, longitude;
    String address;
    FusedLocationProviderClass fusedLocationProviderClass;
    private FusedLocationProviderClient fusedLocationProviderClient;
    String TAG = LocationDialog.class.getName();
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    // load native image filters library
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    public DetailPostPublishFragment() {
        // Required empty public constructor
    }

    public static DetailPostPublishFragment newInstance() {
        return new DetailPostPublishFragment();
    }

    public static DetailPostPublishFragment newInstance(Bundle extras) {
        DetailPostPublishFragment fragment = new DetailPostPublishFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_post_publish, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
//        lat = App_pref.getAuthorizedUser(getActivity()).getData().getLat();
//        lng = App_pref.getAuthorizedUser(getActivity()).getData().getLong();
        mBinding.uiPublishDetailPosting.txtSelectLocation.setText(App_pref.getAuthorizedUser(getActivity()).getData().getLocation());
        activityMain = this.getActivity();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);

        thumbViewDistance = LayoutInflater.from(activityMain).inflate(R.layout.layout_seekbar_thumb, null, false);
        thumbViewTime = LayoutInflater.from(activityMain).inflate(R.layout.layout_seekbar_thumb, null, false);

        mBinding.uiPublishDetailPosting.seekBarDistance.setThumb(getThumb("400 M", thumbViewDistance));
        mBinding.uiPublishDetailPosting.seekBarTime.setThumb(getThumb("3 Hours", thumbViewDistance));
        fusedLocationProviderClient = new FusedLocationProviderClient(getActivity());
        fusedLocationProviderClass = new FusedLocationProviderClass(getActivity(), fusedLocationProviderClient, this);

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_placeholder)
                .error(R.drawable.user_placeholder);

        Glide.with(mContext)
                .load(ApiClient.WebService.imageUrl + App_pref.getAuthorizedUser(activityMain).getData().getUserImagePath())
                .apply(options)
                .into(mBinding.uiPublishDetailPosting.imgProfile);

        postType = getArguments().getString("Type", "");

        setClicks();
        disposable.add(
                RxTextView.textChangeEvents(mBinding.uiPublishDetailPosting.edtAddHashtags)
                        .skipInitialValue()
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(searchHashTagList()));
        mBinding.uiPublishDetailPosting.edtAddHashtags.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                    long id) {

                selectedHashTag = parent.getItemAtPosition(pos).toString();
                idOfHashTag = Integer.parseInt(spinnerMap.get(pos));
                selectedTags.add(selectedHashTag);

            }
        });

        return view;
    }

    public void setClicks() {
        mBinding.uiPublishDetailPosting.imgBack.setOnClickListener(v -> {
            DetailPostPublishActivity.postingActivity.onBackPressed();
        });

        mBinding.uiPublishDetailPosting.btnPublish.setOnClickListener(v -> {
            if (mBinding.uiPublishDetailPosting.seekBarDistance.getProgress() > 5 && mBinding.uiPublishDetailPosting.seekBarTime.getProgress() > 3)
                goForPayment();
            else {
                callApiSaveData();
            }

        });

        mBinding.uiPublishDetailPosting.txtSelectLocation.setOnClickListener(view -> {
            //DialogOpen
            LocationSelectDialog locationSelectDialog = new LocationSelectDialog();
            locationSelectDialog.setListener(this);
            locationSelectDialog.show(((AppCompatActivity) mActivity).getSupportFragmentManager(), LocationSelectDialog.class.getSimpleName());
            locationSelectDialog.setCancelable(false);


        });

        mBinding.uiPublishDetailPosting.seekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String progressString = "";
                if (progress <= 400) {
                    distance = "400 M";
                    distanceAmount = 0;
                } else if (progress < 1000) {
                    distance = progress + " M";
                    if (progress == 500) {
                        distanceAmount = 0.50;
                    } else if (progress - 400 != 0) {
                        distanceAmount = ((progress) / 100) * 0.10;
                    } else {
                        distanceAmount = 0;
                    }
                } else {
                    int progressInt = (progress / 1000);
                    distance = progressInt + " KM";
                    distanceAmount = (((progressInt * 1000)) / 100) * 0.10;
                }
                // You can have your own calculation for progress
                seekBar.setThumb(getThumb(distance, thumbViewDistance));
                Log.e("distanceAmount", "Amount Payable due to Distance " + decimalFormat.format(distanceAmount));
                Log.e("timeAmount", "Amount Payable due to Time " + decimalFormat.format(timeAmount));
                amountToPay = distanceAmount + timeAmount;
                mBinding.uiPublishDetailPosting.txtTotal.setText(getString(R.string.pound_sign) + " " + decimalFormat.format(amountToPay));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBinding.uiPublishDetailPosting.seekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String progressString = "";
                if (progress <= 3) {
                    time = "3 Hours";
                    timeAmount = 0;
                } else if (progress < 24) {
                    if (progress == 4) {
                        time = progress + " Hours";
                        timeAmount = 0.50;
                    } else {
                        time = progress + " Hours";
                        timeAmount = ((progress) * 0.15) - 0.10;
                    }
                } else {
                    int progressInt = (progress / 24);
                    if (progressInt == 1) {
                        time = progressInt + " Day";
                        timeAmount = (21 * 0.15) - 0.10;
                    } else {
                        time = progressInt + " Days";
                        timeAmount = (((progressInt * 24)) * 0.15) - 0.10;
                    }
                }
                // You can have your own calculation for progress
                seekBar.setThumb(getThumb(time, thumbViewTime));
                Log.e("distanceAmount", "Amount Payable due to Distance " + distanceAmount);
                Log.e("timeAmount", "Amount Payable due to Time " + timeAmount);
                amountToPay = distanceAmount + timeAmount;
                mBinding.uiPublishDetailPosting.txtTotal.setText(getString(R.string.pound_sign) + " " + decimalFormat.format(amountToPay));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_SELECT_PLACE) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                this.onPlaceSelected(place);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(getContext(), data);
                this.onError(status);
            }
        }

    }

   /* private void searchTag() {
        workingData.clear();
        mAdapter = new HashTagDataAdapter(getActivity(), workingData);
        String word = mBinding.uiPublishDetailPosting.edtAddHashtags.getText().toString();
        String last = word.substring(word.lastIndexOf("#") + 1);
        *//* Log.e("tagData", last);*//*
        workingData.clear();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getHashTagList(last)
                        , getCompositeDisposable(), getHashTagSearch, this);
    }*/

    public void goForPayment() {
//        getActivity().startActivity(new Intent(getActivity(), DetailPostPaymentActivity.class));
        Intent intent = new Intent(activityMain, DetailPostPaymentActivity.class);
        String datalist = selectedTags.toString();
        String seperatedString = datalist.substring(1, datalist.length() - 1).replaceAll(" ", ",");
        intent.putExtra("Type", getArguments().getString("Type"));
        intent.putExtra("tagUserID", getArguments().getString("tagUserID"));
        intent.putExtra("lat", String.valueOf(lat));
        intent.putExtra("lng", String.valueOf(lng));
        intent.putExtra("distance", distance);
        intent.putExtra("amount", amountToPay);
        intent.putExtra("time", time);
        intent.putExtra("hashTag", seperatedString);
        intent.putExtra("isHighlight", mBinding.uiPublishDetailPosting.switchHighlight.isChecked());

        switch (postType) {
            case "Image":
                intent.putExtra("uploadImgUri", getArguments().getString("uploadImgUri"));
                if (getArguments().containsKey("originalImgUri"))
                    intent.putExtra("originalImgUri", getArguments().getString("originalImgUri"));
                break;

            case "Text":
                intent.putExtra("TextDesc", getArguments().getString("TextDesc"));
                intent.putExtra("TextTitle", getArguments().getString("TextTitle"));
                break;

            case "ImageWithText":
                intent.putExtra("uploadImgUri", getArguments().getString("uploadImgUri"));
                if (getArguments().containsKey("originalImgUri"))
                    intent.putExtra("originalImgUri", getArguments().getString("originalImgUri"));
                intent.putExtra("TextDesc", getArguments().getString("TextDesc"));
                intent.putExtra("TextTitle", getArguments().getString("TextTitle"));
                break;

            case "Video":
                intent.putExtra("Thumb", getArguments().getString("Thumb"));
                intent.putExtra("VideoFile", getArguments().getString("VideoFile"));
                intent.putExtra("caption", getArguments().getString("caption"));
                break;
        }
        startActivity(intent);
    }

    private void callApiSaveData() {
        String datalist = selectedTags.toString();
        String seperatedString = datalist.substring(1, datalist.length() - 1).replaceAll(" ", ",");
//        Toast.makeText(getActivity(), seperatedString, Toast.LENGTH_SHORT).show();
        switch (postType) {
            case "Image":
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                MultipartBody.Part originalImageBody = null;
                MultipartBody.Part body = null;

                byte[] uploadBytes = loadImageFromStorage(getArguments().getString("uploadImgUri"));
                RequestBody uploadRequestFile = RequestBody.create(MediaType.parse("multipart/form-data"), uploadBytes);
                body = MultipartBody.Part.createFormData("UploadPhoto", userId + "post_image.png", uploadRequestFile);

                if (getArguments().containsKey("originalImgUri")) {
                    byte[] bytes = loadImageFromStorage(getArguments().getString("originalImgUri"));
                    RequestBody originalRequestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    originalImageBody = MultipartBody.Part.createFormData("OriginalImage", userId + "original_post_image.png", originalRequestFile);
                }

                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(getActivity()).
                                        create(WebserviceBuilder.class).
                                        postDetailPhoto(body, originalImageBody, "", "", userId,
                                                getArguments().getString("tagUserID"), String.valueOf(lat), String.valueOf(lng), distance, time,
                                                seperatedString, mBinding.uiPublishDetailPosting.switchHighlight.isChecked(), false, "0")
                                , getCompositeDisposable(), postDetailPhoto, this);

                break;

            case "Text":
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                String desc = getArguments().getString("TextDesc", "");
                String tit = getArguments().getString("TextTitle", "");
                String descToSend = "";
                try {
                    descToSend = URLEncoder.encode(desc, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String titToSend = "";
                try {
                    titToSend = URLEncoder.encode(tit, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }


                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(getActivity()).
                                        create(WebserviceBuilder.class).
                                        postDetailText(descToSend, titToSend, userId, getArguments().getString("tagUserID"),
                                                String.valueOf(lat), String.valueOf(lng), distance, time, seperatedString,
                                                mBinding.uiPublishDetailPosting.switchHighlight.isChecked(), false, "0")
                                , getCompositeDisposable(), postDetailText, this);

                break;

            case "ImageWithText":
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                MultipartBody.Part originalImgBody = null;
                MultipartBody.Part uploadBody = null;

                byte[] uploadByteArray = loadImageFromStorage(getArguments().getString("uploadImgUri"));
                RequestBody uploadRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), uploadByteArray);
                uploadBody = MultipartBody.Part.createFormData("UploadPhoto", userId + "post_image.png", uploadRequestBody);

                if (getArguments().containsKey("originalImgUri")) {
                    byte[] bytes = loadImageFromStorage(getArguments().getString("originalImgUri"));
                    RequestBody originalRequestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    originalImgBody = MultipartBody.Part.createFormData("OriginalImage", userId + "original_post_image.png", originalRequestFile);
                }

                String description = getArguments().getString("TextDesc", "");
                String title = getArguments().getString("TextTitle", "");
                String descriptionToSend = "";
                try {
                    descriptionToSend = URLEncoder.encode(description, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String titleToSend = "";
                try {
                    titleToSend = URLEncoder.encode(title, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(getActivity()).
                                        create(WebserviceBuilder.class).
                                        postDetailPhoto(uploadBody, originalImgBody, descriptionToSend, titleToSend, userId,
                                                getArguments().getString("tagUserID"), String.valueOf(lat), String.valueOf(lng), distance, time,
                                                seperatedString, mBinding.uiPublishDetailPosting.switchHighlight.isChecked(), false, "0")
                                , getCompositeDisposable(), postDetailPhoto, this);
                break;

            case "Video":
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), loadImageFromStorage(getArguments().getString("Thumb")));
                MultipartBody.Part thumbBody = MultipartBody.Part.createFormData("UploadThumbnail", userId + "thumbnile.png", requestFile);
                File file = new File(getArguments().getString("VideoFile"));
                RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), file);
                MultipartBody.Part vFile = MultipartBody.Part.createFormData("UploadVideo", file.getName(), videoBody);

                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(getActivity()).
                                        create(WebserviceBuilder.class).
                                        postDetailVideo(vFile, thumbBody, getArguments().getString("caption"), userId,
                                                getArguments().getString("tagUserID"), String.valueOf(lat), String.valueOf(lng), distance,
                                                time, seperatedString, mBinding.uiPublishDetailPosting.switchHighlight.isChecked(), false, "0")
                                , getCompositeDisposable(), postDetailVideo, this);

                break;


        }

    }

    private byte[] loadImageFromStorage(String path) {
        try {
            File f = new File(path);
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
            ByteArrayOutputStream uploadStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, uploadStream);
            return uploadStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteImage(String path) {
        File fDelete = new File(path);
        if (fDelete.exists()) {
            if (fDelete.delete()) {
                Log.e("delete image : ", "file Deleted :" + path);
            } else {
                Log.e("delete image : ", "file not Deleted :" + path);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case postDetailPhoto:
                DetailPostPhotoData detailPostPhotoData = (DetailPostPhotoData) o;
                if (detailPostPhotoData.isSuccess()) {
                    deleteImage(getArguments().getString("uploadImgUri"));
                    if (getArguments().containsKey("originalImgUri"))
                        deleteImage(getArguments().getString("originalImgUri"));
                    Toast.makeText(getActivity(), detailPostPhotoData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    Intent intent = new Intent(getActivity(), MenuActivity.class);
                    intent.putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    activityMain.startActivity(intent);
                    activityMain.finish();
                    activityMain.overridePendingTransition(0, 0);
                } else {
                    Toast.makeText(getActivity(), detailPostPhotoData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                }

                break;

            case postDetailVideo:
                DetailPostVideoData detailPostVideoData = (DetailPostVideoData) o;
                if (detailPostVideoData.isSuccess()) {
                    deleteImage(getArguments().getString("Thumb"));
                    Toast.makeText(getActivity(), detailPostVideoData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    Intent intent = new Intent(getActivity(), MenuActivity.class);
                    intent.putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    activityMain.startActivity(intent);
                    activityMain.finish();
                    activityMain.overridePendingTransition(0, 0);
                } else {
                    Toast.makeText(getActivity(), detailPostVideoData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                }

                break;

            case postDetailText:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);

                DetailPostPhotoData detailPostTextData = (DetailPostPhotoData) o;
                if (detailPostTextData.isSuccess()) {
                    Toast.makeText(getActivity(), detailPostTextData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), MenuActivity.class);
                    intent.putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                    getActivity().overridePendingTransition(0, 0);
                } else {
                    Toast.makeText(getActivity(), detailPostTextData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;

            case getHashTagSearch:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                HashTagListModel hashTagListModel = (HashTagListModel) o;
                if (hashTagListModel.isSuccess()) {
                    spinnerArray = new String[hashTagListModel.getData().size()];
                    spinnerMap = new HashMap<Integer, String>();
                    for (int i = 0; i < hashTagListModel.getData().size(); i++) {
                        spinnerMap.put(i, String.valueOf(hashTagListModel.getData().get(i).getID()));
                        spinnerArray[i] = hashTagListModel.getData().get(i).getTag();

                    }
                    if (spinnerArray.length > 0) {
                        adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, spinnerArray);
                        mBinding.uiPublishDetailPosting.edtAddHashtags.setThreshold(1);
                        mBinding.uiPublishDetailPosting.edtAddHashtags.setAdapter(adapter);
                    }

                    break;
                }
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        Log.e(TAG, "onFailure: ", throwable);
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    public Drawable getThumb(String progress, View thumbView) {
        ((TextView) thumbView.findViewById(R.id.tvProgress)).setText(progress);

        thumbView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        Bitmap bitmap = Bitmap.createBitmap(thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        thumbView.layout(0, 0, thumbView.getMeasuredWidth(), thumbView.getMeasuredHeight());
        thumbView.draw(canvas);

        return new BitmapDrawable(getResources(), bitmap);
    }


    private DisposableObserver<TextViewTextChangeEvent> searchHashTagList() {

        return new DisposableObserver<TextViewTextChangeEvent>() {
            @Override
            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    Log.e("edtLengthBefore", "" + mBinding.uiPublishDetailPosting.edtAddHashtags.getText().length());
                    if (mBinding.uiPublishDetailPosting.edtAddHashtags.getText().length() > 1) {
                        String word = mBinding.uiPublishDetailPosting.edtAddHashtags.getText().toString();
                        String last = word.substring(word.lastIndexOf("#") + 1);
                        workingData.clear();
                        mAdapter = new HashTagDataAdapter(getActivity(), workingData);
                        Log.e("tagData", "last" + last);
                        Log.e("edtLength", "" + mBinding.uiPublishDetailPosting.edtAddHashtags.getText().length());
                        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(true);
                        disposable.add(apiService.getHashTagList(last).
                                subscribeOn(Schedulers.io()).
                                observeOn(AndroidSchedulers.mainThread()).
                                subscribeWith(new DisposableSingleObserver<HashTagListModel>() {
                                    @Override
                                    public void onSuccess(HashTagListModel hashTagListModel) {

                                        if (hashTagListModel.isSuccess() && hashTagListModel.getData().size() > 0) {
                                            //  *//* workingData.clear();
                                            workingData.add(hashTagListModel);
                                            // mAdapter.notifyDataSetChanged();
                                            mAdapter = new HashTagDataAdapter(getActivity(), workingData);
                                            mBinding.uiPublishDetailPosting.edtAddHashtags.setThreshold(1);
                                            mBinding.uiPublishDetailPosting.edtAddHashtags.setAdapter(mAdapter);
                                            mBinding.uiPublishDetailPosting.edtAddHashtags.setTokenizer(new SpaceTokenizer());
                                            spinnerArray = new String[hashTagListModel.getData().size()];
                                            spinnerMap = new HashMap<Integer, String>();
                                            for (int i = 0; i < hashTagListModel.getData().size(); i++) {
                                                spinnerMap.put(i, String.valueOf(hashTagListModel.getData().get(i).getID()));
                                                spinnerArray[i] = hashTagListModel.getData().get(i).getTag();


                                            }
                                            if (spinnerArray.length > 0) {
                                                adapter = new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, spinnerArray);
                                                mBinding.uiPublishDetailPosting.edtAddHashtags.setThreshold(1);
                                                mBinding.uiPublishDetailPosting.edtAddHashtags.setAdapter(adapter);
                                                mAdapter.notifyDataSetChanged();
                                            }

                                            mBinding.loading.progressBar.setVisibility(View.GONE);
                                            disableScreen(false);
                                        } else {
                                            mBinding.loading.progressBar.setVisibility(View.GONE);
                                            disableScreen(false);
                                        }
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.d("fail", e.toString());
                                        mBinding.loading.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                    }
                                }));
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
        //... your stuff
    }

    @Override
    public void onPlaceSelected(Place place) {
        Log.e("on Place Selected", "Place Selected: " + place.getAddress() + "" + place.getLatLng().latitude + "" + place.getLatLng().longitude);
        lat = place.getLatLng().latitude;
        lng = place.getLatLng().longitude;
        mBinding.uiPublishDetailPosting.txtSelectLocation.setText(place.getAddress());
    }

    @Override
    public void onError(Status status) {

    }

    @Override
    public void onMenualSelectLocationListner() {
        if (App_pref.getAuthorizedUser(getActivity()).getData().getAdminUserRoleName().equalsIgnoreCase("Individual")) {
            if (Utils.isNetworkAvailable(getContext())) {
                try {
                    AutocompleteFilter filter = new AutocompleteFilter.Builder()
                            .setTypeFilter(Place.TYPE_COUNTRY)
                            .build();
                    Intent intent = new PlaceAutocomplete.IntentBuilder
                            (PlaceAutocomplete.MODE_FULLSCREEN)
//                                .setBoundsBias(BOUNDS_MOUNTAIN_VIEW)
                            .setFilter(filter)
                            .build(getActivity());
                    getActivity().startActivityForResult(intent, REQUEST_SELECT_PLACE);
                } catch (GooglePlayServicesRepairableException |
                        GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getContext(), R.string.check_your_network_connection, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onCurrentSelectLocationListner() {
//getCurrentLocation()
        buildLocationSettingsRequest();
    }

    protected void buildLocationSettingsRequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void getLocation() {
       /* GPSTracker gpsTracker = new GPSTracker(getActivity());
        Location gpsLocation = gpsTracker.getLocation();
        if (gpsLocation != null) {
            getData(gpsLocation);
            Log.e("Get LatLong", "getLocation: " + gpsLocation.getLongitude() + " " + gpsLocation.getLongitude());
        }*/
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                getCurrentLocation();

            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            getCurrentLocation();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();
                    // permission was granted, yay! Do the
                    // location-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void getCurrentLocation() {
        fusedLocationProviderClass.getLocation();
    }

    @Override
    public void onLocationChange(Location location) {
        Log.e("on Place Selected", "Place Selected: "  + "" + location.getLatitude() + "" + location.getLongitude());
        lat = location.getLatitude();
        lng = location.getLongitude();
        Geocoder geocoder;
        List<Address> addresses;
        String address="";
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            address = addresses.get(0).getAddressLine(0)+addresses.get(0).getLocality()+" "+
                    addresses.get(0).getAdminArea()+" "+ addresses.get(0).getCountryName()+" "+addresses.get(0).getPostalCode();
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }


        mBinding.uiPublishDetailPosting.txtSelectLocation.setText(address);
    }
}
