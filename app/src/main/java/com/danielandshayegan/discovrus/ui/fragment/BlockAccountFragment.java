package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.BlockAccountAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentBlockAccountBinding;
import com.danielandshayegan.discovrus.models.BlockAccountListData;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.HiddenDataListModel;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveBlockUserDetails;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveHiddenUserDetails;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getBlockList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getCommentList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BlockAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlockAccountFragment extends BaseFragment implements View.OnClickListener, SingleCallback, BlockAccountAdapter.UnblockAccountListner {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    int userId;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentBlockAccountBinding mBinding;
    BlockAccountAdapter mAdapter;

    public BlockAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BlockAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlockAccountFragment newInstance() {
        BlockAccountFragment fragment = new BlockAccountFragment();
        Bundle args = new Bundle();
      /*  args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_block_account, container, false);
        mBinding.uiAccount.toolbar.txtTitle.setText("Blocked Accounts");
        getBlockListData();
        mBinding.uiAccount.toolbar.imgBack.setOnClickListener(this);
        return mBinding.getRoot();
    }

    private void getBlockListData() {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }

    }

    private void callAPI() {
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getBlockList(userId)
                        , getCompositeDisposable(), getBlockList, this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getBlockList:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                BlockAccountListData blockAccountListData = (BlockAccountListData) o;
                if (blockAccountListData.isSuccess()) {
                    if (blockAccountListData.getData() != null && blockAccountListData.getData().size() > 0) {
                        mBinding.uiAccount.recylerView.setVisibility(View.VISIBLE);
                        mBinding.uiAccount.txtNoData.setVisibility(View.GONE);
                        mAdapter = new BlockAccountAdapter(blockAccountListData.getData(), getContext(), this);
                        mBinding.uiAccount.recylerView.setAdapter(mAdapter);
                    } else {
                        mBinding.uiAccount.recylerView.setVisibility(View.GONE);
                        mBinding.uiAccount.txtNoData.setText("No blocked accounts yet.");
                        mBinding.uiAccount.txtNoData.setVisibility(View.VISIBLE);
                    }
                }

                break;
            case SaveBlockUserDetails:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse hiddenUserDetails = (CommonApiResponse) o;
                if (hiddenUserDetails.isSuccess()) {
                    getBlockListData();
                } else {
                    Toast.makeText(getActivity(), hiddenUserDetails.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUnblockAccountListner(BlockAccountListData.DataBean dataBean) {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callHiddenAPI(dataBean);
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void callHiddenAPI(BlockAccountListData.DataBean dataBean) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                SaveBlockUserDetails(dataBean.getUserID(), userId)
                        , getCompositeDisposable(), SaveBlockUserDetails, this);
    }
}
