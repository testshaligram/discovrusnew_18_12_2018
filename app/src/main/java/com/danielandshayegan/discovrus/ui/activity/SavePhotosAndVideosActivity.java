package com.danielandshayegan.discovrus.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivitySettingBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.NotificationSettingFragment;
import com.danielandshayegan.discovrus.ui.fragment.SavePhotosAndVideosFragment;

public class SavePhotosAndVideosActivity extends BaseActivity {
    ActivitySettingBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setting);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, SavePhotosAndVideosFragment.newInstance(), "savephotosandvideosFragment")
                .commit();
    }
}