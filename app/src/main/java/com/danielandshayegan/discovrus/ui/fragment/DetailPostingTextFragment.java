package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.system.ErrnoException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daasuu.mp4compose.filter.GlFilter;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.BitmapUtils;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostingTextBinding;
import com.danielandshayegan.discovrus.models.ConnectPeopleData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.DetailPostConnectPeopleActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingImageWithTextActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingTextActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MultipartBody;

import static android.app.Activity.RESULT_OK;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postPhoto;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;
import static com.danielandshayegan.discovrus.ui.fragment.RegisterFragment.SELECT_CATEGORY;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPostingTextFragment extends BaseFragment {

    FragmentDetailPostingTextBinding mBinding;
    View view;
    int userId;
    Bitmap bitmap;

    Activity activityMain;
    private WebserviceBuilder apiService;
    List<PostListData.Post> postDataList = new ArrayList<>();
    public HashMap<String, ConnectPeopleData.DataBean> selectedPeopleList = new HashMap<>();

    // load native image filters library
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    public DetailPostingTextFragment() {
        // Required empty public constructor
    }

    public static DetailPostingTextFragment newInstance() {
        return new DetailPostingTextFragment();
    }

    public static DetailPostingTextFragment newInstance(Bundle extras) {
        DetailPostingTextFragment fragment = new DetailPostingTextFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_posting_text, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);

        setClicks();

        return view;
    }

    public void setClicks() {
        mBinding.uiTextDetailPosting.txtConnectPeople.setOnClickListener(view -> {
//            if (finalImage != null)
            if (selectedPeopleList.size() > 0)
                mActivity.startActivityForResult(new Intent(mActivity, DetailPostConnectPeopleActivity.class).putExtra("peopleList", selectedPeopleList), 601);
            else
                mActivity.startActivityForResult(new Intent(mActivity, DetailPostConnectPeopleActivity.class), 601);
//            else
//                Toast.makeText(mActivity, "Please select image.", Toast.LENGTH_LONG).show();
        });

        mBinding.uiTextDetailPosting.imgBack.setOnClickListener(v -> {
            DetailPostingActivity.postingActivity.onBackPressed();
        });

        mBinding.uiTextDetailPosting.btnNext.setOnClickListener(v -> {
            String tagUserID = "";
            if (selectedPeopleList.size() > 0) {
                Iterator myVeryOwnIterator = selectedPeopleList.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    String key = (String) myVeryOwnIterator.next();
                    ConnectPeopleData.DataBean value = selectedPeopleList.get(key);
                    if (tagUserID.equalsIgnoreCase(""))
                        tagUserID = "" + value.getUserId();
                    else
                        tagUserID = tagUserID + "," + value.getUserId();
                }
            }
            getActivity().startActivity(new Intent(getActivity(), DetailPostingTextActivity.class).putExtra("tagUserID",tagUserID));

        });

        mBinding.uiTextDetailPosting.imgSelect.setOnClickListener(view -> {
            String tagUserID = "";
            if (selectedPeopleList.size() > 0) {
                Iterator myVeryOwnIterator = selectedPeopleList.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    String key = (String) myVeryOwnIterator.next();
                    ConnectPeopleData.DataBean value = selectedPeopleList.get(key);
                    if (tagUserID.equalsIgnoreCase(""))
                        tagUserID = "" + value.getUserId();
                    else
                        tagUserID = tagUserID + "," + value.getUserId();
                }
            }
            getActivity().startActivity(new Intent(getActivity(), DetailPostingTextActivity.class).putExtra("tagUserID",tagUserID));
        });

        mBinding.uiTextDetailPosting.layoutSelectType.setOnClickListener(view -> {
            mBinding.uiTextDetailPosting.layoutSelectType.setVisibility(View.GONE);
            mBinding.uiTextDetailPosting.strip.setVisibility(VISIBLE);
        });

        mBinding.uiTextDetailPosting.layoutExtend.setOnClickListener(view1 -> {
            mBinding.uiTextDetailPosting.layoutSelectType.setVisibility(VISIBLE);
            mBinding.uiTextDetailPosting.strip.setVisibility(View.GONE);
        });

        mBinding.uiTextDetailPosting.imgTypeImage.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingImageFragment.newInstance(), "DetailPostingImageFragment")
                    .commit();
        });

        mBinding.uiTextDetailPosting.imgTypeImageWithText.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingTextFragment.newInstance(), "DetailPostingImageWithTextFragment")
                    .commit();
        });

        mBinding.uiTextDetailPosting.imgTypeText.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingTextFragment.newInstance(), "DetailPostingTextFragment")
                    .commit();
        });

        mBinding.uiTextDetailPosting.imgTypeVideo.setOnClickListener(view -> {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, DetailPostingVideoFragment.newInstance(), "DetailPostingVideoFragment")
                    .commit();
        });

    }

   /* public boolean validation() {

        boolean value = true;
        if (mBinding.uiImageFilter.edtTitle.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter title", Toast.LENGTH_SHORT).show();
            value = false;
        }
        if (mBinding.uiImageFilter.edtDesc.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter description", Toast.LENGTH_SHORT).show();
            value = false;
        }

        return value;

    }*/

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        Log.e("onactivityResult", "done");
        if (reqCode == 601 && resultCode == RESULT_OK) {
            selectedPeopleList = (HashMap<String, ConnectPeopleData.DataBean>) data.getSerializableExtra("peopleList");
            mBinding.uiTextDetailPosting.txtConnectPeople.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_post_connect_people_selected, 0);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
    }

}
