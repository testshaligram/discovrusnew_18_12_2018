package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.BusinessProfileReviewAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentBusinessProfileListBinding;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.ReferenceListByUserId;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessProfileReviewFragment extends BaseFragment {

    FragmentBusinessProfileListBinding mBinding;
    View view;
    private WebserviceBuilder apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    int userId, logInUserId;
    public static BusinessProfileReviewFragment managementFragment;
    private LinearLayoutManager layoutManager;
    private int currentPage = 1, pageSize = 5;
    private boolean isLoading = true;
    private boolean isLastPage = true;
    public static final int PAGE_SIZE = 1;
    BusinessProfileReviewAdapter businessReviewsAdapter;
    List<ReferenceListByUserId.DataBean> referenceList = new ArrayList<>();

    public BusinessProfileReviewFragment() {
        // Required empty public constructor
    }

    public static BusinessProfileReviewFragment newInstance() {
        return new BusinessProfileReviewFragment();
    }

    public static BusinessProfileReviewFragment newInstance(Bundle extras) {
        BusinessProfileReviewFragment fragment = new BusinessProfileReviewFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_business_profile_list, container, false);
        view = mBinding.getRoot();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        userId = BusinessProfileFragment.businessProfileFragment.userId;
        logInUserId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        managementFragment = this;

        layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.recyclerBusinessProfile.setLayoutManager(layoutManager);

        businessReviewsAdapter = new BusinessProfileReviewAdapter(mContext);

        mBinding.recyclerBusinessProfile.setItemAnimator(new SlideInUpAnimator());
        mBinding.recyclerBusinessProfile.setAdapter(businessReviewsAdapter);
        mBinding.recyclerBusinessProfile.addOnScrollListener(recycleReload);

        currentPage = 1;
        callAPI();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public void callAPI() {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
//            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getReferencesByUserId(userId, logInUserId, "b", currentPage, pageSize).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<ReferenceListByUserId>() {
                    @Override
                    public void onSuccess(ReferenceListByUserId value) {
                        if (value.isSuccess()) {
                            isLoading = false;
//                            isLastPage = false;
                            businessReviewsAdapter.clear();
                            referenceList.clear();
                            referenceList.addAll(value.getData());
                            if (referenceList.size() > 0)
                                isLastPage = false;
                            else
                                isLastPage = true;
                            if (currentPage == 1)
                            businessReviewsAdapter.clear();
                            businessReviewsAdapter.addAll(referenceList);
                            businessReviewsAdapter.notifyDataSetChanged();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                        } else {
                            Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
//                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));
    }

    public void saveUserReference(String referenceIds) {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
//            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.saveUserReference(logInUserId, userId, referenceIds).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<CommonApiResponse>() {
                    @Override
                    public void onSuccess(CommonApiResponse value) {
                        if (value.isSuccess()) {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                            currentPage = 1;
                            callAPI();
                        } else {
                            Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
//                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));

    }

    private RecyclerView.OnScrollListener recycleReload = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            if (!isLoading && !isLastPage) {

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems();
                }
            }
        }
    };

    private void loadMoreItems() {
        isLoading = true;
        currentPage += 1;
        callAPI();
    }

}


