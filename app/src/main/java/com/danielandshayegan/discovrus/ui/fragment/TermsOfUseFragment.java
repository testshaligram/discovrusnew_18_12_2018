package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.databinding.FragmentTermsOfUseBinding;


import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.models.DataPolicyModel;
import com.danielandshayegan.discovrus.models.TermsOfuseModel;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getPolicy;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TermsOfUseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TermsOfUseFragment extends BaseFragment implements SingleCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentTermsOfUseBinding mBinding;

    public TermsOfUseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TermsOfUseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TermsOfUseFragment newInstance() {
        TermsOfUseFragment fragment = new TermsOfUseFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms_of_use, container, false);
        mBinding.uiPolicy.toolbar.txtTitle.setText(R.string.str_terms);
        mBinding.uiPolicy.toolbar.imgBack.setOnClickListener(view -> {
            getActivity().finish();
        });
        getDataPolicy();
        // Inflate the layout for this fragment
        return mBinding.getRoot();
    }


    private void getDataPolicy() {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }

    }

    private void callAPI() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getTermsOfUse()
                        , getCompositeDisposable(), getPolicy, this);
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames){
            case getPolicy:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                TermsOfuseModel TermsOfuseModel= (TermsOfuseModel) o;
                if(TermsOfuseModel.isSuccess()){
                    mBinding.uiPolicy.txtInstruction.setText(TermsOfuseModel.getData().get(0).getTermsUse());
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }}
