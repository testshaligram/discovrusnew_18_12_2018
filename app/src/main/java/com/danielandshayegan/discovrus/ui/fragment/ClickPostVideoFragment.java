package com.danielandshayegan.discovrus.ui.fragment;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.MediaRouteButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Selection;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.ClickPostCommentAdapter;
import com.danielandshayegan.discovrus.adapter.LikeAdapter;
import com.danielandshayegan.discovrus.custome_veiws.OverlapItemDecoration;
import com.danielandshayegan.discovrus.databinding.FragmentClickPostVideoBinding;
import com.danielandshayegan.discovrus.dialog.OptionMenuDialog;
import com.danielandshayegan.discovrus.eventbus.EventDataObject;
import com.danielandshayegan.discovrus.eventbus.GlobalBus;
import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.models.ClickPostCommentData;
import com.danielandshayegan.discovrus.models.DeleteCommentData;
import com.danielandshayegan.discovrus.models.LikeListData;
import com.danielandshayegan.discovrus.models.PostCommentData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.SaveCommentLikeData;
import com.danielandshayegan.discovrus.models.SaveLikeData;
import com.danielandshayegan.discovrus.models.SavePostCommentReplyData;
import com.danielandshayegan.discovrus.models.VideoSaveStatePostDetail;
import com.danielandshayegan.discovrus.models.postDetailData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.LikesActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.DeepLinkHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;
import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deleteComment;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getCommentList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveCommentLike;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveComments;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveLike;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePostCommentReply;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClickPostVideoFragment extends BaseFragment implements ClickPostCommentAdapter.DeletePosCommentListner, SingleCallback, ClickPostCommentAdapter.LikeCommentListner {

    View view;
    public FragmentClickPostVideoBinding mBinding;
    ClickPostCommentAdapter clickPostCommentAdapter;
    List<ClickPostCommentData.DataBean> clickPostCommentList = new ArrayList<>();
    private WebserviceBuilder apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    public PostListData.Post dataBean;
    int userId, postId;


    SimpleExoPlayerView simpleExoPlayerView;
    ImageView thumbnailImageView;
    TextView castInfoTextView;
    View shutterView;
    SeekBar exoSeekBar;
    ImageButton exoPlayButton;
    ImageButton exoPauseButton;
    ImageButton replayImageButton;
    FrameLayout exoButtonsFrameLayout;
    LinearLayout controlViewLinearLayout;
    MediaRouteButton mediaRouteButton;
    AspectRatioFrameLayout aspectRatioFrameLayout;
    ProgressBar progressBar;
    EventDataObject.VideoRotation videoRotation;
    // endregion

    // region Member Variables
    private String videoUrl;
    private VideoSaveStatePostDetail videoSavedState;
    private SimpleExoPlayer exoPlayer;
    private ClickPostVideoFragment.PlaybackState playbackState;
    private long currentPosition = 0;
    String userImagePath;
    boolean isRefresh;
    public static ClickPostVideoFragment postVideoFragment;
    public String myUrl;
    private static final String host = "api.linkedin.com";
    private static final String shareUrl = "https://" + host + "/v1/people/~/shares";
    ClickPostCommentData.DataBean replyToData;
    boolean isReplyClicked = false;
    ArrayList<LikeListData.DataBean> likedList = new ArrayList<>();
    int commentId = 0;

    private ExoPlayer.EventListener exoPlayerEventListener = new ExoPlayer.EventListener() {
        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            switch (playbackState) {
                case ExoPlayer.STATE_BUFFERING:
                    progressBar.setVisibility(View.VISIBLE);
                    break;

                case ExoPlayer.STATE_READY:
                    replayImageButton.setVisibility(View.GONE);
                    exoButtonsFrameLayout.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    break;
                case ExoPlayer.STATE_ENDED:
                    exoButtonsFrameLayout.setVisibility(View.GONE);
                    replayImageButton.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {

        }

        @Override
        public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {

        }

        @Override
        public void onPositionDiscontinuity(int reason) {

        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

        }

        @Override
        public void onSeekProcessed() {

        }
    };

    private PlaybackControlView.VisibilityListener playbackControlViewVisibilityListener = new PlaybackControlView.VisibilityListener() {
        @Override
        public void onVisibilityChange(int visibility) {
            int orientation = getContext().getResources().getConfiguration().orientation;
            switch (orientation) {
                case ORIENTATION_PORTRAIT:
                    videoRotation = new EventDataObject.VideoRotation(false);
                    GlobalBus.getBus().post(videoRotation);
                    if (visibility == View.GONE) {
                        hidePortraitSystemUI();
                    } else {
                        showPortraitSystemUI();
                    }
                    break;
                case ORIENTATION_LANDSCAPE:
                    videoRotation = new EventDataObject.VideoRotation(true);
                    GlobalBus.getBus().post(videoRotation);
                    if (visibility == View.GONE) {
                        hideLandscapeSystemUI();
                    } else {
                        showLandscapeSystemUI();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private SeekBar.OnSeekBarChangeListener seekBarOnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                currentPosition = (long) (exoPlayer.getDuration() * (progress / 1000.0D));
                updateLocalVideoPosition(currentPosition);


            } else {
                playbackState = PlaybackState.PLAYING;
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };


    public ClickPostVideoFragment() {
        // Required empty public constructor
    }

    public static ClickPostVideoFragment newInstance() {
        return new ClickPostVideoFragment();
    }

    public static ClickPostVideoFragment newInstance(Bundle extras) {
        ClickPostVideoFragment fragment = new ClickPostVideoFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_click_post_video, container, false);
        view = mBinding.getRoot();

        postVideoFragment = ClickPostVideoFragment.this;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.uiClickPost.rvClickPost.setLayoutManager(mLayoutManager);
        mBinding.uiClickPost.rvClickPost.setItemAnimator(new DefaultItemAnimator());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mBinding.uiClickPost.rvLikeList.setLayoutManager(layoutManager);
        mBinding.uiClickPost.rvLikeList.addItemDecoration(new OverlapItemDecoration());

       /* clickPostCommentAdapter = new ClickPostCommentAdapter(clickPostCommentList, getActivity(), this);
        mBinding.uiClickPost.rvClickPost.setAdapter(clickPostCommentAdapter);*/
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        userImagePath = App_pref.getAuthorizedUser(getActivity()).getData().getUserImagePath();
        if (userImagePath != null) {
            Picasso.with(getActivity()).
                    load(ApiClient.WebService.imageUrl + userImagePath).
                    into(mBinding.uiClickPost.imgUserProfile);
            Log.i("imgPath", ApiClient.WebService.imageUrl + userImagePath);
        }
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        mBinding.uiClickPost.imgBack.setOnClickListener(v -> {
            if (exoPlayer != null)
                exoPlayer.release();
            Intent intent = new Intent();
            intent.putExtra("isRefresh", isRefresh);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        });
        mBinding.uiClickPost.imgCancelReply.setOnClickListener(view -> {
            isReplyClicked = false;
            mBinding.uiClickPost.edtComment.setVisibility(View.VISIBLE);
            mBinding.uiClickPost.imgCancelReply.setVisibility(View.GONE);
            mBinding.uiClickPost.edtCommentReply.setVisibility(View.GONE);
            mBinding.uiClickPost.txtReplyToUserName.setVisibility(View.GONE);
            mBinding.uiClickPost.edtCommentReply.setText("");
        });


        mBinding.uiClickPost.tvLikeText.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), LikesActivity.class);
            intent.putExtra("postDetails", dataBean);
            startActivity(intent);
        });

        thumbnailImageView = view.findViewById(R.id.exo_thumbnail);
        shutterView = view.findViewById(R.id.exo_shutter);
        exoSeekBar = view.findViewById(R.id.exo_progress);
        exoPlayButton = view.findViewById(R.id.exo_play);
        exoPauseButton = view.findViewById(R.id.exo_pause);
        replayImageButton = view.findViewById(R.id.exo_replay);
        exoButtonsFrameLayout = view.findViewById(R.id.exo_btns_fl);
        controlViewLinearLayout = view.findViewById(R.id.control_view_ll);
        mediaRouteButton = view.findViewById(R.id.mrb);
        aspectRatioFrameLayout = view.findViewById(R.id.exo_content_frame);
        simpleExoPlayerView = view.findViewById(R.id.simple_exo_player_view);
        progressBar = view.findViewById(R.id.progress_bar);

        mBinding.uiClickPost.btnCommentPost.setOnClickListener(v -> {
            if (isReplyClicked) {
                if (mBinding.uiClickPost.edtCommentReply.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter reply", Toast.LENGTH_SHORT).show();
                } else {
                    isRefresh = true;
                    mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                    mBinding.uiClickPost.txtReplyToUserName.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    String commentToPass = "";
                    try {
                        commentToPass = URLEncoder.encode(mBinding.uiClickPost.edtCommentReply.getText().toString().replace("Reply to @" + replyToData.getUserName() + " ", ""), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            savePostCommentReply(postId, userId, commentToPass, commentId)
                                    , getCompositeDisposable(), savePostCommentReply, this);
                }
            } else {
                if (validationForCommentPost()) {
                    isRefresh = true;
                    mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    String commentToPass = "";
                    try {
                        commentToPass = URLEncoder.encode(mBinding.uiClickPost.edtComment.getText().toString(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            postComment(String.valueOf(0), String.valueOf(postId), String.valueOf(userId), commentToPass)
                                    , getCompositeDisposable(), saveComments, this);
                }
            }

        });

        mBinding.uiClickPost.edtComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.uiClickPost.scrollCommentPost.scrollTo(0, mBinding.uiClickPost.constUserDetails.getTop());

            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            exoSeekBar.setThumbTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
        }
        exoSeekBar.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY));
        setClickListeners();
        mBinding.uiClickPost.imgFav.setOnClickListener(v -> {
            isRefresh = true;
            boolean isLikes = !dataBean.isLiked();
            if (isLikes) {
                //  int likeCount = Integer.parseInt(mBinding.uiClickPost.txtFav.getText().toString().replaceAll("k","").replaceAll("m",""));
                int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiClickPost.txtFav.getText().toString()));
                int addCount = likeCount + 1;
                String countToPrint = Utils.convertLikes(addCount);
                mBinding.uiClickPost.txtFav.setText(countToPrint);
                dataBean.setLiked(isLikes);
                mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
            } else {
                // int likeCount = Integer.parseInt(mBinding.uiClickPost.txtFav.getText().toString().replaceAll("k","").replaceAll("m",""));
                int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiClickPost.txtFav.getText().toString()));
                if (likeCount != 0) {
                    int minusLikeCount = likeCount - 1;
                    String countToPrint = Utils.convertLikes(minusLikeCount);
                    mBinding.uiClickPost.txtFav.setText(countToPrint);
                    dataBean.setLiked(isLikes);
                    mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));
                }
            }
            getLikeApiCall(isLikes);
        });

       /* mBinding.uiClickPost.simpleExoPlayerView.setOnLongClickListener(view -> {
            mBinding.uiClickPost.layoutOptions.setVisibility(View.VISIBLE);
            return false;
        });*/

        mBinding.uiClickPost.imgToolDown.setOnClickListener(view -> {
            isRefresh = true;
            OptionMenuDialog optionMenuDialog = new OptionMenuDialog();
            Bundle bundle = new Bundle();
            bundle.putParcelable("postDetails", dataBean);
            bundle.putString("from", "ClickPostVideoFragment");
            optionMenuDialog.setArguments(bundle);
            optionMenuDialog.show(getFragmentManager(), "option menu");
        });

        mBinding.uiClickPost.simpleExoPlayerView.setOnTouchListener((view, motionEvent) -> gestureDetector.onTouchEvent(motionEvent));

        mBinding.uiClickPost.layoutOptions.setOnClickListener(view -> mBinding.uiClickPost.layoutOptions.setVisibility(View.GONE));

        mBinding.uiClickPost.imgCopyLink.setOnClickListener(view -> {
            ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Image post url", myUrl);
            clipboard.setPrimaryClip(clip);
            mBinding.uiClickPost.btnCopiedLink.setVisibility(View.VISIBLE);
            new Handler().postDelayed(() -> mBinding.uiClickPost.btnCopiedLink.setVisibility(View.INVISIBLE), 2000);
        });

        mBinding.uiClickPost.imgAppShare.setOnClickListener(view -> shareDeepLink(myUrl));

        mBinding.uiClickPost.imgLinkedInShare.setOnClickListener(view -> {
            linkedInShare();
        });

        return view;
        // initilizeCommentData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getActivity().getApplicationContext()).onActivityResult(getActivity(), requestCode, resultCode, data);

        // Add this line to your existing onActivityResult() method
        DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
        deepLinkHelper.onActivityResult(getActivity(), requestCode, resultCode, data);

        if (requestCode == 505 && resultCode == RESULT_OK) {
            boolean isRefresh = data.getBooleanExtra("isRefresh", false);
            if (isRefresh) {
                callApiComments();
            }

        }
    }

    final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {
        public void onLongPress(MotionEvent e) {
            Log.e("onLongPress", "Long press detected");
            mBinding.uiClickPost.layoutOptions.setVisibility(View.VISIBLE);
        }
    });

    public void shareDeepLink(String deepLink) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link");
        intent.putExtra(Intent.EXTRA_TEXT, deepLink);

        startActivity(intent);
    }

    public void linkedInShare() {
        if (!LISessionManager.getInstance(getActivity()).getSession().isValid()) {
            //if not valid then start authentication
            LISessionManager.getInstance(getActivity().getApplicationContext()).init(getActivity(), Scope.build(Scope.W_SHARE)//pass the build scope here
                    , new AuthListener() {
                        @Override
                        public void onAuthSuccess() {
                            // Authentication was successful. You can now do
                            // other calls with the SDK.
//                                Toast.makeText(getActivity(), "Successfully authenticated with LinkedIn.", Toast.LENGTH_SHORT).show();

                            //on successful authentication fetch basic profile data of user
                            shareToLinkedIn();
                        }

                        @Override
                        public void onAuthError(LIAuthError error) {
                            // Handle authentication errors
                            Log.e("TAG", "Auth Error :" + error.toString());
//                                Toast.makeText(getActivity(), "Failed to authenticate with LinkedIn. Please try again.", Toast.LENGTH_SHORT).show();
                        }
                    }, true);//if TRUE then it will show dialog if
            // any device has no LinkedIn app installed to download app else won't show anything
        } else {
//                Toast.makeText(getActivity(), "You are already authenticated.", Toast.LENGTH_SHORT).show();

            //if user is already authenticated fetch basic profile data for user
            shareToLinkedIn();
        }
    }

    public void shareToLinkedIn() {

        String shareJsonText = "{ \n" +
                "\"comment\":\"" + "Discovrus" + "\"," +
                "\"visibility\":{ " +
                "\"code\":\"anyone\"" +
                "}," +
                "\"content\":{ " +
                "\"title\":\" " + dataBean.getTitle() + " \"," +
                "\"description\":\" " + dataBean.getDescription() + " \"," +
//                        "\"submitted-url\":\"https://www.numetriclabz.com/android-linkedin integrationlogin-and-make-userprofile\"," +
                "\"submitted-url\":\" " + myUrl + " \"," +
                "\"submitted-image-url\":\"" + ApiClient.WebService.imageUrl + dataBean.getPostPath() + "\"" + "}" +
                "}";

        Log.i("linkedinData", dataBean.getTitle() + dataBean.getDescription() + ApiClient.WebService.imageUrl + dataBean.getThumbFileName());
        Log.i("linkedinData", shareJsonText);
        // Call the APIHealper.getInstance method and pass the current context.
        APIHelper apiHelper = APIHelper.getInstance(getActivity());
               /* We need to share text call apiHelper.postRequest method.
                   This Method post the text on your linkedin profile.If successful,
                   it will return reposne from Linkedin containing json string.
                   In this Json string,containing statuscode: 200 and shared url.
                */
        apiHelper.postRequest(getActivity(),
                shareUrl, shareJsonText, new ApiListener() {
                    @Override
                    public void onApiSuccess(ApiResponse apiResponse) {
                        Log.e("Response", apiResponse.toString());
                        Toast.makeText(getActivity(),
                                "Shared Sucessfully", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onApiError(LIApiError error) {
                        Log.e("Response", error.toString());
                        Toast.makeText(getActivity(),
                                error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    public boolean validationForCommentPost() {
        boolean value = true;
        if (mBinding.uiClickPost.edtComment.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter comment", Toast.LENGTH_SHORT).show();
            value = false;
        }
        return value;

    }

    public void getLikeApiCall(boolean isLikes) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveLike(postId, dataBean.getUserID(), isLikes, userId)
                        , getCompositeDisposable(), saveLike, this);
    }

    public void setClickListeners() {
        exoPlayButton.setOnClickListener(view -> {
            playbackState = PlaybackState.PLAYING;
            exoPlayButton.setVisibility(View.GONE);
            resumeLocalVideo();
        });

        exoPauseButton.setOnClickListener(view1 -> {
            playbackState = PlaybackState.PAUSED;
            exoPlayButton.setVisibility(View.GONE);
            pauseLocalVideo();
        });

        replayImageButton.setOnClickListener(view1 -> {
            replayImageButton.setVisibility(View.GONE);
            exoButtonsFrameLayout.setVisibility(View.VISIBLE);
            updateLocalVideoPosition(0);
        });

    }

    public void simpleMethod() {
        exoPlayer.release();
        Intent intent = new Intent();
        intent.putExtra("isRefresh", isRefresh);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (videoUrl != null) {
            //resumeLocalVideo();
            if (videoUrl != null) {
                setUpExoPlayer();
                setUpSimpleExoPlayerView();

                exoSeekBar.setOnSeekBarChangeListener(seekBarOnSeekBarChangeListener);
                resumeLocalVideo();
            }

            int orientation = view.getResources().getConfiguration().orientation;

            switch (orientation) {
                case ORIENTATION_PORTRAIT:
                    videoRotation = new EventDataObject.VideoRotation(false);
                    GlobalBus.getBus().post(videoRotation);
                    showPortraitSystemUI();
                    break;
                case ORIENTATION_LANDSCAPE:
                    videoRotation = new EventDataObject.VideoRotation(true);
                    GlobalBus.getBus().post(videoRotation);
                    showLandscapeSystemUI();
                    break;
                default:
                    break;
            }

            VideoSaveStatePostDetail videoSavedState = getVideoSavedState();
            if (videoSavedState != null && !TextUtils.isEmpty(videoSavedState.getVideoUrl())) {
                long currentPosition = videoSavedState.getCurrentPosition();
                videoUrl = videoSavedState.getVideoUrl();
                playbackState = videoSavedState.getPlaybackState();

                boolean autoPlay = false;
                switch (playbackState) {
                    case PLAYING:
                        autoPlay = true;
                        break;
                    case PAUSED:
                        autoPlay = false;
                        break;
                    default:
                        break;
                }

                playLocalVideo(currentPosition, autoPlay);
                updateLocalVideoVolume(videoSavedState.getCurrentVolume());
            }
            playLocalVideo(0, true);
        }

        if (dataBean != null) {
            //callApiComments();
            //getLikeListData();

            String likeCount = Utils.convertLikes(dataBean.getLikeCount());
            String commentCount = Utils.convertLikes(dataBean.getCommentCount());
            String viewCount = Utils.convertLikes(dataBean.getViewCount());
            mBinding.uiClickPost.txtFav.setText(likeCount);
            mBinding.uiClickPost.txtComment.setText(commentCount);
            mBinding.uiClickPost.txtView.setText(viewCount);
            mBinding.uiClickPost.imgView.setImageResource(R.drawable.ic_green_view);

            if (dataBean.isLiked())
                mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
            else
                mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));

            if (dataBean.isCommented())
                mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
            else
                mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));
            final Uri myLink = Uri.parse("https://discovrus.page.link/ugkL");
            Uri.Builder builder = new Uri.Builder();
            builder.scheme("https")
                    .authority("discovrus.page.link")
                    .path("/").appendQueryParameter("link", myLink.toString()).appendQueryParameter("apn", "com.danielandshayegan.discovrus")
                    .appendQueryParameter("ibi", "com.sgit.discovrus").appendQueryParameter("afl", "www.google.com")
                    .appendQueryParameter("postId", String.valueOf(dataBean.getPostId())).appendQueryParameter("st", dataBean.getTitle())
                    .appendQueryParameter("si", ApiClient.WebService.imageUrl + dataBean.getThumbFileName())
                    .appendQueryParameter("type", "postVideo");
            myUrl = builder.build().toString();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        // Retain this fragment across configuration changes.
        setRetainInstance(true);
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);

        if (this.getArguments().containsKey("postDetails")) {
            getPostDetails();
            playbackState = PlaybackState.PLAYING;
        } else {
//        callGetPostData(this.getArguments().getString("postId"));
        }


    }

    public void callGetPostData(String postid) {
        try {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getPostDetail(postid).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<postDetailData>() {
                    @Override
                    public void onSuccess(postDetailData value) {
                        if (value.isSuccess()) {
                            if (value.getData().get(0) != null) {
                                dataBean = value.getData().get(0);
                                postId = dataBean.getPostId();
                                getLikeListData();
                                callApiComments();

                                if (dataBean.getPostPath() != null) {
                                    videoUrl = ApiClient.WebService.imageUrl + dataBean.getPostPath();
                                }

                                playbackState = PlaybackState.PLAYING;
                                String likeCount = Utils.convertLikes(dataBean.getLikeCount());
                                String commentCount = Utils.convertLikes(dataBean.getCommentCount());
                                String viewCount = Utils.convertLikes(dataBean.getViewCount());
                                mBinding.uiClickPost.txtFav.setText(likeCount);
                                mBinding.uiClickPost.txtComment.setText(commentCount);
                                mBinding.uiClickPost.txtView.setText(viewCount);
                                mBinding.uiClickPost.imgView.setImageResource(R.drawable.ic_green_view);
                                if (dataBean.isLiked())
                                    mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
                                else
                                    mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));

                                if (dataBean.isCommented())
                                    mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
                                else
                                    mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));
                                final Uri myLink = Uri.parse("https://discovrus.page.link/ugkL");
                                Uri.Builder builder = new Uri.Builder();
                                builder.scheme("https")
                                        .authority("discovrus.page.link")
                                        .path("/").appendQueryParameter("link", myLink.toString()).appendQueryParameter("apn", "com.danielandshayegan.discovrus")
                                        .appendQueryParameter("ibi", "com.sgit.discovrus").appendQueryParameter("afl", "www.google.com")
                                        .appendQueryParameter("postId", String.valueOf(dataBean.getPostId())).appendQueryParameter("st", dataBean.getTitle())
                                        .appendQueryParameter("si", ApiClient.WebService.imageUrl + dataBean.getThumbFileName())
                                        .appendQueryParameter("type", "postVideo");
                                myUrl = builder.build().toString();

                                if (videoUrl != null) {
                                    setUpExoPlayer();
                                    setUpSimpleExoPlayerView();

                                    exoSeekBar.setOnSeekBarChangeListener(seekBarOnSeekBarChangeListener);
                                    resumeLocalVideo();
                                }

                                int orientation = view.getResources().getConfiguration().orientation;

                                switch (orientation) {
                                    case ORIENTATION_PORTRAIT:
                                        videoRotation = new EventDataObject.VideoRotation(false);
                                        GlobalBus.getBus().post(videoRotation);
                                        showPortraitSystemUI();
                                        break;
                                    case ORIENTATION_LANDSCAPE:
                                        videoRotation = new EventDataObject.VideoRotation(true);
                                        GlobalBus.getBus().post(videoRotation);
                                        showLandscapeSystemUI();
                                        break;
                                    default:
                                        break;
                                }

                                VideoSaveStatePostDetail videoSavedState = getVideoSavedState();
                                if (videoSavedState != null && !TextUtils.isEmpty(videoSavedState.getVideoUrl())) {
                                    long currentPosition = videoSavedState.getCurrentPosition();
                                    videoUrl = videoSavedState.getVideoUrl();
                                    playbackState = videoSavedState.getPlaybackState();

                                    boolean autoPlay = false;
                                    switch (playbackState) {
                                        case PLAYING:
                                            autoPlay = true;
                                            break;
                                        case PAUSED:
                                            autoPlay = false;
                                            break;
                                        default:
                                            break;
                                    }

                                    playLocalVideo(currentPosition, autoPlay);
                                    updateLocalVideoVolume(videoSavedState.getCurrentVolume());
                                }
                                playLocalVideo(0, true);

                            }
                        } else {
                            Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (!TextUtils.isEmpty(videoUrl)) {
            VideoSaveStatePostDetail videoSavedState = new VideoSaveStatePostDetail();
            videoSavedState.setVideoUrl(videoUrl);
            videoSavedState.setCurrentPosition(exoPlayer.getCurrentPosition());
            videoSavedState.setCurrentVolume(exoPlayer.getVolume());
            videoSavedState.setPlaybackState(playbackState);
            setVideoSavedState(videoSavedState);
        }
        removeListeners();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        exoPlayer.release();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (videoUrl != null) {
            setUpExoPlayer();
            setUpSimpleExoPlayerView();

            exoSeekBar.setOnSeekBarChangeListener(seekBarOnSeekBarChangeListener);
        }

        int orientation = view.getResources().getConfiguration().orientation;

        switch (orientation) {
            case ORIENTATION_PORTRAIT:
                videoRotation = new EventDataObject.VideoRotation(false);
                GlobalBus.getBus().post(videoRotation);
                showPortraitSystemUI();
                break;
            case ORIENTATION_LANDSCAPE:
                videoRotation = new EventDataObject.VideoRotation(true);
                GlobalBus.getBus().post(videoRotation);
                showLandscapeSystemUI();
                break;
            default:
                break;
        }

        VideoSaveStatePostDetail videoSavedState = getVideoSavedState();
        if (videoSavedState != null && !TextUtils.isEmpty(videoSavedState.getVideoUrl())) {
            long currentPosition = videoSavedState.getCurrentPosition();
            videoUrl = videoSavedState.getVideoUrl();
            playbackState = videoSavedState.getPlaybackState();

            boolean autoPlay = false;
            switch (playbackState) {
                case PLAYING:
                    autoPlay = true;
                    break;
                case PAUSED:
                    autoPlay = false;
                    break;
                default:
                    break;
            }

            playLocalVideo(currentPosition, autoPlay);
            updateLocalVideoVolume(videoSavedState.getCurrentVolume());
        }

//        playLocalVideo(0, true);

    }

    private void getPostDetails() {
        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
            dataBean = (PostListData.Post) bundle.getParcelable("postDetails");
            if (dataBean != null) {
                postId = dataBean.getPostId();
                getLikeListData();
                callApiComments();
                if (dataBean.getPostPath() != null) {
                    videoUrl = ApiClient.WebService.imageUrl + dataBean.getPostPath();
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (exoPlayer != null)
            exoPlayer.release();
    }

    public void callApiComments() {
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        disableScreen(true);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getCommentList(postId, userId)
                        , getCompositeDisposable(), getCommentList, this);
    }

    @Override
    public void onCommentDeleted(ClickPostCommentData.DataBean clickpostData) {
        isRefresh = true;
        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                deleteComment(String.valueOf(clickpostData.getCommentId()))
                        , getCompositeDisposable(), deleteComment, this);
    }

    @Override
    public void onCommentReplyClick(ClickPostCommentData.DataBean clickpostData) {
        replyToData = clickpostData;
        mBinding.uiClickPost.edtComment.setText("");
        isReplyClicked = true;
        commentId = clickpostData.getCommentId();
        mBinding.uiClickPost.edtComment.setVisibility(View.GONE);
        mBinding.uiClickPost.txtReplyToUserName.setVisibility(View.VISIBLE);
        mBinding.uiClickPost.txtReplyToUserName.setText(Html.fromHtml("<font color='#9E9CAC'> Reply to @" + clickpostData.getUserName() + " </font>"));
        Selection.setSelection(mBinding.uiClickPost.edtCommentReply.getText(), mBinding.uiClickPost.edtCommentReply.getText().length());
        mBinding.uiClickPost.edtCommentReply.setVisibility(View.VISIBLE);
        mBinding.uiClickPost.imgCancelReply.setVisibility(View.VISIBLE);
        mBinding.uiClickPost.scrollCommentPost.scrollTo(0, 0);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case getCommentList:

                disableScreen(false);
                clickPostCommentList.clear();
                ClickPostCommentData clickPostCommentData = (ClickPostCommentData) o;
                if (clickPostCommentData.isSuccess()) {

                    clickPostCommentList.addAll(clickPostCommentData.getData());
//                    clickPostCommentAdapter.notifyDataSetChanged();
                    clickPostCommentAdapter = new ClickPostCommentAdapter(postId, clickPostCommentList, getActivity(), this, this);
                    mBinding.uiClickPost.rvClickPost.setAdapter(clickPostCommentAdapter);
                    if (clickPostCommentData.getData().size() > 0) {
                        int commentSize = clickPostCommentData.getData().size();
                        for (int i = 0; i < commentSize - 1; i++) {

                            int commentUserId = clickPostCommentData.getData().get(i).getCommentUserID();
                            if (commentUserId == userId) {
                                mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
                                break;
                            }
                        }
                    }
                }
                break;
            case saveLike:
                SaveLikeData saveLikeData = (SaveLikeData) o;
                if (saveLikeData.isSuccess()) {
                    getLikeListData();
                } else {
                    Toast.makeText(getActivity(), saveLikeData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
            case deleteComment:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteCommentData deleteCommentData = (DeleteCommentData) o;
                if (deleteCommentData.isSuccess()) {
                    int commentCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiClickPost.txtComment.getText().toString()));
                    int addCount = commentCount - 1;
                    if (addCount == 0)
                        mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinding.uiClickPost.txtComment.setText(countToPrint);
                    //  Toast.makeText(getActivity(), deleteCommentData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(getActivity(), "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case saveComments:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                PostCommentData postCommentData = (PostCommentData) o;
                if (postCommentData.isSuccess()) {
                    mBinding.uiClickPost.edtComment.setText("");
                    int commentCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiClickPost.txtComment.getText().toString()));
                    int addCount = commentCount + 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinding.uiClickPost.txtComment.setText(countToPrint);
                    mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
                    callApiComments();
                }
                break;
            case savePostCommentReply:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                SavePostCommentReplyData postCommentReplyData = (SavePostCommentReplyData) o;
                if (postCommentReplyData.isSuccess()) {
                    mBinding.uiClickPost.edtComment.setText("");
                    mBinding.uiClickPost.edtCommentReply.setText("");
                    mBinding.uiClickPost.edtComment.setVisibility(View.VISIBLE);
                    mBinding.uiClickPost.edtCommentReply.setVisibility(View.GONE);
                    mBinding.uiClickPost.txtReplyToUserName.setVisibility(View.GONE);
                    mBinding.uiClickPost.imgCancelReply.setVisibility(View.GONE);
                    callApiComments();
                }
                break;
            case saveCommentLike:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                SaveCommentLikeData saveCommentLikeData = (SaveCommentLikeData) o;
                if (saveCommentLikeData.isSuccess()) {
                    //  Toast.makeText(getActivity(), saveCommentLikeData.getMessage().get(0), Toast.LENGTH_SHORT).show();

                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        disableScreen(false);
        Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_SHORT).show();

    }

    private void setUpExoPlayer() {
        // Create a default TrackSelector
        TrackSelector trackSelector = createTrackSelector();

        // Create a default LoadControl
        LoadControl loadControl = new DefaultLoadControl();

        // Create the player
        exoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector, loadControl);
        resumeLocalVideo();

        exoPlayer.addListener(exoPlayerEventListener);
    }

    private void setUpThumbnail() {
        String thumbnailUrl = "";

        if (!TextUtils.isEmpty(thumbnailUrl)) {
            Glide.with(getActivity())
                    .load(thumbnailUrl)
//                                .placeholder(R.drawable.ic_placeholder)
//                                .error(R.drawable.ic_error)
                    .into(thumbnailImageView);
        }
    }

    private void setUpSimpleExoPlayerView() {
        simpleExoPlayerView.setPlayer(exoPlayer);
        simpleExoPlayerView.setControllerVisibilityListener(playbackControlViewVisibilityListener);
    }

    // This snippet hides the system bars.
    private void hidePortraitSystemUI() {
        final View decorView = getActivity().getWindow().getDecorView();

        // Set the IMMERSIVE flag.
        // Set the content_navigation to appear under the system bars so that the content_navigation
        // doesn't resize when the system bars hide and show.
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN// hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content_navigation appear under the system bars.
    private void showPortraitSystemUI() {
        final View decorView = getActivity().getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    // This snippet hides the system bars.
    private void hideLandscapeSystemUI() {
        final View decorView = getActivity().getWindow().getDecorView();

        // Set the IMMERSIVE flag.
        // Set the content_navigation to appear under the system bars so that the content_navigation
        // doesn't resize when the system bars hide and show.
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    // This snippet shows the system bars. It does this by removing all the flags
    // except for the ones that make the content_navigation appear under the system bars.
    private void showLandscapeSystemUI() {
        final View decorView = getActivity().getWindow().getDecorView();

        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

    }

    public void setVideoSavedState(VideoSaveStatePostDetail videoSavedState) {
        this.videoSavedState = videoSavedState;
    }

    public VideoSaveStatePostDetail getVideoSavedState() {
        return videoSavedState;
    }

    private TrackSelector createTrackSelector() {
        // Create a default TrackSelector
        // Measures bandwidth during playback. Can be null if not required.
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        return trackSelector;
    }

    private MediaSource getMediaSource(String videoUrl) {
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        // Produces DataSource instances through which media data is loaded.
//        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), "VirtualManager"), bandwidthMeter);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(ApplicationClass.getInstance().getApplicationContext(), Util.getUserAgent(ApplicationClass.getInstance().getApplicationContext(), "VirtualManager"), bandwidthMeter);
        // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        // This is the MediaSource representing the media to be played.
        MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(videoUrl),
                dataSourceFactory, extractorsFactory, null, null);
        // VirtualManagers the video indefinitely.
//        VirtualManageringMediaSource VirtualManageringSource = new VirtualManageringMediaSource(mediaSource);
        return mediaSource;
    }

    private void removeListeners() {
        if (exoPlayer != null) {
            exoPlayer.removeListener(exoPlayerEventListener);
            simpleExoPlayerView.setControllerVisibilityListener(null);
            exoSeekBar.setOnSeekBarChangeListener(null);
        }
    }

    private void playLocalVideo(long position, boolean autoPlay) {
        updateLocalVideoPosition(position);
        // Prepare the player with the source.
        if (videoUrl != null) {
            exoPlayer.prepare(getMediaSource(videoUrl));
            if (!autoPlay)
                pauseLocalVideo();
        }
    }

    private void updateLocalVideoPosition(long position) {
        exoPlayer.seekTo(position);
    }


    private void resumeLocalVideo() {
        exoPlayer.setPlayWhenReady(true);
    }


    private void pauseLocalVideo() {
        exoPlayer.setPlayWhenReady(false);
    }


    private void updateLocalVideoVolume(float volume) {
        exoPlayer.setVolume(volume);
    }

    @Override
    public void onLikeComment(ClickPostCommentData.DataBean clickPostLike) {
        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        int commentId = clickPostLike.getCommentId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveCommentLike(commentId, userId, postId)
                        , getCompositeDisposable(), saveCommentLike, this);
    }

    // endregion

    // region Inner Classes

    /**
     * indicates whether we are doing a local or a remote playback
     */
    public enum PlaybackLocation {
        LOCAL,
        REMOTE
    }

    /**
     * List of various states that we can be in
     */
    public enum PlaybackState {
        PLAYING, PAUSED, BUFFERING, IDLE
    }

    private void getLikeListData() {
        if (Utils.isNetworkAvailable(getContext())) {

            Utils.getLikeListData(getActivity(), dataBean.getPostId(), new CallbackTask() {
                @Override
                public void onFail(Object object) {
                }

                @Override
                public void onSuccess(Object object) {
                    LikeListData followersUserListData = (LikeListData) object;

                    if (followersUserListData.isSuccess()) {
                        if (followersUserListData.getData() != null) {
                            likedList.clear();
                            likedList = (ArrayList<LikeListData.DataBean>) followersUserListData.getData();
                            if (followersUserListData.getData().size() > 3) {
                                mBinding.uiClickPost.rvLikeList.setVisibility(View.VISIBLE);
                                /*RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mBinding.uiClickPost.tvLikeText.getLayoutParams();
                                params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.rvLikeList);

                                mBinding.uiClickPost.tvLikeText.setLayoutParams(params);*/
                                mBinding.uiClickPost.tvLikeText.setVisibility(View.VISIBLE);
                                mBinding.uiClickPost.rvLikeList.setAdapter(new LikeAdapter(getActivity(), likedList, dataBean));
                                mBinding.uiClickPost.tvLikeText.setText(" " + getActivity().getString(R.string.and) + " " + (followersUserListData.getData().size() - 3) + " " + getActivity().getString(R.string.others) + " " + getActivity().getString(R.string.liked));
                            } else if (followersUserListData.getData().size() == 0) {
                                mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                                mBinding.uiClickPost.tvLikeText.setVisibility(View.GONE);
                            } else {
                                mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                                mBinding.uiClickPost.tvLikeText.setVisibility(View.VISIBLE);
                                mBinding.uiClickPost.tvLikeText.setText(followersUserListData.getData().size() + " " + getActivity().getString(R.string.liked));
                            }
                        } else {
                            mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                            mBinding.uiClickPost.tvLikeText.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                }
            });


            //       callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

/*    private void getLikeListData() {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void callAPI() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getLikeListData(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), dataBean.getPostId())
                        , getCompositeDisposable(), getLikeListData, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                LikeListData followersUserListData = (LikeListData) o;

                                if (followersUserListData.isSuccess()) {
                                    if (followersUserListData.getData() != null && followersUserListData.getData().size() > 0) {

                                        if (followersUserListData.getData().size() > 3) {
                                            mBinding.uiClickPost.rvLikeList.setVisibility(View.VISIBLE);
                                            mBinding.uiClickPost.rvLikeList.setAdapter(new LikeAdapter(followersUserListData.getData()));
                                            mBinding.uiClickPost.tvLikeText.setText(" " + getActivity().getString(R.string.and) + " " + (followersUserListData.getData().size() - 3) + " " + getActivity().getString(R.string.others) + " " + getActivity().getString(R.string.liked));
                                        } else {
                                            mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                                            mBinding.uiClickPost.tvLikeText.setText(followersUserListData.getData().size() + " " + getActivity().getString(R.string.liked));
                                        }
                                    } else {
                                        mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

                            }
                        });
    }

    class LikeAdapter extends RecyclerView.Adapter<LikeAdapter.MyViewHolder> {

        private ArrayList<LikeListData.DataBean> likedList = new ArrayList<>();

        LikeAdapter(List<LikeListData.DataBean> data) {
            likedList = (ArrayList<LikeListData.DataBean>) data;
        }

        @NonNull
        @Override
        public LikeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutLikeListBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_like_list, parent, false);
            LikeAdapter.MyViewHolder holder = new LikeAdapter.MyViewHolder(mBinder);
            return holder;
        }

        @Override
        public int getItemCount() {
            return 3;
        }

        @Override
        public void onBindViewHolder(@NonNull LikeAdapter.MyViewHolder holder, int position) {
            Picasso.with(getActivity()).
                    load(likedList.get(position).getUserImagePath()).
                    into(holder.binding.imgUser);

            holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), LikesActivity.class);
                    intent.putExtra("postDetails", dataBean);
                    startActivity(intent);
                }
            });
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            LayoutLikeListBinding binding;

            public MyViewHolder(LayoutLikeListBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }*/
}
