package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.databinding.FragmentTwoFactorAuthBinding;


import com.danielandshayegan.discovrus.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TwoFactorAuthFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TwoFactorAuthFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentTwoFactorAuthBinding mBinding;

    public TwoFactorAuthFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment TwoFactorAuthFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TwoFactorAuthFragment newInstance() {
        TwoFactorAuthFragment fragment = new TwoFactorAuthFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_two_factor_auth, container, false);
        mBinding.uiTwoFector.seetingToolbar.txtTitle.setText(R.string.str_title_two_way);
        mBinding.uiTwoFector.settingList.btnActivate.setOnClickListener(view -> {
            Toast.makeText(getContext(), "Activate", Toast.LENGTH_SHORT).show();
        });
        mBinding.uiTwoFector.seetingToolbar.imgBack.setOnClickListener(view -> {
            getActivity().finish();
        });
        // Inflate the layout for this fragment
        return mBinding.getRoot();
    }

}
