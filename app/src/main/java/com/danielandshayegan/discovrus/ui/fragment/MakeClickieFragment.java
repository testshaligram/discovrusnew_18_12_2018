package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.ClickiesListAdapter;
import com.danielandshayegan.discovrus.adapter.MyClickiesListAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentClickiesBinding;
import com.danielandshayegan.discovrus.databinding.FragmentMakeClickieBinding;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.ClickiesActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostPublishActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingActivity;
import com.danielandshayegan.discovrus.ui.activity.MakeClickieActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.utils.LayoutToImage;
import com.danielandshayegan.discovrus.utils.Utils;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postPhoto;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveUserClickie;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class MakeClickieFragment extends BaseFragment implements SingleCallback {

    FragmentMakeClickieBinding mBinding;
    View view;
    int userId;

    Activity activityMain;
    private WebserviceBuilder apiService;
    MultipartBody.Part body;
    Bitmap bitmap;

    public MakeClickieFragment() {
        // Required empty public constructor
    }

    public static MakeClickieFragment newInstance() {
        return new MakeClickieFragment();
    }

    public static MakeClickieFragment newInstance(Bundle extras) {
        MakeClickieFragment fragment = new MakeClickieFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_make_clickie, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);

        setData();
        setClicks();

        return view;
    }

    public void setData() {
        String imgPath = getArguments().getString("Image", "");
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.user_placeholder)
                .error(R.drawable.clickie_1);

        Glide.with(mContext)
                .load(ApiClient.WebService.imageUrl + imgPath)
                .apply(options)
                .into(mBinding.uiMakeClickie.imgClickie);

    }

    public void setClicks() {
        mBinding.uiMakeClickie.imgBack.setOnClickListener(v -> {
            MakeClickieActivity.clickiesActivity.onBackPressed();
        });

        mBinding.uiMakeClickie.btnNext.setOnClickListener(v -> {
            if (mBinding.uiMakeClickie.edtClickieText.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(activityMain, "Please enter text on clickie", Toast.LENGTH_LONG).show();
            } else {
                mBinding.uiMakeClickie.edtClickieText.setCursorVisible(false);
                LayoutToImage layoutToImage = new LayoutToImage(activityMain, mBinding.uiMakeClickie.frame);
                bitmap = layoutToImage.convert_layout();
                /*String bitmapPath = MediaStore.Images.Media.insertImage(mContext.getContentResolver(), bitmap,"title", null);
                Uri bitmapUri = Uri.parse(bitmapPath);*/
                if (mBinding.uiMakeClickie.checkbox.isChecked()) {
                    saveImageToGallery();
                } else {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    Intent intent = new Intent();
                    intent.putExtra("imageByteArray", byteArray);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    mActivity.finish();
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void saveImageToGallery() {
        Dexter.withActivity(getActivity()).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] byteArray = stream.toByteArray();
                            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), byteArray);
                            body = MultipartBody.Part.createFormData("UserClickieImage", userId + "clickie_image.png", requestFile);
                            callApiSaveData();
                        } else {
                            Toast.makeText(getActivity(), "Permissions are not granted!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    public void callApiSaveData() {
        mBinding.loading.progressBar.setVisibility(VISIBLE);
        disableScreen(true);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveUserClickie(userId, body)
                        , getCompositeDisposable(), saveUserClickie, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case saveUserClickie:
                CommonApiResponse apiResponse = (CommonApiResponse) o;
                if (apiResponse.isSuccess()) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    body = null;
                    Intent intent = new Intent();
                    intent.putExtra("imageByteArray", byteArray);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    mActivity.finish();
                } else {
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    Toast.makeText(getActivity(), apiResponse.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }
}
