package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.inputmethodservice.Keyboard;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.knifetexteditor.KnifeText;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostingImageWithTextEditorBinding;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostingTextEditorBinding;
import com.danielandshayegan.discovrus.interfaces.KeyboardHeightObserver;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.DetailPostPublishActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingTextActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPostingTextEditorFragment extends BaseFragment {

    FragmentDetailPostingTextEditorBinding mBinding;
    View view;
    int userId;
    int titleFontCount = 0, descFontCount = 0;
    public Uri mImageUri;

    Activity activityMain;
    List<PostListData.Post> postDataList = new ArrayList<>();

    // load native image filters library
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    public DetailPostingTextEditorFragment() {
        // Required empty public constructor
    }

    public static DetailPostingTextEditorFragment newInstance() {
        return new DetailPostingTextEditorFragment();
    }

    public static DetailPostingTextEditorFragment newInstance(Bundle extras) {
        DetailPostingTextEditorFragment fragment = new DetailPostingTextEditorFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_posting_text_editor, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();
//        changeKeyboardHeight(50);

        mBinding.uiTexteditorDetailPosting.edtTitle.setSelection(mBinding.uiTexteditorDetailPosting.edtTitle.getEditableText().length());
        mBinding.uiTexteditorDetailPosting.edtDesc.setSelection(mBinding.uiTexteditorDetailPosting.edtDesc.getEditableText().length());

        setClicks();

        return view;
    }

    public void setClicks() {
        mBinding.uiTexteditorDetailPosting.imgBack.setOnClickListener(v -> {
            DetailPostingTextActivity.postingActivity.onBackPressed();
        });

        mBinding.uiTexteditorDetailPosting.btnNext.setOnClickListener(v -> {
            if (validation()) {
                String str = mBinding.uiTexteditorDetailPosting.edtTitle.toHtml();
                String strDesc = mBinding.uiTexteditorDetailPosting.edtDesc.toHtml();
                String stringTitle, stringDesc;
                switch (titleFontCount) {
                    case 1:
                        stringTitle = "<font face='dancing_script_regular'>" + str + "</font>";
                        break;
                    case 2:
                        stringTitle = "<font face='amaticsc_regular'>" + str + "</font>";
                        break;
                    case 3:
                        stringTitle = "<font face='great_vibes_regular'>" + str + "</font>";
                        break;
                    case 4:
                        stringTitle = "<font face='opificio_light_rounded'>" + str + "</font>";
                        break;
                    default:
                        stringTitle = str;
                        break;
                }
                switch (descFontCount) {
                    case 1:
                        stringDesc = "<font face='dancing_script_regular'>" + strDesc + "</font>";
                        break;
                    case 2:
                        stringDesc = "<font face='amaticsc_regular'>" + strDesc + "</font>";
                        break;
                    case 3:
                        stringDesc = "<font face='great_vibes_regular'>" + strDesc + "</font>";
                        break;
                    case 4:
                        stringDesc = "<font face='opificio_light_rounded'>" + strDesc + "</font>";
                        break;
                    default:
                        stringDesc = strDesc;
                        break;
                }

                startActivity(new Intent(activityMain, DetailPostPublishActivity.class)
                        .putExtra("Type", "Text")
                        .putExtra("TextTitle", stringTitle)
                        .putExtra("TextDesc", stringDesc)
                        .putExtra("tagUserID", getArguments().getString("tagUserID", "")));
            }

        });

        mBinding.uiTexteditorDetailPosting.font.setOnClickListener(view -> {
            if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus()) {
                if (titleFontCount == 4)
                    titleFontCount = 1;
                else
                    titleFontCount++;
            } else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus()) {
                if (descFontCount == 4)
                    descFontCount = 1;
                else
                    descFontCount++;
            }
            int count = 0;
            if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus())
                count = titleFontCount;
            else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus())
                count = descFontCount;

            switch (count) {
                case 1:
                    if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/dancing_script_regular.ttf"));
                    } else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/dancing_script_regular.ttf"));
                    }
                    break;
                case 2:
                    if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/amaticsc_regular.ttf"));
                    } else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/amaticsc_regular.ttf"));
                    }
                    break;
                case 3:
                    if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/great_vibes_regular.ttf"));
                    } else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/great_vibes_regular.ttf"));
                    }
                    break;
                case 4:
                    if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/opificio_light_rounded.ttf"));
                    } else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/opificio_light_rounded.ttf"));
                    }
                    break;
                default:
                    if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/avenir_roman.ttf"));
                    } else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/avenir_roman.ttf"));
                    }
                    break;
            }
        });

        mBinding.uiTexteditorDetailPosting.bold.setOnClickListener(view -> {
            if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus())
                mBinding.uiTexteditorDetailPosting.edtTitle.bold(!mBinding.uiTexteditorDetailPosting.edtTitle.contains(KnifeText.FORMAT_BOLD));
            else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus())
                mBinding.uiTexteditorDetailPosting.edtDesc.bold(!mBinding.uiTexteditorDetailPosting.edtDesc.contains(KnifeText.FORMAT_BOLD));
        });
        mBinding.uiTexteditorDetailPosting.italic.setOnClickListener(view -> {
            if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus())
                mBinding.uiTexteditorDetailPosting.edtTitle.italic(!mBinding.uiTexteditorDetailPosting.edtTitle.contains(KnifeText.FORMAT_ITALIC));
            else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus())
                mBinding.uiTexteditorDetailPosting.edtDesc.italic(!mBinding.uiTexteditorDetailPosting.edtDesc.contains(KnifeText.FORMAT_ITALIC));
        });
        mBinding.uiTexteditorDetailPosting.underline.setOnClickListener(view -> {
            if (mBinding.uiTexteditorDetailPosting.edtTitle.hasFocus())
                mBinding.uiTexteditorDetailPosting.edtTitle.underline(!mBinding.uiTexteditorDetailPosting.edtTitle.contains(KnifeText.FORMAT_UNDERLINED));
            else if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus())
                mBinding.uiTexteditorDetailPosting.edtDesc.underline(!mBinding.uiTexteditorDetailPosting.edtDesc.contains(KnifeText.FORMAT_UNDERLINED));
        });
        mBinding.uiTexteditorDetailPosting.bullet.setOnClickListener(view -> {
            if (mBinding.uiTexteditorDetailPosting.edtDesc.hasFocus())
                mBinding.uiTexteditorDetailPosting.edtDesc.bullet(!mBinding.uiTexteditorDetailPosting.edtDesc.contains(KnifeText.FORMAT_BULLET));
        });


    }


    public boolean validation() {
        boolean value = true;
        if (mBinding.uiTexteditorDetailPosting.edtTitle.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter title", Toast.LENGTH_SHORT).show();
            value = false;
        }
        if (mBinding.uiTexteditorDetailPosting.edtDesc.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter description", Toast.LENGTH_SHORT).show();
            value = false;
        }
        return value;

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
