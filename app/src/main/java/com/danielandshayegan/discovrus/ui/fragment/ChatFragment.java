package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.PaperClipListAdapter;
import com.danielandshayegan.discovrus.custome_veiws.touchhelper.SwipeAndDragHelper;
import com.danielandshayegan.discovrus.databinding.FragmentChatBinding;
import com.danielandshayegan.discovrus.models.PaperClippedListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;

import java.util.ArrayList;
import java.util.List;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getPaperClippedData;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment implements SingleCallback {

    View view;
    FragmentChatBinding mBinding;
    PaperClipListAdapter paperClipListAdapter;
    List<PaperClippedListData.DataBean> paperClippedData = new ArrayList<>();
    int loginUserId;
    public static ChatFragment chatFragment;
    LayoutInflater inflater;


    public ChatFragment() {
        // Required empty public constructor
    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    public static ChatFragment newInstance(Bundle extras) {
        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_chat, container, false);
        view = mBinding.getRoot();
        inflater = this.inflater;
        chatFragment = this;
        loginUserId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.uiChatList.rvRecentChat.setLayoutManager(mLayoutManager);
              //mBinding.uiChatList.rvRecentChat.setItemAnimator(new DefaultItemAnimator());

        callPaperClippedDataApi();

        return view;
    }

    private void callPaperClippedDataApi() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getPperCllipedData(loginUserId)
                        , getCompositeDisposable(), getPaperClippedData, this);
    }


    @Override
    public void onResume() {
        super.onResume();
        MenuActivity.fragmentView = 3;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case getPaperClippedData:


                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                paperClippedData.clear();
                PaperClippedListData paperClippedListData = (PaperClippedListData) o;
                if (paperClippedListData.isSuccess()) {

                    paperClippedData.addAll(paperClippedListData.getData());
//                    clickPostCommentAdapter.notifyDataSetChanged();
                    paperClipListAdapter = new PaperClipListAdapter(paperClippedData, getActivity());

                    SwipeAndDragHelper swipeAndDragHelper = new SwipeAndDragHelper(paperClipListAdapter);
                    ItemTouchHelper touchHelper = new ItemTouchHelper(swipeAndDragHelper);
                    paperClipListAdapter.setTouchHelper(touchHelper);
                    mBinding.uiChatList.rvRecentChat.setAdapter(paperClipListAdapter);

                    touchHelper.attachToRecyclerView(mBinding.uiChatList.rvRecentChat);


                }

                break;
        }

    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

    }

}
