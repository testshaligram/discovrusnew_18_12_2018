package com.danielandshayegan.discovrus.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivitySettingBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.AnalyticsFragment;

public class AnalyticsActivity extends BaseActivity {
    ActivitySettingBinding mBinding;
    AnalyticsFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setting);

        fragment = AnalyticsFragment.newInstance();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, fragment, "analyticsFragment")
                .commit();
    }

    public void handleClicks(View v)
    {
        fragment.handleClicks(v);
    }
}