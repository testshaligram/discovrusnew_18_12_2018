package com.danielandshayegan.discovrus.ui.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentAnalyticsBinding;
import com.danielandshayegan.discovrus.databinding.RowAnalyticsCardBinding;
import com.danielandshayegan.discovrus.models.AnalyticsData;
import com.danielandshayegan.discovrus.models.IdValueData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.MyMarkerView;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getAnalyticsData;

public class AnalyticsFragment extends BaseFragment {

    FragmentAnalyticsBinding binding;
    private int isMonthly;
    private int bgDrawables[] = {R.drawable.today_bg_1, R.drawable.today_bg_2, R.drawable.today_bg_3};
    private ArrayList<IdValueData> todayArray = new ArrayList<>();
    private AnalyticsData ad;

    public AnalyticsFragment() {
    }

    public static AnalyticsFragment newInstance() {
        AnalyticsFragment fragment = new AnalyticsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_analytics, container, false);
        binding.txtTitle.setText(getActivity().getString(R.string.analytics));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        binding.rvToday.setLayoutManager(layoutManager);

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        binding.chartMP.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                float x = e.getX();

                binding.ivChartSelected.setVisibility(View.VISIBLE);
                binding.group1.setVisibility(View.VISIBLE);
                binding.group2.setVisibility(View.VISIBLE);
                binding.group3.setVisibility(View.VISIBLE);

                if (isMonthly == 0) {
                    for (int i = 0; i < ad.getData().getDayWiseAnalyticDetails().size(); i++) {
                        if (x == i) {
                            binding.tvLikesCount.setText(String.valueOf(ad.getData().getDayWiseAnalyticDetails().get(i).getLikes()));
                            binding.tvCommentsCount.setText(String.valueOf(ad.getData().getDayWiseAnalyticDetails().get(i).getComments()));
                            binding.tvClicksCount.setText(String.valueOf(ad.getData().getDayWiseAnalyticDetails().get(i).getClicks()));
                            break;
                        }
                    }
                } else {
                    for (int i = 0; i < ad.getData().getMonthWiseAnalyticDetails().size(); i++) {
                        if (x == i) {
                            binding.tvLikesCount.setText(String.valueOf(ad.getData().getMonthWiseAnalyticDetails().get(i).getLikes()));
                            binding.tvCommentsCount.setText(String.valueOf(ad.getData().getMonthWiseAnalyticDetails().get(i).getComments()));
                            binding.tvClicksCount.setText(String.valueOf(ad.getData().getMonthWiseAnalyticDetails().get(i).getClicks()));
                            break;
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });

        callGetAnalyticsData();
        return binding.getRoot();
    }

    public void handleClicks(View v) {
        switch (v.getId()) {
            case R.id.tvDaily:
                isMonthly = 0;
                binding.tvDaily.setTextColor(ContextCompat.getColor(getActivity(), R.color.chart_orange_selected));
                binding.tvMonthly.setTextColor(ContextCompat.getColor(getActivity(), R.color.post_text_color));

                binding.ivChartSelected.setVisibility(View.GONE);
                binding.group1.setVisibility(View.GONE);
                binding.group2.setVisibility(View.GONE);
                binding.group3.setVisibility(View.GONE);

                callGetAnalyticsData();
                break;

            case R.id.tvMonthly:
                isMonthly = 1;
                binding.tvDaily.setTextColor(ContextCompat.getColor(getActivity(), R.color.post_text_color));
                binding.tvMonthly.setTextColor(ContextCompat.getColor(getActivity(), R.color.chart_orange_selected));

                binding.ivChartSelected.setVisibility(View.GONE);
                binding.group1.setVisibility(View.GONE);
                binding.group2.setVisibility(View.GONE);
                binding.group3.setVisibility(View.GONE);

                callGetAnalyticsData();
                break;
        }
    }

    private void callGetAnalyticsData() {
        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity()).
                        create(WebserviceBuilder.class).
                        getAnalyticsData(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), isMonthly)
                , getCompositeDisposable(), getAnalyticsData, new SingleCallback() {
                    @Override
                    public void onSingleSuccess(Object object, WebserviceBuilder.ApiNames apiNames) {
                        try {
                            ad = (AnalyticsData) object;
                            if (ad.isSuccess()) {
                                if (ad.getData() != null) {
                                    if (todayArray.isEmpty()) {
                                        IdValueData id;
                                        id = new IdValueData();
                                        id.setTitle(getActivity().getString(R.string.interactions));
                                        id.setValue(ad.getData().getInteractions());
                                        todayArray.add(id);

                                        id = new IdValueData();
                                        id.setTitle(getActivity().getString(R.string.followers_online));
                                        id.setValue(ad.getData().getFollowersOnline());
                                        todayArray.add(id);

                                        id = new IdValueData();
                                        id.setTitle(getActivity().getString(R.string.new_followers));
                                        id.setValue(ad.getData().getNewFollowers());
                                        todayArray.add(id);

                                        binding.rvToday.setAdapter(new AnalyticsAdapter());
                                    }

                                    ArrayList<Entry> values = new ArrayList<Entry>();
                                    IAxisValueFormatter xAxisFormatter;

                                    if (isMonthly == 0) {

                                        for (int i = 0; i < ad.getData().getDayWiseAnalyticDetails().size(); i++) {
                                            values.add(new Entry(i,
                                                    ad.getData().getDayWiseAnalyticDetails().get(i).getTotal(),
                                                    getResources().getDrawable(R.drawable.chart_data1)));
                                        }
                                        xAxisFormatter = new IAxisValueFormatter() {
                                            @Override
                                            public String getFormattedValue(float value, AxisBase axis) {
                                                return ad.getData().getDayWiseAnalyticDetails().get((int) value).getDayName();
                                            }
                                        };

                                    } else {
                                        for (int i = 0; i < ad.getData().getMonthWiseAnalyticDetails().size(); i++) {
                                            values.add(new Entry(i,
                                                    ad.getData().getMonthWiseAnalyticDetails().get(i).getTotal(),
                                                    getResources().getDrawable(R.drawable.chart_data1)));
                                        }
                                        xAxisFormatter = new IAxisValueFormatter() {
                                            @Override
                                            public String getFormattedValue(float value, AxisBase axis) {
                                                Calendar c = Calendar.getInstance();
                                                c.set(Calendar.DAY_OF_MONTH, 1);
                                                c.set(Calendar.MONTH, (ad.getData().getMonthWiseAnalyticDetails().get((int) value).getMonths()) - 1);
                                                return new SimpleDateFormat("MMMM", Locale.ENGLISH).format(c.getTime());
                                            }
                                        };

                                    }
                                    setUpChart(values, xAxisFormatter);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                        throwable.printStackTrace();
                    }
                });
    }

    class AnalyticsAdapter extends RecyclerView.Adapter<AnalyticsAdapter.MyViewHolder> {

        private LayoutInflater layoutInflater;

        public AnalyticsAdapter() {
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private final RowAnalyticsCardBinding binding;

            public MyViewHolder(final RowAnalyticsCardBinding itemBinding) {
                super(itemBinding.getRoot());
                this.binding = itemBinding;
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (layoutInflater == null) {
                layoutInflater = LayoutInflater.from(parent.getContext());
            }
            RowAnalyticsCardBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.row_analytics_card, parent, false);
            return new MyViewHolder(binding);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            try {
                holder.binding.clMain.setBackgroundResource(bgDrawables[position]);
                holder.binding.tvCounts.setText(String.valueOf(todayArray.get(position).getValue()));
                holder.binding.tvCountsTitle.setText(String.valueOf(todayArray.get(position).getTitle()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return todayArray.size();
        }
    }

    private void setUpChart(ArrayList<Entry> values, IAxisValueFormatter xAxisFormatter) {

        binding.chartMP.getAxisLeft().setGridColor(Color.parseColor("#90FFFFFF"));
        binding.chartMP.getAxisRight().setEnabled(false);
        binding.chartMP.setDragEnabled(false);
        binding.chartMP.setPinchZoom(false);
        binding.chartMP.setNoDataTextColor(ContextCompat.getColor(mContext, R.color.white));
        binding.chartMP.setDescription(null);
        binding.chartMP.getAxisLeft().setDrawLabels(false);
        binding.chartMP.getAxisRight().setDrawLabels(false);
        binding.chartMP.getLegend().setEnabled(false);
        binding.chartMP.setHighlightPerTapEnabled(true);
        binding.chartMP.setDoubleTapToZoomEnabled(false);

        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.layout_chart_item_selecteds);
        mv.setChartView(binding.chartMP); // For bounds control
        binding.chartMP.setMarker(mv);

        LineDataSet set1 = new LineDataSet(values, "");
        set1.setLineWidth(3f);
        set1.setHighLightColor(ContextCompat.getColor(getActivity(), R.color.chart_blue_selected));
        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set1.setCubicIntensity(0.25f);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1);
        LineData data = new LineData(dataSets);

        XAxis xAxis = binding.chartMP.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(ResourcesCompat.getFont(getActivity(), R.font.avenir_next_medium));
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setTextColor(Color.WHITE);

        xAxis.setValueFormatter(xAxisFormatter);
        xAxis.setLabelCount(values.size());

        Paint paint = binding.chartMP.getRenderer().getPaintRender();
        int height = 200;

        LinearGradient linGrad = new LinearGradient(0, 0, 0, height,
                getResources().getColor(R.color.chart_blue_selected),
                getResources().getColor(R.color.chart_light_blue),
                Shader.TileMode.REPEAT);
        paint.setShader(linGrad);

        binding.chartMP.setData(data);
        binding.chartMP.notifyDataSetChanged();
        binding.chartMP.invalidate();
    }
}