package com.danielandshayegan.discovrus.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentSavePhotosVideosBinding;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.RequestBody;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveGeneralSettings;

public class SavePhotosAndVideosFragment extends BaseFragment {

    FragmentSavePhotosVideosBinding binding;
    HashMap<String, Integer> settingsList = new HashMap<>();

    public SavePhotosAndVideosFragment() {
    }

    public static SavePhotosAndVideosFragment newInstance() {
        SavePhotosAndVideosFragment fragment = new SavePhotosAndVideosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_save_photos_videos, container, false);
        binding.txtTitle.setText(getActivity().getString(R.string.save_photos_videos));

        settingsList.put("LoginId", App_pref.getAuthorizedUser(getActivity()).getData().getUserId());

        setData();

        binding.swSavePhotos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                settingsList.put("SavePhotos", isChecked ? 1 : 0);
                callSetSettings();
            }
        });

        binding.swSaveVideos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                settingsList.put("SaveVideos", isChecked ? 1 : 0);
                callSetSettings();
            }
        });

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return binding.getRoot();
    }

    private void setData() {
        try {
            GeneralSettingsData nsData = Utils.getGeneralSettingsData(getActivity());

            if (nsData != null && nsData.isSuccess() && nsData.getData() != null) {
                binding.swSavePhotos.setChecked(nsData.getData().get(0).isSavePhotos());
                binding.swSaveVideos.setChecked(nsData.getData().get(0).isSaveVideos());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callSetSettings() {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), new JSONObject(settingsList).toString());

        ObserverUtil.subscribeToSingle(ApiClient.getClient(getActivity()).
                        create(WebserviceBuilder.class).
                        saveGeneralSettings(requestBody)
                , getCompositeDisposable(), saveGeneralSettings, new SingleCallback() {
                    @Override
                    public void onSingleSuccess(Object object, WebserviceBuilder.ApiNames apiNames) {
                        try {
                            if (object != null)
                                Utils.setGeneralSettingsData(getActivity(), (GeneralSettingsData) object);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                        throwable.printStackTrace();
                    }
                });
    }
}