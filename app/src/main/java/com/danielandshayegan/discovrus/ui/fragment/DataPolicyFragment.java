package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.adapter.FollowersAdapter;
import com.danielandshayegan.discovrus.adapter.HiddenAccountAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentDataPolicyBinding;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.DataPolicyModel;
import com.danielandshayegan.discovrus.models.FollowPeopleData;
import com.danielandshayegan.discovrus.models.FollowersUserListData;
import com.danielandshayegan.discovrus.models.HiddenDataListModel;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getFollowList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getHiddenList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getPolicy;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DataPolicyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DataPolicyFragment extends BaseFragment implements SingleCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentDataPolicyBinding mBinding;

    public DataPolicyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DataPolicyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DataPolicyFragment newInstance() {
        DataPolicyFragment fragment = new DataPolicyFragment();
     /*   Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_data_policy, container, false);
        mBinding.uiPolicy.toolbar.txtTitle.setText(R.string.str_data_policy);
        mBinding.uiPolicy.toolbar.imgBack.setOnClickListener(view -> {
            getActivity().finish();
        });
        getDataPolicy();
        // Inflate the layout for this fragment
        return mBinding.getRoot();
    }


    private void getDataPolicy() {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }

    }

    private void callAPI() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getDataPolicy()
                        , getCompositeDisposable(), getPolicy, this);
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames){
            case getPolicy:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DataPolicyModel dataPolicyModel= (DataPolicyModel) o;
                if(dataPolicyModel.isSuccess()){
                    mBinding.uiPolicy.txtInstruction.setText(dataPolicyModel.getData().get(0).getPolicy());
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
}
