package com.danielandshayegan.discovrus.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivityCommonIntentBinding;
import com.danielandshayegan.discovrus.ui.fragment.ConfirmEmailFragment;
import com.danielandshayegan.discovrus.ui.fragment.ForgotPasswordFragment;
import com.danielandshayegan.discovrus.ui.fragment.LoginFragment;
import com.danielandshayegan.discovrus.ui.fragment.MainFragment;
import com.danielandshayegan.discovrus.ui.fragment.RegisterFragment;


public class CommonIntentActivity extends AppCompatActivity {


    public ActivityCommonIntentBinding mBinding;
    private String intentFrgament;
    public static final String ACTIVITY_INTENT = "activityIntent";
    public static final String FRAGMENT_MAIN_MENU = "mainMenu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_common_intent);

        Intent intent = getIntent();
        if (null != intent) {
            intentFrgament = intent.getExtras().getString(ACTIVITY_INTENT);
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_intent);
            if (fragment == null) {
                switch (intentFrgament) {
                    case MainActivity.FRAGMENT_INDIVIDUAL:
                        fragment = RegisterFragment.newInstance(getIntent().getExtras());
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_intent, fragment, "")
                                .commit();
                        break;
                    case MainActivity.FRAGMENT_BUSINESS:
                        fragment = RegisterFragment.newInstance(getIntent().getExtras());
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_intent, fragment, "")
                                .commit();
                        break;
                    case MainActivity.FRAGMENT_LOGIN:
                        fragment = LoginFragment.newInstance(getIntent().getExtras());
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_intent, fragment, "")
                                .commit();
                        break;
                    case MainActivity.FRAGMENT_FORGOTPASS:
                        fragment = ForgotPasswordFragment.newInstance(getIntent().getExtras());
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_intent, fragment, "")
                                .commit();
                        break;
                    case MainActivity.FRAGMENT_CONFEMAIL:
                        fragment = ConfirmEmailFragment.newInstance(getIntent().getExtras());
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_intent, fragment, "")
                                .commit();
                        break;
                    case MainActivity.FRAGMENT_MAIN:
                        fragment = MainFragment.newInstance(getIntent().getExtras());
                        getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.frame_intent, fragment, "")
                                .commit();
                        break;

                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_intent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
//        overridePendingTransition(R.anim.slide_out_up, R.anim.stay);

        if (intentFrgament.equals(MainActivity.FRAGMENT_INDIVIDUAL) || intentFrgament.equals(MainActivity.FRAGMENT_BUSINESS) || intentFrgament.equals(MainActivity.FRAGMENT_LOGIN) || intentFrgament.equals(MainActivity.FRAGMENT_FORGOTPASS) ||
                intentFrgament.equals(MainActivity.FRAGMENT_CONFEMAIL)) {

        } else if (intentFrgament.equals(MainActivity.FRAGMENT_MAIN)) {
            Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
            intent1.putExtra("animation", "splash");
            startActivity(intent1);
            finish();
        } else {
            super.onBackPressed();
        }

    }
}
