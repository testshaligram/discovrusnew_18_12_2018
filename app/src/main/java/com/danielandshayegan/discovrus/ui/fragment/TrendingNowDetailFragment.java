package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.TrendingNowAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentTrendingNowDetailBinding;
import com.danielandshayegan.discovrus.models.TrendingDetailsByIdModel;
import com.danielandshayegan.discovrus.models.TrendingNowModel;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.FirebaseApp;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.DeepLinkHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TrendingNowDetailFragment extends BaseFragment {


    FragmentTrendingNowDetailBinding mBinding;
    View view;
    int userId;
    TrendingNowModel.DataBean dataBean;
    GoogleApiClient mGoogleApiClient;
    private static final String DEEP_LINK_URL = "https://discovrus.page.link/6SuK";  //https://discovrus.page.link/6SuK
    String myUrl;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<TrendingDetailsByIdModel.DataBean> trendingDetails = new ArrayList<>();
    int trendingId;
    private static final String host = "api.linkedin.com";
    private static final String shareUrl = "https://" + host + "/v1/people/~/shares";

    public TrendingNowDetailFragment() {
        // Required empty public constructor
    }

    public static TrendingNowDetailFragment newInstance() {
        return new TrendingNowDetailFragment();
    }

    public static TrendingNowDetailFragment newInstance(Bundle extras) {
        TrendingNowDetailFragment fragment = new TrendingNowDetailFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_trending_now_detail, container, false);
        view = mBinding.getRoot();
        FirebaseApp.initializeApp(getActivity());

        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        mBinding.btnTrendingClose.setOnClickListener(v -> {

            Bundle bundle = new Bundle();
            bundle.putBoolean("isTrending", true);
            bundle.putInt("postType", 0);
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_up_trending, R.anim.no_anim)
                    .replace(R.id.trending_relative_main, PostManagementFragment.newInstance(bundle), "")
                    .commit();
        });
        ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(getActivity(), new OnPinchListener());

        mBinding.trendingRelativeMain.setOnTouchListener((View.OnTouchListener) (v, event) -> {
            // TODO Auto-generated method stub
            scaleGestureDetector.onTouchEvent(event);
            return true;
        });
        mBinding.imgAppShare.setOnClickListener(v -> shareDeepLink(String.valueOf(myUrl)));
        mBinding.imgLinkedInShare.setOnClickListener(v -> {
            if (!LISessionManager.getInstance(getActivity()).getSession().isValid()) {
                //if not valid then start authentication
                LISessionManager.getInstance(getActivity().getApplicationContext()).init(getActivity(), Scope.build(Scope.W_SHARE)//pass the build scope here
                        , new AuthListener() {
                            @Override
                            public void onAuthSuccess() {
                                // Authentication was successful. You can now do
                                // other calls with the SDK.
//                                Toast.makeText(getActivity(), "Successfully authenticated with LinkedIn.", Toast.LENGTH_SHORT).show();

                                //on successful authentication fetch basic profile data of user
                                shareComment();
                            }

                            @Override
                            public void onAuthError(LIAuthError error) {
                                // Handle authentication errors
                                Log.e("TAG", "Auth Error :" + error.toString());
//                                Toast.makeText(getActivity(), "Failed to authenticate with LinkedIn. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        }, true);//if TRUE then it will show dialog if
                // any device has no LinkedIn app installed to download app else won't show anything
            } else {
//                Toast.makeText(getActivity(), "You are already authenticated.", Toast.LENGTH_SHORT).show();

                //if user is already authenticated fetch basic profile data for user
                shareComment();
            }
        });

//        mBinding.imgLinkedInShare.setOnClickListener(view -> shareComment());

        mBinding.imgMessageShare.setOnClickListener(view -> {
            String smsBody = "Sms Body";
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.putExtra("sms_body", String.valueOf(myUrl));
            sendIntent.setType("vnd.android-dir/mms-sms");
            startActivity(sendIntent);
        });

        return view;
    }

    private void shareDeepLink(String deepLink) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link");
        intent.putExtra(Intent.EXTRA_TEXT, deepLink);

        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(getActivity(), "From onActivityResult of fragment", Toast.LENGTH_LONG).show();
        LISessionManager.getInstance(getActivity().getApplicationContext()).onActivityResult(getActivity(), requestCode, resultCode, data);

        // Add this line to your existing onActivityResult() method
        DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
        deepLinkHelper.onActivityResult(getActivity(), requestCode, resultCode, data);
    }

    private void shareComment() {

        // Creating a string to share on LinkedIn profile
      /*  String shareJsonText = "{ \n" +
                "\"comment\":\"" + trendingDetails.get(0).getTitle() + "\"," +
                "\"visibility\":{ " +
                "\"code\":\"anyone\"" +
                "}," +
                "\"content_navigation\":{ " +
                "\"title\":\"" + trendingDetails.get(0).getTitle() + "," +
                "\"description\":\"" + trendingDetails.get(0).getDescription() + "," +
//                        "\"submitted-url\":\"https://www.numetriclabz.com/android-linkedin integrationlogin-and-make-userprofile\"," +
                "\"submitted-url\":\"https://www.google.com\"," +
                "\"submitted-image-url\":\"" + ApiClient.WebService.imageUrl + trendingDetails.get(0).getImageName() + "" + " }" +
                "}";*/
        String shareJsonText = "{ \n" +
                "\"comment\":\"" + "Discovrus" + "\"," +
                "\"visibility\":{ " +
                "\"code\":\"anyone\"" +
                "}," +
                "\"content\":{ " +
                "\"title\":\" " + trendingDetails.get(0).getTitle() + " \"," +
                "\"description\":\" " + trendingDetails.get(0).getDescription() + " \"," +
//                        "\"submitted-url\":\"https://www.numetriclabz.com/android-linkedin integrationlogin-and-make-userprofile\"," +
                "\"submitted-url\":\"https://www.google.com\"," +
                "\"submitted-image-url\":\"" + ApiClient.WebService.imageUrl + trendingDetails.get(0).getImagePath() + "\"" + "}" +
                "}";

        Log.i("linkedinData", trendingDetails.get(0).getTitle() + trendingDetails.get(0).getDescription() + ApiClient.WebService.imageUrl + trendingDetails.get(0).getImagePath());
        Log.i("linkedinData", shareJsonText);
        // Call the APIHealper.getInstance method and pass the current context.
        APIHelper apiHelper = APIHelper.getInstance(getActivity());
               /* We need to share text call apiHelper.postRequest method.
                   This Method post the text on your linkedin profile.If successful,
                   it will return reposne from Linkedin containing json string.
                   In this Json string,containing statuscode: 200 and shared url.
                */
        apiHelper.postRequest(getActivity(),
                shareUrl, shareJsonText, new ApiListener() {
                    @Override
                    public void onApiSuccess(ApiResponse apiResponse) {
                        Log.e("Response", apiResponse.toString());
                        Toast.makeText(getActivity(),
                                "Shared Sucessfully", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onApiError(LIApiError error) {
                        Log.e("Response", error.toString());
                        Toast.makeText(getActivity(),
                                error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    public class OnPinchListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

        float startingSpan;
        float endSpan;
        float startFocusX;
        float startFocusY;


        public boolean onScaleBegin(ScaleGestureDetector detector) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("isTrending", true);
            bundle.putInt("postType", 0);
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_up_trending, R.anim.no_anim)
                    .replace(R.id.trending_relative_main, PostManagementFragment.newInstance(bundle), "")
                    .commit();
            /*startingSpan = detector.getCurrentSpan();
            startFocusX = detector.getFocusX();
            startFocusY = detector.getFocusY();*/
            return true;
        }


        public boolean onScale(ScaleGestureDetector detector) {
//            mBinding.trendingRelativeMain.scale(detector.getCurrentSpan()/startingSpan, startFocusX, startFocusY);
            return true;
        }

        public void onScaleEnd(ScaleGestureDetector detector) {
//            mBinding.trendingRelativeMain.restore();
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
           /* dataBean = (TrendingNowModel.DataBean) bundle.getParcelable("trendingDataModel");
            if (dataBean != null) {
                if (dataBean.getTrendingImagePath() != null) {
                    Picasso.with(getActivity()).
                            load(TrendingNowAdapter.IMAGE_PATH + dataBean.getTrendingImagePath()).
                            into(mBinding.imgTrending);
                }
                if (dataBean.getUserImagePath() != null) {
                    Picasso.with(getActivity()).
                            load(TrendingNowAdapter.IMAGE_PATH + dataBean.getUserImagePath()).
                            into(mBinding.imgTrendingUser);
                }
                if (dataBean.getTitle() != null) {
                    mBinding.txtTitle.setText(Html.fromHtml(dataBean.getTitle()));
                }
                if (dataBean.getDescription() != null) {
                    mBinding.txtDescription.setText(Html.fromHtml(dataBean.getDescription()));
                }*/

            trendingId = bundle.getInt("trendingId");
            if (trendingId != 0) {

                mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                callAPITrendingList(trendingId);
            }

        }


    }

    public void callAPITrendingList(int trendingId) {

        disposable.add(apiService.getTrendingListById(trendingId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<TrendingDetailsByIdModel>() {
                    @Override
                    public void onSuccess(TrendingDetailsByIdModel value) {
                        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(false);
                        if (value.isSuccess()) {
                            if (value.getData().getImagePath() != null) {
                                trendingDetails.add(value.getData());
                                Picasso.with(getActivity()).
                                        load(TrendingNowAdapter.IMAGE_PATH + value.getData().getImagePath()).
                                        into(mBinding.imgTrending);
                            }
                            if (value.getData().getImagePath() != null) {
                                Picasso.with(getActivity()).
                                        load(TrendingNowAdapter.IMAGE_PATH + value.getData().getImagePath()).
                                        into(mBinding.imgTrendingUser);
                            }
                            if (value.getData().getTitle() != null) {
                                mBinding.txtTitle.setText(Html.fromHtml(value.getData().getTitle()));
                            }
                            if (value.getData().getDescription() != null) {
                                mBinding.txtDescription.setText(Html.fromHtml(value.getData().getDescription()));
                            }

                            // String IOSParameters = new DynamicLinks.IOSParameters("com.example.ios");
                            final Uri myLink = Uri.parse("https://discovrus.page.link/6SuK");
                            Uri.Builder builder = new Uri.Builder();
                            builder.scheme("https")
                                    .authority("discovrus.page.link")
                                    .path("/").appendQueryParameter("link", myLink.toString()).appendQueryParameter("apn", "com.danielandshayegan.discovrus").appendQueryParameter("ibi", "com.sgit.discovrus").appendQueryParameter("afl", "www.google.com").appendQueryParameter("dataId", String.valueOf(trendingId)).appendQueryParameter("st", value.getData().getTitle()).appendQueryParameter("si", ApiClient.WebService.imageUrl + value.getData().getImagePath());
                            //.appendQueryParameter("type", "discovrus.page.link");
                            myUrl = builder.build().toString();
                        } else {
                            Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(false);
                        Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }
                }));
    }
}

