package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.databinding.FragmentChangePasswordBinding;


import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getHiddenList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.userChangePassword;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChangePasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePasswordFragment extends BaseFragment implements SingleCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentChangePasswordBinding mBinding;
    int userId;


    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ChangePasswordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
    /*    Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false);
        mBinding.uiChangePassword.toolbar.txtTitle.setText(R.string.str_change_password);
        mBinding.uiChangePassword.toolbar.imgBack.setOnClickListener(view -> {
            getActivity().finish();
        });
        mBinding.uiChangePassword.uiChnagePassword.btnSubmit.setOnClickListener(view -> {
            if (isValidation()) {
                if (Utils.isNetworkAvailable(getContext())) {
                    mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callAPI();
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        mBinding.uiChangePassword.uiChnagePassword.edtCurrentPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorCurrentPassword.setText(getString(R.string.empty_oldpassword));
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorCurrentPassword.setVisibility(View.VISIBLE);
                } else
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorCurrentPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mBinding.uiChangePassword.uiChnagePassword.edtNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorNewPassword.setText(getString(R.string.empty_newPassword));
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorNewPassword.setVisibility(View.VISIBLE);
                } else
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorNewPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mBinding.uiChangePassword.uiChnagePassword.edtConfirmNewPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setText(getString(R.string.empty_cofirmPassword));
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setVisibility(View.VISIBLE);
                } else
                    mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setVisibility(View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return mBinding.getRoot();
        // Inflate the layout for this fragment
    }

    private void callAPI() {
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                userChangePassword(userId, mBinding.uiChangePassword.uiChnagePassword.edtCurrentPassword.getText().toString(), mBinding.uiChangePassword.uiChnagePassword.edtNewPassword.getText().toString())
                        , getCompositeDisposable(), userChangePassword, this);
    }

    private boolean isValidation() {
        boolean validate = true;
        if (mBinding.uiChangePassword.uiChnagePassword.edtCurrentPassword.getText().toString().length() == 0) {
//            Snackbar.make(getActivity().findViewById(R.id.realtabcontent), getString(R.string.empty_oldpassword), Snackbar.LENGTH_LONG).show();
            mBinding.uiChangePassword.uiChnagePassword.txtErrorCurrentPassword.setText(getString(R.string.empty_oldpassword));
            mBinding.uiChangePassword.uiChnagePassword.txtErrorCurrentPassword.setVisibility(View.VISIBLE);
            validate = false;
        }
        if (mBinding.uiChangePassword.uiChnagePassword.edtNewPassword.getText().toString().length() == 0) {
//            Snackbar.make(getActivity().findViewById(R.id.realtabcontent), getString(R.string.empty_newPassword), Snackbar.LENGTH_LONG).show();
            mBinding.uiChangePassword.uiChnagePassword.txtErrorNewPassword.setVisibility(View.VISIBLE);
            mBinding.uiChangePassword.uiChnagePassword.txtErrorNewPassword.setText(getString(R.string.empty_newPassword));
            validate = false;
        } else if (mBinding.uiChangePassword.uiChnagePassword.edtNewPassword.getText().toString().length() < 6) {
//            Snackbar.make(getActivity().findViewById(R.id.realtabcontent), getString(R.string.pass_six_digit), Snackbar.LENGTH_LONG).show();
            mBinding.uiChangePassword.uiChnagePassword.txtErrorNewPassword.setVisibility(View.VISIBLE);
            mBinding.uiChangePassword.uiChnagePassword.txtErrorNewPassword.setText(getString(R.string.pass_six_digit));
            validate = false;
        }
        if (mBinding.uiChangePassword.uiChnagePassword.edtConfirmNewPassword.getText().toString().length() == 0) {
//            Snackbar.make(getActivity().findViewById(R.id.realtabcontent), getString(R.string.empty_cofirmPassword), Snackbar.LENGTH_LONG).show();
            mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setVisibility(View.VISIBLE);
            mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setText(getString(R.string.empty_cofirmPassword));
            validate = false;
        } else if (mBinding.uiChangePassword.uiChnagePassword.edtConfirmNewPassword.getText().toString().length() < 6) {
//            Snackbar.make(getActivity().findViewById(R.id.realtabcontent), getString(R.string.pass_six_digit), Snackbar.LENGTH_LONG).show();
            mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setVisibility(View.VISIBLE);
            mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setText(getString(R.string.pass_six_digit));
            validate = false;
        }
        if (!mBinding.uiChangePassword.uiChnagePassword.edtConfirmNewPassword.getText().toString().equals(mBinding.uiChangePassword.uiChnagePassword.edtNewPassword.getText().toString())) {
//            Snackbar.make(getActivity().findViewById(R.id.realtabcontent), getString(R.string.error_confirm), Snackbar.LENGTH_LONG).show();
            mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setVisibility(View.VISIBLE);
            mBinding.uiChangePassword.uiChnagePassword.txtErrorConfirmPassword.setText(getString(R.string.error_confirm));
            validate = false;
        }
        return validate;
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case userChangePassword:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse commonApiResponse = (CommonApiResponse) o;
                if (commonApiResponse.isSuccess()) {
                    Toast.makeText(mContext, commonApiResponse.getMessage().get(0).toString(), Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getContext(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
}
