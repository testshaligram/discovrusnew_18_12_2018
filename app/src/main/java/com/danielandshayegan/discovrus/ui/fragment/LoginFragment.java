package com.danielandshayegan.discovrus.ui.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.FragmentLoginBinding;
import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.models.LoginData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity;
import com.danielandshayegan.discovrus.ui.activity.MainActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.utils.Validators;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.single;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;
import static com.danielandshayegan.discovrus.ui.fragment.RegisterFragment.ANIMATION;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends BaseFragment implements SingleCallback {


    //    FragmentRegisterBinding mBinding;
    FragmentLoginBinding mBinding;
    View view;


    public LoginFragment() {
        // Required empty public constructor
    }

    static LoginFragment newInstance() {
        return new LoginFragment();
    }

    public static LoginFragment newInstance(Bundle extras) {
        LoginFragment fragment = new LoginFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_login, container, false);
        view = mBinding.getRoot();
        mBinding.uiLogin.imgArrowDown.setOnClickListener(v -> {

            Intent intentLogin = new Intent(getActivity(), MainActivity.class).putExtra(ANIMATION, "loginAnimation");
            startActivity(intentLogin);
            mActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            mActivity.finish();
           /* getActivity().overridePendingTransition(R.anim.slide_in_down, R.anim.stay);
            getActivity().finish();*/
        });

        mBinding.uiLogin.btnLogin.setOnClickListener(v -> {
            if (validation()) {
                if (Utils.isNetworkAvailable(mActivity)) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callAPI();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        mBinding.uiLogin.txtForgotPassword.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, MainActivity.FRAGMENT_FORGOTPASS);
            startActivity(intent);
            mActivity.overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            mActivity.finish();
        });

        view.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            //r will be populated with the coordinates of your view that area still visible.
            view.getWindowVisibleDisplayFrame(r);

            int heightDiff = view.getRootView().getHeight() - (r.bottom - r.top);
            if (heightDiff > 500) { // if more than 100 pixels, its probably a keyboard...

                mBinding.uiLogin.imgCap.setVisibility(View.GONE);
            } else {
                mBinding.uiLogin.imgCap.setVisibility(View.VISIBLE);
            }
        });

        view.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            //r will be populated with the coordinates of your view that area still visible.
            view.getWindowVisibleDisplayFrame(r);

            int heightDiff = view.getRootView().getHeight() - (r.bottom - r.top);
            if (heightDiff > 500) { // if more than 100 pixels, its probably a keyboard...

                mBinding.uiLogin.imgCap.setVisibility(View.GONE);
            } else {
                mBinding.uiLogin.imgCap.setVisibility(View.VISIBLE);
            }
        });


        return view;
    }


    private void callAPI() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                userLogin(mBinding.uiLogin.edtEmail.getText().toString(), null, mBinding.uiLogin.edtPassword.getText().toString(), true, false, false, false)
                        , getCompositeDisposable(), single, this);
    }


    public boolean validation() {

        boolean value = true;
        if (mBinding.uiLogin.edtEmail.length() == 0) {
            mBinding.uiLogin.txtErrorEmail.setText(R.string.null_email);
            mBinding.uiLogin.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        } else if (!Validators.isValidEmail(String.valueOf(mBinding.uiLogin.edtEmail.getText().toString()))) {
            mBinding.uiLogin.txtErrorEmail.setText(R.string.valid_email);
            mBinding.uiLogin.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        }
        if (mBinding.uiLogin.edtPassword.length() == 0) {
            mBinding.uiLogin.txtErrorPass.setText(R.string.null_password);
            mBinding.uiLogin.txtErrorPass.setVisibility(View.VISIBLE);
            value = false;
        } else if (mBinding.uiLogin.edtPassword.length() < 6) {
            mBinding.uiLogin.txtErrorPass.setText(R.string.pass_six_digit);
            mBinding.uiLogin.txtErrorPass.setVisibility(View.VISIBLE);
            value = false;
        }
        return value;

    }

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                LoginData loginModel = (LoginData) o;
                if (loginModel.isSuccess()) {
                    App_pref.saveAuthorizedUser(getActivity(), loginModel);
                    Toast.makeText(getActivity(), loginModel.getMessage().get(0), Toast.LENGTH_LONG).show();

                    if (App_pref.getAuthorizedUser(getActivity()) != null) {
                        ApplicationClass.OnlineUserListAPI(getActivity(), true);
                    }

                    Utils.callGetGeneralSettings(getActivity(), App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), new CallbackTask() {
                        @Override
                        public void onFail(Object object) {

                        }

                        @Override
                        public void onSuccess(Object object) {
                            Utils.setGeneralSettingsData(mActivity, (GeneralSettingsData) object);
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });

                    Intent intent = new Intent(getActivity(), MenuActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    // intent.putExtra("email", loginModel.getData().getEmail());
                    startActivity(intent);
                    mActivity.overridePendingTransition(R.anim.enter, R.anim.stay);
                    mActivity.finish();

                } else {

                    Toast.makeText(getContext(), loginModel.getMessage().get(0), Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
}
