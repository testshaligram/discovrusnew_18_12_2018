package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.animation.SpringForce;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.ClickPostCommentAdapter;
import com.danielandshayegan.discovrus.adapter.LikeAdapter;
import com.danielandshayegan.discovrus.adapter.TextWithImageAdapter;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CustomHtml;
import com.danielandshayegan.discovrus.custome_veiws.OverlapItemDecoration;
import com.danielandshayegan.discovrus.databinding.FragmentClickImageBinding;
import com.danielandshayegan.discovrus.dialog.OptionMenuDialog;
import com.danielandshayegan.discovrus.eventbus.EventDataObject;
import com.danielandshayegan.discovrus.eventbus.GlobalBus;
import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.models.ClickPostCommentData;
import com.danielandshayegan.discovrus.models.DeleteCommentData;
import com.danielandshayegan.discovrus.models.LikeListData;
import com.danielandshayegan.discovrus.models.PostCommentData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.SaveCommentLikeData;
import com.danielandshayegan.discovrus.models.SaveLikeData;
import com.danielandshayegan.discovrus.models.SavePostCommentReplyData;
import com.danielandshayegan.discovrus.models.postDetailData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.LikesActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.DeepLinkHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

import static android.app.Activity.RESULT_OK;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deleteComment;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getCommentList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveCommentLike;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveComments;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveLike;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePostCommentReply;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClickPostImageFragment extends BaseFragment implements ClickPostCommentAdapter.DeletePosCommentListner, SingleCallback, ClickPostCommentAdapter.LikeCommentListner//, CallbackTask
{
    View view;
    public FragmentClickImageBinding mBinding;
    ClickPostCommentAdapter clickPostCommentAdapter;
    List<ClickPostCommentData.DataBean> clickPostCommentList = new ArrayList<>();
    private WebserviceBuilder apiService;
    public PostListData.Post dataBean;
    int userId, postId;
    String userImagePath;
    private CompositeDisposable disposable = new CompositeDisposable();
    boolean isRefresh;
    public String myUrl;
    private static final String host = "api.linkedin.com";
    private static final String shareUrl = "https://" + host + "/v1/people/~/shares";
    public static ClickPostImageFragment postImageFragment;
    ClickPostCommentData.DataBean replyToData;
    ArrayList<LikeListData.DataBean> likedList = new ArrayList<>();

    SpringAnimation xAnimation, yAnimation;
    float dx = 0f, dy = 0f;
    boolean isReplyClicked = false;
    int commentId = 0;

    public ClickPostImageFragment() {
        // Required empty public constructor
    }

    public static ClickPostImageFragment newInstance() {
        return new ClickPostImageFragment();
    }

    public static ClickPostImageFragment newInstance(Bundle extras) {
        ClickPostImageFragment fragment = new ClickPostImageFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_click_image, container, false);
        view = mBinding.getRoot();
        // initilizeCommentData();

        postImageFragment = ClickPostImageFragment.this;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.uiClickPost.rvClickPost.setLayoutManager(mLayoutManager);
        mBinding.uiClickPost.rvClickPost.setItemAnimator(new DefaultItemAnimator());


        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mBinding.uiClickPost.rvLikeList.setLayoutManager(layoutManager);
        mBinding.uiClickPost.rvLikeList.addItemDecoration(new OverlapItemDecoration());

        mBinding.uiClickPost.tvLikeText.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), LikesActivity.class);
            intent.putExtra("postDetails", dataBean);
            startActivity(intent);
        });

       /* clickPostCommentAdapter = new ClickPostCommentAdapter(clickPostCommentList, getActivity(), this);
        mBinding.uiClickPost.rvClickPost.setAdapter(clickPostCommentAdapter);*/
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        userImagePath = App_pref.getAuthorizedUser(getActivity()).getData().getUserImagePath();
        if (userImagePath != null) {
            Picasso.with(getActivity()).
                    load(ApiClient.WebService.imageUrl + userImagePath).
                    into(mBinding.uiClickPost.imgUserProfile);
            Log.i("imgPath", ApiClient.WebService.imageUrl + userImagePath);
        }
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        mBinding.uiClickPost.imgBack.setOnClickListener(v -> {
            EventDataObject.CommentDelete commentDelete = new EventDataObject.CommentDelete(true);
            GlobalBus.getBus().post(commentDelete);
            Intent intent = new Intent();
            intent.putExtra("isRefresh", isRefresh);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        });
        mBinding.uiClickPost.imgCancelReply.setOnClickListener(view -> {
            isReplyClicked = false;
            mBinding.uiClickPost.edtComment.setVisibility(View.VISIBLE);
            mBinding.uiClickPost.imgCancelReply.setVisibility(View.GONE);
            mBinding.uiClickPost.edtCommentReply.setVisibility(View.GONE);
            mBinding.uiClickPost.txtReplyToUserName.setVisibility(View.GONE);
            mBinding.uiClickPost.edtCommentReply.setText("");
        });
        mBinding.uiClickPost.imgFav.setOnClickListener(v -> {
            isRefresh = true;
            boolean isLikes = !dataBean.isLiked();
            if (isLikes) {
                //  int likeCount = Integer.parseInt(mBinding.uiClickPost.txtFav.getText().toString().replaceAll("k","").replaceAll("m",""));
                int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiClickPost.txtFav.getText().toString()));
                int addCount = likeCount + 1;
                String countToPrint = Utils.convertLikes(addCount);
                mBinding.uiClickPost.txtFav.setText(countToPrint);
                dataBean.setLiked(isLikes);
                mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
            } else {
                // int likeCount = Integer.parseInt(mBinding.uiClickPost.txtFav.getText().toString().replaceAll("k","").replaceAll("m",""));
                int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiClickPost.txtFav.getText().toString()));
                if (likeCount != 0) {
                    int minusLikeCount = likeCount - 1;
                    String countToPrint = Utils.convertLikes(minusLikeCount);
                    mBinding.uiClickPost.txtFav.setText(countToPrint);
                    dataBean.setLiked(isLikes);
                    mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));
                }
            }
            getLikeApiCall(isLikes);
        });
        mBinding.uiClickPost.btnCommentPost.setOnClickListener(v -> {
            if (isReplyClicked) {
                if (mBinding.uiClickPost.edtCommentReply.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter reply", Toast.LENGTH_SHORT).show();
                } else {
                    isRefresh = true;
                    mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                    mBinding.uiClickPost.txtReplyToUserName.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    String commentToPass = "";
                    try {
                        commentToPass = URLEncoder.encode(mBinding.uiClickPost.edtCommentReply.getText().toString().replace("Reply to @" + replyToData.getUserName() + " ", ""), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            savePostCommentReply(postId, userId, commentToPass, commentId)
                                    , getCompositeDisposable(), savePostCommentReply, this);
                }
            } else {
                if (validationForCommentPost()) {
                    isRefresh = true;
                    mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    String commentToPass = "";
                    try {
                        commentToPass = URLEncoder.encode(mBinding.uiClickPost.edtComment.getText().toString(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            postComment(String.valueOf(0), String.valueOf(postId), String.valueOf(userId), commentToPass)
                                    , getCompositeDisposable(), saveComments, this);
                }
            }

        });
       /* mBinding.uiClickPost.edtComment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Toast.makeText(getActivity(),
                        ((EditText) v).getId() + " has focus - " + hasFocus,
                        Toast.LENGTH_LONG).show();
                //focusOnView();
                mBinding.uiClickPost.scrollCommentPost.scrollTo(0, mBinding.uiClickPost.edtComment.getTop());
            }
        });*/
        mBinding.uiClickPost.edtComment.setOnClickListener(v ->
                mBinding.uiClickPost.scrollCommentPost.scrollTo(0, mBinding.uiClickPost.constUserDetails.getTop()));

        mBinding.uiClickPost.imgToolOptions.setOnClickListener(view -> {
            isRefresh = true;
            OptionMenuDialog optionMenuDialog = new OptionMenuDialog();
            Bundle bundle = new Bundle();
            bundle.putParcelable("postDetails", dataBean);
            bundle.putString("from", "ClickPostImageFragment");
            optionMenuDialog.setArguments(bundle);
            optionMenuDialog.show(getFragmentManager(), "option menu");
        });

        mBinding.uiClickPost.imgPost.setOnLongClickListener(view -> {
            mBinding.uiClickPost.layoutOptions.setVisibility(View.VISIBLE);
            return false;
        });

        mBinding.uiClickPost.layoutOptions.setOnClickListener(view -> mBinding.uiClickPost.layoutOptions.setVisibility(View.GONE));

        mBinding.uiClickPost.imgCopyLink.setOnClickListener(view -> {
            ClipboardManager clipboard = (ClipboardManager) mActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Image post url", myUrl);
            clipboard.setPrimaryClip(clip);
            mBinding.uiClickPost.btnCopiedLink.setVisibility(View.VISIBLE);
            new Handler().postDelayed(() -> mBinding.uiClickPost.btnCopiedLink.setVisibility(View.INVISIBLE), 2000);
        });

        mBinding.uiClickPost.imgAppShare.setOnClickListener(view -> shareDeepLink(myUrl));

        mBinding.uiClickPost.imgLinkedInShare.setOnClickListener(view -> {
            linkedInShare();
        });


        mBinding.uiClickPost.edtCommentReply.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("Reply to @" + replyToData.getUserName() + " ")) {
                    mBinding.uiClickPost.txtReplyToUserName.setText(Html.fromHtml("<font color='#9E9CAC'> Reply to @" + replyToData.getUserName() + " </font>"));
                    Selection.setSelection(mBinding.uiClickPost.edtCommentReply.getText(), mBinding.uiClickPost.edtCommentReply.getText().length());

                }

            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getActivity().getApplicationContext()).onActivityResult(getActivity(), requestCode, resultCode, data);

        // Add this line to your existing onActivityResult() method
        DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();
        deepLinkHelper.onActivityResult(getActivity(), requestCode, resultCode, data);

        if (requestCode == 505 && resultCode == RESULT_OK) {
            boolean isRefresh = data.getBooleanExtra("isRefresh", false);
            if (isRefresh) {
                callApiComments();
            }

        }

    }

    public void shareDeepLink(String deepLink) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Firebase Deep Link");
        intent.putExtra(Intent.EXTRA_TEXT, deepLink);

        startActivity(intent);
    }

    public void linkedInShare() {
        if (!LISessionManager.getInstance(getActivity()).getSession().isValid()) {
            //if not valid then start authentication
            LISessionManager.getInstance(getActivity().getApplicationContext()).init(getActivity(), Scope.build(Scope.W_SHARE)//pass the build scope here
                    , new AuthListener() {
                        @Override
                        public void onAuthSuccess() {
                            // Authentication was successful. You can now do
                            // other calls with the SDK.
//                                Toast.makeText(getActivity(), "Successfully authenticated with LinkedIn.", Toast.LENGTH_SHORT).show();

                            //on successful authentication fetch basic profile data of user
                            shareToLinkedIn();
                        }

                        @Override
                        public void onAuthError(LIAuthError error) {
                            // Handle authentication errors
                            Log.e("TAG", "Auth Error :" + error.toString());
//                                Toast.makeText(getActivity(), "Failed to authenticate with LinkedIn. Please try again.", Toast.LENGTH_SHORT).show();
                        }
                    }, true);//if TRUE then it will show dialog if
            // any device has no LinkedIn app installed to download app else won't show anything
        } else {
//                Toast.makeText(getActivity(), "You are already authenticated.", Toast.LENGTH_SHORT).show();

            //if user is already authenticated fetch basic profile data for user
            shareToLinkedIn();
        }
    }

    public void shareToLinkedIn() {

        String shareJsonText = "{ \n" +
                "\"comment\":\"" + "Discovrus" + "\"," +
                "\"visibility\":{ " +
                "\"code\":\"anyone\"" +
                "}," +
                "\"content\":{ " +
                "\"title\":\" " + dataBean.getTitle() + " \"," +
                "\"description\":\" " + dataBean.getDescription() + " \"," +
//                        "\"submitted-url\":\"https://www.numetriclabz.com/android-linkedin integrationlogin-and-make-userprofile\"," +
                "\"submitted-url\":\" " + myUrl + "\"," +
                "\"submitted-image-url\":\"" + ApiClient.WebService.imageUrl + dataBean.getPostPath() + "\"" + "}" +
                "}";

        Log.i("linkedinData", dataBean.getTitle() + dataBean.getDescription() + ApiClient.WebService.imageUrl + dataBean.getPostPath());
        Log.i("linkedinData", shareJsonText);
        // Call the APIHealper.getInstance method and pass the current context.
        APIHelper apiHelper = APIHelper.getInstance(getActivity());
               /* We need to share text call apiHelper.postRequest method.
                   This Method post the text on your linkedin profile.If successful,
                   it will return reposne from Linkedin containing json string.
                   In this Json string,containing statuscode: 200 and shared url.
                */
        apiHelper.postRequest(getActivity(),
                shareUrl, shareJsonText, new ApiListener() {
                    @Override
                    public void onApiSuccess(ApiResponse apiResponse) {
                        Log.e("Response", apiResponse.toString());
                        Toast.makeText(getActivity(),
                                "Shared Sucessfully", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onApiError(LIApiError error) {
                        Log.e("Response", error.toString());
                        Toast.makeText(getActivity(),
                                error.toString(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.getArguments().containsKey("postDetails")) {
            getPostDetails();
        } else {
            callGetPostData(this.getArguments().getString("postId"));
        }

    }

    public void callGetPostData(String postId) {
        try {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getPostDetail(postId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<postDetailData>() {
                    @Override
                    public void onSuccess(postDetailData value) {
                        if (value.isSuccess()) {
                            if (value.getData().get(0) != null) {
                                dataBean = value.getData().get(0);
                                setData();
                            }
                        } else {
                            Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));

    }

    public void simpleMethod() {
        EventDataObject.CommentDelete commentDelete = new EventDataObject.CommentDelete(true);
        GlobalBus.getBus().post(commentDelete);
        Intent intent = new Intent();
        intent.putExtra("isRefresh", isRefresh);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    public boolean validationForCommentPost() {
        boolean value = true;
        if (mBinding.uiClickPost.edtComment.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter comment", Toast.LENGTH_SHORT).show();
            value = false;
        }
        return value;

    }

    public void getLikeApiCall(boolean isLikes) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveLike(postId, dataBean.getUserID(), isLikes, userId)
                        , getCompositeDisposable(), saveLike, this);
    }

    private void getPostDetails() {
        if (getArguments() != null) {
            Bundle bundle = this.getArguments();
            dataBean = bundle.getParcelable("postDetails");
            if (dataBean != null) {
                setData();
            }

        }
    }

    public void setData() {
        postId = dataBean.getPostId();
        Log.i("postId", " " + postId);
        getLikeListData();
        callApiComments();

        if (dataBean.getPostPath() != null) {
            Picasso.with(getActivity()).
                    load(ApiClient.WebService.imageUrl + dataBean.getPostPath()).
                    into(mBinding.uiClickPost.imgPost);
        }

        String likeCount = Utils.convertLikes(dataBean.getLikeCount());
        String commentCount = Utils.convertLikes(dataBean.getCommentCount());
        String viewCount = Utils.convertLikes(dataBean.getViewCount());
        mBinding.uiClickPost.txtFav.setText(likeCount);
        mBinding.uiClickPost.txtComment.setText(commentCount);
        mBinding.uiClickPost.txtView.setText(viewCount);
        mBinding.uiClickPost.imgView.setImageResource(R.drawable.ic_green_view);
        final CaseInsensitiveAssetFontLoader fontLoader = new CaseInsensitiveAssetFontLoader(mActivity.getApplicationContext(), "fonts");
        if (dataBean.getDescription() != null) {
            mBinding.uiClickPost.txtTitlePost.setText(dataBean.getDescription());
            /*String title = String.valueOf(dataBean.getDescription());
            try {
                title = URLDecoder.decode(dataBean.getTitle(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }*/
            //mBinding.uiClickPost.txtTitle.setText(CustomHtml.fromHtml(title, fontLoader));
//            mBinding.uiClickPost.txtTitlePost.setText(CustomHtml.fromHtml(title, fontLoader));

        }
        if (dataBean.getDescription() != null && !dataBean.getDescription().equalsIgnoreCase("")) {
            mBinding.uiClickPost.recyclerImageWithText.setVisibility(View.VISIBLE);
            mBinding.uiClickPost.imgPost.setVisibility(View.INVISIBLE);
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
            mBinding.uiClickPost.recyclerImageWithText.setLayoutManager(linearLayoutManager);

            List<String> dataList = new ArrayList<>();
            dataList.add("Image");
            dataList.add("Text");

            PagerSnapHelper snapHelper = new PagerSnapHelper();
            mBinding.uiClickPost.recyclerImageWithText.setOnFlingListener(null);
            mBinding.uiClickPost.recyclerImageWithText.clearOnScrollListeners();
            snapHelper.attachToRecyclerView(mBinding.uiClickPost.recyclerImageWithText);

            TextWithImageAdapter textWithImageAdapter = new TextWithImageAdapter(dataList, dataBean, mContext);

            mBinding.uiClickPost.recyclerImageWithText.setItemAnimator(new SlideInUpAnimator());
            mBinding.uiClickPost.recyclerImageWithText.setAdapter(textWithImageAdapter);
            /*mBinding.uiClickPost.recyclerImageWithText.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    textWithImageAdapter.setSelectedPosition(newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                }
            });*/
            /*String desc = "";
            try {
                desc = URLDecoder.decode(dataBean.getDescription(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            mBinding.uiClickPost.txtDescriptionPost.setText(CustomHtml.fromHtml(desc, fontLoader));
            if (desc.contains("<ul>")) {
                mBinding.uiClickPost.txtDescriptionPost.setText(Html.fromHtml(desc));
                switch (Utils.fontName) {
                    case "dancing_script_bold":
                        mBinding.uiClickPost.txtDescriptionPost.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                        break;
                    case "hanaleifill_regular":
                        mBinding.uiClickPost.txtDescriptionPost.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                        break;
                    case "merienda_bold":
                        mBinding.uiClickPost.txtDescriptionPost.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                        break;
                    case "opificio_light_rounded":
                        mBinding.uiClickPost.txtDescriptionPost.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                        break;
                    default:
                        mBinding.uiClickPost.txtDescriptionPost.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                        break;
                }
            }*/
        } else {
            mBinding.uiClickPost.recyclerImageWithText.setVisibility(View.GONE);
            mBinding.uiClickPost.imgPost.setVisibility(View.VISIBLE);
        }

        if (dataBean.isLiked())
            mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_red_like));
        else
            mBinding.uiClickPost.imgFav.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_heart_icon));

        if (dataBean.isCommented())
            mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
        else
            mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));
        final Uri myLink = Uri.parse("https://discovrus.page.link/ugkL");
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("discovrus.page.link")
                .path("/").appendQueryParameter("link", myLink.toString()).appendQueryParameter("apn", "com.danielandshayegan.discovrus")
                .appendQueryParameter("ibi", "com.sgit.discovrus").appendQueryParameter("afl", "www.google.com")
                .appendQueryParameter("postId", String.valueOf(dataBean.getPostId())).appendQueryParameter("st", dataBean.getTitle())
                .appendQueryParameter("si", ApiClient.WebService.imageUrl + dataBean.getPostPath())
                .appendQueryParameter("type", "postImage");
        myUrl = builder.build().toString();

        setSpringAnimation();
    }

    @SuppressLint("ClickableViewAccessibility")
    public void setSpringAnimation() {

        mBinding.uiClickPost.layoutText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                xAnimation = new SpringAnimation(mBinding.uiClickPost.layoutText, DynamicAnimation.X).setSpring(new SpringForce());
                xAnimation.getSpring().setFinalPosition(mBinding.uiClickPost.layoutText.getX());
                xAnimation.getSpring().setStiffness(SpringForce.STIFFNESS_LOW);
                xAnimation.getSpring().setDampingRatio(SpringForce.DAMPING_RATIO_LOW_BOUNCY);

                yAnimation = new SpringAnimation(mBinding.uiClickPost.layoutText, DynamicAnimation.Y).setSpring(new SpringForce());
                yAnimation.getSpring().setFinalPosition(mBinding.uiClickPost.layoutText.getY());
                yAnimation.getSpring().setStiffness(SpringForce.STIFFNESS_LOW);
                yAnimation.getSpring().setDampingRatio(SpringForce.DAMPING_RATIO_LOW_BOUNCY);

                mBinding.uiClickPost.layoutText.getViewTreeObserver().removeOnGlobalLayoutListener(this);

            }
        });

        mBinding.uiClickPost.layoutText.setOnTouchListener((view, motionEvent) -> {
            switch (motionEvent.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    mBinding.uiClickPost.scrollCommentPost.setEnableScrolling(false);
                    // capture the difference between view's top left corner and touch point
                    dx = view.getX() - motionEvent.getRawX();
                    dy = view.getY() - motionEvent.getRawY();

                    // cancel animations so we can grab the view during previous animation
                    xAnimation.cancel();
                    yAnimation.cancel();

                    break;

                case MotionEvent.ACTION_MOVE:
                    //  a different approach would be to change the view's LayoutParams.
                    mBinding.uiClickPost.layoutText.animate()
                            .x(motionEvent.getRawX() + dx)
                            .y(motionEvent.getRawY() + dy)
                            .setDuration(0)
                            .start();
                    break;

                case MotionEvent.ACTION_UP:
                    xAnimation.start();
                    yAnimation.start();
                    mBinding.uiClickPost.scrollCommentPost.setEnableScrolling(true);
                    break;

            }
            return true;
        });
    }

    public void callApiComments() {

        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getCommentList(postId, userId)
                        , getCompositeDisposable(), getCommentList, this);
    }

    @Override
    public void onCommentDeleted(ClickPostCommentData.DataBean clickpostData) {
        isRefresh = true;
        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                deleteComment(String.valueOf(clickpostData.getCommentId()))
                        , getCompositeDisposable(), deleteComment, this);
    }

    @Override
    public void onCommentReplyClick(ClickPostCommentData.DataBean clickpostData) {
        replyToData = clickpostData;
        mBinding.uiClickPost.edtComment.setText("");
        isReplyClicked = true;
        commentId = clickpostData.getCommentId();
        mBinding.uiClickPost.edtComment.setVisibility(View.GONE);
        mBinding.uiClickPost.txtReplyToUserName.setVisibility(View.VISIBLE);
        mBinding.uiClickPost.txtReplyToUserName.setText(Html.fromHtml("<font color='#9E9CAC'> Reply to @" + clickpostData.getUserName() + " </font>"));
        Selection.setSelection(mBinding.uiClickPost.edtCommentReply.getText(), mBinding.uiClickPost.edtCommentReply.getText().length());
        mBinding.uiClickPost.edtCommentReply.setVisibility(View.VISIBLE);
        mBinding.uiClickPost.imgCancelReply.setVisibility(View.VISIBLE);
        mBinding.uiClickPost.scrollCommentPost.scrollTo(0, 0);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case getCommentList:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                clickPostCommentList.clear();
                ClickPostCommentData clickPostCommentData = (ClickPostCommentData) o;
                if (clickPostCommentData.isSuccess()) {

                    clickPostCommentList.addAll(clickPostCommentData.getData());
//                    clickPostCommentAdapter.notifyDataSetChanged();
                    clickPostCommentAdapter = new ClickPostCommentAdapter(postId, clickPostCommentList, getActivity(), this, this);
                    mBinding.uiClickPost.rvClickPost.setAdapter(clickPostCommentAdapter);
                    if (clickPostCommentData.getData().size() > 0) {
                        int commentSize = clickPostCommentData.getData().size();
                        for (int i = 0; i < commentSize - 1; i++) {

                            int commentUserId = clickPostCommentData.getData().get(i).getCommentUserID();
                            if (commentUserId == userId) {
                                mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
                                break;
                            }

                        }
                    }
                }
                break;
            case saveLike:
                SaveLikeData saveLikeData = (SaveLikeData) o;
                if (saveLikeData.isSuccess()) {
                    getLikeListData();
                } else {
                    Toast.makeText(getActivity(), saveLikeData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
            case deleteComment:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteCommentData deleteCommentData = (DeleteCommentData) o;
                if (deleteCommentData.isSuccess()) {
                    int commentCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiClickPost.txtComment.getText().toString()));
                    int addCount = commentCount - 1;
                    if (addCount == 0)
                        mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_comment_icon));
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinding.uiClickPost.txtComment.setText(countToPrint);
                    //  Toast.makeText(getActivity(), deleteCommentData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(getActivity(), "false", Toast.LENGTH_SHORT).show();
                }
                break;
            case saveComments:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                PostCommentData postCommentData = (PostCommentData) o;
                if (postCommentData.isSuccess()) {
                    mBinding.uiClickPost.edtComment.setText("");
                    int commentCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.uiClickPost.txtComment.getText().toString()));
                    int addCount = commentCount + 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinding.uiClickPost.txtComment.setText(countToPrint);
                    mBinding.uiClickPost.imgComment.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_green_comment));
                    callApiComments();
                }
                break;
            case savePostCommentReply:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                SavePostCommentReplyData postCommentReplyData = (SavePostCommentReplyData) o;
                if (postCommentReplyData.isSuccess()) {
                    mBinding.uiClickPost.edtComment.setText("");
                    mBinding.uiClickPost.edtCommentReply.setText("");
                    mBinding.uiClickPost.edtComment.setVisibility(View.VISIBLE);
                    mBinding.uiClickPost.edtCommentReply.setVisibility(View.GONE);
                    mBinding.uiClickPost.txtReplyToUserName.setVisibility(View.GONE);
                    mBinding.uiClickPost.imgCancelReply.setVisibility(View.GONE);
                    callApiComments();
                }
                break;
            case saveCommentLike:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                SaveCommentLikeData saveCommentLikeData = (SaveCommentLikeData) o;
                if (saveCommentLikeData.isSuccess()) {
                    // Toast.makeText(getActivity(), saveCommentLikeData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onLikeComment(ClickPostCommentData.DataBean clickPostLike) {
        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        int commentId = clickPostLike.getCommentId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveCommentLike(commentId, userId, postId)
                        , getCompositeDisposable(), saveCommentLike, this);
    }

    /*@Override
    public void onFail(Object object) {

    }

    @Override
    public void onSuccess(Object object) {
        Log.e("in success returned", "caaling block api");
        callBlockUserApi();
    }

    @Override
    public void onFailure(Throwable t) {

    }*/

    private void getLikeListData() {
        if (Utils.isNetworkAvailable(getContext())) {
            Utils.getLikeListData(getActivity(), dataBean.getPostId(), new CallbackTask() {
                @Override
                public void onFail(Object object) {
                }

                @Override
                public void onSuccess(Object object) {
                    LikeListData followersUserListData = (LikeListData) object;

                    if (followersUserListData.isSuccess()) {
                        if (followersUserListData.getData() != null) {
                            likedList.clear();
                            likedList = (ArrayList<LikeListData.DataBean>) followersUserListData.getData();
                            if (followersUserListData.getData().size() > 3) {
                                mBinding.uiClickPost.rvLikeList.setVisibility(View.VISIBLE);
                                mBinding.uiClickPost.tvLikeText.setVisibility(View.VISIBLE);
                                mBinding.uiClickPost.rvLikeList.setAdapter(new LikeAdapter(getActivity(), likedList, dataBean));
                                mBinding.uiClickPost.tvLikeText.setText(" " + getActivity().getString(R.string.and) + " " + (followersUserListData.getData().size() - 3) + " " + getActivity().getString(R.string.others) + " " + getActivity().getString(R.string.liked));
                            } else if (followersUserListData.getData().size() == 0) {
                                mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                                mBinding.uiClickPost.tvLikeText.setVisibility(View.GONE);
                            } else {
                                mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                                mBinding.uiClickPost.tvLikeText.setVisibility(View.VISIBLE);
                                mBinding.uiClickPost.tvLikeText.setText(followersUserListData.getData().size() + " " + getActivity().getString(R.string.liked));
                            }
                        } else {
                            mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                            mBinding.uiClickPost.tvLikeText.setVisibility(View.GONE);
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                }
            });


            //       callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

/*
    private void callAPI() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getLikeListData(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), dataBean.getPostId())
                        , getCompositeDisposable(), getLikeListData, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                LikeListData followersUserListData = (LikeListData) o;

                                if (followersUserListData.isSuccess()) {
                                    if (followersUserListData.getData() != null && followersUserListData.getData().size() > 0) {

                                        if (followersUserListData.getData().size() > 3) {
                                            mBinding.uiClickPost.rvLikeList.setVisibility(View.VISIBLE);
                                            likedList.clear();
                                            likedList = (ArrayList<LikeListData.DataBean>) followersUserListData.getData();
                                            mBinding.uiClickPost.rvLikeList.setAdapter(new LikeAdapter());
                                            mBinding.uiClickPost.tvLikeText.setText(" " + getActivity().getString(R.string.and) + " " + (followersUserListData.getData().size() - 3) + " " + getActivity().getString(R.string.others) + " " + getActivity().getString(R.string.liked));
                                        } else {
                                            mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                                            mBinding.uiClickPost.tvLikeText.setText(followersUserListData.getData().size() + " " + getActivity().getString(R.string.liked));
                                        }
                                    } else {
                                        mBinding.uiClickPost.rvLikeList.setVisibility(View.GONE);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

                            }
                        });
    }
*/

/*
    class LikeAdapter extends RecyclerView.Adapter<LikeAdapter.MyViewHolder> {

        LikeAdapter() {
        }

        @NonNull
        @Override
        public LikeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutLikeListBinding mBinder = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.layout_like_list, parent, false);
            MyViewHolder holder = new MyViewHolder(mBinder);
            return holder;
        }

        @Override
        public int getItemCount() {
            return 3;
        }

        @Override
        public void onBindViewHolder(@NonNull LikeAdapter.MyViewHolder holder, int position) {
            Picasso.with(getActivity()).
                    load(ApiClient.WebService.imageUrl + likedList.get(position).getUserImagePath()).
                    into(holder.binding.imgUser);

            holder.binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), LikesActivity.class);
                    intent.putExtra("postDetails", dataBean);
                    startActivity(intent);
                }
            });
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            LayoutLikeListBinding binding;

            public MyViewHolder(LayoutLikeListBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
*/
}
