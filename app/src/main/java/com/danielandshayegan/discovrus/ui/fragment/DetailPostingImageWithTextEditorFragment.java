package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.system.ErrnoException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.daasuu.mp4compose.filter.GlFilter;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.BitmapUtils;
import com.danielandshayegan.discovrus.custome_veiws.knifetexteditor.KnifeText;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostingImageWithTextEditorBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.DetailPostPublishActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingActivity;
import com.danielandshayegan.discovrus.ui.activity.DetailPostingImageWithTextActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.MultipartBody;

import static android.app.Activity.RESULT_OK;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postPhoto;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;
import static com.danielandshayegan.discovrus.ui.fragment.RegisterFragment.SELECT_CATEGORY;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPostingImageWithTextEditorFragment extends BaseFragment {

    FragmentDetailPostingImageWithTextEditorBinding mBinding;
    View view;
    int userId;
    byte[] byteArray;
    int titleFontCount = 0, descFontCount = 0;
    public Uri mImageUri;

    Activity activityMain;
    List<PostListData.Post> postDataList = new ArrayList<>();

    // load native image filters library
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    public DetailPostingImageWithTextEditorFragment() {
        // Required empty public constructor
    }

    public static DetailPostingImageWithTextEditorFragment newInstance() {
        return new DetailPostingImageWithTextEditorFragment();
    }

    public static DetailPostingImageWithTextEditorFragment newInstance(Bundle extras) {
        DetailPostingImageWithTextEditorFragment fragment = new DetailPostingImageWithTextEditorFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_posting_image_with_text_editor, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();

        mBinding.uiImageWithTexteditorDetailPosting.edtTitle.setSelection(mBinding.uiImageWithTexteditorDetailPosting.edtTitle.getEditableText().length());
        mBinding.uiImageWithTexteditorDetailPosting.edtDesc.setSelection(mBinding.uiImageWithTexteditorDetailPosting.edtDesc.getEditableText().length());

        setClicks();

        setData();

        return view;
    }

    public void setData() {
        if (getArguments() != null) {
            if (getArguments().containsKey("originalImgUri"))
                loadImageFromStorage(getArguments().getString("originalImgUri"));
            else
                loadImageFromStorage(getArguments().getString("uploadImgUri"));
        }
    }

    private void loadImageFromStorage(String path) {
        try {
            File f = new File(path);
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
            mBinding.uiImageWithTexteditorDetailPosting.imgPost.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setClicks() {
        mBinding.uiImageWithTexteditorDetailPosting.imgBack.setOnClickListener(v -> {
            DetailPostingImageWithTextActivity.postingActivity.onBackPressed();
        });

        mBinding.uiImageWithTexteditorDetailPosting.btnNext.setOnClickListener(v -> {
            if (validation()) {
                String str = mBinding.uiImageWithTexteditorDetailPosting.edtTitle.toHtml();
                String strDesc = mBinding.uiImageWithTexteditorDetailPosting.edtDesc.toHtml();
                String stringTitle, stringDesc;
                switch (titleFontCount) {
                    case 1:
                        stringTitle = "<font face='dancing_script_regular'>" + str + "</font>";
                        break;
                    case 2:
                        stringTitle = "<font face='amaticsc_regular'>" + str + "</font>";
                        break;
                    case 3:
                        stringTitle = "<font face='great_vibes_regular'>" + str + "</font>";
                        break;
                    case 4:
                        stringTitle = "<font face='opificio_light_rounded'>" + str + "</font>";
                        break;
                    default:
                        stringTitle = "<font face='avenir_roman'>" + str + "</font>";
                        break;
                }
                switch (descFontCount) {
                    case 1:
                        stringDesc = "<font face='dancing_script_regular'>" + strDesc + "</font>";
                        break;
                    case 2:
                        stringDesc = "<font face='amaticsc_regular'>" + strDesc + "</font>";
                        break;
                    case 3:
                        stringDesc = "<font face='great_vibes_regular'>" + strDesc + "</font>";
                        break;
                    case 4:
                        stringDesc = "<font face='opificio_light_rounded'>" + strDesc + "</font>";
                        break;
                    default:
                        stringDesc = "<font face='avenir_roman'>" + strDesc + "</font>";
                        break;
                }

                Intent intent = new Intent(activityMain, DetailPostPublishActivity.class);
                intent.putExtra("Type", "ImageWithText");
                intent.putExtra("tagUserID", getArguments().getString("tagUserID"));
                intent.putExtra("uploadImgUri", getArguments().getString("uploadImgUri"));
                if (getArguments().containsKey("originalImgUri"))
                    intent.putExtra("originalImgUri", getArguments().getString("originalImgUri"));
                intent.putExtra("TextTitle", stringTitle);
                intent.putExtra("TextDesc", stringDesc);
                startActivity(intent);

            }

        });

        mBinding.uiImageWithTexteditorDetailPosting.font.setOnClickListener(view -> {
            if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus()) {
                if (titleFontCount == 4)
                    titleFontCount = 1;
                else
                    titleFontCount++;
            } else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus()) {
                if (descFontCount == 4)
                    descFontCount = 1;
                else
                    descFontCount++;
            }
            int count = 0;
            if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus())
                count = titleFontCount;
            else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus())
                count = descFontCount;

            switch (count) {
                case 1:
                    if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/dancing_script_regular.ttf"));
                    } else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/dancing_script_regular.ttf"));
                    }
                    break;
                case 2:
                    if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/amaticsc_regular.ttf"));
                    } else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/amaticsc_regular.ttf"));
                    }
                    break;
                case 3:
                    if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/great_vibes_regular.ttf"));
                    } else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/great_vibes_regular.ttf"));
                    }
                    break;
                case 4:
                    if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/opificio_light_rounded.ttf"));
                    } else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/opificio_light_rounded.ttf"));
                    }
                    break;
                default:
                    if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtTitle.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/avenir_roman.ttf"));
                    } else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus()) {
                        mBinding.uiImageWithTexteditorDetailPosting.edtDesc.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/avenir_roman.ttf"));
                    }
                    break;
            }
        });

        mBinding.uiImageWithTexteditorDetailPosting.bold.setOnClickListener(view -> {
            if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus())
                mBinding.uiImageWithTexteditorDetailPosting.edtTitle.bold(!mBinding.uiImageWithTexteditorDetailPosting.edtTitle.contains(KnifeText.FORMAT_BOLD));
            else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus())
                mBinding.uiImageWithTexteditorDetailPosting.edtDesc.bold(!mBinding.uiImageWithTexteditorDetailPosting.edtDesc.contains(KnifeText.FORMAT_BOLD));
        });

        mBinding.uiImageWithTexteditorDetailPosting.italic.setOnClickListener(view -> {
            if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus())
                mBinding.uiImageWithTexteditorDetailPosting.edtTitle.italic(!mBinding.uiImageWithTexteditorDetailPosting.edtTitle.contains(KnifeText.FORMAT_ITALIC));
            else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus())
                mBinding.uiImageWithTexteditorDetailPosting.edtDesc.italic(!mBinding.uiImageWithTexteditorDetailPosting.edtDesc.contains(KnifeText.FORMAT_ITALIC));
        });

        mBinding.uiImageWithTexteditorDetailPosting.underline.setOnClickListener(view -> {
            if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.hasFocus())
                mBinding.uiImageWithTexteditorDetailPosting.edtTitle.underline(!mBinding.uiImageWithTexteditorDetailPosting.edtTitle.contains(KnifeText.FORMAT_UNDERLINED));
            else if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus())
                mBinding.uiImageWithTexteditorDetailPosting.edtDesc.underline(!mBinding.uiImageWithTexteditorDetailPosting.edtDesc.contains(KnifeText.FORMAT_UNDERLINED));
        });

        mBinding.uiImageWithTexteditorDetailPosting.bullet.setOnClickListener(view -> {
            if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.hasFocus())
                mBinding.uiImageWithTexteditorDetailPosting.edtDesc.bullet(!mBinding.uiImageWithTexteditorDetailPosting.edtDesc.contains(KnifeText.FORMAT_BULLET));
        });


    }


    public boolean validation() {

        boolean value = true;
        if (mBinding.uiImageWithTexteditorDetailPosting.edtTitle.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter title", Toast.LENGTH_SHORT).show();
            value = false;
        }
        if (mBinding.uiImageWithTexteditorDetailPosting.edtDesc.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter description", Toast.LENGTH_SHORT).show();
            value = false;
        }

        return value;

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
