package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.system.ErrnoException;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daasuu.mp4compose.filter.GlFilter;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.UserPostListAdapter;
import com.danielandshayegan.discovrus.custome_veiws.BitmapUtils;
import com.danielandshayegan.discovrus.databinding.FragmentImageFilterBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.ui.activity.PostFilteringActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.zomato.photofilters.imageprocessors.Filter;
import com.zomato.photofilters.imageprocessors.subfilters.BrightnessSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.ContrastSubFilter;
import com.zomato.photofilters.imageprocessors.subfilters.SaturationSubfilter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postPhoto;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageFilterFragment extends BaseFragment implements SingleCallback, FiltersListFragment.FiltersListFragmentListener, EditImageFragment.EditImageFragmentListener {

    FragmentImageFilterBinding mBinding;
    View view;
    int userId;
    int postType;
    FiltersListFragment filtersListFragment;
    EditImageFragment editImageFragment;
    public Uri mImageUri;
    String videoUri;
    Bitmap bitmap;
    MultipartBody.Part body;
    public static final String IMAGE_NAME = "cap_top.png";
    public static ImageFilterFragment imageFilterFragment;
    private ProgressDialog mProgressDialog;
    Bitmap originalImage;
    // to backup image with filter applied
    Bitmap filteredImage;
    private GlFilter filter;
    // the final image after applying
    // brightness, saturation, contrast
    Bitmap finalImage;
    int brightnessFinal = 0;
    float saturationFinal = 1.0f;
    float contrastFinal = 1.0f;
    Activity activityMain;
    UserPostListAdapter userPostListAdapter;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<PostListData.Post> postDataList = new ArrayList<>();

    // load native image filters library
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    public ImageFilterFragment() {
        // Required empty public constructor
    }

    public static ImageFilterFragment newInstance() {
        return new ImageFilterFragment();
    }

    public static ImageFilterFragment newInstance(Bundle extras) {
        ImageFilterFragment fragment = new ImageFilterFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_image_filter, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        imageFilterFragment = this;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 2;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBinding.uiImageFilter.previewCard.getLayoutParams();
        layoutParams.width = (int) (devicewidth - getResources().getDimension(R.dimen._15sdp));
        layoutParams.setMargins((int) getResources().getDimension(R.dimen._10sdp), (int) getResources().getDimension(R.dimen._7sdp),
                (int) getResources().getDimension(R.dimen._7sdp), (int) getResources().getDimension(R.dimen._5sdp));
        mBinding.uiImageFilter.previewCard.setLayoutParams(layoutParams);

        loadImage();

        if (getArguments() != null) {
            postData();
        }

        mBinding.uiImageFilter.userPostRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        mBinding.uiImageFilter.userPostRecyclerView.setOnFlingListener(null);

        userPostListAdapter = new UserPostListAdapter(mContext);

        mBinding.uiImageFilter.userPostRecyclerView.setItemAnimator(new SlideInUpAnimator());
        mBinding.uiImageFilter.userPostRecyclerView.setAdapter(userPostListAdapter);

        mBinding.uiImageFilter.imgBack.setOnClickListener(v -> {
           /* getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.frame_replace, PostManagementFragment.newInstance(), "")
                    .commit();*/
            getActivity().finish();
//            activityMain.onBackPressed();
        });

        mBinding.uiImageFilter.imagePreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PostFilteringActivity.isFromFilterFragment = true;
                startCropImageActivity(Uri.parse(getArguments().getString("originalImageUri")));
            }
        });

        callAPI();

        mBinding.uiImageFilter.btnDone.setOnClickListener(v -> {
            mBinding.uiImageFilter.btnDone.setEnabled(false);
            if (postType == 2) {

                if (validation()) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    mBinding.uiImageFilter.btnDone.setEnabled(true);
                    saveImageToGallery();
                } else {
                    mBinding.uiImageFilter.btnDone.setEnabled(true);
                }
            } else if (postType == 1) {
                mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                disableScreen(true);
                mBinding.uiImageFilter.btnDone.setEnabled(true);
                saveImageToGallery();
            } else if (postType == 3) {

                if (validation()) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    mBinding.uiImageFilter.btnDone.setEnabled(true);
                    String description, title;
                    description = mBinding.uiImageFilter.edtDesc.getText().toString();
                    title = mBinding.uiImageFilter.edtTitle.getText().toString();
                    Log.d("textData", description + " " + title);
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            uploadPostDataText(description, title, userId)
                                    , getCompositeDisposable(), postPhoto, this);
/*
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            uploadPostDataText("test of text", "hello text", userId)
                                    , getCompositeDisposable(), postPhoto, this);*/
                } else {
                    mBinding.uiImageFilter.btnDone.setEnabled(true);
                }
            }

        });


        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPager.setVisibility(VISIBLE);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());

        // adding filter list fragment
        filtersListFragment = new FiltersListFragment();
        filtersListFragment.setListener(this);

        // adding edit image fragment
        editImageFragment = new EditImageFragment();
        editImageFragment.setListener(this);

        adapter.addFragment(filtersListFragment, "");
        adapter.addFragment(editImageFragment, getString(R.string.tab_edit));

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onFilterSelected(Filter filter) {
        // reset image controls
        showProgressDialog();
        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        resetControls();

        // applying the selected filter
        filteredImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        // preview filtered image
        mBinding.uiImageFilter.imagePreview.setImageBitmap(filter.processFilter(filteredImage));

        finalImage = filteredImage.copy(Bitmap.Config.ARGB_8888, true);
        hideProgressDialog();
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    @Override
    public void onBrightnessChanged(final int brightness) {
        brightnessFinal = brightness;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightness));
        mBinding.uiImageFilter.imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onSaturationChanged(final float saturation) {
        saturationFinal = saturation;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new SaturationSubfilter(saturation));
        mBinding.uiImageFilter.imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onContrastChanged(final float contrast) {
        contrastFinal = contrast;
        Filter myFilter = new Filter();
        myFilter.addSubFilter(new ContrastSubFilter(contrast));
        mBinding.uiImageFilter.imagePreview.setImageBitmap(myFilter.processFilter(finalImage.copy(Bitmap.Config.ARGB_8888, true)));
    }

    @Override
    public void onEditCompleted() {
        // once the editing is done i.e seekbar is drag is completed,
        // apply the values on to filtered image
        final Bitmap bitmap = filteredImage.copy(Bitmap.Config.ARGB_8888, true);

        Filter myFilter = new Filter();
        myFilter.addSubFilter(new BrightnessSubFilter(brightnessFinal));
        myFilter.addSubFilter(new ContrastSubFilter(contrastFinal));
        myFilter.addSubFilter(new SaturationSubfilter(saturationFinal));
        finalImage = myFilter.processFilter(bitmap);
    }

    private void resetControls() {
        if (editImageFragment != null) {
            editImageFragment.resetControls();
        }
        brightnessFinal = 0;
        saturationFinal = 1.0f;
        contrastFinal = 1.0f;
    }

    public boolean validation() {

        boolean value = true;
        if (mBinding.uiImageFilter.edtTitle.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter title", Toast.LENGTH_SHORT).show();
            value = false;
        }
        if (mBinding.uiImageFilter.edtDesc.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter description", Toast.LENGTH_SHORT).show();
            value = false;
        }

        return value;

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public Fragment getFragment(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    // load the default image from assets on app launch
    private void loadImage() {
        originalImage = BitmapUtils.getBitmapFromAssets(getActivity(), IMAGE_NAME, 300, 300);
        filteredImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        finalImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        mBinding.uiImageFilter.imagePreview.setImageBitmap(originalImage);
    }

    @Override
    public void onEditStarted() {

    }

    private void saveImageToGallery() {
        Dexter.withActivity(getActivity()).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            finalImage.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                            byte[] byteArray = stream.toByteArray();
                            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), byteArray);
                            body = MultipartBody.Part.createFormData("UploadPhoto", userId + "post_image.png", requestFile);
                            callApiSaveData();
                            finalImage.recycle();
                        } else {
                            Toast.makeText(getActivity(), "Permissions are not granted!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

    }

    private void callApiSaveData() {
        String title = null, description = null;
        if (postType == 1) {
            title = null;
            description = null;
            mBinding.loading.progressBar.setVisibility(VISIBLE);
            disableScreen(true);
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    uploadPostData(description, title, userId, body)
                            , getCompositeDisposable(), postPhoto, this);
        }
        if (postType == 2 || postType == 3) {
            if (validation()) {
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                try {
                    title = URLEncoder.encode(mBinding.uiImageFilter.edtTitle.getText().toString(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    description = URLEncoder.encode(mBinding.uiImageFilter.edtDesc.getText().toString(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(getActivity()).
                                        create(WebserviceBuilder.class).
                                        uploadPostData(description, title, userId, body)
                                , getCompositeDisposable(), postPhoto, this);
            } else {
                Toast.makeText(getActivity(), "Please Enter Title", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MenuActivity.fragmentView = 2;
    }

    private void postData() {
        if (getArguments() != null) {

            setupViewPager(mBinding.uiImageFilter.viewpager);
            mBinding.uiImageFilter.tabs.setupWithViewPager(mBinding.uiImageFilter.viewpager);

            mImageUri = Uri.parse(getArguments().getString("imageUri"));
            postType = getArguments().getInt("postType");
            videoUri = getArguments().getString("videoUri");

            switch (postType) {
                case 1:
                    mBinding.uiImageFilter.relativeFilter.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.imagePreview.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.viewpager.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.edtTitle.setVisibility(View.GONE);
                    mBinding.uiImageFilter.edtDesc.setVisibility(View.GONE);
                    imageData();
                    break;
                case 2:
                    mBinding.uiImageFilter.relativeFilter.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.imagePreview.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.viewpager.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.edtTitle.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.viewpager.setVisibility(VISIBLE);
                    mBinding.uiImageFilter.edtDesc.setVisibility(View.VISIBLE);
                    imageData();
                    break;
                case 3:
                    mBinding.uiImageFilter.relativeFilter.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.edtTitle.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.edtDesc.setVisibility(View.VISIBLE);
                    mBinding.uiImageFilter.imagePreview.setVisibility(View.GONE);
                    mBinding.uiImageFilter.viewpager.setVisibility(View.GONE);
                    break;

            }

        }
    }

    public void imageData() {

        String imageUri = (String.valueOf(mImageUri)).split("file://")[1];
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageUri, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 800, 800);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bitmap = BitmapFactory.decodeFile(imageUri, options);

       /* try {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
// clear bitmap memory
        originalImage.recycle();
        finalImage.recycle();
        finalImage.recycle();

        originalImage = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        filteredImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        finalImage = originalImage.copy(Bitmap.Config.ARGB_8888, true);
        mBinding.uiImageFilter.imagePreview.setImageBitmap(originalImage);
        bitmap.recycle();

        filtersListFragment.prepareThumbnail(originalImage);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case postPhoto:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                body = null;

                UploadPostData uploadPostData = (UploadPostData) o;
                if (uploadPostData.isSuccess()) {
                    Toast.makeText(getActivity(), uploadPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
//                    PostManagementFragment.managementFragment.callAPI(false);
                  /*  getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.frame_replace, PostManagementFragment.newInstance(), "")
                            .commit();*/
                    Intent intent = new Intent(getActivity(), MenuActivity.class);
                    intent.putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    getActivity().startActivity(intent);
                    getActivity().finish();
                    getActivity().overridePendingTransition(0, 0);
//                    activityMain.onBackPressed();
                } else {
                    Toast.makeText(getActivity(), uploadPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setMultiTouchEnabled(true)
                .start(getActivity());
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(activityMain);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Applying filter...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }


    public void callAPI() {
        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        disposable.add(apiService.getPostById(userId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<PostListData>() {
                    @Override
                    public void onSuccess(PostListData value) {

                        if (value.isSuccess() && value.getData().size() > 0) {
                            postDataList.clear();
                            postDataList.addAll(value.getData().get(0).getPost());
                            userPostListAdapter.clear();
                            userPostListAdapter.addAll(postDataList);
                            PostListData.PostList data = value.getData().get(0);
                            if (data != null) {
                                mBinding.uiImageFilter.postTimeTv.setText("");
                                mBinding.uiImageFilter.userLocationTv.setText(data.getLocation());
                                mBinding.uiImageFilter.userNameTv.setText(data.getUserName());

                                if (data.getPost() != null && data.getPost().size() != 0) {
                                    String dateStr = data.getPost().get(0).getPostCreatedDate();

                                    if (dateStr.length() <= 10) {
                                        mBinding.uiImageFilter.postTimeTv.setText(dateStr);
                                    } else {
                                        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
                                        df.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        Date date = null;
                                        try {
                                            date = df.parse(dateStr);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        df.setTimeZone(TimeZone.getDefault());
                                        String formattedDate = df.format(date);
                                        mBinding.uiImageFilter.postTimeTv.setText(formattedDate);
                                    }
                                }

                                RequestOptions options = new RequestOptions()
                                        .centerCrop()
                                        .placeholder(R.drawable.user_placeholder)
                                        .error(R.drawable.user_placeholder);


                                Glide.with(mContext)
                                        .load(ApiClient.WebService.imageUrl + data.getUserImagePath())
                                        .apply(options)
                                        .into(mBinding.uiImageFilter.userProfileIv);

                            }
                            try {
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else if (value.isSuccess() && value.getData().size() == 0) {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        } else {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                    }
                }));
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri imageUri = result.getUri();
//                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {
                    mImageUri = imageUri;
                    imageData();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }
    }
}
