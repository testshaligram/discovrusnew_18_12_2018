package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.ClickiesListAdapter;
import com.danielandshayegan.discovrus.adapter.DefaultClickiesListAdapter;
import com.danielandshayegan.discovrus.adapter.MyClickiesListAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentClickiesBinding;
import com.danielandshayegan.discovrus.models.ChartDataList;
import com.danielandshayegan.discovrus.models.ClickieList;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.ClickiesActivity;
import com.github.mikephil.charting.components.Legend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClickiesFragment extends BaseFragment {

    public FragmentClickiesBinding mBinding;
    View view;
    int userId;

    Activity activityMain;
    private WebserviceBuilder apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    List<ClickieList.DataBean> myClickies = new ArrayList<>();
    List<ClickieList.DataBean> defaultClickies = new ArrayList<>();
    List<ClickieList.DataBean> clickies = new ArrayList<>();
    DefaultClickiesListAdapter defaultClickiesAdapter;
    ClickiesListAdapter clickiesAdapter;
    public MyClickiesListAdapter myClickiesAdapter;
    public static ClickiesFragment clickiesFragment;

    public ClickiesFragment() {
        // Required empty public constructor
    }

    public static ClickiesFragment newInstance() {
        return new ClickiesFragment();
    }

    public static ClickiesFragment newInstance(Bundle extras) {
        ClickiesFragment fragment = new ClickiesFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_clickies, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        clickiesFragment = ClickiesFragment.this;
        setRecyclerView();
        getClickiesData();
        setClicks();

        return view;
    }

    public void setRecyclerView() {
        /*myClickies.add(R.drawable.my_clickie_1);
        myClickies.add(R.drawable.my_clickie_2);
        myClickies.add(R.drawable.my_clickie_3);
        myClickies.add(R.drawable.my_clickie_4);

        defaultClickies.add(R.drawable.default_clickie_1);
        defaultClickies.add(R.drawable.default_clickie_2);
        defaultClickies.add(R.drawable.default_clickie_3);
        defaultClickies.add(R.drawable.default_clickie_4);

        clickies.add(R.drawable.clickie_1);
        clickies.add(R.drawable.clickie_2);
        clickies.add(R.drawable.clickie_3);
        clickies.add(R.drawable.clickie_4);
        clickies.add(R.drawable.clickie_5);*/

        LinearLayoutManager layoutManager = new LinearLayoutManager(activityMain);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        layoutManager.setReverseLayout(true);
//        layoutManager.setStackFromEnd(true);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(activityMain);
        layoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
//        layoutManager1.setReverseLayout(true);
//        layoutManager1.setStackFromEnd(true);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(activityMain);
        layoutManager2.setOrientation(LinearLayoutManager.HORIZONTAL);
//        layoutManager2.setReverseLayout(true);
//        layoutManager2.setStackFromEnd(true);

        mBinding.uiClickies.recyclerMyClickies.setLayoutManager(layoutManager);
        mBinding.uiClickies.recyclerMyClickies.addItemDecoration(new ItemDecorator());
        mBinding.uiClickies.recyclerDefaultClickies.setLayoutManager(layoutManager1);
        mBinding.uiClickies.recyclerDefaultClickies.addItemDecoration(new ItemDecorator());
        mBinding.uiClickies.recyclerClickies.setLayoutManager(layoutManager2);
        mBinding.uiClickies.recyclerClickies.addItemDecoration(new ItemDecorator());

        myClickiesAdapter = new MyClickiesListAdapter(mContext);
        mBinding.uiClickies.recyclerMyClickies.setAdapter(myClickiesAdapter);
        myClickiesAdapter.clear();
        myClickiesAdapter.addAll(myClickies);

        defaultClickiesAdapter = new DefaultClickiesListAdapter(mContext);
        mBinding.uiClickies.recyclerDefaultClickies.setAdapter(defaultClickiesAdapter);
        defaultClickiesAdapter.clear();
        defaultClickiesAdapter.addAll(defaultClickies);

        clickiesAdapter = new ClickiesListAdapter(mContext);
        mBinding.uiClickies.recyclerClickies.setAdapter(clickiesAdapter);
        clickiesAdapter.clear();
        clickiesAdapter.addAll(clickies);

    }

    public void setClicks() {
        mBinding.uiClickies.imgBack.setOnClickListener(v -> {
            ClickiesActivity.clickiesActivity.onBackPressed();
        });

        mBinding.uiClickies.btnDone.setOnClickListener(v -> {
            mBinding.uiClickies.btnDone.setVisibility(View.GONE);
            myClickiesAdapter.hideDelete();
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private class ItemDecorator extends RecyclerView.ItemDecoration {
        //private final static int vertOverlap = -250;
        private final static int vertOverlap = 2;

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//            int childCount = parent.getChildCount();
            int position = parent.getChildAdapterPosition(view);
//            if (position != 0)
            outRect.right = vertOverlap;
                outRect.set(0, 0, vertOverlap, 0);

/*
            for (int i = 0; i <= childCount - 1; i++) {
                outRect.set(0, 0, vertOverlap, 0);
            }
*/

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 301 && resultCode == RESULT_OK) {
            byte[] byteArray = data.getByteArrayExtra("imageByteArray");
            Intent intent = new Intent();
            intent.putExtra("imageByteArray", byteArray);
            ((Activity) mContext).setResult(Activity.RESULT_OK, intent);
            ((Activity) mContext).finish();
        }
    }

    public void getClickiesData() {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getClickieList(userId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<ClickieList>() {
                    @Override
                    public void onSuccess(ClickieList value) {
                        if (value.isSuccess()) {
                            if (value.getData().size() > 0) {
                                myClickies.clear();
                                clickies.clear();
                                defaultClickies.clear();
                                for (int i = 0; i < value.getData().size(); i++) {
                                    ClickieList.DataBean data = value.getData().get(i);
                                    if (data.getType().equalsIgnoreCase("P"))
                                        clickies.add(data);
                                    else if (data.getType().equalsIgnoreCase("T"))
                                        defaultClickies.add(data);
                                    else if (data.getType().equalsIgnoreCase("U"))
                                        myClickies.add(data);
                                }
                                myClickiesAdapter.clear();
                                myClickiesAdapter.addAll(myClickies);

                                defaultClickiesAdapter.clear();
                                defaultClickiesAdapter.addAll(defaultClickies);

                                clickiesAdapter.clear();
                                clickiesAdapter.addAll(clickies);
                            }
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        } else {
                            Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                    }
                }));
    }

    public void deleteClickie(int clickieId) {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.deleteClickie(clickieId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<CommonApiResponse>() {
                    @Override
                    public void onSuccess(CommonApiResponse value) {
                        if (value.isSuccess()) {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            getClickiesData();
                        } else {
                            Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));

    }

    @Override
    public void onStop() {
        mBinding.uiClickies.btnDone.setVisibility(View.GONE);
        myClickiesAdapter.hideDelete();
        super.onStop();
    }
}
