package com.danielandshayegan.discovrus.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivityPaymentSuccessBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;

import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

public class PaymentSuccessActivity extends BaseActivity {
    ActivityPaymentSuccessBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_payment_success);


        mBinding.btnGoToNewsFeed.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "mainMenu");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION
                    |Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });

        mBinding.imgBack.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
            intent.putExtra(ACTIVITY_INTENT, "mainMenu");
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION
                    |Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            overridePendingTransition(0, 0);
        });

    }
}
