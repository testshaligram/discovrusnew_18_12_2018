package com.danielandshayegan.discovrus.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.PostCommentReplyAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentPostCommentReplyBinding;
import com.danielandshayegan.discovrus.models.ClickPostCommentData;
import com.danielandshayegan.discovrus.models.CommentReplyData;
import com.danielandshayegan.discovrus.models.DeleteCommentData;
import com.danielandshayegan.discovrus.models.SaveCommentLikeData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deletePostCommentReply;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveCommentLike;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostCommentReplyFragment extends BaseFragment implements PostCommentReplyAdapter.DeletePosCommentReplyListner, PostCommentReplyAdapter.LikeCommentReplyListner, SingleCallback {

    View view;
    public FragmentPostCommentReplyBinding mBinding;
    PostCommentReplyAdapter postCommentReplyAdapter;
    List<ClickPostCommentData.DataBean> clickPostCommentList = new ArrayList<>();
    private WebserviceBuilder apiService;
    List<CommentReplyData.DataBean> replyList = new ArrayList<>();
    int userId, postId;
    private CompositeDisposable disposable = new CompositeDisposable();
    public static PostCommentReplyFragment postImageFragment;
    boolean isRefresh;

    public PostCommentReplyFragment() {
        // Required empty public constructor
    }

    public static PostCommentReplyFragment newInstance() {
        return new PostCommentReplyFragment();
    }

    public static PostCommentReplyFragment newInstance(Bundle extras) {
        PostCommentReplyFragment fragment = new PostCommentReplyFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_post_comment_reply, container, false);
        view = mBinding.getRoot();

        postImageFragment = PostCommentReplyFragment.this;
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.uiCommentReply.recyclerCommentReply.setLayoutManager(mLayoutManager);
        mBinding.uiCommentReply.recyclerCommentReply.setItemAnimator(new DefaultItemAnimator());
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        mBinding.uiCommentReply.imgBack.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("isRefresh", isRefresh);
            getActivity().setResult(Activity.RESULT_OK, intent);
            getActivity().finish();
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.getArguments().containsKey("commentId")) {
            postId = this.getArguments().getInt("postId");
            callGetReplyData(this.getArguments().getInt("commentId"));
        }

    }

    public void callGetReplyData(int commentId) {
        try {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getPostCommentReply(commentId, userId, postId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<CommentReplyData>() {
                    @Override
                    public void onSuccess(CommentReplyData value) {
                        if (value.isSuccess()) {
                            if (value.getData() != null) {
                                replyList.clear();
                                replyList.addAll(value.getData());
                                mBinding.uiCommentReply.txtNoData.setVisibility(View.GONE);
                                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                setAdapter();
                            }
                        } else {
                            if (value.getMessage().get(0).equalsIgnoreCase("No Data Available.")) {
                                mBinding.uiCommentReply.txtNoData.setVisibility(View.VISIBLE);
                                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            } else {
                                Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));

    }

    public void setAdapter() {
        postCommentReplyAdapter = new PostCommentReplyAdapter(replyList, getActivity(), this, this);
        mBinding.uiCommentReply.recyclerCommentReply.setAdapter(postCommentReplyAdapter);
    }

    public void simpleMethod() {
        Intent intent = new Intent();
        intent.putExtra("isRefresh", isRefresh);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    @Override
    public void onCommentDeleted(CommentReplyData.DataBean clickpostData) {
        isRefresh = true;
        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                deletePostCommentReply(clickpostData.getCommentId())
                        , getCompositeDisposable(), deletePostCommentReply, this);
    }

    @Override
    public void onCommentReplyClick(CommentReplyData.DataBean clickpostData) {

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case deletePostCommentReply:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                DeleteCommentData deleteCommentData = (DeleteCommentData) o;
                if (deleteCommentData.isSuccess()) {
                    callGetReplyData(this.getArguments().getInt("commentId"));
                } else {
                    Toast.makeText(getActivity(), deleteCommentData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
            case saveCommentLike:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                SaveCommentLikeData saveCommentLikeData = (SaveCommentLikeData) o;
                if (saveCommentLikeData.isSuccess()) {
                    // Toast.makeText(getActivity(), saveCommentLikeData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onLikeComment(CommentReplyData.DataBean clickPostLike) {
        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        int commentId = clickPostLike.getCommentId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveCommentLike(commentId, userId, postId)
                        , getCompositeDisposable(), saveCommentLike, this);
    }
}
