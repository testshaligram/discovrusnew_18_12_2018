package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.BlockAccountAdapter;
import com.danielandshayegan.discovrus.adapter.HiddenAccountAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentHiddenAccountBinding;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.HiddenDataListModel;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.utils.Utils;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveHiddenUserDetails;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getBlockList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getHiddenList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HiddenAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HiddenAccountFragment extends BaseFragment implements View.OnClickListener, SingleCallback, HiddenAccountAdapter.UnhideAccountListner {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    HiddenAccountAdapter mAdapter;

    FragmentHiddenAccountBinding mBinding;
    int userId;


    public HiddenAccountFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HiddenAccountFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HiddenAccountFragment newInstance() {
        HiddenAccountFragment fragment = new HiddenAccountFragment();
       /* Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);*/
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_hidden_account, container, false);
        mBinding.uiAccount.toolbar.txtTitle.setText("Hidden Accounts");
        getHiddenListData();
        // mBinding.uiAccount.recylerView.setAdapter(new HiddenAccountAdapter());
        mBinding.uiAccount.toolbar.imgBack.setOnClickListener(this);
        return mBinding.getRoot();
    }

    private void getHiddenListData() {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callAPI();
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }

    }

    private void callAPI() {
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getHiddenList(userId)
                        , getCompositeDisposable(), getHiddenList, this);
    }


    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getHiddenList:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                HiddenDataListModel hiddenDataListModel = (HiddenDataListModel) o;
                if (hiddenDataListModel.isSuccess()) {
                    if (hiddenDataListModel.getData() != null && hiddenDataListModel.getData().size() > 0) {
                        mBinding.uiAccount.recylerView.setVisibility(View.VISIBLE);
                        mBinding.uiAccount.txtNoData.setVisibility(View.GONE);
                        mAdapter = new HiddenAccountAdapter(hiddenDataListModel.getData(), getContext(), this);
                        mBinding.uiAccount.recylerView.setAdapter(mAdapter);
                    } else {
                        mBinding.uiAccount.recylerView.setVisibility(View.GONE);
                        mBinding.uiAccount.txtNoData.setText("No hidden accounts yet.");
                        mBinding.uiAccount.txtNoData.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case SaveHiddenUserDetails:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                CommonApiResponse hiddenUserDetails = (CommonApiResponse) o;
                if (hiddenUserDetails.isSuccess()) {
                    getHiddenListData();
                } else {
                    Toast.makeText(getActivity(), hiddenUserDetails.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.uiLoading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                getActivity().finish();
                break;
        }
    }

    @Override
    public void onUnhideAccountListner(HiddenDataListModel.DataBean dataBean) {
        if (Utils.isNetworkAvailable(getContext())) {
            mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            callHiddenAPI(dataBean);
        } else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void callHiddenAPI(HiddenDataListModel.DataBean dataBean) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                SaveHiddenUserDetails(dataBean.getUserID(), userId)
                        , getCompositeDisposable(), SaveHiddenUserDetails, this);
    }
}
