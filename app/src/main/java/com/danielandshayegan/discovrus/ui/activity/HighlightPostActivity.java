package com.danielandshayegan.discovrus.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivitySettingBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.HighlightPostFragment;

public class HighlightPostActivity extends BaseActivity {
    ActivitySettingBinding mBinding;
    HighlightPostFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_setting);

        fragment = HighlightPostFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, fragment, "highlightFragment")
                .commit();
    }

   /*// public void handleClicks(View v)
    {
        fragment.handleClicks(v);
    }*/
}