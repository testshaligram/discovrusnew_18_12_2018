package com.danielandshayegan.discovrus.ui.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.databinding.FragmentSettingBinding;


import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.dialog.LogoutDialog;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.BlockContactActivity;
import com.danielandshayegan.discovrus.ui.activity.ChangePasswordActivity;
import com.danielandshayegan.discovrus.ui.activity.DataHistoryActivity;
import com.danielandshayegan.discovrus.ui.activity.DataPloicyActivity;
import com.danielandshayegan.discovrus.ui.activity.GeneralProfileActivity;
import com.danielandshayegan.discovrus.ui.activity.HiddenContactActivity;
import com.danielandshayegan.discovrus.ui.activity.NotificationSettingsActivity;
import com.danielandshayegan.discovrus.ui.activity.SavePhotosAndVideosActivity;
import com.danielandshayegan.discovrus.ui.activity.LinkSocialAccountActivity;
import com.danielandshayegan.discovrus.ui.activity.LiveLocationStatus;
import com.danielandshayegan.discovrus.ui.activity.SplashActivity;
import com.danielandshayegan.discovrus.ui.activity.SyncContactActivity;
import com.danielandshayegan.discovrus.ui.activity.TermsofUseActivity;
import com.danielandshayegan.discovrus.ui.activity.TwoFectorAuthActivity;
import com.facebook.login.LoginManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends BaseFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    FragmentSettingBinding fragmentSettingBinding;

    public SettingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentSettingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        fragmentSettingBinding.uiSetting.seetingToolbar.txtTitle.setText(R.string.str_settings);
        fragmentSettingBinding.uiSetting.settingList.txtLogoutOf.setText("Log Out of - "+App_pref.getAuthorizedUser(getContext()).getData().getFirstName()+ App_pref.getAuthorizedUser(getContext()).getData().getLastName());
        setClick();
        if(!App_pref.getAuthorizedUser(getContext()).getData().getSocialmediaId().isEmpty()){
            fragmentSettingBinding.uiSetting.settingList.cardChangePassword.setVisibility(View.GONE);
        }
        else
            fragmentSettingBinding.uiSetting.settingList.cardChangePassword.setVisibility(View.VISIBLE);

        return fragmentSettingBinding.getRoot();
    }

    private void setClick() {
        fragmentSettingBinding.uiSetting.settingList.cardProfile.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardHidden.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardBlockedAccount.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardChangePassword.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardTwoFactorAuth.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardDataHistory.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardLiveLocation.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardSyncContect.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardLinkedAccounts.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardLogoutOf.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardNotification.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardSavePhoto.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardDataPolicy.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.settingList.cardTermsOfUse.setOnClickListener(this);
        fragmentSettingBinding.uiSetting.seetingToolbar.imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_profile:
                Intent intent = new Intent(getActivity(), GeneralProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.card_hidden:
                Intent intentHidden = new Intent(getActivity(), HiddenContactActivity.class);
                startActivity(intentHidden);
                break;
            case R.id.card_blocked_account:
                Intent intentBlockContact = new Intent(getActivity(), BlockContactActivity.class);
                startActivity(intentBlockContact);
                break;
            case R.id.card_notification:
                Intent intent1 = new Intent(getActivity(), NotificationSettingsActivity.class);
                startActivity(intent1);
                break;
            case R.id.card_save_photo:
                Intent intent2 = new Intent(getActivity(), SavePhotosAndVideosActivity.class);
                startActivity(intent2);
                break;

            case R.id.img_back:
                getActivity().finish();
                break;
            case R.id.card_change_password:
                Intent intentChangePassword = new Intent(getActivity(), ChangePasswordActivity.class);
                startActivity(intentChangePassword);
                break;
            case R.id.card_two_factor_auth:
                Intent intentTwoFector = new Intent(getActivity(), TwoFectorAuthActivity.class);
                startActivity(intentTwoFector);
                break;
            case R.id.card_data_history:
                Intent intentDataHistory = new Intent(getActivity(), DataHistoryActivity.class);
                startActivity(intentDataHistory);
                break;
            case R.id.card_linked_accounts:
                Intent intentlink = new Intent(getActivity(), LinkSocialAccountActivity.class);
                startActivity(intentlink);
                break;
            case R.id.card_live_location:
                Intent intentLive = new Intent(getActivity(), LiveLocationStatus.class);
                startActivity(intentLive);
                break;
            case R.id.card_sync_contect:
                Intent intentSyncContact = new Intent(getActivity(), SyncContactActivity.class);
                startActivity(intentSyncContact);
                break;
            case R.id.card_data_policy:
                Intent intentDataPlicy = new Intent(getActivity(), DataPloicyActivity.class);
                startActivity(intentDataPlicy);
                break;
            case R.id.card_terms_of_use:
                Intent intentTerms = new Intent(getActivity(), TermsofUseActivity.class);
                startActivity(intentTerms);
                break;

            case R.id.card_logout_of:
                LogoutDialog dialogFragment = new LogoutDialog();
                dialogFragment.show(getActivity().getFragmentManager(), "Dialog Logout");
                break;
        }
    }
}
