package com.danielandshayegan.discovrus.ui.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivityBaseBinding;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.activity.LaunchpadActivity;
import com.danielandshayegan.discovrus.ui.activity.SplashActivity;
import com.danielandshayegan.discovrus.ui.newsfeed.NewsFeedDetailsFragment;
import com.danielandshayegan.discovrus.ui.newsfeed.NewsFeedFragment;
import com.danielandshayegan.discovrus.ui.postlocation.PostLocationFragment;
import com.danielandshayegan.discovrus.ui.profile.ProfileFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

@SuppressLint("Registered")
public class BaseActivityNew extends AbstractBaseActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();

    public ActivityBaseBinding baseBinding;
    public BaseActivityNew me;
    private FragmentManager fragmentManager;
    private int selectedTab = 1;
    PostLocationFragment postLocationFragment;
    NewsFeedFragment postFragment;
    ProfileFragment profileFragment;

    CoordinatorLayout.LayoutParams params;
    AppBarLayout.Behavior behavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
        init();
    }

    // Initialize all the views variables
    private void init() {
        me = this;

        /**Setup window setting*/
      //  setToolBar(baseBinding.mToolbar);

        /**Initialize of fragmentManager*/
        fragmentManager = getSupportFragmentManager();

        if (getRedirectClass() == null)
            setRedirectClass(SplashActivity.class);

        setupTabLayout();
    }

    protected void setToolBar(Toolbar mToolBar) {
        try {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            getWindow().setWindowAnimations(0);
            setSupportActionBar(mToolBar);
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_accent);
            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.newsfeed_gradient_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setIndicatorIcon(int res) {
        getSupportActionBar().setHomeAsUpIndicator(res);
    }

    protected void setTitle(String title) {

        //baseBinding.toolbarTitle.setText("");
    }

    /*protected void setDoneButton(String text) {
        basebaseBinding.btnDone.setText(text);
        basebaseBinding.btnDone.setVisibility(View.VISIBLE);
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        Fragment f = fragmentManager.findFragmentById(R.id.frmContainer);
        int index = -1;
        if (f instanceof PostLocationFragment) {
            index = 0;
        } else if (f instanceof NewsFeedFragment) {
            index = 1;
        }
        TabLayout.Tab tab = baseBinding.tabs.getTabAt(index);
        assert tab != null;
        tab.select();
    }

    public void setFullScreen() {
        try {
            getSupportActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SafeVarargs
    protected final void onFinishAll(Class<? extends Activity>... aClass) {
        for (Class<? extends Activity> activity : aClass) {
            EventBus.getDefault().post(activity);
        }
    }

    @Subscribe
    public void onFinish(Class<?> aClass) {
        if (aClass == this.getClass()) {
            finish();
        }
    }

    protected void finishTo(@NonNull Class<?> aClass) {
        EventBus.getDefault().post(aClass);
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() > 0)
        {
            getSupportFragmentManager().popBackStack();
        }
        else {
            super.onBackPressed();
        }
    }

    public void handleClicks(View view) {
        switch (view.getId()) {
            case R.id.ivHat:
                startActivity(new Intent(me, LaunchpadActivity.class));
                break;

            default:
                Fragment f = getSupportFragmentManager().findFragmentById(R.id.frmContainer);
                if (f instanceof NewsFeedFragment) {
                    postFragment.handleClicks(view);
                }
                else if (f instanceof NewsFeedDetailsFragment)
                {
                    ((NewsFeedDetailsFragment) f).handleClicks(view);
                }
                break;
        }
    }

    private void setupTabLayout() {
        postLocationFragment = new PostLocationFragment();
        postFragment = new NewsFeedFragment();
        profileFragment = new ProfileFragment(); //TODO: Apply business profile fragment

        View tabHome = getLayoutInflater().inflate(R.layout.custom_tab, null);
        final Drawable home = AppCompatResources.getDrawable(this, R.drawable.selector_map_tab);
        tabHome.findViewById(R.id.icon).setBackground(home);
        baseBinding.tabs.addTab(baseBinding.tabs.newTab().setCustomView(tabHome), true);

        View tabStory = getLayoutInflater().inflate(R.layout.custom_tab, null);
        tabStory.findViewById(R.id.icon).setBackgroundResource(R.drawable.selector_feed_tab);
        baseBinding.tabs.addTab(baseBinding.tabs.newTab().setCustomView(tabStory));

        View tabCreate = getLayoutInflater().inflate(R.layout.custom_tab, null);
        tabCreate.findViewById(R.id.icon).setBackgroundResource(R.drawable.selector_chat_tab);
        baseBinding.tabs.addTab(baseBinding.tabs.newTab().setCustomView(tabCreate));

        View tabCreat1e = getLayoutInflater().inflate(R.layout.custom_tab, null);
        tabCreat1e.findViewById(R.id.icon).setBackgroundResource(R.drawable.selector_profile_tab);
        baseBinding.tabs.addTab(baseBinding.tabs.newTab().setCustomView(tabCreat1e));

        int beginSpace = 96;
        int endSpace = 90;

        ViewGroup slidingTabStrip = (ViewGroup) baseBinding.tabs.getChildAt(0);

        for (int i = 0; i < slidingTabStrip.getChildCount() - 1; i++) {
            View v = slidingTabStrip.getChildAt(i);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            if (i == 1) {
                params.rightMargin = endSpace;
            }
            if (i == 2) {
                params.leftMargin = beginSpace;
            }
        }

        baseBinding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.e("pos", "" + tab.getPosition());
                selectedTab = tab.getPosition();
              //  baseBinding.appbar.setExpanded(true, true);
                setCurrentTabFragment(selectedTab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                /*if (selectedTab == 0 && feedFragment != null) {
                    feedFragment.gotop();
                }*/
            }
        });

        baseBinding.tabs.getTabAt(selectedTab).select();
        setCurrentTabFragment(selectedTab);
    }

    private void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
                replaceFragment(postLocationFragment);
                break;

            case 1:
                replaceFragment(postFragment);
                break;

            case 2:
                startActivity(new Intent(me, ConversationActivity.class));
                break;

            case 3:
                replaceFragment(profileFragment);
                break;
        }
    }

    public void replaceFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frmContainer, selectedFragment);
        transaction.addToBackStack(selectedFragment.getClass().getSimpleName());
        transaction.commit();
    }
}