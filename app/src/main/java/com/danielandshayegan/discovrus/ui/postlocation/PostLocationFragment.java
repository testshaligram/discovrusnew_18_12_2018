package com.danielandshayegan.discovrus.ui.postlocation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Selection;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.CommentsAdapter;
import com.danielandshayegan.discovrus.adapter.MapCategoryAdapter;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CaseInsensitiveAssetFontLoader;
import com.danielandshayegan.discovrus.custome_veiws.CustomFontHtml.CustomHtml;
import com.danielandshayegan.discovrus.custome_veiws.OnSwipeTouchListener;
import com.danielandshayegan.discovrus.custome_veiws.circlelist.CircularListViewContentAlignment;
import com.danielandshayegan.discovrus.databinding.FragmentPostLocationBinding;
import com.danielandshayegan.discovrus.db.AppDatabase;
import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;
import com.danielandshayegan.discovrus.models.ClickPostCommentData;
import com.danielandshayegan.discovrus.models.DeleteCommentData;
import com.danielandshayegan.discovrus.models.MapListData;
import com.danielandshayegan.discovrus.models.MapPostDetail;
import com.danielandshayegan.discovrus.models.PostCommentData;
import com.danielandshayegan.discovrus.models.SaveLikeData;
import com.danielandshayegan.discovrus.models.SavePostCommentReplyData;
import com.danielandshayegan.discovrus.models.SavePostViewData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailActivity;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailVideoActivity;
import com.danielandshayegan.discovrus.ui.activity.ProfileDetailActivity;
import com.danielandshayegan.discovrus.utils.GPSTracker;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.gson.Gson;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.utils.BitmapUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deleteComment;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getCommentList;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getMapByPostId;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveComments;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveLike;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePostCommentReply;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePostView;
import static com.mapbox.mapboxsdk.style.expressions.Expression.all;
import static com.mapbox.mapboxsdk.style.expressions.Expression.division;
import static com.mapbox.mapboxsdk.style.expressions.Expression.get;
import static com.mapbox.mapboxsdk.style.expressions.Expression.gt;
import static com.mapbox.mapboxsdk.style.expressions.Expression.gte;
import static com.mapbox.mapboxsdk.style.expressions.Expression.has;
import static com.mapbox.mapboxsdk.style.expressions.Expression.literal;
import static com.mapbox.mapboxsdk.style.expressions.Expression.lt;
import static com.mapbox.mapboxsdk.style.expressions.Expression.toNumber;
import static com.mapbox.mapboxsdk.style.layers.Property.NONE;
import static com.mapbox.mapboxsdk.style.layers.Property.VISIBLE;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textColor;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textField;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textIgnorePlacement;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.textSize;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostLocationFragment extends BaseFragment implements SingleCallback, CommentsAdapter.DeletePosCommentListner {

    FragmentPostLocationBinding mBinding;
    View view;
    private MapView mapView;
    private MapboxMap mapboxMap;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    int userId;
    String userImagePath;
    GPSTracker gpsTracker;
    Location gpsLocation;
    List<ClickPostCommentData.DataBean> clickPostCommentList = new ArrayList<>();
    CommentsAdapter commentsAdapter;
    ClickPostCommentData.DataBean replyToData;
    boolean isReplyClicked = false;
    int commentId = 0;
    int postId;
    boolean isRefresh;
    MapPostDetail.DataBean postDetailData;
    List<CategoriesTableModel> categoriesList;
    MapListData SearchResult;
    MapListData TempData;
    List<String> layerIdList = new ArrayList<>();
    //LaunchpadFragment fragment;

    public PostLocationFragment() {
        // Required empty public constructor
    }

    public static PostLocationFragment newInstance() {
        return new PostLocationFragment();
    }

    public static PostLocationFragment newInstance(Bundle extras) {
        PostLocationFragment fragment = new PostLocationFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_location, container, false);
        view = mBinding.getRoot();

        Mapbox.getInstance(mActivity, getString(R.string.mapbox_access_token));

        disableScreen(false);
        Utils.dismissProgressBar();
        userId = 6;//App_pref.getAuthorizedUser(getActivity()).getData().getUserId(); //TODO: change user id
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);

        layerIdList.add("unclustered-cross");
        layerIdList.add("unclustered-points");
        layerIdList.add("cluster-0");
        layerIdList.add("cluster-1");
        layerIdList.add("cluster-2");
        layerIdList.add("count");

        mapView = view.findViewById(R.id.mapView);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap map) {
                mapboxMap = map;
                mapboxMap.getUiSettings().setAttributionEnabled(false);
                mapboxMap.getUiSettings().setLogoEnabled(false);

                mapboxMap.addOnCameraMoveListener(new MapboxMap.OnCameraMoveListener() {
                    @Override
                    public void onCameraMove() {
                        Log.e("on camera move", "lat long---> " + mapboxMap.getCameraPosition().target);
                        Log.e("on camera move", "zoom level ---> " + mapboxMap.getCameraPosition().zoom);
                        if (mapboxMap.getCameraPosition().zoom > 10) {
                            Layer layer = mapboxMap.getLayer("unclustered-points");
                            if (layer != null)
                                layer.setProperties(visibility(NONE));
                            Layer layer1 = mapboxMap.getLayer("unclustered-cross");
                            if (layer1 != null)
                                layer1.setProperties(visibility(VISIBLE));
                        } else {
                            Layer layer = mapboxMap.getLayer("unclustered-points");
                            if (layer != null)
                                layer.setProperties(visibility(VISIBLE));
                            Layer layer1 = mapboxMap.getLayer("unclustered-cross");
                            if (layer1 != null)
                                layer1.setProperties(visibility(NONE));
                        }

                    }
                });

                mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                        23.008698, 72.503071), 4));
                if (Utils.latitude == 0 && Utils.longitude == 0) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermission();
                        Log.e("ask for permission", "true");
                    } else {
                        getLocationCall();
                        Log.e("Prmsion given", "success");
                    }
                } else
                    getMapData(Utils.latitude, Utils.longitude);
                mapboxMap.addImage("cross-icon-id", BitmapUtils.getBitmapFromDrawable(
                        getResources().getDrawable(R.drawable.ic_cross)));
                mapboxMap.addImage("circle-icon-id", BitmapUtils.getBitmapFromDrawable(
                        getResources().getDrawable(R.drawable.ic_circle_small)));
                mapboxMap.addImage("circle-icon-large-id", BitmapUtils.getBitmapFromDrawable(
                        getResources().getDrawable(R.drawable.ic_circle)));

                mapboxMap.setOnMapClickListener(point -> {
                    PointF screenPoint = mapboxMap.getProjection().toScreenLocation(point);
                    List<Feature> features = mapboxMap.queryRenderedFeatures(screenPoint, "unclustered-cross");
                    if (!features.isEmpty()) {
                        // we received a click event on the callout layer
                        Feature feature = features.get(0);
                        String json = new Gson().toJson(feature);
                        try {
                            JSONObject object = new JSONObject(json);
                            JSONObject obj = object.getJSONObject("properties");
                            postId = obj.getInt("PostId");
//                            Toast.makeText(mActivity, "postId" + postId, Toast.LENGTH_SHORT).show();
                            callApiGetPostDetail();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        PointF symbolScreenPoint = mapboxMap.getProjection().toScreenLocation(convertToLatLng(feature));
                    }
                });

            }
        });
        initSearch();

        mBinding.relativePostDetail.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeRight() {
                mBinding.relativePostDetail.setVisibility(View.GONE);
            }

            @Override
            public void onSingleTap() {
                mBinding.relativePostDetail.setVisibility(View.GONE);
            }
        });

        mBinding.relativePostComments.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeRight() {
                mBinding.relativePostComments.setVisibility(View.GONE);
            }

            @Override
            public void onSingleTap() {
                mBinding.relativePostComments.setVisibility(View.GONE);
            }
        });

        mBinding.btnComment.setOnClickListener(view -> setCommentData());

        mBinding.txtPost.setOnClickListener(v -> {
            if (isReplyClicked) {
                if (mBinding.edtCommentReply.getText().toString().trim().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter reply", Toast.LENGTH_SHORT).show();
                } else {
                    isRefresh = true;
                    //Utils.showProgressBar(me);
                    disableScreen(false);
                    Utils.dismissProgressBar();
                    String commentToPass = "";
                    try {
                        commentToPass = URLEncoder.encode(mBinding.edtCommentReply.getText().toString().replace("Reply to @" + replyToData.getUserName() + " ", ""), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            savePostCommentReply(postId, userId, commentToPass, commentId)
                                    , getCompositeDisposable(), savePostCommentReply, this);
                }
            } else {
                if (validationForCommentPost()) {
                    isRefresh = true;
                    //Utils.showProgressBar(me);
                    disableScreen(false);
                    String commentToPass = "";
                    try {
                        commentToPass = URLEncoder.encode(mBinding.edtComment.getText().toString(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            postComment(String.valueOf(0), String.valueOf(postId), String.valueOf(userId), commentToPass)
                                    , getCompositeDisposable(), saveComments, this);
                }
            }

        });

        mBinding.btnLike.setOnClickListener(v -> {
            isRefresh = true;
            boolean isLikes = !postDetailData.isIsCommentLike();
            if (isLikes) {
                //  int likeCount = Integer.parseInt(mBinding.uiClickPost.txtFav.getText().toString().replaceAll("k","").replaceAll("m",""));
                int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.btnLike.getText().toString()));
                int addCount = likeCount + 1;
                String countToPrint = Utils.convertLikes(addCount);
                mBinding.btnLike.setText(countToPrint);
                postDetailData.setIsCommentLike(isLikes);
                mBinding.btnLike.setBackground(mContext.getResources().getDrawable(R.drawable.map_like_red_icon));
            } else {
                // int likeCount = Integer.parseInt(mBinding.uiClickPost.txtFav.getText().toString().replaceAll("k","").replaceAll("m",""));
                int likeCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.btnLike.getText().toString()));
                if (likeCount != 0) {
                    int minusLikeCount = likeCount - 1;
                    String countToPrint = Utils.convertLikes(minusLikeCount);
                    mBinding.btnLike.setText(countToPrint);
                    postDetailData.setIsCommentLike(isLikes);
                    mBinding.btnLike.setBackground(mContext.getResources().getDrawable(R.drawable.map_like_icon));
                }
            }
            getLikeApiCall(isLikes);

        });

        mBinding.btnGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                MenuActivity.menuActivity.openUserProfile(postDetailData.getUserId(), postDetailData.getUserType());
                Bundle bundle = new Bundle();
                bundle.putInt("userId", postDetailData.getUserId());

                Intent intent = new Intent(mActivity, ProfileDetailActivity.class);
                if (postDetailData.getUserType().equalsIgnoreCase("Business"))
                    intent.putExtra("clickFragmentName", "BusinessProfile");
                else
                    intent.putExtra("clickFragmentName", "IndividualProfile");
                intent.putExtra("fromProfileTab", false);
                intent.putExtras(bundle);
                mActivity.startActivity(intent);
            }
        });

        mBinding.constraintTextOnly.setOnClickListener(view -> {
            savePostView(postDetailData.getPostId());
            Intent intent = new Intent(getActivity(), ClickPostDetailActivity.class);
            intent.putExtra("clickFragmentName", "postText");
            intent.putExtra("postId", String.valueOf(postDetailData.getPostId()));
            startActivity(intent);
            getActivity().overridePendingTransition(0, 0);
        });

        mBinding.imgPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePostView(postDetailData.getPostId());
                switch (postDetailData.getType()) {
                    case "Video":
                        Bundle bundle = new Bundle();
                        bundle.putString("postId", String.valueOf(postDetailData.getPostId()));
                        Intent intent = new Intent(getActivity(), ClickPostDetailVideoActivity.class);
                        intent.putExtra("clickFragmentName", "postVideo");
                        intent.putExtras(bundle);
                        startActivity(intent);
                        getActivity().overridePendingTransition(0, 0);
                        break;
                    case "Photo":
                        Intent intent1 = new Intent(getActivity(), ClickPostDetailActivity.class);
                        intent1.putExtra("clickFragmentName", "postImage");
                        intent1.putExtra("postId", String.valueOf(postDetailData.getPostId()));
                        startActivity(intent1);
                        getActivity().overridePendingTransition(0, 0);
                        break;
                    case "PhotoAndText":
                        Intent intent2 = new Intent(getActivity(), ClickPostDetailActivity.class);
                        intent2.putExtra("clickFragmentName", "postImage");
                        intent2.putExtra("postId", String.valueOf(postDetailData.getPostId()));
                        startActivity(intent2);
                        getActivity().overridePendingTransition(0, 0);
                        break;
                }
            }
        });

        categoriesList = AppDatabase.getAppDatabase(mContext).categoryDao().getAll();

        mBinding.fab.setOnClickListener(view -> setCategoryList(categoriesList));

        mBinding.relativeCategory.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeRight() {
                for (int i = 0; i < layerIdList.size(); i++) {
                    mapboxMap.removeLayer(layerIdList.get(i));
                }
                mapboxMap.clear();
                mapboxMap.removeAnnotations();
                mapboxMap.removeSource("post");
                mBinding.relativeCategory.setVisibility(View.GONE);
                List<MapListData.FeaturesBean> featuresList = new ArrayList<>();

                for (int j = 0; j < SearchResult.getFeatures().size(); j++) {
                    for (int i = 0; i < categoriesList.size(); i++) {
                        MapListData.FeaturesBean.PropertiesBeanX properties = SearchResult.getFeatures().get(j).getProperties();
                        if (categoriesList.get(i).getIsSelected().equalsIgnoreCase("1")) {
                            if (properties.getCategoryName().equalsIgnoreCase(categoriesList.get(i).getCategoryName())) {
                                featuresList.add(SearchResult.getFeatures().get(j));
                            }
                        }
                    }
                }

                for (int i = 0; i < SearchResult.getFeatures().size(); i++) {
                    if (SearchResult.getFeatures().get(i).getProperties().getCategoryName().equalsIgnoreCase("")) {
                        featuresList.add(SearchResult.getFeatures().get(i));
                    }
                }
                TempData.setFeatures(featuresList);

                String json = new Gson().toJson(TempData);
                Log.e("JsonData", "TempData--------> " + json);
                new loadIconTask().execute(json);
            }
        });

        return view;
    }

    private LatLng convertToLatLng(Feature feature) {
        Point symbolPoint = (Point) feature.geometry();
        return new LatLng(symbolPoint.latitude(), symbolPoint.longitude());
    }

    private void initSearch() {
        mBinding.txtSearch.setOnClickListener(view -> {
            Intent intent = new PlaceAutocomplete.IntentBuilder()
                    .accessToken(Mapbox.getAccessToken())
                    .placeOptions(PlaceOptions.builder()
                            .backgroundColor(Color.parseColor("#EEEEEE"))
                            .limit(10)
//                                .addInjectedFeature(home)
//                                .addInjectedFeature(work)
                            .build(PlaceOptions.MODE_CARDS))
                    .build(mActivity);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {

            // Retrieve selected location's CarmenFeature
            CarmenFeature selectedCarmenFeature = PlaceAutocomplete.getPlace(data);

            // Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above
            FeatureCollection featureCollection = FeatureCollection.fromFeatures(
                    new Feature[]{Feature.fromJson(selectedCarmenFeature.toJson())});

            // Retrieve and update the source designated for showing a selected location's symbol layer icon
            /*GeoJsonSource source = mapboxMap.getSourceAs(geojsonSourceLayerId);
            if (source != null) {
                source.setGeoJson(featureCollection);
            }*/

            // Move map camera to the selected location
            CameraPosition newCameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(((Point) selectedCarmenFeature.geometry()).latitude(),
                            ((Point) selectedCarmenFeature.geometry()).longitude()))
                    .zoom(10)
                    .build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition), 4000);
        }
    }

      /*  @Override
@Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        //MenuActivity.fragmentView = 1;
    }

    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }*/

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            getLocationCall();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    private void getLocationCall() {
        Log.e("getLocationCall", "success");
        //Utils.showProgressBar(me);
        gpsTracker = new GPSTracker(getActivity());
        gpsLocation = gpsTracker.getLocation();

        try {
            Log.e("try", "success");

            Thread.sleep(1000);
            if (gpsLocation != null) {
                Log.e("gpsLocation!= null", "success");
                Utils.dismissProgressBar();
                Utils.latitude = gpsLocation.getLatitude();
                Utils.longitude = gpsLocation.getLongitude();
                getMapData(gpsLocation.getLatitude(), gpsLocation.getLongitude());
            } else {
                Log.e("gpsLocation null", "success");
                Toast.makeText(getActivity(), "we are unable to detect your location.please search manually", Toast.LENGTH_LONG).show();
                Utils.dismissProgressBar();
            }
        } catch (InterruptedException e) {
            Log.e("catch", "success");
            Thread.interrupted();
            Utils.dismissProgressBar();
            Toast.makeText(getActivity(), "we are unable to detect your location.please search manually", Toast.LENGTH_LONG).show();
            disableScreen(false);
        }
    }

    public void getMapData(double latitude, double longitude) {
        try {
            //Utils.showProgressBar(me);
            disableScreen(false);
            Utils.dismissProgressBar();
        } catch (Exception e) {
            e.printStackTrace();
            disableScreen(false);
            Utils.dismissProgressBar();
        }
        disposable.add(apiService.getMapList(latitude, longitude, 1000, userId).
//        disposable.add(apiService.getMapList(22.994555, 72.616315, 1000).
        subscribeOn(Schedulers.io()).
                        observeOn(AndroidSchedulers.mainThread()).
                        subscribeWith(new DisposableSingleObserver<MapListData>() {
                            @Override
                            public void onSuccess(MapListData value) {
                                if (value != null) {
                                    SearchResult = value;
                                    TempData = value;
                                    if (SearchResult.getFeatures() != null && SearchResult.getFeatures().size() != 0) {

                                        List<MapListData.FeaturesBean> featuresList = new ArrayList<>();

                                        for (int j = 0; j < SearchResult.getFeatures().size(); j++) {
                                            for (int i = 0; i < categoriesList.size(); i++) {
                                                MapListData.FeaturesBean.PropertiesBeanX properties = SearchResult.getFeatures().get(j).getProperties();
                                                if (categoriesList.get(i).getIsSelected().equalsIgnoreCase("1")) {
                                                    if (properties.getCategoryName().equalsIgnoreCase(categoriesList.get(i).getCategoryName())) {
                                                        featuresList.add(SearchResult.getFeatures().get(j));
                                                    }
                                                }
                                            }
                                        }

                                        for (int i = 0; i < SearchResult.getFeatures().size(); i++) {
                                            if (SearchResult.getFeatures().get(i).getProperties().getCategoryName().equalsIgnoreCase("")) {
                                                featuresList.add(SearchResult.getFeatures().get(i));
                                            }
                                        }
                                        TempData.setFeatures(featuresList);

                                        String json = new Gson().toJson(TempData);
//                                String json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1688,\"CategoryId\":11,\"CategoryName\":\"IT\",\"RadiansDistance\":1.3706361977287351,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[23.0497,72.5117]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1711,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[23.022505,72.5713621]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1712,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-151.3597,63.0781]}}]}";
//                                String json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1688,\"CategoryId\":11,\"CategoryName\":\"IT\",\"RadiansDistance\":1.3706361977287351,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[100.507665295,0.0035208782436]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1711,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[11188462.0943,389.378064197]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1712,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-151.3597,63.0781]}}]}";
//                                String json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1688,\"CategoryId\":11,\"CategoryName\":\"IT\",\"RadiansDistance\":1.3706361977287351,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117,23.0497]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1711,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5713621,23.022505]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1712,\"CategoryId\":0,\"CategoryName\":\"\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-151.3597,63.0781]}}]}";
//                                String json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1688,\"CategoryId\":11,\"poi\":\"restaurant\",\"CategoryName\":\"IT\",\"RadiansDistance\":1.3706361977287351,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117,23.0497]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1711,\"CategoryId\":0,\"poi\":\"lodging\",\"CategoryName\":\"Education\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/location-pin.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5713621,23.022505]}},{\"type\":\"Feature\",\"properties\":{\"Distance\":null,\"PostId\":1712,\"CategoryId\":0,\"poi\":\"cafe\",\"CategoryName\":\"Food\",\"RadiansDistance\":6.740036593165599,\"Icon\":\"/files/category/Icon/IT.png\"},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-151.3597,63.0781]}}]}";
//                                json = "{\"type\":\"FeatureCollection\",\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"\"}},\"features\":[{\"type\":\"Feature\",\"properties\":{\"CategoryId\":11,\"CategoryName\":\"IT\",\"Icon\":\"/files/category/Icon/IT.png\",\"Lat\":\"23.500497\",\"Long\":\"72.5117\",\"PostId\":1688,\"PostNo\":0,\"RadiansDistance\":1.371268312512451},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117003,23.0497]}},{\"type\":\"Feature\",\"properties\":{\"CategoryId\":0,\"CategoryName\":\"\",\"Icon\":\"/files/category/Icon/location-pin.png\",\"Lat\":\"24.03500230\",\"Long\":\"72.543910\",\"PostId\":1256,\"PostNo\":0,\"RadiansDistance\":3.7273995709421888},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117,24.0497]}},{\"type\":\"Feature\",\"properties\":{\"CategoryId\":0,\"CategoryName\":\"\",\"Icon\":\"/files/category/Icon/location-pin.png\",\"Lat\":\"23.022505\",\"Long\":\"72.5713621\",\"PostId\":1711,\"PostNo\":0,\"RadiansDistance\":6.746924368285192},\"geometry\":{\"type\":\"Point\",\"coordinates\":[72.5117,25.0497]}}]}";
                                        new loadIconTask().execute(json);
                                /*mapboxMap.addImage("Education-64", BitmapUtils.getBitmapFromDrawable(
                                        getResources().getDrawable(R.drawable.education)));
                                mapboxMap.addImage("IT-64", BitmapUtils.getBitmapFromDrawable(
                                        getResources().getDrawable(R.drawable.it)));
                                mapboxMap.addImage("Food-64", BitmapUtils.getBitmapFromDrawable(
                                        getResources().getDrawable(R.drawable.food)));*/

//                                addClusteredGeoJsonSource(json);
                                        Utils.dismissProgressBar();
                                        disableScreen(false);
                                    } else {
                                        disableScreen(false);
                                        Utils.dismissProgressBar();
                                    }
                                } else {
                                    if (value.getFeatures() == null && value.getFeatures().size() == 0) {
                                        Toast.makeText(getActivity(), "No data available", Toast.LENGTH_LONG).show();
                                        Utils.dismissProgressBar();
                                        disableScreen(false);
                                    } else {
                                        Toast.makeText(getActivity(), "Something want wrong; Please try after sometime.", Toast.LENGTH_LONG).show();
                                        Utils.dismissProgressBar();
                                        disableScreen(false);
                                    }
                                }
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.d("fail", e.toString());
                                Utils.dismissProgressBar();
                                disableScreen(false);
                                Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                            }

                        }));

    }

    private void addClusteredGeoJsonSource(String json) {

        // Add a new source from the GeoJSON data and set the 'cluster' option to true.
       /* try {
            mapboxMap.addSource(
                    // Point to GeoJSON data. This example visualizes all M1.0+ earthquakes from
                    // 12/22/15 to 1/21/16 as logged by USGS' Earthquake hazards program.
                    new GeoJsonSource("earthquakes",
                            new URL("https://www.mapbox.com/mapbox-gl-js/assets/earthquakes.geojson"),
                            new GeoJsonOptions()
                                    .withCluster(true)
                                    .withClusterMaxZoom(8)
                                    .withClusterRadius(50)
                    )
            );
        } catch (MalformedURLException malformedUrlException) {
            Log.e("dataClusterActivity", "Check the URL " + malformedUrlException.getMessage());
        }
*/
        try {
            mapboxMap.addSource(new GeoJsonSource("post", json,
                            new GeoJsonOptions()
                                    .withCluster(true)
                                    .withClusterMaxZoom(8)
                                    .withClusterRadius(50)
                    )
            );
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Use the earthquakes GeoJSON source to create three layers: One layer for each cluster category.
        // Each point range gets a different fill color.
        int[][] layers = new int[][]{
                new int[]{300, ContextCompat.getColor(mActivity, R.color.colorPrimary)},
                new int[]{100, ContextCompat.getColor(mActivity, R.color.colorPrimary)},
                new int[]{0, ContextCompat.getColor(mActivity, R.color.colorPrimary)}
        };

        //Creating a marker layer for single data points
        SymbolLayer unclustered = new SymbolLayer("unclustered-cross", "post");

        unclustered.setProperties(
                iconImage("{CategoryName}-64"),
                iconSize(
                        division(
                                get("mag"), literal(2.0f)
                        )
                )

               /* ,
                iconColor(
                        interpolate(exponential(1), get("mag"),
                                stop(2.0, rgb(0, 255, 0)),
                                stop(4.5, rgb(0, 0, 255)),
                                stop(7.0, rgb(255, 0, 0))
                        )
                )*/
        );
        mapboxMap.addLayer(unclustered);

//        new loadIconTask().execute(json);

        //Creating a marker layer for single data points
        SymbolLayer unclustered1 = new SymbolLayer("unclustered-points", "post");

        unclustered1.setProperties(
                iconImage("circle-icon-id"),
                iconSize(
                        division(
                                get("mag"), literal(10.0f)
                        )
                )
                /*iconColor(
                        interpolate(exponential(1), get("mag"),
                                stop(2.0, rgb(0, 255, 0)),
                                stop(4.5, rgb(0, 0, 255)),
                                stop(7.0, rgb(255, 0, 0))
                        )
                )*/
        );
        mapboxMap.addLayer(unclustered1);

        for (int i = 0; i < layers.length; i++) {
            //Add clusters' circles
            /*CircleLayer circles = new CircleLayer("cluster-" + i, "earthquakes");
            circles.setProperties(
                    circleColor(layers[i][1]),
                    circleRadius(18f)
            );*/

            SymbolLayer circles = new SymbolLayer("cluster-" + i, "post");
            circles.setProperties(
                    iconImage("circle-icon-large-id"),
                    iconSize(division(get("mag"), literal(18f)))
            );

            Expression pointCount = toNumber(get("point_count"));
            // Add a filter to the cluster layer that hides the circles based on "point_count"
            circles.setFilter(
                    i == 0
                            ? all(has("point_count"),
                            gte(pointCount, literal(layers[i][0]))
                    ) : all(has("point_count"),
                            gt(pointCount, literal(layers[i][0])),
                            lt(pointCount, literal(layers[i - 1][0]))
                    )
            );
            mapboxMap.addLayer(circles);

        }

        //Add the count labels
        SymbolLayer count = new SymbolLayer("count", "post");
        count.setProperties(
                textField(Expression.toString(get("point_count"))),
                textSize(12f),
                textColor(Color.WHITE),
                textIgnorePlacement(true),
                textAllowOverlap(true)
        );
        mapboxMap.addLayer(count);

//        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
//                Utils.latitude, Utils.longitude), 11));
        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                22.994555, 72.616315), 11));

    }

    class loadIconTask extends AsyncTask<String, Void, List<Bitmap>> {
        List<String> categoryNameList = new ArrayList<>();
        List<Bitmap> bitmapList = new ArrayList<>();
        String json;

        @Override
        protected List<Bitmap> doInBackground(String... strings) {
            json = strings[0];
            JSONArray array = null;
            try {
                JSONObject object = new JSONObject(strings[0]);
                array = object.getJSONArray("features");
            } catch (JSONException e) {
                e.printStackTrace();
                disableScreen(false);
                Utils.dismissProgressBar();
            }
            for (int i = 0; i < array.length(); i++) {
                try {
                    JSONObject properties = array.getJSONObject(i).getJSONObject("properties");
                    if (properties != null) {
                        String Icon = properties.getString("Icon");
                        URL url = null;
                        try {
                            url = new URL(ApiClient.WebService.imageUrl + Icon);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            disableScreen(false);
                            Utils.dismissProgressBar();
                        }
                        Bitmap bitmap = null;
                        try {
                            bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                        } catch (IOException e) {
                            e.printStackTrace();
                            disableScreen(false);
                            Utils.dismissProgressBar();
                        }
                        String CategoryName = properties.getString("CategoryName");
                        categoryNameList.add(CategoryName);
                        bitmapList.add(bitmap);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return bitmapList;
        }

        @Override
        protected void onPostExecute(List<Bitmap> list) {
            super.onPostExecute(list);
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    mapboxMap.addImage(categoryNameList.get(i) + "-64", list.get(i));
                }
            }
            addClusteredGeoJsonSource(json);
        }
    }

    public void getLikeApiCall(boolean isLikes) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                saveLike(postId, postDetailData.getUserId(), isLikes, userId)
                        , getCompositeDisposable(), saveLike, this);
    }

    public void savePostView(int postId) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                savePostView(postId, userId)
                        , getCompositeDisposable(), savePostView, this);
    }

    public void setCommentData() {
        mBinding.relativePostComments.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mBinding.recyclerPostComments.setLayoutManager(mLayoutManager);
        mBinding.recyclerPostComments.setItemAnimator(new DefaultItemAnimator());
        userImagePath = App_pref.getAuthorizedUser(getActivity()).getData().getUserImagePath();
        if (userImagePath != null) {
            Log.i("imgPath", userImagePath);
            Picasso.with(getActivity()).
                    load(ApiClient.WebService.imageUrl + userImagePath).
                    into(mBinding.imgUserProfile);
            Log.i("imgPath", ApiClient.WebService.imageUrl + userImagePath);
        }
        Picasso.with(mActivity).
                load(ApiClient.WebService.imageUrl + postDetailData.getPostPath()).
                placeholder(R.drawable.user_placeholder).
                into(mBinding.imageViewPost);
        mBinding.btnCommentCount.setText(Utils.convertLikes(postDetailData.getCommentCount()));
        if (postDetailData.isIsCommented()) {
            mBinding.btnCommentCount.setBackground(mActivity.getResources().getDrawable(R.drawable.map_comment_green_icon));
        } else {
            mBinding.btnCommentCount.setBackground(mActivity.getResources().getDrawable(R.drawable.map_comment_icon));
        }
        callApiComments();

    }

    public void callApiComments() {
        //Utils.showProgressBar(me);
        disableScreen(false);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getCommentList(postId, userId)
                        , getCompositeDisposable(), getCommentList, this);
    }

    public void callApiGetPostDetail() {
        //Utils.showProgressBar(me);
        disableScreen(false);
        mBinding.relativePostDetail.setVisibility(View.VISIBLE);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                getMapByPostId(postId, userId, Utils.latitude, Utils.longitude)
                        , getCompositeDisposable(), getMapByPostId, this);
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case getCommentList:
                Utils.dismissProgressBar();
                disableScreen(false);
                clickPostCommentList.clear();
                ClickPostCommentData clickPostCommentData = (ClickPostCommentData) o;
                if (clickPostCommentData.isSuccess()) {

                    clickPostCommentList.addAll(clickPostCommentData.getData());
//                    clickPostCommentAdapter.notifyDataSetChanged();
                    commentsAdapter = new CommentsAdapter(postId, clickPostCommentList, getActivity(), this);
                    mBinding.recyclerPostComments.setAdapter(commentsAdapter);
                    if (clickPostCommentData.getData().size() > 0) {
                        if (clickPostCommentData.getData().get(0).isIsCommented()) {
                            mBinding.btnCommentCount.setBackground(mContext.getResources().getDrawable(R.drawable.map_comment_green_icon));
                        }
                    }
                }
                break;

            case deleteComment:
                Utils.dismissProgressBar();
                disableScreen(false);
                DeleteCommentData deleteCommentData = (DeleteCommentData) o;
                if (deleteCommentData.isSuccess()) {
                    int commentCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.btnCommentCount.getText().toString()));
                    int addCount = commentCount - 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinding.btnCommentCount.setText(countToPrint);
                    //  Toast.makeText(getActivity(), deleteCommentData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(getActivity(), "false", Toast.LENGTH_SHORT).show();
                }
                break;

            case saveComments:
                Utils.dismissProgressBar();
                disableScreen(false);
                PostCommentData postCommentData = (PostCommentData) o;
                if (postCommentData.isSuccess()) {
                    mBinding.edtComment.setText("");
                    int commentCount = Integer.parseInt(Utils.convertToOriginalCount(mBinding.btnCommentCount.getText().toString()));
                    int addCount = commentCount + 1;
                    String countToPrint = Utils.convertLikes(addCount);
                    mBinding.btnCommentCount.setText(countToPrint);
                    mBinding.btnCommentCount.setBackground(mContext.getResources().getDrawable(R.drawable.map_comment_green_icon));
                    callApiComments();
                }
                break;

            case savePostCommentReply:
                Utils.dismissProgressBar();
                disableScreen(false);
                SavePostCommentReplyData postCommentReplyData = (SavePostCommentReplyData) o;
                if (postCommentReplyData.isSuccess()) {
                    mBinding.edtComment.setText("");
                    mBinding.edtCommentReply.setText("");
                    mBinding.edtComment.setVisibility(View.VISIBLE);
                    mBinding.edtCommentReply.setVisibility(View.GONE);
                    callApiComments();
                }
                break;

            case getMapByPostId:
                Utils.dismissProgressBar();
                disableScreen(false);
                MapPostDetail mapPostDetail = (MapPostDetail) o;
                if (mapPostDetail.isSuccess()) {
                    postDetailData = mapPostDetail.getData().get(0);
                    if (postDetailData != null) {
                        Picasso.with(getActivity()).
                                load(ApiClient.WebService.imageUrl + postDetailData.getUserImagePath()).
                                placeholder(R.drawable.user_placeholder).
                                into(mBinding.imageViewUserProfile);
                        mBinding.textUserName.setText(postDetailData.getUserName());
                        mBinding.textUserAddress.setText(postDetailData.getLocation());
                        mBinding.textPostTime.setText(postDetailData.getTime());
                        mBinding.textPostDistance.setText(new DecimalFormat("##.##").format(postDetailData.getDistance()) + " min");
                        if (postDetailData.isIsCommented()) {
                            mBinding.btnComment.setBackground(mActivity.getResources().getDrawable(R.drawable.map_comment_green_icon));
                        } else {
                            mBinding.btnComment.setBackground(mActivity.getResources().getDrawable(R.drawable.map_comment_icon));
                        }
                        if (postDetailData.isIsCommentLike()) {
                            mBinding.btnLike.setBackground(mActivity.getResources().getDrawable(R.drawable.map_like_red_icon));
                        } else {
                            mBinding.btnLike.setBackground(mActivity.getResources().getDrawable(R.drawable.map_like_icon));
                        }
                        String likeCount = Utils.convertLikes(postDetailData.getCommentLikeCount());
                        String commentCount = Utils.convertLikes(postDetailData.getCommentCount());
                        mBinding.btnLike.setText(likeCount);
                        mBinding.btnComment.setText(commentCount);
                        mBinding.btnPostCount.setText(new DecimalFormat("00").format(postDetailData.getPostCountInWeek()));
                        switch (postDetailData.getType()) {
                            case "Photo":
                                mBinding.btnPostCount.setText(new DecimalFormat("00").format(postDetailData.getPostCountInWeek()));
                                Picasso.with(getActivity()).
                                        load(ApiClient.WebService.imageUrl + postDetailData.getPostPath()).
                                        placeholder(R.drawable.placeholder_image).
                                        into(mBinding.imgPost);
                                if (userImagePath != null) {
//            Log.i("imgPath", userImagePath);
                                    Picasso.with(getActivity()).
                                            load(ApiClient.WebService.imageUrl + userImagePath).
                                            into(mBinding.imgUserProfile);
                                }
                                mBinding.imgPost.setVisibility(View.VISIBLE);
                                mBinding.imgPlay.setVisibility(View.GONE);
                                mBinding.relativeImageWithText.setVisibility(View.GONE);
                                mBinding.scrollOnlyText.setVisibility(View.GONE);
                                break;
                            case "Text":
                                mBinding.btnPostCount.setText(new DecimalFormat("00").format(postDetailData.getPostCountInWeek()));
                                if (userImagePath != null) {
//            Log.i("imgPath", userImagePath);
                                    Picasso.with(getActivity()).
                                            load(ApiClient.WebService.imageUrl + userImagePath).
                                            into(mBinding.imgUserProfile);
                                }
                                String title = "";
                                try {
                                    title = URLDecoder.decode(postDetailData.getTitle(), "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                String desc = "";
                                try {
                                    desc = URLDecoder.decode(postDetailData.getDescription(), "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                final CaseInsensitiveAssetFontLoader fontLoader = new CaseInsensitiveAssetFontLoader(mActivity.getApplicationContext(), "fonts");
                                mBinding.textTitle.setText(CustomHtml.fromHtml(title, fontLoader));
                                mBinding.textDescription.setText(CustomHtml.fromHtml(desc, fontLoader));
                                if (desc.contains("<ul>")) {
                                    mBinding.textDescription.setText(Html.fromHtml(desc));
                                    switch (Utils.fontName) {
                                        case "dancing_script_bold":
                                            mBinding.textDescription.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                                            break;
                                        case "hanaleifill_regular":
                                            mBinding.textDescription.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                                            break;
                                        case "merienda_bold":
                                            mBinding.textDescription.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                                            break;
                                        case "opificio_light_rounded":
                                            mBinding.textDescription.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                                            break;
                                        default:
                                            mBinding.textDescription.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                                            break;
                                    }
                                }
//                                mBinding.textTitle.setText(postDetailData.getTitle());
//                                mBinding.textDescription.setText(postDetailData.getDescription());
                                mBinding.imgPost.setVisibility(View.GONE);
                                mBinding.imgPlay.setVisibility(View.GONE);
                                mBinding.relativeImageWithText.setVisibility(View.GONE);
                                mBinding.scrollOnlyText.setVisibility(View.VISIBLE);
                                break;
                            case "PhotoAndText":
                                mBinding.btnPostCount.setText(new DecimalFormat("00").format(postDetailData.getPostCountInWeek()));
                                if (userImagePath != null) {
                                    Picasso.with(getActivity()).
                                            load(ApiClient.WebService.imageUrl + userImagePath).
                                            into(mBinding.imgUserProfile);
                                }

                                Picasso.with(getActivity()).
                                        load(ApiClient.WebService.imageUrl + postDetailData.getPostPath()).
                                        placeholder(R.drawable.placeholder_image).
                                        into(mBinding.imgPost);
                                String ttl = "";
                                try {
                                    ttl = URLDecoder.decode(postDetailData.getTitle(), "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                String dsc = "";
                                try {
                                    dsc = URLDecoder.decode(postDetailData.getDescription(), "UTF-8");
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                final CaseInsensitiveAssetFontLoader fontLoader1 = new CaseInsensitiveAssetFontLoader(mActivity.getApplicationContext(), "fonts");
                                mBinding.textTitleIwt.setText(CustomHtml.fromHtml(ttl, fontLoader1));
                                mBinding.textDescriptionIwt.setText(CustomHtml.fromHtml(dsc, fontLoader1));
                                if (dsc.contains("<ul>")) {
                                    mBinding.textDescriptionIwt.setText(Html.fromHtml(dsc));
                                    switch (Utils.fontName) {
                                        case "dancing_script_bold":
                                            mBinding.textDescriptionIwt.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/dancing_script_bold.ttf"));
                                            break;
                                        case "hanaleifill_regular":
                                            mBinding.textDescriptionIwt.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/hanaleifill_regular.ttf"));
                                            break;
                                        case "merienda_bold":
                                            mBinding.textDescriptionIwt.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/merienda_bold.ttf"));
                                            break;
                                        case "opificio_light_rounded":
                                            mBinding.textDescriptionIwt.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/opificio_light_rounded.ttf"));
                                            break;
                                        default:
                                            mBinding.textDescriptionIwt.setTypeface(Typeface.createFromAsset(mActivity.getAssets(), "fonts/avenir_roman.ttf"));
                                            break;
                                    }
                                }
//                                mBinding.textTitleIwt.setText(postDetailData.getTitle());
//                                mBinding.textDescriptionIwt.setText(postDetailData.getDescription());
                                mBinding.imgPost.setVisibility(View.VISIBLE);
                                mBinding.imgPlay.setVisibility(View.GONE);
                                mBinding.relativeImageWithText.setVisibility(View.VISIBLE);
                                mBinding.scrollOnlyText.setVisibility(View.GONE);
                                break;
                            case "Video":
                                mBinding.btnPostCount.setText(new DecimalFormat("00").format(postDetailData.getPostCountInWeek()));
                                if (userImagePath != null) {
                                    Picasso.with(getActivity()).
                                            load(ApiClient.WebService.imageUrl + userImagePath).
                                            into(mBinding.imgUserProfile);
                                }
                                Picasso.with(getActivity()).
                                        load(ApiClient.WebService.imageUrl + postDetailData.getThumbFileName()).
                                        placeholder(R.drawable.placeholder_image).
                                        into(mBinding.imgPost);
                                mBinding.imgPost.setVisibility(View.VISIBLE);
                                mBinding.imgPlay.setVisibility(View.VISIBLE);
                                mBinding.relativeImageWithText.setVisibility(View.GONE);
                                mBinding.scrollOnlyText.setVisibility(View.GONE);
                                break;
                        }

                    }
                }
                break;

            case saveLike:
                SaveLikeData saveLikeData = (SaveLikeData) o;
                if (saveLikeData.isSuccess()) {
//                    callAPI(false);
                } else {
                    Toast.makeText(getActivity(), saveLikeData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;

            case savePostView:
                SavePostViewData savePostViewData = (SavePostViewData) o;
                if (savePostViewData.isSuccess()) {
//                    Toast.makeText(getActivity(), savePostViewData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), savePostViewData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        Utils.dismissProgressBar();
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCommentDeleted(ClickPostCommentData.DataBean clickpostData) {
        //Utils.showProgressBar(me);
        disableScreen(false);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                deleteComment(String.valueOf(clickpostData.getCommentId()))
                        , getCompositeDisposable(), deleteComment, this);
    }

    @Override
    public void onCommentReplyClick(ClickPostCommentData.DataBean clickpostData) {
        replyToData = clickpostData;
        mBinding.edtComment.setText("");
        isReplyClicked = true;
        commentId = clickpostData.getCommentId();
        mBinding.edtComment.setVisibility(View.GONE);
        mBinding.edtCommentReply.setText(Html.fromHtml("<font color='#9E9CAC'> Reply to @" + clickpostData.getUserName() + " </font>"));
        Selection.setSelection(mBinding.edtCommentReply.getText(), mBinding.edtCommentReply.getText().length());
        mBinding.edtCommentReply.setVisibility(View.VISIBLE);
        mBinding.scrollCommentPost.scrollTo(0, 0);
    }

    public boolean validationForCommentPost() {
        boolean value = true;
        if (mBinding.edtComment.getText().toString().trim().length() == 0) {
            Toast.makeText(getActivity(), "Please enter comment", Toast.LENGTH_SHORT).show();
            value = false;
        }
        return value;

    }

    public void setCategoryList(List<CategoriesTableModel> categoriesTableModels) {
        mBinding.relativeCategory.setVisibility(View.VISIBLE);
        MapCategoryAdapter mapCategoryAdapter = new MapCategoryAdapter(categoriesTableModels, mActivity);

        Display display = mActivity.getWindowManager().getDefaultDisplay();
        mBinding.circularListView.setRadius(Math.min(300, display.getWidth() / 2));
        mBinding.circularListView.setAdapter(mapCategoryAdapter);
        mBinding.circularListView.scrollFirstItemToCenter();
        mBinding.circularListView.setCircularListViewContentAlignment(CircularListViewContentAlignment.Left);

//        mBinding.circularListView.setCircularListViewListener((circularListView, firstVisibleItem, visibleItemCount, totalItemCount) -> {
//         refreshCircular();
//        });
    }

   /* void refreshCircular() {
        if (mIsAdapterDirty) {
            mCircularListView.scrollFirstItemToCenter();
            mIsAdapterDirty = false;
        }

        TextView centerView = (TextView) mCircularListView.getCentralChild();

        if (centerView != null) {
            centerView.setTextColor(getResources().getColor(R.color.center_text));
        }
        for (int i = 0; i < mCircularListView.getChildCount(); i++) {
            TextView view = (TextView) mCircularListView.getChildAt(i);
            if (view != null && view != centerView) {
                view.setTextColor(getResources().getColor(R.color.default_text));
            }
        }
    }*/
  /* public void handleClicks(View v)
   {
       fragment.handleClicks(v);
   }*/
}
