package com.danielandshayegan.discovrus.ui.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.danielandshayegan.discovrus.ApplicationClass;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivityMainBinding;
import com.danielandshayegan.discovrus.instagram.InstagramResponse;
import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.models.LoginData;
import com.danielandshayegan.discovrus.models.SocialMediaLoginData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.single;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;


public class MainActivity extends BaseActivity implements SingleCallback {


    ActivityMainBinding mBinding;
    String animationType = "";
    Animation fadeIn, fadeOut;
    long signUpDuration;
    long loginDuration;
    boolean facebookSocialMediaLogin;
    private CallbackManager callbackManager;
    int signUpType; //email = 1, linkedin = 2, insta = 3; individual = 4, business = 5;
    public static final String SCALEX = "scaleX";
    public static final String SCALEY = "scaleY";
    public static final String TRANSITIONY = "translationY";
    public static final String ACTIVITY_INTENT = "activityIntent";
    public static final String FRAGMENT_LOGIN = "Login";
    public static final String FRAGMENT_INDIVIDUAL = "IndividualRegister";
    public static final String FRAGMENT_BUSINESS = "BusinessRegister";
    public static final String FRAGMENT_FORGOTPASS = "ForgotPass";
    public static final String FRAGMENT_CONFEMAIL = "ConfirmEmail";
    public static final String FRAGMENT_MAIN = "MainFrgament";
    InstagramResponse mApp;
    boolean isFirstTime = true;
    private static final String TAG = "tag";
    SocialMediaLoginData socialMediaLoginData;
    private static final int RECORD_REQUEST_CODE = 101;
    boolean isEmail = false;
    boolean nextButtonVisibility = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        makeRequest();
        FacebookSdk.sdkInitialize(this);
        mApp = new InstagramResponse(MainActivity.this, Utils.CLIENT_ID,
                Utils.CLIENT_SECRATE_ID, Utils.REDIRECT_URI);
        fadeIn = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_in_img);
        fadeOut = AnimationUtils.loadAnimation(MainActivity.this, R.anim.fade_out);
        mBinding.signup.btnSignup.setOnClickListener(v -> optionAnimation(true));
        mBinding.options.imgArrow.setOnClickListener(v -> {
            backanimation();
            accountTypeSection();
        });
        mBinding.signup.btnLogin.setOnClickListener(v -> optionAnimation(false));
        mBinding.options.imgArrowDown.setOnClickListener(v -> {

            backanimation();
            accountTypeSection();
            signUpType = 1;

        });

        Intent intent = getIntent();
        if (null != intent) {


            animationType = intent.getExtras().getString("animation");
            if (animationType.equals("splash")) {
                mBinding.lineMainBg.setVisibility(View.VISIBLE);
                if (isFirstTime) {
                    startAnimation(true);
                }

            }
            if (animationType.equals("loginAnimation")) {
                mBinding.lineMainBg.setVisibility(View.GONE);
                mBinding.lineLoginBg.setVisibility(View.VISIBLE);
                optionAnimation(false);
            }

        }

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();

        mBinding.options.loginButtonFacebook.setReadPermissions("email");
        // mBinding.login.loginButtonFacebook.setFragment(this);
        mBinding.options.loginButtonFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();
                Log.i("accessToken", accessToken);

//                Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("LoginActivity", response.toString());
                        // Get facebook data from login
                        String firstName = null, lastName = null, email = null, id = null;

                        Bundle bFacebookData = getFacebookData(object);
                        try {
                            firstName = object.getString("first_name");
                            lastName = object.getString("last_name");
                            email = object.getString("email");
                            id = object.getString("id");


                            socialMediaLoginData = new SocialMediaLoginData();
                            socialMediaLoginData.setFirstName(firstName);
                            if (lastName != null) {
                                socialMediaLoginData.setLastName(lastName);
                            }
                            socialMediaLoginData.setEmailAddress(email);
                            // socialMediaLoginData.setPhoto(object.getString("profile_pic"));
                            URL imageURL = null;
                            try {
                                imageURL = new URL("https://graph.facebook.com/" + id + "/picture?type=large");
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            }
                            if (imageURL != null)
                                socialMediaLoginData.setPhoto(imageURL.toString());
                            else {
                                socialMediaLoginData.setPhoto("");

                            }

                            socialMediaLoginData.setId(id);
                            socialMediaLoginData.setLinkedIn(false);
                            socialMediaLoginData.setFaceBook(true);

                            Log.d("IsFacebook", String.valueOf(socialMediaLoginData.getIsFaceBook()));
                            if (facebookSocialMediaLogin) {
                                Log.e("socialMedia", "pageTransfer");
                                mBinding.options.txtSignup.startAnimation(fadeOut);
                                mBinding.lineLoginBg.setVisibility(View.VISIBLE);
                                accountType();
                            } else {
                                mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                                disableScreen(true);
                                callLoginAPI(id, "FaceBook", email);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email"); // Parámetros que pedimos a facebook
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
//                Toast.makeText(getActivity(), "cancel", Toast.LENGTH_SHORT).show();
                Log.i("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(MainActivity.this, exception.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i("LoginActivity", exception.toString());
            }
        });

    }

    public void accountTypeSection() {
        mBinding.options.txtNext.setText("");
        mBinding.options.txtAccount.setText("");
        mBinding.options.txtProfile.setText("");
        mBinding.options.imgIndividual.setVisibility(View.GONE);
        mBinding.options.imgBusiness.setVisibility(View.GONE);
        mBinding.options.txtNext.setVisibility(View.GONE);
        mBinding.options.txtAccount.setVisibility(View.GONE);
        mBinding.options.txtProfile.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }

    public void startAnimation(boolean mainBg) {
        mBinding.signup.btnSignup.setVisibility(View.VISIBLE);


        if (!mainBg) {
            signUpDuration = 1200;
            loginDuration = 1600;
        } else {
            signUpDuration = 1600;
            loginDuration = 2000;
        }
        Animator translationAnimator = ObjectAnimator
                .ofFloat(mBinding.signup.btnSignup, View.TRANSLATION_Y, 1500f, 0f)
                .setDuration(signUpDuration);
        translationAnimator.start();


        if (mBinding.signup.btnSignup.getVisibility() != View.GONE) {
            mBinding.signup.btnLogin.setVisibility(View.VISIBLE);
            Animator translationAnimatorLogin = ObjectAnimator
                    .ofFloat(mBinding.signup.btnLogin, View.TRANSLATION_Y, 1500f, 0f)
                    .setDuration(loginDuration);
            translationAnimatorLogin.start();

            translationAnimatorLogin.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {

                    if (mainBg) {
                        mBinding.lineMainBg.startAnimation(fadeOut);
                        mBinding.lineWcBg.startAnimation(fadeIn);
                    } else {
                        mBinding.lineSignBg.startAnimation(fadeOut);
                        mBinding.lineWcBg.startAnimation(fadeIn);
                    }

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }

    private Bundle getFacebookData(JSONObject object) {

        try {
            Bundle bundle = new Bundle();
            String id = null;
            try {
                id = object.getString("id");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                URL profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=200&height=150");
                bundle.putString("profile_pic", profile_pic.toString());
                Log.i("profile_pic", profile_pic + "");

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return null;
            }

            bundle.putString("idFacebook", id);
            if (object.has("first_name"))
                bundle.putString("first_name", object.getString("first_name"));
            if (object.has("last_name"))
                bundle.putString("last_name", object.getString("last_name"));
            if (object.has("email"))
                bundle.putString("email", object.getString("email"));


            return bundle;


        } catch (JSONException e) {
            //  Log.d(TAG,"Error parsing JSON");
        }
        return null;
    }

    public void optionAnimation(boolean signUp) {
        signUpType = 0;
        mBinding.options.txtLogin.setText(getString(R.string.login));
        mBinding.options.constOption.setVisibility(View.VISIBLE);
        mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email));
        mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin));
        mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta));
        mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook));
        // mBinding.lineSignBg.setVisibility(View.VISIBLE);

        mBinding.options.txtProfile.setVisibility(View.GONE);
        mBinding.options.txtAccount.setVisibility(View.GONE);

        if (signUp) {

            mBinding.signup.btnSignup.setVisibility(View.GONE);
            mBinding.signup.btnLogin.setVisibility(View.GONE);
            mBinding.options.lineSignup.setVisibility(View.VISIBLE);
            mBinding.options.imgArrow.setVisibility(View.VISIBLE);
            mBinding.options.txtLogin.setVisibility(View.VISIBLE);
            mBinding.options.txtLogin.startAnimation(fadeIn);
            mBinding.options.imgArrowDown.setVisibility(View.GONE);

            mBinding.options.imgMail.setOnClickListener(v -> {

                signUpType = 1;
                isEmail = true;
                mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email_selected));
                mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin));
                mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta));
                mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook));


            });
            mBinding.options.imgInsta.setOnClickListener(v -> {
                signUpType = 3;
                isEmail = false;
                mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta_selected));
                mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook));
                mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin));
                mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email));


            });

            mBinding.options.imgFacebook.setOnClickListener(v -> {
                signUpType = 8;
                isEmail = false;
                mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook_selected));
                mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin));
                mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta));
                mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email));


            });
            mBinding.options.imgLinkedin.setOnClickListener(v -> {
                signUpType = 2;
                isEmail = false;
                mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta));
                mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook));
                mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin_selected));
                mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email));

            });

        }
        if (!signUp) {
            mBinding.signup.btnSignup.setVisibility(View.GONE);
            mBinding.signup.btnLogin.setVisibility(View.GONE);
            mBinding.options.lineSignup.setVisibility(View.GONE);
            mBinding.options.imgArrow.setVisibility(View.INVISIBLE);
            mBinding.options.txtLogin.setVisibility(View.GONE);
            mBinding.options.imgArrowDown.setVisibility(View.VISIBLE);

            mBinding.options.imgMail.setOnClickListener(v -> {
                mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email_selected));
                mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin));
                mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta));
                mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook));

                Intent intent = new Intent(getApplicationContext(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_LOGIN);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            });
            mBinding.options.imgInsta.setOnClickListener(v -> {

                mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta_selected));
                mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin));
                mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook));
                mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email));
                signUpType = 6;

                instaGramLogin(false);

            });

            mBinding.options.imgFacebook.setOnClickListener(v -> {
                mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook_selected));
                mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta));
                mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin));
                mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email));
                signUpType = 9;
                facebookLogin(false);

            });

            mBinding.options.imgLinkedin.setOnClickListener(v -> {
                mBinding.options.imgFacebook.setImageDrawable(getResources().getDrawable(R.drawable.facebook));
                mBinding.options.imgInsta.setImageDrawable(getResources().getDrawable(R.drawable.insta));
                mBinding.options.imgLinkedin.setImageDrawable(getResources().getDrawable(R.drawable.linkedin_selected));
                mBinding.options.imgMail.setImageDrawable(getResources().getDrawable(R.drawable.email));
                signUpType = 7;
                linkedInLogin(false);


            });

        }

        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(mBinding.imgDiscovrus, SCALEX, 0.7f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(mBinding.imgDiscovrus, SCALEY, 0.7f);
        scaleDownX.setDuration(500);
        scaleDownY.setDuration(500);

        ObjectAnimator moveUpY = ObjectAnimator.ofFloat(mBinding.imgDiscovrus, TRANSITIONY, -550);
        moveUpY.setDuration(500);

        AnimatorSet scaleDown = new AnimatorSet();
        AnimatorSet moveUp = new AnimatorSet();

        scaleDown.play(scaleDownX).with(scaleDownY);
        moveUp.play(moveUpY);

        scaleDown.start();
        moveUp.start();

        if (signUp) {
            mBinding.options.txtLogin.startAnimation(fadeOut);
        }
        mBinding.options.imgMail.setVisibility(View.VISIBLE);

        Animator translationAnimatorMail = ObjectAnimator
                .ofFloat(mBinding.options.imgMail, View.TRANSLATION_Y, 1500f, 0f)
                .setDuration(400);
        translationAnimatorMail.start();
        translationAnimatorMail.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (signUp) {
                    mBinding.options.txtLogin.setText("");
                    mBinding.options.txtLogin.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


        if (mBinding.options.imgMail.getVisibility() != View.GONE) {
            mBinding.options.imgLinkedin.setVisibility(View.VISIBLE);
            Animator translationAnimatorLinkedIn = ObjectAnimator
                    .ofFloat(mBinding.options.imgLinkedin, View.TRANSLATION_Y, 1500f, 0f)
                    .setDuration(800);
            translationAnimatorLinkedIn.start();


            if (signUp) {
                mBinding.options.txtSignup.startAnimation(fadeIn);
                mBinding.options.txtSignup.setText(getString(R.string.sign_up));
            }
            if (mBinding.options.imgLinkedin.getVisibility() != View.GONE) {
                mBinding.options.imgInsta.setVisibility(View.VISIBLE);

                Animator translationAnimatorInsta = ObjectAnimator
                        .ofFloat(mBinding.options.imgInsta, View.TRANSLATION_Y, 1500f, 0f)
                        .setDuration(1200);
                translationAnimatorInsta.start();

                if (mBinding.options.imgInsta.getVisibility() != View.GONE) {
                    mBinding.options.imgFacebook.setVisibility(View.VISIBLE);

                    Animator translationAnimatorFace = ObjectAnimator
                            .ofFloat(mBinding.options.imgFacebook, View.TRANSLATION_Y, 1500f, 0f)
                            .setDuration(1600);
                    translationAnimatorFace.start();


                    translationAnimatorFace.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mBinding.lineSignBg.setVisibility(View.VISIBLE);
                            if (animationType.equals("loginAnimation")) {
                                mBinding.lineLoginBg.setVisibility(View.GONE);
                                mBinding.lineSignBg.startAnimation(fadeIn);
                            } else if (animationType.equals("splash")) {
                                mBinding.lineWcBg.startAnimation(fadeOut);
                                mBinding.lineSignBg.startAnimation(fadeIn);
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });

                }
            }
        }
        mBinding.options.lineSignup.setOnClickListener(v -> {
            if (signUpType == 1) {
                mBinding.options.txtSignup.startAnimation(fadeOut);
                mBinding.lineLoginBg.setVisibility(View.VISIBLE);
                accountType();
            } else if (signUpType == 4 || signUpType == 6 || signUpType == 7 || signUpType == 9) {
                Intent intent = new Intent(getApplicationContext(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_INDIVIDUAL);
                if (socialMediaLoginData != null && !isEmail) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("socialMediaData", socialMediaLoginData);
                    intent.putExtras(bundle);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
                finish();
            } else if (signUpType == 5 || signUpType == 6 || signUpType == 7 || signUpType == 9) {
                Intent intent = new Intent(getApplicationContext(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_BUSINESS);
                if (socialMediaLoginData != null && !isEmail) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("socialMediaData", socialMediaLoginData);
                    intent.putExtras(bundle);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
                finish();
            } else if (signUpType == 2) {
                linkedInLogin(true);

            } else if (signUpType == 3) {
                instaGramLogin(true);
            } else if (signUpType == 8) {
                //facebook
                facebookLogin(true);
            }
        });

    }

    private void facebookLogin(boolean socialMediaSignUp) {
        facebookSocialMediaLogin = socialMediaSignUp;
        mBinding.options.loginButtonFacebook.performClick();
        Toast.makeText(this, "Perform Facebook login", Toast.LENGTH_SHORT).show();
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.danielandshayegan.discovrus",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    private void linkedInLogin(boolean socialMediaSignUp) {

        Log.e("socialMedia", "start");

        LISessionManager.getInstance(getApplicationContext()).init(MainActivity.this, buildScope()//pass the build scope here
                , new AuthListener() {
                    @Override
                    public void onAuthSuccess() {

                        // Toast.makeText(MainActivity.this, "Successfully authenticated with LinkedIn.", Toast.LENGTH_SHORT).show();

                        Log.e("socialMedia", "success");
                        fetchBasicProfileDataLinkedIn(socialMediaSignUp);
                    }

                    @Override
                    public void onAuthError(LIAuthError error) {

                        Log.e(TAG, "Auth Error :" + error.toString());
                        Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }, true);//if TRUE then it will show dialog_out if
        // any device has no LinkedIn app installed to download app else won't show anything
    }

    private void instaGramLogin(boolean socialMediaSignUp) {
        Log.e("socialMedia", "profile_layout");
        mApp.authorize();
        mApp.setListener(new InstagramResponse.OAuthAuthenticationListener() {

            @Override
            public void onSuccess() {


                String instaName = mApp.getName();
                String instaUserName = mApp.getUserName();
                String accessToken = mApp.getmAccessToken();
                String id = mApp.getId();
                String photo = mApp.getPhoto();
                String lastName = null;
                String firstName = null;
                if (instaName.split("\\w+").length > 1) {
                    lastName = instaName.substring(instaName.lastIndexOf(" ") + 1);
                    firstName = instaName.substring(0, instaName.lastIndexOf(' '));
                } else {
                    firstName = instaName;
                }

                Log.e("instaDetails", " " + mApp);

                socialMediaLoginData = new SocialMediaLoginData();
                socialMediaLoginData.setFirstName(firstName);
                if (lastName != null) {
                    socialMediaLoginData.setLastName(lastName);
                }
                socialMediaLoginData.setEmailAddress("");
                if (photo != null) {
                    socialMediaLoginData.setPhoto(photo);
                } else {
                    socialMediaLoginData.setPhoto("");
                }
                socialMediaLoginData.setLinkedIn(false);
                socialMediaLoginData.setFaceBook(false);
                socialMediaLoginData.setId(id);
                if (socialMediaSignUp) {
                    Log.e("socialMedia", "profile_layout");
                    mBinding.options.txtSignup.startAnimation(fadeOut);
                    mBinding.lineLoginBg.setVisibility(View.VISIBLE);
                    accountType();
                } else {
                    mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callLoginAPI(mApp.getId(), "Instagram", null);
                }

            }

            @Override
            public void onFail(String error) {
                Log.i("instaDetails", " " + error);
            }
        });
    }

    private static Scope buildScope() {

        return Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS, Scope.W_SHARE);
    }

    private void fetchBasicProfileDataLinkedIn(boolean socialMediaSignUp) {
        Log.e("socialMedia", "profile_layout");
        //  String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,headline,public-profile_layout-url,picture-url,email-address,picture-urls::(original))";

        String url = "https://api.linkedin.com/v1/people/~:" +
                "(id,first-name,last-name,maiden-name,headline,public-profile-url,picture-url," +
                "formatted-name,phonetic-first-name,phonetic-last-name,formatted-phonetic-name,location," +
                "email-address,industry,current-share,num-connections,num-connections-capped,summary,specialties,positions," +
                "educations:(id,school-name,field-of-study,start-date,end-date,degree,activities,notes),associations,interests,num-recommenders,date-of-birth," +
                "publications:(id,title,publisher:(name),authors:(id,name),date,url,summary)," +
                "patents:(id,title,summary,number,status:(id,name),office:(name),inventors:(id,name),date,url),languages:(id,language:(name),proficiency:(level,name))," +
                "skills,certifications:(id,name,authority:(name),number,start-date,end-date),courses:(id,name,number)," +
                "recommendations-received:(id,recommendation-type,recommendation-text,recommender),honors-awards,three-current-positions,three-past-positions,volunteer" +
                ",picture-urls::(original))";

        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {

                JSONObject jsonObject = apiResponse.getResponseDataAsJson();
                try {
                    String pic = "";
                    String id = jsonObject.getString("id");
                    String name = jsonObject.getString("firstName");
                    String lastName = jsonObject.getString("lastName");
                    if (jsonObject.has("pictureUrl")) {
                        pic = jsonObject.getString("pictureUrl");
                    }
                    String mail = jsonObject.getString("emailAddress");
                    socialMediaLoginData = new SocialMediaLoginData();
                    socialMediaLoginData.setFirstName(name);
                    if (lastName != null) {
                        socialMediaLoginData.setLastName(lastName);
                    }
                    socialMediaLoginData.setEmailAddress(mail);
                    socialMediaLoginData.setPhoto(pic);
                    socialMediaLoginData.setId(id);
                    socialMediaLoginData.setLinkedIn(true);
                    socialMediaLoginData.setFaceBook(false);
                    if (socialMediaSignUp) {
                        Log.e("socialMedia", "pageTransfer");
                        mBinding.options.txtSignup.startAnimation(fadeOut);
                        mBinding.lineLoginBg.setVisibility(View.VISIBLE);
                        accountType();
                    } else {
                        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(true);
                        callLoginAPI(id, "LinkedIn", mail);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.i("loginDetails", e.getMessage());
                }

            }

            @Override
            public void onApiError(LIApiError liApiError) {

                Log.e("loginDetails", "Fetch profile_layout Error   :" + liApiError.getLocalizedMessage());
                Toast.makeText(MainActivity.this, "Failed to fetch basic profile_layout data. Please try again.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        LISessionManager.getInstance(getApplicationContext()).onActivityResult(this, requestCode, resultCode, data);
    }

    public void handleLogoutLinkedIn() {
        LISessionManager.getInstance(getApplicationContext()).clearSession();
    }

    public void accountType() {
        if (mBinding.options.lineSignup.getVisibility() == View.GONE) {
            mBinding.options.lineSignup.setVisibility(View.VISIBLE);
        }
        mBinding.lineSignBg.startAnimation(fadeOut);
        mBinding.lineLoginBg.startAnimation(fadeIn);
        mBinding.options.txtNext.setVisibility(View.VISIBLE);
        mBinding.options.txtNext.setText(getString(R.string.next));
        mBinding.options.txtAccount.setText(getString(R.string.which_account));
        mBinding.options.txtProfile.setText(getString(R.string.create_your_profile));
        mBinding.options.imgBusiness.setImageDrawable(getResources().getDrawable(R.drawable.business));
        mBinding.options.imgIndividual.setImageDrawable(getResources().getDrawable(R.drawable.individual));

        // signUpType = 0;
        Animator translationAnimatorFacebook = ObjectAnimator
                .ofFloat(mBinding.options.imgFacebook, View.TRANSLATION_Y, 0f, 20000f)
                .setDuration(200);
        translationAnimatorFacebook.start();

        Animator translationAnimatorInsta = ObjectAnimator
                .ofFloat(mBinding.options.imgInsta, View.TRANSLATION_Y, 0f, 20000f)
                .setDuration(400);
        translationAnimatorInsta.start();

        Animator translationAnimatorLinkedIn = ObjectAnimator
                .ofFloat(mBinding.options.imgLinkedin, View.TRANSLATION_Y, 0f, 2000f)
                .setDuration(800);
        translationAnimatorLinkedIn.start();

        Animator translationAnimatorMail = ObjectAnimator
                .ofFloat(mBinding.options.imgMail, View.TRANSLATION_Y, 0f, 2000f)
                .setDuration(1200);
        translationAnimatorMail.start();

        mBinding.options.txtNext.startAnimation(fadeIn);
        mBinding.options.txtNext.setText(R.string.next);

        mBinding.options.txtProfile.setVisibility(View.VISIBLE);
        mBinding.options.txtProfile.startAnimation(fadeIn);
        mBinding.options.txtAccount.setVisibility(View.VISIBLE);
        mBinding.options.txtAccount.setAnimation(fadeIn);

        translationAnimatorMail.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mBinding.options.imgIndividual.setVisibility(View.VISIBLE);

                Animator translationAnimatorIndividual = ObjectAnimator
                        .ofFloat(mBinding.options.imgIndividual, View.TRANSLATION_Y, 1500f, 0f)
                        .setDuration(400);
                translationAnimatorIndividual.start();

                if (mBinding.options.imgIndividual.getVisibility() != View.GONE) {
                    mBinding.options.imgBusiness.setVisibility(View.VISIBLE);
                    Animator translationAnimatorBusiness = ObjectAnimator
                            .ofFloat(mBinding.options.imgBusiness, View.TRANSLATION_Y, 1500f, 0f)
                            .setDuration(800);
                    translationAnimatorBusiness.start();
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        mBinding.options.imgIndividual.setOnClickListener(v -> {
            signUpType = 4;
            mBinding.options.imgIndividual.setImageDrawable(getResources().getDrawable(R.drawable.individual_selected));
            mBinding.options.imgBusiness.setImageDrawable(getResources().getDrawable(R.drawable.business));
        });
        mBinding.options.imgBusiness.setOnClickListener(v -> {
            signUpType = 5;
            mBinding.options.imgIndividual.setImageDrawable(getResources().getDrawable(R.drawable.individual));
            mBinding.options.imgBusiness.setImageDrawable(getResources().getDrawable(R.drawable.business_selected));
        });
    }

    public void backanimation() {

        mBinding.options.constOption.setVisibility(View.GONE);


        ObjectAnimator scaleUpX = ObjectAnimator.ofFloat(mBinding.imgDiscovrus, SCALEX, 1.0f);
        ObjectAnimator scaleUpY = ObjectAnimator.ofFloat(mBinding.imgDiscovrus, SCALEY, 1.0f);
        scaleUpX.setDuration(500);
        scaleUpY.setDuration(500);

        ObjectAnimator moveDownY = ObjectAnimator.ofFloat(mBinding.imgDiscovrus, TRANSITIONY, 0f);
        moveDownY.setDuration(500);

        AnimatorSet scaleDown = new AnimatorSet();
        AnimatorSet moveUp = new AnimatorSet();

        scaleDown.play(scaleUpX).with(scaleUpY);
        moveUp.play(moveDownY);

        scaleDown.start();
        moveUp.start();

        moveUp.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                if (mBinding.lineLoginBg.getVisibility() == View.VISIBLE) {
                    mBinding.lineLoginBg.startAnimation(fadeOut);
                    mBinding.lineWcBg.startAnimation(fadeIn);
                    startAnimation(true);
                } else {
                    startAnimation(false);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        isFirstTime = false;
        Log.i("isFirstTime", " " + isFirstTime);
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    protected void makeRequest() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.READ_CONTACTS}, RECORD_REQUEST_CODE);
    }

    private void callLoginAPI(String id, String loginType, String mail) {

        String email = null, socialId = null, password = null;
        boolean isEmail = false, isLinkedIn = false, isInstagram = false, isFacebook = false;

        switch (loginType) {
            case "Instagram":
                email = mail;
                socialId = id;
                password = null;
                isEmail = false;
                isInstagram = true;
                isLinkedIn = false;
                isFacebook = false;
                break;
            case "LinkedIn":
                email = mail;
                socialId = id;
                password = null;
                isEmail = false;
                isInstagram = false;
                isLinkedIn = true;
                isFacebook = false;
                break;
            case "FaceBook":
                email = mail;
                socialId = id;
                password = null;
                isEmail = false;
                isInstagram = false;
                isLinkedIn = false;
                isFacebook = true;


        }

        if (Utils.isNetworkAvailable(MainActivity.this)) {
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getApplicationContext()).
                                    create(WebserviceBuilder.class).
                                    userLogin(email, socialId, password, isEmail, isLinkedIn, isInstagram, isFacebook)
                            , getCompositeDisposable(), single, this);
        } else {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.uiLoading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                LoginData loginModel = (LoginData) o;
                Log.i("error", loginModel.getMessage().get(0).toString());
                if (loginModel.isSuccess()) {
                    App_pref.saveAuthorizedUser(getApplicationContext(), loginModel);
                    Toast.makeText(getApplicationContext(), loginModel.getMessage().get(0), Toast.LENGTH_LONG).show();
                    /*Intent intent = new Intent(getApplicationContext(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN);
                    intent.putExtra("email", loginModel.getData().getEmail());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.stay);
                    finish();*/
                    if (App_pref.getAuthorizedUser(MainActivity.this) != null) {
                        ApplicationClass.OnlineUserListAPI(MainActivity.this, true);
                    }
                    Utils.callGetGeneralSettings(MainActivity.this, App_pref.getAuthorizedUser(MainActivity.this).getData().getUserId(), new CallbackTask() {
                        @Override
                        public void onFail(Object object) {

                        }

                        @Override
                        public void onSuccess(Object object) {
                            Utils.setGeneralSettingsData(MainActivity.this, (GeneralSettingsData) object);

                            Intent intent = new Intent(getApplicationContext(), MenuActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                            intent.putExtra("email", loginModel.getData().getEmail());
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.stay);
                            finish();
                        }

                        @Override
                        public void onFailure(Throwable t) {

                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), loginModel.getMessage().get(0).toString(), Toast.LENGTH_LONG).show();
                    Log.i("error", loginModel.getMessage().get(0).toString());
                    if (loginModel.getMessage().get(0).toString().equalsIgnoreCase("Invalid login details") || loginModel.getMessage().get(0).toString().equalsIgnoreCase("User Not Registered.")) {
                        Toast.makeText(getApplicationContext(), loginModel.getMessage().get(0).toString(), Toast.LENGTH_LONG).show();
                        mBinding.options.txtSignup.startAnimation(fadeOut);
                        mBinding.lineLoginBg.setVisibility(View.VISIBLE);
                        nextButtonVisibility = true;
                        accountType();
                    }
                }
                break;

        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        Log.i("error", throwable.getMessage());
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getApplicationContext(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
        mBinding.uiLoading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
    }
}