package com.danielandshayegan.discovrus.ui.newsfeed;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.PeopleLikesAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentNewsfeedDetailsBinding;
import com.danielandshayegan.discovrus.models.LikePeopleData;
import com.danielandshayegan.discovrus.models.NewsFeedData;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.common.BaseActivityNew;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.webservice.APIs;
import com.danielandshayegan.discovrus.webservice.JSONCallback;
import com.danielandshayegan.discovrus.webservice.Retrofit;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class NewsFeedDetailsFragment extends BaseFragment {
    FragmentNewsfeedDetailsBinding binding;
    NewsFeedData.PostData postData;
    ArrayList<LikePeopleData> likedList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_newsfeed_details, container, false);
        setUpToolBar();

        if (getArguments() != null && getArguments().containsKey("PostData")) {
            postData = getArguments().getParcelable("PostData");
            binding.setItem(postData);
            binding.executePendingBindings();
            setData();
        }
        return binding.getRoot();
    }

    private void setData() {
        getLikeListData();

        switch (postData.getNewsFeedTypeEnum()) {
            case 0: //NewsFeedTypeEnum.Photo*/
                binding.ivPostImage.setVisibility(View.VISIBLE);
                binding.playerView.setVisibility(View.GONE);
                binding.toolbar.toolbarTitle.setVisibility(View.GONE);
                binding.toolbar.toolbarTitle.setText("");
                break;

            case 1: //NewsFeedTypeEnum.Text*/
                binding.ivPostImage.setVisibility(View.GONE);
                binding.playerView.setVisibility(View.GONE);
                binding.toolbar.toolbarTitle.setVisibility(View.VISIBLE);
                binding.toolbar.toolbarTitle.setText(postData.getTitle());
                break;

            case 2: //NewsFeedTypeEnum.Video*/
                binding.ivPostImage.setVisibility(View.GONE);
                binding.playerView.setVisibility(View.VISIBLE);
                binding.toolbar.toolbarTitle.setVisibility(View.GONE);
                binding.toolbar.toolbarTitle.setText("");
                break;

            case 3: //NewsFeedTypeEnum.PhotoAndText*/
                binding.ivPostImage.setVisibility(View.VISIBLE);
                binding.playerView.setVisibility(View.GONE);
                binding.toolbar.toolbarTitle.setVisibility(View.GONE);
                binding.toolbar.toolbarTitle.setText("");
                break;
        }
        binding.viewFooter.viewBottomShadow.setVisibility(View.GONE);
        binding.viewFooter.setItem(postData);
        binding.viewFooter.executePendingBindings();
    }

    private void setUpToolBar() {
        binding.toolbar.ivBack.setImageResource(R.drawable.back_button);
        binding.toolbar.ivBack.setVisibility(View.VISIBLE);
        binding.toolbar.ivHat.setVisibility(View.GONE);
        binding.toolbar.ivTools.setVisibility(View.VISIBLE);
        binding.toolbar.ivTools.setImageResource(R.drawable.option_icon);
        binding.toolbar.toolbarTitle.setMovementMethod(new ScrollingMovementMethod());
    }

    public void handleClicks(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                ((BaseActivityNew) me).onBackPressed();
                break;
        }
    }

    private void getLikeListData() {

        if (Utils.isNetworkAvailable(getContext())) {

            try {
                HashMap<String, String> params = new HashMap<>();
                params.put("LoginId", "2");//   TODO: change static id
                params.put("PostId", postData.getPostId());

                Log.e("HashMap Params", params.toString());

                try {
                    Retrofit.with(me)
                            .setAPI(APIs.GET_LIKE_PEOPLE_LIST)
                            .setGetParameters(params)
                            .setCallBackListener(new JSONCallback(me) {
                                @Override
                                protected void onFailed(int statusCode, String message) {
                                    Log.e("Like Post api error", message);
                                }

                                @Override
                                protected void onSuccess(int statusCode, JSONObject jsonObject) throws JSONException {
                                    /*{"Data":[],"Success":true,"Message":["Success"],"Count":"0","TotalRecord":0}*/
                                    if (jsonObject.getJSONArray("Data").length() > 0) {
                                        likedList.clear();

                                        Gson gson = new Gson();
                                        likedList.addAll(gson.fromJson(jsonObject.getJSONArray("Data").toString(),
                                            new TypeToken<ArrayList<LikePeopleData>>() {
                                            }.getType()));
                                        if (likedList.size() > 3) {
                                            binding.rvLikeList.setVisibility(View.VISIBLE);
                                            binding.tvLikeText.setVisibility(View.VISIBLE);
                                            binding.rvLikeList.setAdapter(new PeopleLikesAdapter(getActivity(), likedList, postData));
                                            binding.tvLikeText.setText(" " + getActivity().getString(R.string.and) + " " + (likedList.size() - 3) + " " + getActivity().getString(R.string.others) + " " + getActivity().getString(R.string.liked));
                                        } else if (likedList.size() == 0) {
                                            binding.rvLikeList.setVisibility(View.GONE);
                                            binding.tvLikeText.setVisibility(View.GONE);
                                        } else {
                                            binding.rvLikeList.setVisibility(View.GONE);
                                            binding.tvLikeText.setVisibility(View.VISIBLE);
                                            binding.tvLikeText.setText(likedList.size() + " " + getActivity().getString(R.string.liked));
                                        }
                                    } else {
                                        binding.rvLikeList.setVisibility(View.GONE);
                                        binding.tvLikeText.setVisibility(View.GONE);
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

/*            Utils.getLikeListData(getActivity(), postData.getPostId(), new CallbackTask() {
                @Override
                public void onFail(Object object) {
                }

                @Override
                public void onSuccess(Object object) {
                   *//* LikeListData followersUserListData = (LikeListData) object;

                    if (followersUserListData.isSuccess()) {
                        if (followersUserListData.getData() != null) {
                            likedList.clear();
                            likedList = (ArrayList<NewsFeedData.PostData>) followersUserListData.getData();
                            if (followersUserListData.getData().size() > 3) {
                                binding.rvLikeList.setVisibility(View.VISIBLE);
                                binding.tvLikeText.setVisibility(View.VISIBLE);
                                binding.rvLikeList.setAdapter(new PeopleLikesAdapter(getActivity(), likedList, postData));
                                binding.tvLikeText.setText(" " + getActivity().getString(R.string.and) + " " + (followersUserListData.getData().size() - 3) + " " + getActivity().getString(R.string.others) + " " + getActivity().getString(R.string.liked));
                            } else if (followersUserListData.getData().size() == 0) {
                                binding.rvLikeList.setVisibility(View.GONE);
                                binding.tvLikeText.setVisibility(View.GONE);
                            } else {
                                binding.rvLikeList.setVisibility(View.GONE);
                                binding.tvLikeText.setVisibility(View.VISIBLE);
                                binding.tvLikeText.setText(followersUserListData.getData().size() + " " + getActivity().getString(R.string.liked));
                            }
                        } else {
                            binding.rvLikeList.setVisibility(View.GONE);
                            binding.tvLikeText.setVisibility(View.GONE);
                        }*//*
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                }
            });*/


            //       callAPI();
         else {
            Toast.makeText(getContext(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
        }
    }
}