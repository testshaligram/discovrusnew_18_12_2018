package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.UserPostListAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentTextPostBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import okhttp3.MultipartBody;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postPhoto;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class TextPostFragment extends BaseFragment implements SingleCallback {

    public FragmentTextPostBinding mBinding;
    View view;
    int userId;
    int postType;
    public Uri mImageUri;
    String videoUri;
    MultipartBody.Part body;
    Activity activityMain;
    public static TextPostFragment context;
    UserPostListAdapter userPostListAdapter;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<PostListData.Post> postDataList = new ArrayList<>();

    public TextPostFragment() {
        // Required empty public constructor
    }

    public static TextPostFragment newInstance() {
        return new TextPostFragment();
    }

    public static TextPostFragment newInstance(Bundle extras) {
        TextPostFragment fragment = new TextPostFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_text_post, container, false);
        view = mBinding.getRoot();
        context = this;
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels / 2;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBinding.uiTextPost.previewCard.getLayoutParams();
        layoutParams.width = (int) (devicewidth - getResources().getDimension(R.dimen._15sdp));

        layoutParams.setMargins((int) getResources().getDimension(R.dimen._10sdp), (int) getResources().getDimension(R.dimen._7sdp),
                (int) getResources().getDimension(R.dimen._7sdp), (int) getResources().getDimension(R.dimen._5sdp));
        mBinding.uiTextPost.previewCard.setLayoutParams(layoutParams);

        if (getArguments() != null) {
            postData();
        }

        mBinding.uiTextPost.userPostRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        mBinding.uiTextPost.userPostRecyclerView.setOnFlingListener(null);

        userPostListAdapter = new UserPostListAdapter(mContext);

        mBinding.uiTextPost.userPostRecyclerView.setItemAnimator(new SlideInUpAnimator());
        mBinding.uiTextPost.userPostRecyclerView.setAdapter(userPostListAdapter);

        mBinding.uiTextPost.imgBack.setOnClickListener(v -> {
//            activityMain.onBackPressed();
            /*getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.frame_replace, PostManagementFragment.newInstance(), "")
                    .commit();*/
            getActivity().finish();
        });

        callAPI();

        mBinding.uiTextPost.btnDone.setOnClickListener(v -> {
            mBinding.uiTextPost.btnDone.setEnabled(false);
            if (postType == 3) {

                if (validation()) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    mBinding.uiTextPost.btnDone.setEnabled(true);
                    String description = "", title = "";
                    try {
                        description = URLEncoder.encode(mBinding.uiTextPost.edtDesc.getText().toString(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    try {
                        title = URLEncoder.encode(mBinding.uiTextPost.edtTitle.getText().toString(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    Log.d("textData", description + " " + title);
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            uploadPostDataText(description, title, userId)
                                    , getCompositeDisposable(), postPhoto, this);

                    /*ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            uploadPostDataText("test of text", "hello text", userId)
                                    , getCompositeDisposable(), postPhoto, this);*/
                } else {
                    mBinding.uiTextPost.btnDone.setEnabled(true);
                }
            }

        });

        return view;
    }

    public boolean validation() {

        boolean value = true;
        if (mBinding.uiTextPost.edtTitle.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter title", Toast.LENGTH_SHORT).show();
            value = false;
        }
        if (mBinding.uiTextPost.edtDesc.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter description", Toast.LENGTH_SHORT).show();
            value = false;
        }

        return value;

    }

    @Override
    public void onResume() {
        super.onResume();
        MenuActivity.fragmentView = 2;
    }

    private void postData() {
        if (getArguments() != null) {

            mImageUri = Uri.parse(getArguments().getString("imageUri"));
            postType = getArguments().getInt("postType");
            videoUri = getArguments().getString("videoUri");

            switch (postType) {
                case 3:
                    mBinding.uiTextPost.relativeFilter.setVisibility(View.VISIBLE);
                    mBinding.uiTextPost.edtTitle.setVisibility(View.VISIBLE);
                    mBinding.uiTextPost.edtDesc.setVisibility(View.VISIBLE);
                    break;

            }

        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case postPhoto:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                body = null;

                UploadPostData uploadPostData = (UploadPostData) o;
                if (uploadPostData.isSuccess()) {
                    Toast.makeText(getActivity(), uploadPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
//                    PostManagementFragment.managementFragment.callAPI(false);
//                    activityMain.onBackPressed();
                  /*  getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.frame_replace, PostManagementFragment.newInstance(), "")
                            .commit();*/
                    Intent intent = new Intent(getActivity(), MenuActivity.class);
                    intent.putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    getActivity().startActivity(intent);
                    getActivity().finish();

                    getActivity().overridePendingTransition(0, 0);
                } else {
                    Toast.makeText(getActivity(), uploadPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }


    public void callAPI() {
        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        disposable.add(apiService.getPostById(userId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<PostListData>() {
                    @Override
                    public void onSuccess(PostListData value) {


                        if (value.isSuccess()) {

                            if (value.isSuccess() && value.getData().size() > 0) {
                                postDataList.clear();
                                postDataList.addAll(value.getData().get(0).getPost());
                                userPostListAdapter.clear();
                                userPostListAdapter.addAll(postDataList);
                                PostListData.PostList data = value.getData().get(0);
                                if (data != null) {
                                    mBinding.uiTextPost.postTimeTv.setText("");
                                    mBinding.uiTextPost.userLocationTv.setText(data.getLocation());
                                    mBinding.uiTextPost.userNameTv.setText(data.getUserName());

                                    String dateStr = data.getPost().get(0).getPostCreatedDate();
                                    if (dateStr.length() <= 10) {
                                        mBinding.uiTextPost.postTimeTv.setText(dateStr);
                                    } else {
                                        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
                                        df.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        Date date = null;
                                        try {
                                            date = df.parse(dateStr);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        df.setTimeZone(TimeZone.getDefault());
                                        String formattedDate = df.format(date);
                                        if (formattedDate != null) {
                                            mBinding.uiTextPost.postTimeTv.setText(formattedDate);
                                        }
                                    }

                                    RequestOptions options = new RequestOptions()
                                            .centerCrop()
                                            .placeholder(R.drawable.user_placeholder)
                                            .error(R.drawable.user_placeholder);


                                    Glide.with(mContext)
                                            .load(ApiClient.WebService.imageUrl + data.getUserImagePath())
                                            .apply(options)
                                            .into(mBinding.uiTextPost.userProfileIv);

                                }
                                try {
                                    mBinding.loading.progressBar.setVisibility(View.GONE);
                                    disableScreen(false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (value.isSuccess() && value.getData().size() == 0) {
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            }
                        } else {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                    }
                }));
    }

}
