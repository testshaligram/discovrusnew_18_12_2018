package com.danielandshayegan.discovrus.ui.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.ui.fragment.DataHistoryFragment;
import com.danielandshayegan.discovrus.ui.fragment.DataPolicyFragment;

public class DataPloicyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_ploicy);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.realtabcontent, DataPolicyFragment.newInstance(), "General Profile")
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.realtabcontent);
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
