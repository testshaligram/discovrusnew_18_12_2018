package com.danielandshayegan.discovrus.ui.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daasuu.mp4compose.FillMode;
import com.daasuu.mp4compose.composer.Mp4Composer;
import com.daasuu.mp4compose.filter.GlFilter;
import com.daasuu.mp4compose.filter.GlGrayScaleFilter;
import com.daasuu.mp4compose.filter.GlInvertFilter;
import com.daasuu.mp4compose.filter.GlSepiaFilter;
import com.daasuu.mp4compose.filter.GlVignetteFilter;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.UserPostListAdapter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GLContrastFilter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GlBrightnessFilter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GlGammaFilter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GlHueFilter;
import com.danielandshayegan.discovrus.custome_veiws.videofilter.GlPostrizedFilter;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.VideoFilterAdapter;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.VideoFilterNameModel;
import com.danielandshayegan.discovrus.databinding.VideoFilterFrgamentBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.VideoPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.sherazkhilji.videffects.BlackAndWhiteEffect;
import com.sherazkhilji.videffects.BrightnessEffect;
import com.sherazkhilji.videffects.ContrastEffect;
import com.sherazkhilji.videffects.GammaEffect;
import com.sherazkhilji.videffects.HueEffect;
import com.sherazkhilji.videffects.InvertColorsEffect;
import com.sherazkhilji.videffects.NoEffect;
import com.sherazkhilji.videffects.PosterizeEffect;
import com.sherazkhilji.videffects.SepiaEffect;
import com.sherazkhilji.videffects.VignetteEffect;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postVideo;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;


public class VideoFilterFrgament extends BaseFragment implements VideoFilterAdapter.VideoFilterListener, SingleCallback {

    View view;
    VideoFilterFrgamentBinding mBinding;
    int postType;
    String videoUri;
    VideoFilterListFragment videoFilterListFragment;
    MediaPlayer mediaPlayer;
    private GlFilter filter;
    private ProgressDialog mProgressDialog;
    List<VideoFilterNameModel> videoFilterNameModelList;
    private Mp4Composer mp4Composer;
    String destPath;
    VideoFilterListFragment.FilterVideoList listener;
    VideoFilterAdapter mAdapter;
    Uri filterUril;
    private String pathToStoredVideo;
    File fileForCompress;
    int userId;
    byte[] byteArrayThumbnile;
    private String filePath;
    private FFmpeg ffmpeg;
    File destCompress;
    File fileFiler;
    Activity activityMain;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<PostListData.Post> postDataList = new ArrayList<>();
    UserPostListAdapter userPostListAdapter;

    public VideoFilterFrgament() {
        // Required empty public constructor
    }

    public static VideoFilterFrgament newInstance() {
        return new VideoFilterFrgament();
    }

    public static VideoFilterFrgament newInstance(Bundle extras) {
        VideoFilterFrgament fragment = new VideoFilterFrgament();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.video_filter_frgament, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        disableScreen(false);
        activityMain = this.getActivity();
        loadFFMpegBinary();
        mediaPlayer = new MediaPlayer();
        videoFilterNameModelList = new ArrayList<>();
        VideoFilterNameModel videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Normal");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Black & White");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Contrast");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Sepia effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Invert color effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Vignette effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Hue effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Brightness effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Gamma effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Posterized effect");
        videoFilterNameModelList.add(videoFilterNameModel);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //if you need three fix imageview in width
        int devicewidth = displaymetrics.widthPixels;
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mBinding.previewCard.getLayoutParams();
        layoutParams.width = (int) (devicewidth - getResources().getDimension(R.dimen._15sdp));
        layoutParams.setMargins((int) getResources().getDimension(R.dimen._5sdp), (int) getResources().getDimension(R.dimen._7sdp),
                (int) getResources().getDimension(R.dimen._7sdp), (int) getResources().getDimension(R.dimen._5sdp));
        mBinding.previewCard.setLayoutParams(layoutParams);

        mBinding.recyclerPostData.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

        mBinding.recyclerPostData.setOnFlingListener(null);

        userPostListAdapter = new UserPostListAdapter(mContext);

        mBinding.recyclerPostData.setItemAnimator(new SlideInUpAnimator());
       // mBinding.recyclerPostData.setAdapter(userPostListAdapter);

        mAdapter = new VideoFilterAdapter(videoFilterNameModelList, this, getActivity());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
      //  showProgressDialog();
        callAPI();
        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
      /*  int space = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8,
                getResources().getDisplayMetrics());
        mBinding.recyclerView.addItemDecoration(new SpacesItemDecoration(space));*/
        mBinding.recyclerView.setAdapter(mAdapter);

        if (getArguments() != null) {

            postData();
        }

        mBinding.btnDone.setOnClickListener(v -> {
            saveVideo();
        });
        mBinding.imgBack.setOnClickListener(v -> {
//            activityMain.onBackPressed();
            mediaPlayer.pause();
          /*  getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.relative_filter, PostManagementFragment.newInstance(), "")
                    .commit();*/
            getActivity().finish();
        });
        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        mediaPlayer.pause();
    }

    private void postData() {
        if (getArguments() != null) {

            postType = getArguments().getInt("postType");
            videoUri = getArguments().getString("videoUri");


            switch (postType) {

                case 4:

                    loadVideo();

            }
            //  Bitmap bitmap = BitmapUtils.getBitmapFromGallery(this, data.getData(), 800, 800);


        }
    }

    private void loadVideo() {
        try {
            mediaPlayer.setDataSource(videoUri);

            mBinding.mVideoSurfaceView.init(mediaPlayer,
                    new NoEffect());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MenuActivity.fragmentView = 2;
        mBinding.mVideoSurfaceView.onResume();

    }


    @Override
    public void onFilterSelected(VideoFilterNameModel videoFilterNameModel) {
        switch (videoFilterNameModel.getFilterName()) {
            case "Normal":
                mBinding.mVideoSurfaceView.init(mediaPlayer,
                        new NoEffect());
                mBinding.mVideoSurfaceView.onResume();
                break;
            case "Black & White":
                mBinding.mVideoSurfaceView.init(mediaPlayer,
                        new BlackAndWhiteEffect());
                mBinding.mVideoSurfaceView.onResume();
                filter = new GlGrayScaleFilter();
                break;
            case "Contrast":
                mBinding.mVideoSurfaceView.init(mediaPlayer,
                        new ContrastEffect(1.5f));
                mBinding.mVideoSurfaceView.onResume();
                filter = new GLContrastFilter();
                break;

            case "Sepia effect":
                mBinding.mVideoSurfaceView.init(mediaPlayer,
                        new SepiaEffect());
                mBinding.mVideoSurfaceView.onResume();
                filter = new GlSepiaFilter();
                break;
            case "Invert color effect":
                mBinding.mVideoSurfaceView.init(mediaPlayer,
                        new InvertColorsEffect());
                mBinding.mVideoSurfaceView.onResume();
                filter = new GlInvertFilter();
                break;
            case "Vignette effect":
                mBinding.mVideoSurfaceView.init(mediaPlayer,
                        new VignetteEffect(0.2f));
                mBinding.mVideoSurfaceView.onResume();
                filter = new GlVignetteFilter();
                break;
            case "Hue effect":
                mBinding.mVideoSurfaceView.init(mediaPlayer,
                        new HueEffect(90));
                mBinding.mVideoSurfaceView.onResume();
                filter = new GlHueFilter();
                break;
            case "Brightness effect":
                mBinding.mVideoSurfaceView.init(mediaPlayer,
                        new BrightnessEffect(90));
                mBinding.mVideoSurfaceView.onResume();
                filter = new GlBrightnessFilter();
                break;
            case "Gamma effect":
                mBinding.mVideoSurfaceView.init(mediaPlayer, new GammaEffect(1.8f));
                mBinding.mVideoSurfaceView.onResume();
                filter = new GlGammaFilter();
                break;
            case "Posterized effect":
                mBinding.mVideoSurfaceView.init(mediaPlayer, new PosterizeEffect());
                mBinding.mVideoSurfaceView.onResume();
                filter = new GlPostrizedFilter();
                break;


        }
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(activityMain);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Uploading video...");
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    public String getVideoFilePath() {
        return getAndroidMoviesFolder().getAbsolutePath() + "/" + new SimpleDateFormat("yyyyMM_dd-HHmmss").format(new Date()) + "filter_apply.mp4";
    }

    public File getAndroidMoviesFolder() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
    }

    private void saveVideo() {
        showProgressDialog();
        disableScreen(true);

        final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        final String fileName = "MP4_" + timeStamp + ".mp4";

        File folder = Environment.getExternalStorageDirectory();
        String mFinalPath = folder.getPath() + File.separator;

        final String filePath = mFinalPath + "DiscovrusTrim/" + fileName;
        fileFiler = new File(filePath);
        fileFiler.getParentFile().mkdirs();


        destPath = getVideoFilePath();
        mp4Composer = null;
        mp4Composer = new Mp4Composer(videoUri, fileFiler.getPath())
                // .rotation(Rotation.ROTATION_270)
                .fillMode(FillMode.PRESERVE_ASPECT_FIT)
                .filter(filter)
                .listener(new Mp4Composer.Listener() {
                    @Override
                    public void onProgress(double progress) {

                        //runOnUiThread(() -> progressBar.setProgress((int) (progress * 100)));
                        Log.i("videoSaving", videoUri);
                    }

                    @Override
                    public void onCompleted() {
                        Log.i("videoSaving", fileFiler.getPath());
                        new filteredVideoSaving().execute();
                    }

                    @Override
                    public void onCanceled() {
                        Log.i("videoSaving", "cancel");
                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                hideProgressDialog();
                                disableScreen(false);

                            }
                        });

                    }

                    @Override
                    public void onFailed(Exception exception) {

                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                hideProgressDialog();
                                disableScreen(false);

                            }
                        });

                        Log.i("videoSaving", exception.toString());

                    }
                })
                .start();
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void videoUpload(File file) {

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), byteArrayThumbnile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("UploadThumbnail", userId + "thumbnile.png", requestFile);
        RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), file);
        MultipartBody.Part vFile = MultipartBody.Part.createFormData("UploadVideo", file.getName(), videoBody);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                uploadVideoPostData("", userId, vFile, body)
                        , getCompositeDisposable(), postVideo, this);
    }

    private void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {

                ffmpeg = FFmpeg.getInstance(getActivity());
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    Log.d("videoSaving", "ffmpeg : correct Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog();
        } catch (Exception e) {
            Log.d("videoSaving", "EXception no controlada : " + e);
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Supported")
                .setMessage("Device Not Supported")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                })
                .create()
                .show();

    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case postVideo:
                hideProgressDialog();
                disableScreen(false);
                VideoPostData videoPostData = (VideoPostData) o;
                if (videoPostData.isSuccess()) {
                  /*  File moviesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
                    if (moviesDir.isDirectory()) {
                        String[] children = moviesDir.list();
                        for (int i = 0; i < children.length; i++) {
                            new File(moviesDir, children[i]).delete();
                        }
                        moviesDir.delete();


                    }

                    File folder = Environment.getExternalStorageDirectory();
                    String mFinalPath = folder.getPath() + File.separator + "DiscovrusTrim";
                    File trimFolder = new File(mFinalPath);
                    if (trimFolder.isDirectory()) {
                        String[] children = trimFolder.list();
                        for (int i = 0; i < children.length; i++) {
                            new File(trimFolder, children[i]).delete();
                        }
                        trimFolder.delete();
                    }*/
                    mediaPlayer.pause();
//                    PostManagementFragment.managementFragment.callAPI(false);
//                    activityMain.onBackPressed();
                  /*  getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.relative_filter, PostManagementFragment.newInstance(), "")
                            .commit();*/
                    Intent intent = new Intent(getActivity(), MenuActivity.class);
                    intent.putExtra(ACTIVITY_INTENT, FRAGMENT_MAIN_MENU);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    getActivity().startActivity(intent);
                    getActivity().finish();

                    getActivity().overridePendingTransition(0, 0);
                    Log.i("videoResponse", "" + videoPostData);

                    Toast.makeText(getActivity(), videoPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(), videoPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        hideProgressDialog();
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }

    }

    class filteredVideoSaving extends AsyncTask<Void, Void, String> {


        @Override
        protected String doInBackground(Void... voids) {

            final ContentValues values = new ContentValues(2);
            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4");
            values.put(MediaStore.Video.Media.DATA, fileFiler.getPath());
            // MediaStoreに登録
            getActivity().getContentResolver().insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                    values);
            getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                    Uri.parse("file://" + fileFiler.getPath())));

            Log.e("videoSavingFIlter", fileFiler.getPath());


            return "file:" + fileFiler.getPath();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("videoSavingFIlter", s);
            if (s != null) {
                filterUril = Uri.parse(s);

                if (filterUril != null) {
                    pathToStoredVideo = getRealPathFromURIPath(filterUril, getActivity());
                    fileForCompress = new File(pathToStoredVideo);
                    Bitmap thumb = ThumbnailUtils.createVideoThumbnail(fileForCompress.getPath(),
                            MediaStore.Images.Thumbnails.MINI_KIND);
                    //  new VideoCompressor().execute();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    thumb.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                    byteArrayThumbnile = stream.toByteArray();
                    thumb.recycle();
                    if (fileForCompress != null && byteArrayThumbnile != null) {

                        executeCompressCommand();

                    }
                }

            }
        }
    }

    private void executeCompressCommand() {


        File cacheFile = new File(Environment.getExternalStorageDirectory() + "/Discovrus");
        Log.e("videoSaving", "" + cacheFile);
        if (!cacheFile.exists()) {
            cacheFile.mkdirs();

        }

        String filePrefix = userId + "video_discovrus" + System.currentTimeMillis();
        String fileExtn = ".mp4";
        String yourRealPath = getPath(getActivity(), filterUril);


        destCompress = new File(cacheFile, filePrefix + fileExtn);
        int fileNo = 0;
        while (destCompress.exists()) {
            fileNo++;
            destCompress = new File(cacheFile, filePrefix + fileNo + fileExtn);
        }

        Log.d("videoSaving", "startTrim: src: " + yourRealPath);
        Log.d("videoSaving", "startTrim: dest: " + destCompress.getAbsolutePath());
        filePath = destCompress.getAbsolutePath();
        String[] complexCommand = {"-y", "-i", yourRealPath, "-s", "480x320", "-r", "25", "-vcodec", "mpeg4", "-b:v", "900k", "-b:a", "48000", "-ac", "2", "-ar", "22050", filePath};
        execFFmpegBinary(complexCommand);

    }

    private void execFFmpegBinary(final String[] command) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.d("videoSaving", "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {

                    Log.i("videoSavingTrimFIle", s);

                    pathToStoredVideo = getRealPathFromURIPath(Uri.parse(filePath), getActivity());
                    File fileForUpload = new File(pathToStoredVideo);
                    videoUpload(fileForUpload);


                }

                @Override
                public void onProgress(String s) {

                }

                @Override
                public void onStart() {

                }

                @Override
                public void onFinish() {


                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // do nothing for now
        }
    }

    private String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content_navigation://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content_navigation".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public void callAPI() {
        hideProgressDialog();
        // mBinding.loading.progressBar.setVisibility(View.VISIBLE);
        disableScreen(true);
        disposable.add(apiService.getPostById(userId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<PostListData>() {
                    @Override
                    public void onSuccess(PostListData value) {

                        if (value.isSuccess()) {

                            userPostListAdapter.clear();
                            if (value.isSuccess() && value.getData().size() > 0) {
                                postDataList.clear();
                                postDataList.addAll(value.getData().get(0).getPost());
                                userPostListAdapter.clear();
                               // userPostListAdapter.addAll(postDataList);
                                //  userPostListAdapter.notifyDataSetChanged();
                                PostListData.PostList data = value.getData().get(0);
                                if (data != null) {
                                    mBinding.postTimeTv.setText("");
                                    mBinding.userLocationTv.setText(data.getLocation());
                                    mBinding.userNameTv.setText(data.getUserName());

                                    String dateStr = data.getPost().get(0).getPostCreatedDate();
                                    if (dateStr.length() <= 10) {
                                        mBinding.postTimeTv.setText(dateStr);
                                    } else {
                                        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
                                        df.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        Date date = null;
                                        try {
                                            date = df.parse(dateStr);
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        df.setTimeZone(TimeZone.getDefault());
                                        String formattedDate = df.format(date);
                                        if (formattedDate != null) {
                                            mBinding.postTimeTv.setText(formattedDate);
                                        }

                                    }

                                    RequestOptions options = new RequestOptions()
                                            .centerCrop()
                                            .placeholder(R.drawable.user_placeholder)
                                            .error(R.drawable.user_placeholder);


                                    Glide.with(mContext)
                                            .load(ApiClient.WebService.imageUrl + data.getUserImagePath())
                                            .apply(options)
                                            .into(mBinding.userProfileIv);

                                }
                                try {
                                    //  mBinding.loading.progressBar.setVisibility(View.GONE);
                                    disableScreen(false);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else if (value.isSuccess() && value.getData().size() == 0) {
                                // mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            }

                        } else {
                            //  mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        // mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                    }
                }));
    }

}
