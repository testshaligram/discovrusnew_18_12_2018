package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.system.ErrnoException;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.ApplozicClient;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.MobiComUserPreference;
import com.applozic.mobicomkit.api.account.user.PushNotificationTask;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.api.account.user.UserLoginTask;
import com.applozic.mobicomkit.uiwidgets.ApplozicSetting;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.WorkingDataAdapter;
import com.danielandshayegan.discovrus.databinding.DialogInfoViewBinding;
import com.danielandshayegan.discovrus.databinding.FragmentRegisterBinding;
import com.danielandshayegan.discovrus.dialog.LocationDialog;
import com.danielandshayegan.discovrus.models.BusinessRegisterData;
import com.danielandshayegan.discovrus.models.IndividualRegisterData;
import com.danielandshayegan.discovrus.models.SocialMediaLoginData;
import com.danielandshayegan.discovrus.models.WorkingAtData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity;
import com.danielandshayegan.discovrus.ui.activity.MainActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.ui.activity.SelectCategoryActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danielandshayegan.discovrus.utils.Validators;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.businessRegister;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.single;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.FRAGMENT_CONFEMAIL;
import static com.danielandshayegan.discovrus.ui.fragment.CategorySelectFragment.CATEGORY_ID;
import static com.danielandshayegan.discovrus.ui.fragment.CategorySelectFragment.CATEGORY_IMAGE;
import static com.danielandshayegan.discovrus.ui.fragment.CategorySelectFragment.CATEGORY_NAME;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends BaseFragment implements SingleCallback {

    private UserLoginTask mAuthTask = null;
    FragmentRegisterBinding mBinding;
    View view;
    public static final String ANIMATION = "animation";
    String registerType;
    public static final String ACTIVITY_INTENT = "activityIntent";
    public static final int SELECT_CATEGORY = 100;
    String terms, login;
    Intent intentLogin;
    private WebserviceBuilder apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    private ArrayList<String> workingData = new ArrayList<>();
    private WorkingDataAdapter mAdapter;
    public Uri mImageUri;
    Bitmap bitmap;
    byte[] byteImage;
    File fileImage;
    String userRoal;
    int categoryId, bussinessCategoryId;
    String firstName, lastName, email, photo, socialMediaId;
    boolean isLinkedIn;
    SocialMediaLoginData object;
    String signUpType;
    private static final int RECORD_REQUEST_CODE = 101;
    MultipartBody.Part body;
    boolean isEmail = false, isInstagram = false, isFacebook = false;
    ByteArrayOutputStream baos;
    double lat, lng;
    String address, postalCode;

    public RegisterFragment() {
        // Required empty public constructor
    }

    static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    public static RegisterFragment newInstance(Bundle extras) {
        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("CheckResult")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_register, container, false);
        view = mBinding.getRoot();
        makeRequest();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        signUpType = "email";

        terms = getString(R.string.txt_terms) + " " + "<b>" + getString(R.string.txt_conditions) + "</b> ";
        login = "<font color=#4a90e2>Already registered ? </font> <font color=#4be9eb>Login</font>";
        Intent intent = getActivity().getIntent();
        if (null != intent) {
            registerType = intent.getExtras().getString(ACTIVITY_INTENT);
            switch (registerType) {
                case "IndividualRegister":
                    mBinding.uiIndividual.mainRelative.setVisibility(View.VISIBLE);
                    mBinding.uiBusiness.mainRelative.setVisibility(View.GONE);
                    mBinding.uiIndividual.txtTerms.setText(Html.fromHtml(terms));
                    mBinding.uiIndividual.txtLogin.setText(Html.fromHtml(login));
                    userRoal = "Individual";
                    categoryId = 0;
                    break;
                case "BusinessRegister":
                    mBinding.uiBusiness.mainRelative.setVisibility(View.VISIBLE);
                    mBinding.uiIndividual.mainRelative.setVisibility(View.GONE);
                    mBinding.uiBusiness.txtTerms.setText(Html.fromHtml(terms));
                    mBinding.uiBusiness.txtLogin.setText(Html.fromHtml(login));
                    userRoal = "Business";
            }
        }
        if (getArguments() != null) {
            object = (SocialMediaLoginData) getArguments().get("socialMediaData");
            if (object != null) {
                firstName = object.getFirstName();
                lastName = object.getLastName();
                email = object.getEmailAddress();
                photo = object.getPhoto();
                isLinkedIn = object.isLinkedIn();
                isFacebook=object.getIsFaceBook();

                mBinding.uiIndividual.edtPass.setEnabled(false);
                mBinding.uiIndividual.edtPass.setInputType(InputType.TYPE_NULL);
                mBinding.uiIndividual.edtPass.setFocusable(false);
                mBinding.uiIndividual.edtPass.setAlpha(Float.valueOf(String.valueOf(0.5)));

                mBinding.uiIndividual.edtPassConfirm.setEnabled(false);
                mBinding.uiIndividual.edtPassConfirm.setInputType(InputType.TYPE_NULL);
                mBinding.uiIndividual.edtPassConfirm.setFocusable(false);
                mBinding.uiIndividual.edtPassConfirm.setAlpha(Float.valueOf(String.valueOf(0.5)));

                mBinding.uiBusiness.edtPass.setEnabled(false);
                mBinding.uiBusiness.edtPass.setInputType(InputType.TYPE_NULL);
                mBinding.uiBusiness.edtPass.setFocusable(false);
                mBinding.uiBusiness.edtPass.setAlpha(Float.valueOf(String.valueOf(0.5)));

                mBinding.uiBusiness.edtPassConfirm.setEnabled(false);
                mBinding.uiBusiness.edtPassConfirm.setInputType(InputType.TYPE_NULL);
                mBinding.uiBusiness.edtPassConfirm.setFocusable(false);
                mBinding.uiBusiness.edtPassConfirm.setAlpha(Float.valueOf(String.valueOf(0.5)));

                Log.e("isLInkedIn", "" + isLinkedIn);

                if (isLinkedIn) {
                    signUpType = "linkedin";
                } else {
                    signUpType = "instagram";
                }
                if(isFacebook){
                    signUpType = "facebook";
                }

                Log.e("isLInkedIn", "" + signUpType);

                socialMediaId = object.getId();
                mBinding.uiIndividual.edtName.setText(firstName);
                mBinding.uiIndividual.edtLastName.setText(lastName);
                mBinding.uiIndividual.edtEmail.setText(email);

                if(isFacebook){
                    mBinding.uiIndividual.edtEmail.setEnabled(false);
                }
                mBinding.uiIndividual.edtEmail.setEnabled(false);

                mBinding.uiBusiness.edtName.setText(firstName);
                mBinding.uiBusiness.edtLastName.setText(lastName);
                mBinding.uiBusiness.edtEmail.setText(email);

                mBinding.uiBusiness.edtEmail.setEnabled(false);

                if(isFacebook){
                    mBinding.uiBusiness.edtEmail.setEnabled(false);
                }

                mBinding.uiBusiness.edtEmail.setEnabled(false);

                if (photo != null) {
                }
                if (!photo.equals("")) {
                    if (userRoal.equals("Business")) {
                        Picasso.with(getActivity()).
                                load(photo).
                                into(mBinding.uiBusiness.userImg);
                    } else {
                        Picasso.with(getActivity()).
                                load(photo).
                                into(mBinding.uiIndividual.userImg);
                    }

                    if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(true);
                        ImageDownloader imageDownloader = new ImageDownloader(photo);
                        imageDownloader.execute();
                    } else {
                        makeRequest();
                        if (getActivity().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                            disableScreen(true);
                            ImageDownloader imageDownloader = new ImageDownloader(photo);
                            imageDownloader.execute();
                        }
                    }


                }
            }

        }
        mBinding.uiIndividual.edtLocation.setFocusable(false);
        mBinding.uiBusiness.edtLocation.setFocusable(false);
        mBinding.uiIndividual.edtLocation.setOnClickListener(view -> {
            LocationDialog locationDialog = new LocationDialog();
            locationDialog.setTargetFragment(this, 500);
            Bundle bundle = new Bundle();
            bundle.putString("RegisterType", "Individual");
            locationDialog.setArguments(bundle);
            locationDialog.show(getFragmentManager(), "Search Location");
        });

        mBinding.uiBusiness.edtLocation.setOnClickListener(view -> {
            LocationDialog locationDialog = new LocationDialog();
            locationDialog.setTargetFragment(this, 501);
            Bundle bundle = new Bundle();
            bundle.putString("RegisterType", "Business");
            locationDialog.setArguments(bundle);
            locationDialog.show(getFragmentManager(), "Search Location");
        });

        mBinding.uiIndividual.txtLogin.setOnClickListener(v -> {
            intentLogin = new Intent(getActivity(), MainActivity.class).putExtra(ANIMATION, "loginAnimation");
            startActivity(intentLogin);
            getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            getActivity().finish();
        });
        mBinding.uiBusiness.txtLogin.setOnClickListener(v -> {
            intentLogin = new Intent(getActivity(), MainActivity.class).putExtra(ANIMATION, "loginAnimation");
            startActivity(intentLogin);
            getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            getActivity().finish();
        });
        mBinding.uiBusiness.btnSignup.setOnClickListener(v -> {
            if (validationBusiness()) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callRegisterBusinessApi();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        });
        mBinding.uiBusiness.edtCategory.setOnClickListener(v -> {
            getActivity().startActivityForResult(new Intent(mContext, SelectCategoryActivity.class), SELECT_CATEGORY);
        });
        mBinding.uiIndividual.img.setOnClickListener(v -> {
            showDialog(mBinding.uiIndividual.edtWorking, getInfoMessage(mBinding.uiIndividual.img));
        });
        setEditTextTouchListener(mBinding.uiIndividual.edtLocation);
        setEditTextTouchListener(mBinding.uiIndividual.edtEmail);
        setEditTextTouchListener(mBinding.uiBusiness.edtBuisnessName);
        setEditTextTouchListener(mBinding.uiBusiness.edtLocation);
        setEditTextTouchListener(mBinding.uiBusiness.edtEmail);
        mBinding.uiIndividual.imgCamera.setOnClickListener(v -> {
            imgPick();

        });
        mBinding.uiIndividual.userImg.setOnClickListener(v -> {
            imgPick();

        });
        mBinding.uiBusiness.userImg.setOnClickListener(v -> imgPick());
        mBinding.uiBusiness.imgCamera.setOnClickListener(v -> imgPick());
        mBinding.uiIndividual.edtWorking.setOnFocusChangeListener(editWorkFocusListener);
        //  RxTextView.textChanges(mBinding.uiIndividual.edtWorking).subscribe(this::onTextChangeWorking);

       /* Observable<Boolean> usernameObservable = RxTextView.textChanges(mBinding.uiIndividual.edtName)
                .map(username -> Validators.isValidString(String.valueOf(username))).skip(1);

        usernameObservable.subscribe(this::onNameTextChanged);*/


        mBinding.uiIndividual.edtWorking.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mBinding.uiIndividual.imgCopy.setImageDrawable(getResources().getDrawable(R.drawable.copy_selected));
            }
        });

        mBinding.uiIndividual.btnSignup.setOnClickListener(v -> {

            if (validationIndividual()) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                    disableScreen(true);
                    callRegisterIndividualApi();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }
        });

        mBinding.uiBusiness.ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(mBinding.uiBusiness.edtCategory, getInfoMessage(mBinding.uiBusiness.edtCategory));
            }
        });

        disposable.add(
                RxTextView.textChangeEvents(mBinding.uiIndividual.edtWorking)
                        .skipInitialValue()
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(searchContactsTextWatcher()));

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setEditTextTouchListener(EditText editText) {
        editText.setOnTouchListener((v, event) -> {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getRawX() >= (editText.getRight() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    // your action here
                    showDialog(v, getInfoMessage(editText));
                    return true;
                }
            }
            return false;
        });
    }

    private String getInfoMessage(View editText) {
        String message = "";
        switch (editText.getId()) {
            case R.id.edt_buisness_name:
                message = "Please enter the registered business or company name";
                break;
            case R.id.edt_location:
                message = "Please enter the location in which your business operates  e.g. Soho";
                break;
            case R.id.edt_email:
                message = "Please enter Personalised business email address e.g. name@discovrus.com";
                break;
            case R.id.img:
                message = "Please enter the registered business or company name";
                break;
            case R.id.edt_category:
                message = "Please select the category which best describes your business or service";
                break;
        }
        return message;
    }

    private void showDialog(View view, String message) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        DialogInfoViewBinding binding = DataBindingUtil.inflate(LayoutInflater.from(mContext), R.layout.dialog_info_view, null, false);
        binding.txtInfoMessage.setText(message);
        PopupWindow popUp = new PopupWindow(binding.getRoot(), dpToPx(240),
                LinearLayout.LayoutParams.WRAP_CONTENT, false);
        popUp.setTouchable(true);
        popUp.setFocusable(true);
        popUp.setOutsideTouchable(true);
        popUp.showAtLocation(view, Gravity.NO_GRAVITY, location[0], location[1]);
        new Handler().postDelayed(popUp::dismiss, 2000);
    }

    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    class ImageDownloader extends AsyncTask<Void, Void, Void> {
        String urlPath = null;

        public ImageDownloader(String url) {
            this.urlPath = url;
            Log.e("PATHIMAGE", urlPath);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            Log.e("imageUrl", urlPath.toString());
            URL u = null;

            try {
                u = new URL(urlPath);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            if (u != null) {
                baos = new ByteArrayOutputStream();
                InputStream is = null;
                try {
                    is = u.openStream();
                    byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
                    int n;

                    while ((n = is.read(byteChunk)) > 0) {
                        baos.write(byteChunk, 0, n);
                    }
                } catch (IOException e) {
                    System.err.printf("Failed while reading bytes from %s: %s", u.toExternalForm(), e.getMessage());
                    e.printStackTrace();
                    // Perform any other exception handling that's appropriate.
                } finally {
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    return null;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            mBinding.loading.progressBar.setVisibility(View.GONE);
            disableScreen(false);
            Bitmap bitmap = BitmapFactory.decodeByteArray(baos.toByteArray(), 0, baos.toByteArray().length);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), baos.toByteArray());
            body = MultipartBody.Part.createFormData("UserPhoto", "image.png", requestFile);
        }
    }


    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        Log.e("onactivityResult", "done");
        if (reqCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    getActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                startCropImageActivity(imageUri);
            }

        }
        if (reqCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());
                Bundle extra = data.getExtras();
                if (null != extra) {
                    if (userRoal.equals("Business")) {

                        mBinding.uiBusiness.userImg.setImageURI(mImageUri);
                    } else {
                        mBinding.uiIndividual.userImg.setImageURI(mImageUri);
                    }
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream byteArrayOutputStreamSSN = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStreamSSN);
                    byteImage = byteArrayOutputStreamSSN.toByteArray();


                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }
        if (reqCode == SELECT_CATEGORY && resultCode == RESULT_OK && data != null) {
            mBinding.uiBusiness.edtCategory.setText(data.getStringExtra(CATEGORY_NAME));
            String imagePath = data.getStringExtra(CATEGORY_IMAGE);
            Picasso.with(getActivity()).
                    load(ApiClient.WebService.imageUrl + imagePath).
                    into(mBinding.uiBusiness.imgCategory);
            bussinessCategoryId = data.getIntExtra(CATEGORY_ID, 0);
        }

        if (reqCode == 500 && resultCode == RESULT_OK) {
            mBinding.uiIndividual.edtLocation.setText(data.getStringExtra("address"));
            address = data.getStringExtra("address");
            lat = data.getDoubleExtra("lat", 0.0);
            lng = data.getDoubleExtra("lng", 0.0);
            postalCode = data.getStringExtra("postalCode");

        }
        if (reqCode == 501 && resultCode == RESULT_OK) {
            mBinding.uiBusiness.edtLocation.setText(data.getStringExtra("address"));
            address = data.getStringExtra("address");
            lat = data.getDoubleExtra("lat", 0.0);
            lng = data.getDoubleExtra("lng", 0.0);
            postalCode = data.getStringExtra("postalCode");
        }
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        getActivity().startActivityForResult(getPickImageChooserIntent(), 200);
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getActivity().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)

                .setMultiTouchEnabled(true)
                .start(getActivity());
    }


    private DisposableObserver<TextViewTextChangeEvent> searchContactsTextWatcher() {

        return new DisposableObserver<TextViewTextChangeEvent>() {
            @Override
            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    Log.e("edtLengthBefore", "" + mBinding.uiIndividual.edtWorking.getText().length());
                    if (mBinding.uiIndividual.edtWorking.getText().length() > 2) {
                        workingData.clear();
                        mAdapter = new WorkingDataAdapter(getActivity(), workingData);
                        Log.e("edtLength", "" + mBinding.uiIndividual.edtWorking.getText().length());
                        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(true);
                        disposable.add(apiService.workingAtData(mBinding.uiIndividual.edtWorking.getText().toString()).
                                subscribeOn(Schedulers.io()).
                                observeOn(AndroidSchedulers.mainThread()).
                                subscribeWith(new DisposableSingleObserver<WorkingAtData>() {
                                    @Override
                                    public void onSuccess(WorkingAtData value) {

                                        if (value.isSuccess() == true && value.getData().size() > 0) {
                                            workingData.clear();
                                            workingData.addAll(value.getData());
                                            mAdapter.notifyDataSetChanged();
                                            mBinding.uiIndividual.edtWorking.setThreshold(1);
                                            mBinding.uiIndividual.edtWorking.setAdapter(mAdapter);
                                            mBinding.loading.progressBar.setVisibility(View.GONE);
                                            disableScreen(false);
                                        } else {
                                            mBinding.loading.progressBar.setVisibility(View.GONE);
                                            disableScreen(false);
                                        }

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.d("fail", e.toString());
                                        mBinding.loading.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                    }
                                }));
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
        //... your stuff
    }

    private View.OnFocusChangeListener editWorkFocusListener = new View.OnFocusChangeListener() {
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                mBinding.uiIndividual.view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            } else {
                mBinding.uiIndividual.view.setBackgroundColor(getResources().getColor(R.color.edt_view_color));
                if (mBinding.uiIndividual.edtWorking.length() > 0) {
                    mBinding.uiIndividual.imgCopy.setImageDrawable(getResources().getDrawable(R.drawable.copy_selected));
                }
            }
        }
    };

    private boolean validationIndividual() {
        boolean value = true;
        if (mBinding.uiIndividual.edtName.length() == 0) {
            mBinding.uiIndividual.txtErrorName.setText(R.string.null_first_name);
            mBinding.uiIndividual.txtErrorName.setVisibility(View.VISIBLE);
            value = false;
        }
        if (mBinding.uiIndividual.edtPassConfirm.length() == 0 && signUpType.equals("email")) {
            mBinding.uiIndividual.txtErrorConfPass.setText(R.string.null_password);
            mBinding.uiIndividual.txtErrorConfPass.setVisibility(View.VISIBLE);
            value = false;
        } else if (mBinding.uiIndividual.edtPassConfirm.length() < 6 && signUpType.equals("email")) {
            mBinding.uiIndividual.txtErrorConfPass.setText(R.string.pass_six_digit);
            mBinding.uiIndividual.txtErrorConfPass.setVisibility(View.VISIBLE);
            value = false;
        } else if (!mBinding.uiIndividual.edtPass.getText().toString().equals(mBinding.uiIndividual.edtPassConfirm.getText().toString()) && signUpType.equals("email")) {
            mBinding.uiIndividual.txtErrorConfPass.setText(R.string.pass_confirmpass_same);
            mBinding.uiIndividual.txtErrorConfPass.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiIndividual.edtPass.length() == 0 && signUpType.equals("email")) {
            mBinding.uiIndividual.txtErrorPass.setText(R.string.null_password);
            mBinding.uiIndividual.txtErrorPass.setVisibility(View.VISIBLE);
            value = false;
        } else if (mBinding.uiIndividual.edtPass.length() < 6 && signUpType.equals("email")) {
            mBinding.uiIndividual.txtErrorPass.setText(R.string.pass_six_digit);
            mBinding.uiIndividual.txtErrorPass.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiIndividual.edtEmail.length() == 0) {
            mBinding.uiIndividual.txtErrorEmail.setText(R.string.null_email);
            mBinding.uiIndividual.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        } else if (!Validators.isValidEmail(String.valueOf(mBinding.uiIndividual.edtEmail.getText().toString()))) {
            mBinding.uiIndividual.txtErrorEmail.setText(R.string.valid_email);
            mBinding.uiIndividual.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiIndividual.edtLocation.length() == 0) {
            mBinding.uiIndividual.txtErrorLocation.setText(R.string.null_location);
            mBinding.uiIndividual.txtErrorLocation.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiIndividual.edtWorking.length() == 0) {
            mBinding.uiIndividual.txtErrorWorking.setText(R.string.null_work_name);
            mBinding.uiIndividual.txtErrorWorking.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiIndividual.edtLastName.length() == 0) {
            mBinding.uiIndividual.txtErrorLastName.setText(R.string.null_last_name);
            mBinding.uiIndividual.txtErrorLastName.setVisibility(View.VISIBLE);
            value = false;
        }
        if (!signUpType.equals("email") && fileImage != null) {
            value = true;
        } else if ((fileImage == null && signUpType.equals("email")) || (!signUpType.equals("email") && baos == null)) {
            Toast.makeText(getActivity(), "Please select image", Toast.LENGTH_SHORT).show();
            value = false;
        }


        return value;
    }

    public boolean validationBusiness() {

        boolean value = true;
        if (mBinding.uiBusiness.edtName.length() == 0) {
            mBinding.uiBusiness.txtErrorName.setText(R.string.null_first_name);
            mBinding.uiBusiness.txtErrorName.setVisibility(View.VISIBLE);
            value = false;
        }
        if (mBinding.uiBusiness.edtPassConfirm.length() == 0 && signUpType.equals("email")) {
            mBinding.uiBusiness.txtErrorConfPass.setText(R.string.null_password);
            mBinding.uiBusiness.txtErrorConfPass.setVisibility(View.VISIBLE);
            value = false;
        } else if (mBinding.uiBusiness.edtPassConfirm.length() < 6 && signUpType.equals("email")) {
            mBinding.uiBusiness.txtErrorConfPass.setText(R.string.pass_six_digit);
            mBinding.uiBusiness.txtErrorConfPass.setVisibility(View.VISIBLE);
            value = false;
        } else if (!mBinding.uiBusiness.edtPass.getText().toString().equals(mBinding.uiBusiness.edtPassConfirm.getText().toString()) && signUpType.equals("email")) {
            mBinding.uiBusiness.txtErrorConfPass.setText(R.string.pass_confirmpass_same);
            mBinding.uiBusiness.txtErrorConfPass.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiBusiness.edtPass.length() == 0 && signUpType.equals("email")) {
            mBinding.uiBusiness.txtErrorPass.setText(R.string.null_password);
            mBinding.uiBusiness.txtErrorPass.setVisibility(View.VISIBLE);
            value = false;
        } else if (mBinding.uiBusiness.edtPass.length() < 6 && signUpType.equals("email")) {
            mBinding.uiBusiness.txtErrorPass.setText(R.string.pass_six_digit);
            mBinding.uiBusiness.txtErrorPass.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiBusiness.edtEmail.length() == 0) {
            mBinding.uiBusiness.txtErrorEmail.setText(R.string.null_email);
            mBinding.uiBusiness.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        } else if (!Validators.isValidEmail(String.valueOf(mBinding.uiBusiness.edtEmail.getText().toString()))) {
            mBinding.uiBusiness.txtErrorEmail.setText(R.string.valid_email);
            mBinding.uiBusiness.txtErrorEmail.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiBusiness.edtLocation.length() == 0) {
            mBinding.uiBusiness.txtErrorLocation.setText(R.string.null_location);
            mBinding.uiBusiness.txtErrorLocation.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiBusiness.edtBuisnessName.length() == 0) {
            mBinding.uiBusiness.txtErrorBusiness.setText(R.string.null_business_name);
            mBinding.uiBusiness.txtErrorBusiness.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiBusiness.edtLastName.length() == 0) {
            mBinding.uiBusiness.txtErrorLastName.setText(R.string.null_last_name);
            mBinding.uiBusiness.txtErrorLastName.setVisibility(View.VISIBLE);
            value = false;
        }

        if (mBinding.uiBusiness.edtCategory.length() == 0) {
            mBinding.uiBusiness.txtErrorCategory.setText(R.string.null_category);
            mBinding.uiBusiness.txtErrorCategory.setVisibility(View.VISIBLE);
            value = false;
        }

        if ((fileImage == null && signUpType.equals("email")) || (!signUpType.equals("email") && baos.toByteArray() == null)) {
            Toast.makeText(getActivity(), "Please select image", Toast.LENGTH_SHORT).show();
            value = false;
        }


        return value;
    }



  /*  private void onNameTextChanged(CharSequence charSequence) {

        if (charSequence.length() == 0) {
            mBinding.uiIndividual.txtErrorName.setText(R.string.null_first_name);
            mBinding.uiIndividual.txtErrorName.setVisibility(View.VISIBLE);
        } else {
            mBinding.uiIndividual.txtErrorName.setText("");
            mBinding.uiIndividual.txtErrorName.setVisibility(View.GONE);
        }

    }*/

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();
    }

    private void callRegisterBusinessApi() {
        if (signUpType.equals("email")) {
            isEmail = true;
            isInstagram = false;
            isLinkedIn = false;
            isFacebook = false;
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
            MultipartBody.Part bodyEmail = MultipartBody.Part.createFormData("UserPhoto", fileImage.getName(), reqFile);
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    userBusinessRegisterEmail(mBinding.uiBusiness.edtName.getText().toString(), mBinding.uiBusiness.edtLastName.getText().toString(), mBinding.uiBusiness.edtEmail.getText().toString(),
                                            mBinding.uiBusiness.edtPass.getText().toString(), mBinding.uiBusiness.edtLocation.getText().toString(), userRoal, 1,
                                            mBinding.uiBusiness.edtBuisnessName.getText().toString(), isEmail, isLinkedIn, isInstagram, isFacebook, "", "", lat, lng, postalCode, bodyEmail)
                            , getCompositeDisposable(), businessRegister, this);
        } else {

            switch (signUpType) {
                case "linkedin":
                    isEmail = false;
                    isInstagram = false;
                    isLinkedIn = true;
                    isFacebook = false;
                    break;
                case "instagram":
                    isEmail = false;
                    isInstagram = true;
                    isLinkedIn = false;
                    isFacebook = false;

                case "facebook":
                    isEmail = false;
                    isInstagram = false;
                    isLinkedIn = false;
                    isFacebook = true;
            }

            if (fileImage != null) {
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
                body = MultipartBody.Part.createFormData("UserPhoto", fileImage.getName(), reqFile);
            }
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    userBusinessRegisterSocialMedia(mBinding.uiBusiness.edtName.getText().toString(), mBinding.uiBusiness.edtLastName.getText().toString(), mBinding.uiBusiness.edtEmail.getText().toString(),
                                            mBinding.uiBusiness.edtLocation.getText().toString(), userRoal, 1, mBinding.uiBusiness.edtBuisnessName.getText().toString(),
                                            isEmail, isLinkedIn, isInstagram, isFacebook, socialMediaId, "", "", lat, lng, postalCode, body)
                            , getCompositeDisposable(), businessRegister, this);

        }
    }

    private void callRegisterIndividualApi() {

        if (signUpType.equals("email")) {
            isEmail = true;
            isInstagram = false;
            isLinkedIn = false;
            isFacebook = false;
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
            MultipartBody.Part bodyEmail = MultipartBody.Part.createFormData("UserPhoto", fileImage.getName(), reqFile);
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    userRegister(mBinding.uiIndividual.edtName.getText().toString(), mBinding.uiIndividual.edtLastName.getText().toString(), mBinding.uiIndividual.edtEmail.getText().toString(),
                                            mBinding.uiIndividual.edtPass.getText().toString(), mBinding.uiIndividual.edtLocation.getText().toString(), userRoal, mBinding.uiIndividual.edtWorking.getText().toString(),
                                            categoryId, isEmail, isLinkedIn, isInstagram, isFacebook, "", "", lat, lng, postalCode, bodyEmail)
                            , getCompositeDisposable(), single, this);
        } else {

            switch (signUpType) {
                case "linkedin":
                    isEmail = false;
                    isInstagram = false;
                    isLinkedIn = true;
                    isFacebook = false;
                    break;
                case "instagram":
                    isEmail = false;
                    isInstagram = true;
                    isLinkedIn = false;
                    isFacebook = false;

                case "facebook":
                    isEmail = false;
                    isInstagram = false;
                    isLinkedIn = false;
                    isFacebook = true;

            }

            if (fileImage != null) {
                RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), fileImage);
                body = MultipartBody.Part.createFormData("UserPhoto", fileImage.getName(), reqFile);
            }
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    userRegisterSocialMedia(mBinding.uiIndividual.edtName.getText().toString(), mBinding.uiIndividual.edtLastName.getText().toString(), mBinding.uiIndividual.edtEmail.getText().toString(),
                                            mBinding.uiIndividual.edtLocation.getText().toString(), userRoal, mBinding.uiIndividual.edtWorking.getText().toString(), categoryId, isEmail,
                                            isLinkedIn, isInstagram, isFacebook, socialMediaId, "", "", lat, lng, postalCode, body)
                            , getCompositeDisposable(), single, this);

        }
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
        switch (apiNames) {
            case single:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                IndividualRegisterData registerdata = (IndividualRegisterData) o;
                if (registerdata.isSuccess() == true) {
                    Toast.makeText(getActivity(), registerdata.getMessage().get(0), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_CONFEMAIL);
                    intent.putExtra("email", registerdata.getData().getEmail());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.stay);

                    break;
                } else {

                    Toast.makeText(getContext(), registerdata.getMessage().get(0), Toast.LENGTH_LONG).show();
                }

                break;

            case businessRegister:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                BusinessRegisterData businessRegisterData = (BusinessRegisterData) o;
                if (businessRegisterData.isSuccess()) {

                    setUpAppLogicUser(User.AuthenticationType.APPLOZIC);
                    Toast.makeText(getActivity(), businessRegisterData.getMessage().get(0), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(), CommonIntentActivity.class).putExtra(ACTIVITY_INTENT, FRAGMENT_CONFEMAIL);
                    intent.putExtra("email", businessRegisterData.getData().getEmail());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.enter, R.anim.stay);
                    break;
                } else {
                    Toast.makeText(getContext(), businessRegisterData.getMessage().get(0), Toast.LENGTH_LONG).show();
                }
                break;


        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }
    private void setUpAppLogicUser(User.AuthenticationType authenticationType) {



        UserLoginTask.TaskListener listener = new UserLoginTask.TaskListener() {

            @Override
            public void onSuccess(RegistrationResponse registrationResponse, final Context context) {
                mAuthTask = null;


                ApplozicClient.getInstance(context).setContextBasedChat(true).setHandleDial(true);

                Map<ApplozicSetting.RequestCode, String> activityCallbacks = new HashMap<ApplozicSetting.RequestCode, String>();
                activityCallbacks.put(ApplozicSetting.RequestCode.USER_LOOUT, MenuActivity.class.getName());
                ApplozicSetting.getInstance(context).setActivityCallbacks(activityCallbacks);
                MobiComUserPreference.getInstance(context).setUserRoleType(registrationResponse.getRoleType());


                PushNotificationTask.TaskListener pushNotificationTaskListener = new PushNotificationTask.TaskListener() {
                    @Override
                    public void onSuccess(RegistrationResponse registrationResponse) {

                    }

                    @Override
                    public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
//
                    }
                };
                PushNotificationTask pushNotificationTask = new PushNotificationTask(Applozic.getInstance(context).getDeviceRegistrationId(), pushNotificationTaskListener, context);
                pushNotificationTask.execute((Void) null);


            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                mAuthTask = null;

                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle(exception.getMessage().toString());
                alertDialog.setMessage(exception.toString());
                alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, getString(R.string.ok_alert),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

            }
        };


        User user = new User();
        user.setUserId(String.valueOf(App_pref.getAuthorizedUser(getContext()).getData().getUserId()));
        user.setEmail(App_pref.getAuthorizedUser(getContext()).getData().getEmail());
        user.setPassword("123456");
        user.setDisplayName(App_pref.getAuthorizedUser(getContext()).getData().getFirstName());
        //user.setContactNumber(App_pref.getAuthorizedUser(getApplicationContext()).getData().getN());
        user.setImageLink(App_pref.getAuthorizedUser(getContext()).getData().getUserImagePath());
        user.setAuthenticationTypeId(authenticationType.getValue());

        mAuthTask = new UserLoginTask(user, listener, getContext());
        mAuthTask.execute((Void) null);
    }

    protected void makeRequest() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, RECORD_REQUEST_CODE);
    }
}
