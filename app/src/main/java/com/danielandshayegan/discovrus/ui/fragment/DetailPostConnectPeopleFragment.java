package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.ConnectPeopleAdapter;
import com.danielandshayegan.discovrus.custome_veiws.chips.ChipsView;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostConnectPeopleBinding;
import com.danielandshayegan.discovrus.models.ConnectPeopleData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.DetailPostConnectPeopleActivity;
import com.danielandshayegan.discovrus.utils.Utils;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MultipartBody;

import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPostConnectPeopleFragment extends BaseFragment {

    public FragmentDetailPostConnectPeopleBinding mBinding;
    View view;
    int userId;
    int postType;
    public Uri mImageUri;
    Bitmap bitmap;
    MultipartBody.Part body;

    Activity activityMain;
    public static DetailPostConnectPeopleFragment peopleFragment;
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<ConnectPeopleData.DataBean> peopleData = new ArrayList<>();
    ConnectPeopleAdapter mAdapter;
    public HashMap<String, ConnectPeopleData.DataBean> selectedPeopleList = new HashMap<String, ConnectPeopleData.DataBean>();

    public DetailPostConnectPeopleFragment() {
        // Required empty public constructor
    }

    public static DetailPostConnectPeopleFragment newInstance() {
        return new DetailPostConnectPeopleFragment();
    }

    public static DetailPostConnectPeopleFragment newInstance(Bundle extras) {
        DetailPostConnectPeopleFragment fragment = new DetailPostConnectPeopleFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_post_connect_people, container, false);
        view = mBinding.getRoot();
        peopleFragment = this;
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        activityMain = this.getActivity();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);

        setChipsView();
        setSearchView();
        setClicks();

        return view;
    }

    public void setChipsView() {
        mBinding.uiConnectPeople.chipsView.setTypeface(ResourcesCompat.getFont(getActivity(), R.font.avenir_next_regular));
        // change EditText config
        mBinding.uiConnectPeople.chipsView.getEditText().setCursorVisible(false);
        mBinding.uiConnectPeople.chipsView.getEditText().setTextIsSelectable(true);

        mBinding.uiConnectPeople.chipsView.setChipsValidator(new ChipsView.ChipValidator() {
            @Override
            public boolean isValid(ConnectPeopleData.DataBean contact) {
                /*if (contact.getDisplayName().equals("asd@qwe.de")) {
                    return false;
                }*/
                return true;
            }
        });

        mBinding.uiConnectPeople.chipsView.setChipsListener(new ChipsView.ChipsListener() {
            @Override
            public void onChipAdded(ChipsView.Chip chip) {
                for (ChipsView.Chip chipItem : mBinding.uiConnectPeople.chipsView.getChips()) {
                    Log.d("ChipList", "chip: " + chipItem.toString());
                }
            }

            @Override
            public void onChipDeleted(ChipsView.Chip chip) {
                selectedPeopleList.remove(String.valueOf(chip.getContact().getUserId()));
                if (peopleData.contains(chip.getContact())) {
                    peopleData.get(peopleData.indexOf(chip.getContact())).setSelected(false);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onTextChanged(CharSequence text) {
//                mAdapter.filterItems(text);
            }

            @Override
            public boolean onInputNotRecognized(String text) {

                return false;
            }
        });

        if (getArguments() != null) {
            selectedPeopleList = (HashMap<String, ConnectPeopleData.DataBean>) getArguments().getSerializable("peopleList");
            if (selectedPeopleList.size() > 0) {
                Iterator myVeryOwnIterator = selectedPeopleList.keySet().iterator();
                while (myVeryOwnIterator.hasNext()) {
                    String key = (String) myVeryOwnIterator.next();
                    ConnectPeopleData.DataBean value = selectedPeopleList.get(key);
                    value.setSelected(false);
                    mBinding.uiConnectPeople.chipsView.addChip(value.getUserName(), ApiClient.WebService.imageUrl + value.getUserImagePath(), value, false);
                }
            }
        }

    }

    public void setClicks() {
        mBinding.uiConnectPeople.imgBack.setOnClickListener(v -> {
            DetailPostConnectPeopleActivity.postingActivity.onBackPressed();
        });

        mBinding.uiConnectPeople.btnNext.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("peopleList", selectedPeopleList);
            mActivity.setResult(Activity.RESULT_OK, intent);
            mActivity.finish();
        });

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setSearchView() {
        mBinding.uiConnectPeople.recyclerPeople.setLayoutManager(new LinearLayoutManager(getActivity()));
        disposable.add(
                RxTextView.textChangeEvents(mBinding.uiConnectPeople.edtSearch)
                        .skipInitialValue()
                        .debounce(300, TimeUnit.MILLISECONDS)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(searchContactsTextWatcher()));

    }

    private DisposableObserver<TextViewTextChangeEvent> searchContactsTextWatcher() {

        return new DisposableObserver<TextViewTextChangeEvent>() {
            @Override
            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    Log.e("edtLengthBefore", "" + mBinding.uiConnectPeople.edtSearch.getText().length());
                    if (mBinding.uiConnectPeople.edtSearch.getText().length() > 1) {
                        peopleData.clear();
                        mAdapter = new ConnectPeopleAdapter(getActivity());
                        mBinding.uiConnectPeople.recyclerPeople.setAdapter(mAdapter);
                        Log.e("edtLength", "" + mBinding.uiConnectPeople.edtSearch.getText().length());
                        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(true);
                        disposable.add(apiService.connectPeople(mBinding.uiConnectPeople.edtSearch.getText().toString()).
                                subscribeOn(Schedulers.io()).
                                observeOn(AndroidSchedulers.mainThread()).
                                subscribeWith(new DisposableSingleObserver<ConnectPeopleData>() {
                                    @Override
                                    public void onSuccess(ConnectPeopleData value) {
                                        if (value.isSuccess() && value.getData().size() > 0) {
                                            peopleData.clear();
                                            if (selectedPeopleList.size() > 0) {
                                                for (int i = 0; i < value.getData().size(); i++) {
                                                    ConnectPeopleData.DataBean dataBean = value.getData().get(i);
                                                    if (selectedPeopleList.containsKey(String.valueOf(dataBean.getUserId()))) {
                                                        dataBean.setSelected(true);
                                                        peopleData.add(dataBean);
                                                    } else {
                                                        peopleData.add(dataBean);
                                                    }
                                                }
                                            } else {
                                                peopleData.addAll(value.getData());
                                            }
                                            mAdapter.clear();
                                            mAdapter.addAll(peopleData);
//                                            mBinding.uiConnectPeople.edtSearch.setThreshold(1);
//                                            mBinding.uiConnectPeople.edtSearch.setAdapter(mAdapter);
                                            mBinding.loading.progressBar.setVisibility(View.GONE);
                                            disableScreen(false);
                                        } else {
                                            mBinding.loading.progressBar.setVisibility(View.GONE);
                                            disableScreen(false);
                                        }

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.d("fail", e.toString());
                                        mBinding.loading.progressBar.setVisibility(View.GONE);
                                        disableScreen(false);
                                    }
                                }));
                    }
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: " + e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };
        //... your stuff
    }


}
