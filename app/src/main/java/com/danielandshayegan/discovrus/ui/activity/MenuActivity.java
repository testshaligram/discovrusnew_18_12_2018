package com.danielandshayegan.discovrus.ui.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.system.ErrnoException;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.Toast;

import com.applozic.mobicomkit.Applozic;
import com.applozic.mobicomkit.ApplozicClient;
import com.applozic.mobicomkit.api.account.register.RegistrationResponse;
import com.applozic.mobicomkit.api.account.user.User;
import com.applozic.mobicomkit.api.account.user.UserLoginTask;
import com.applozic.mobicomkit.listners.AlLoginHandler;
import com.applozic.mobicomkit.uiwidgets.conversation.ConversationUIService;
import com.applozic.mobicomkit.uiwidgets.conversation.activity.ConversationActivity;
import com.applozic.mobicomkit.uiwidgets.people.contact.DeviceContactSyncService;
import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.utils.FileUtils;
import com.danielandshayegan.discovrus.databinding.ActivityMenuBinding;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseActivity;
import com.danielandshayegan.discovrus.ui.fragment.FragmentNavigation;
import com.danielandshayegan.discovrus.ui.fragment.PostManagementFragment;
import com.danielandshayegan.discovrus.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;

import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;
import static com.danielandshayegan.discovrus.ui.fragment.RegisterFragment.SELECT_CATEGORY;

public class MenuActivity extends BaseActivity {

    ActivityMenuBinding mBinding;
    //    BottomSheetBehavior sheetBehavior;
    public static int fragmentView;
    public int postType;
    MultipartBody.Part body;
    int userId;
    private UserLoginTask mAuthTask = null;

    public static final String IMAGE_NAME = "cap_top.png";
    static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    static final String VIDEO_TOTAL_DURATION = "VIDEO_TOTAL_DURATION";
    public Uri mImageUri;
    File fileImage;
    String fragmentName;
    Bundle bundle;
    public static MenuActivity menuActivity;
    ImageView imagePreview;
    private static final int REQUEST_VIDEO_TRIMMER = 0x01;
    private static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    public static String videoUri;
    boolean doubleBackToExitPressedOnce = false;
    Animator translationAnimator;
    Uri originalImageUri;
    PostManagementFragment fragment;

    FragmentNavigation fragmentNavigation;

    // load native image filters library
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_menu);
        body = null;
        userId = App_pref.getAuthorizedUser(getApplicationContext()).getData().getUserId();
        menuActivity = this;
//        sheetBehavior = BottomSheetBehavior.from(mBinding.uiNavigation.uiBottomsheet.constBottomsheet);

        setUpAppLogicUser();
        bottomMenuClick();
        bottomSheetClick();
        Intent intent = getIntent();
        if (null != intent) {

            fragmentName = intent.getExtras().getString(ACTIVITY_INTENT);
            switch (fragmentName) {
                case "chatFragment":
                    Intent intentDeviceSync = new Intent(MenuActivity.this, DeviceContactSyncService.class);
                    DeviceContactSyncService.enqueueWork(MenuActivity.this, intentDeviceSync);

                    Applozic.getInstance(MenuActivity.this).enableDeviceContactSync(true);

                    Intent intentChat = new Intent(MenuActivity.this, ConversationActivity.class);
                    if (ApplozicClient.getInstance(MenuActivity.this).isContextBasedChat()) {
                        intentChat.putExtra(ConversationUIService.CONTEXT_BASED_CHAT, true);
                    }
                    startActivity(intentChat);
                  /*  getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, ChatFragment.newInstance(), "")
                            .commit();
                    mBinding.uiNavigation.discovrusChat.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.chat_selected));*/
                    break;

                case "navigationFragment":
                    fragmentNavigation = FragmentNavigation.newInstance();

                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, fragmentNavigation, "")
//                            .replace(R.id.fragment_replace, FragmentHeatMap.newInstance(), "")
                            .commit();
                    mBinding.uiNavigation.discovrusNavigation.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.discover_navigation_selected));
                    break;

                case "profileFragment":
                    if (App_pref.getAuthorizedUser(this).getData().getAdminUserRoleName().equalsIgnoreCase("Business")) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", App_pref.getAuthorizedUser(this).getData().getUserId());
                        bundle.putString("from", "footer");
                        /*mContext.getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.fragment_replace, BusinessProfileFragment.newInstance(bundle), "")
                                .commit();*/

                        Intent intent2 = new Intent(MenuActivity.this, ProfileDetailActivity.class);
                        intent2.putExtra("clickFragmentName", "BusinessProfile");
                        intent2.putExtra("fromProfileTab", true);
                        intent2.putExtras(bundle);
                        startActivity(intent2);
                        finish();
                        //mContext.mActivity.overridePendingTransition(0, 0);

                    } else {
                        Bundle bundle = new Bundle();
                        bundle.putInt("userId", App_pref.getAuthorizedUser(this).getData().getUserId());
                        bundle.putString("from", "footer");
                       /* mContext.getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.fragment_replace, IndividualProfileFragment.newInstance(bundle), "")
                                .commit();*/
                        Intent intent1 = new Intent(MenuActivity.this, ProfileDetailActivity.class);
                        intent1.putExtra("clickFragmentName", "IndividualProfile");
                        intent1.putExtra("fromProfileTab", true);
                        intent1.putExtras(bundle);
                        startActivity(intent1);
                        finish();
                        //mContext.mActivity.overridePendingTransition(0, 0);
                    }
                    /*fragment = LaunchpadFragment.newInstance();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, fragment, "")
                            .commit();
                    mBinding.uiNavigation.discovrusProfile.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.profile_selected));*/
                    break;

                case FRAGMENT_MAIN_MENU:
                    fragment = PostManagementFragment.newInstance();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, fragment, "")
                            .commit();

                    mBinding.uiNavigation.discovrusNews.setImageDrawable(getResources().getDrawable(R.drawable.newsfeed_selected));
                    fragmentView = 2;
                    break;
            }

        }
        defaultViewTranslation();

    }

    private void setUpAppLogicUser() {
        User user = new User();
        user.setUserId(String.valueOf(App_pref.getAuthorizedUser(this).getData().getUserId()));
        user.setDisplayName(App_pref.getAuthorizedUser(this).getData().getFirstName());
        user.setEmail(App_pref.getAuthorizedUser(this).getData().getEmail());
        user.setAuthenticationTypeId(User.AuthenticationType.APPLOZIC.getValue());
        user.setPassword("123456");
        if (App_pref.getAuthorizedUser(this).getData().getUserImagePath() != null)
            user.setImageLink(App_pref.getAuthorizedUser(this).getData().getUserImagePath());

        Applozic.connectUser(this, user, new AlLoginHandler() {
            @Override
            public void onSuccess(RegistrationResponse registrationResponse, Context context) {

            }

            @Override
            public void onFailure(RegistrationResponse registrationResponse, Exception exception) {
                // If any failure in registration the callback  will come here
            }
        });
    }


    public void bottomSheetClick() {
        mBinding.uiNavigation.uiBottomsheet.imgCamera.setOnClickListener(v -> {
            Log.e("camera Clicked", "-----------------");
            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requirePermissions = true;
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 102);
            }

            if (!requirePermissions) {
                imgPick();
            }
            //  openImageFromGallery();
            postType = 1;

        });
        mBinding.uiNavigation.uiBottomsheet.imgCamText.setOnClickListener(v -> {
            Log.e("text image Clicked", "-----------------");
            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requirePermissions = true;
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 102);
            }

            if (!requirePermissions) {
                imgPick();
            }
            // openImageFromGallery();
            postType = 2;

        });
        mBinding.uiNavigation.uiBottomsheet.imgVideo.setOnClickListener(v -> {
            Log.e("video Clicked", "-----------------");
            postType = 4;

            if (Build.VERSION.SDK_INT >= 23)
                getPermission();
            else
                showDialogForVideo();
        });
        mBinding.uiNavigation.uiBottomsheet.imgText.setOnClickListener(v -> {
            Log.e("text Clicked", "-----------------");
            postType = 3;
//            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            mBinding.btnAddPost.setRotation(0.0f);
            int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                    .setDuration(400);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
                    Bundle bundle = new Bundle();
                    bundle.putInt("postType", postType);
                    bundle.putString("imageUri", "");
                    bundle.putString("videoUri", "");

                  /*  Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);

                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, TextPostFragment.newInstance(bundle), "TextPostFragment")
//                        .addToBackStack(null)
                            .commit();*/
                    Intent intent = new Intent(getApplicationContext(), PostFilteringActivity.class);
                    intent.putExtra("filterFragment", "textAddingPost");
                    intent.putExtras(bundle);
                    startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    overridePendingTransition(0, 0);
//            }
//            fragmentView = 2;

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

        });
    }

    public void openUserProfile(int userId, String userType) {
        Bundle bundle = new Bundle();
        bundle.putInt("userId", userId);
        bundle.putString("userType", userType);
        PostManagementFragment postManagementFragment = new PostManagementFragment();
        postManagementFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .replace(R.id.fragment_replace, postManagementFragment, "")
                .commit();


        mBinding.uiNavigation.discovrusNews.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.newsfeed_selected));
        mBinding.uiNavigation.discovrusProfile.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.profile));
        mBinding.uiNavigation.discovrusNavigation.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.discover_navigation));
        mBinding.uiNavigation.discovrusChat.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.chat));
    }

    public void bottomMenuClick() {
        mBinding.uiNavigation.discovrusChat.setOnClickListener(v -> {
            Intent intentDeviceSync = new Intent(MenuActivity.this, DeviceContactSyncService.class);
            DeviceContactSyncService.enqueueWork(MenuActivity.this, intentDeviceSync);

            Applozic.getInstance(MenuActivity.this).enableDeviceContactSync(true);

            Intent intentChat = new Intent(MenuActivity.this, ConversationActivity.class);
            if (ApplozicClient.getInstance(MenuActivity.this).isContextBasedChat()) {
                intentChat.putExtra(ConversationUIService.CONTEXT_BASED_CHAT, true);
            }
            startActivity(intentChat);
            /*getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, ChatFragment.newInstance(), "")
                    .commit();

            mBinding.uiNavigation.discovrusNews.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.newsfeed));
            mBinding.uiNavigation.discovrusProfile.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.profile));
            mBinding.uiNavigation.discovrusNavigation.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.discover_navigation));
            mBinding.uiNavigation.discovrusChat.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.chat_selected));
//            fragmentView = 3;*/

        });

        mBinding.uiNavigation.discovrusNavigation.setOnClickListener(v -> {
            fragmentNavigation = FragmentNavigation.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, fragmentNavigation, "")
//                    .replace(R.id.fragment_replace, FragmentHeatMap.newInstance(), "")
                    .commit();
//            startActivity(new Intent(MenuActivity.this, DetailPostingActivity.class));
            mBinding.uiNavigation.discovrusNews.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.newsfeed));
            mBinding.uiNavigation.discovrusProfile.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.profile));
            mBinding.uiNavigation.discovrusNavigation.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.discover_navigation_selected));
            mBinding.uiNavigation.discovrusChat.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.chat));
//            fragmentView = 1;
        });


        mBinding.uiNavigation.discovrusProfile.setOnClickListener(v -> {
/*
            fragment = LaunchpadFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, fragment, "")
                    .commit();
*/

            /*if (App_pref.getAuthorizedUser(this).getData().getAdminUserRoleName().equalsIgnoreCase("Business")) {
                Bundle bundle = new Bundle();
                bundle.putInt("userId", App_pref.getAuthorizedUser(this).getData().getUserId());
                bundle.putString("from", "footer");
                        *//*mContext.getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.fragment_replace, BusinessProfileFragment.newInstance(bundle), "")
                                .commit();

                Intent intent2 = new Intent(MenuActivity.this, ProfileDetailActivity.class);
                intent2.putExtra("clickFragmentName", "BusinessProfile");
                intent2.putExtras(bundle);
                startActivity(intent2);*//*

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_replace, BusinessProfileFragment.newInstance(bundle), "")
                        .commit();
                //mContext.mActivity.overridePendingTransition(0, 0);

            } else {
                Bundle bundle = new Bundle();
                bundle.putInt("userId", App_pref.getAuthorizedUser(this).getData().getUserId());
                bundle.putString("from", "footer");
                      *//* mContext.getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.fragment_replace, IndividualProfileFragment.newInstance(bundle), "")
                                .commit();*//**//*
                Intent intent1 = new Intent(MenuActivity.this, ProfileDetailActivity.class);
                intent1.putExtra("clickFragmentName", "IndividualProfile");
                intent1.putExtras(bundle);
                startActivity(intent1);*//*

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_replace, IndividualProfileFragment.newInstance(bundle), "")
                        .commit();
                //mContext.mActivity.overridePendingTransition(0, 0);
            }*/

            if (App_pref.getAuthorizedUser(this).getData().getAdminUserRoleName().equalsIgnoreCase("Business")) {
                Bundle bundle = new Bundle();
                bundle.putInt("userId", App_pref.getAuthorizedUser(this).getData().getUserId());
                bundle.putString("from", "footer");
                        /*mContext.getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.fragment_replace, BusinessProfileFragment.newInstance(bundle), "")
                                .commit();*/

                Intent intent2 = new Intent(MenuActivity.this, ProfileDetailActivity.class);
                intent2.putExtra("clickFragmentName", "BusinessProfile");
                intent2.putExtra("fromProfileTab", true);
                intent2.putExtras(bundle);
                startActivity(intent2);
                finish();
                //mContext.mActivity.overridePendingTransition(0, 0);

            } else {
                Bundle bundle = new Bundle();
                bundle.putInt("userId", App_pref.getAuthorizedUser(this).getData().getUserId());
                bundle.putString("from", "footer");
                       /* mContext.getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                .replace(R.id.fragment_replace, IndividualProfileFragment.newInstance(bundle), "")
                                .commit();*/
                Intent intent1 = new Intent(MenuActivity.this, ProfileDetailActivity.class);
                intent1.putExtra("clickFragmentName", "IndividualProfile");
                intent1.putExtra("fromProfileTab", true);
                intent1.putExtras(bundle);
                startActivity(intent1);
                finish();
                //mContext.mActivity.overridePendingTransition(0, 0);
            }
            mBinding.uiNavigation.discovrusNews.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.newsfeed));
            mBinding.uiNavigation.discovrusProfile.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.profile_selected));
            mBinding.uiNavigation.discovrusNavigation.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.discover_navigation));
            mBinding.uiNavigation.discovrusChat.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.chat));
//            fragmentView = 4;
        });

        mBinding.uiNavigation.discovrusNews.setOnClickListener(v -> {
            fragment = PostManagementFragment.newInstance();
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .add(R.id.fragment_replace, fragment, "")
                    .commit();


            mBinding.uiNavigation.discovrusNews.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.newsfeed_selected));
            mBinding.uiNavigation.discovrusProfile.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.profile));
            mBinding.uiNavigation.discovrusNavigation.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.discover_navigation));
            mBinding.uiNavigation.discovrusChat.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.chat));
//            fragmentView = 2;
        });

        mBinding.btnAddPost.setOnClickListener(v -> {
            if (!PostManagementFragment.isDisable) {
                Log.e("button Clicked", "-----------------");
                if (fragmentView == 2) {
                    if (mBinding.uiNavigation.constraintBottomMenu.getVisibility() == View.GONE) {
                        Log.e("STATE_EXPANDED", "fragmentView=2");
                        openBottomMenu();
                        mBinding.btnAddPost.setRotation(45.0f);
                    } else {
                        Log.e("STATE_COLLAPSED", "fragmentView=2");
                        closeBottomMenu();
                        mBinding.btnAddPost.setRotation(0.0f);
                    }
                }
            } else if (PostManagementFragment.isDisable) {
                PostManagementFragment.managementFragment.trendinUi();
            }
        });
    }

    @Override
    public void onBackPressed() {

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);

      /*  if (f instanceof ImageFilterFragment) {
            getSupportFragmentManager().popBackStack();
        } else if (f instanceof VideoFilterFrgament) {
            getSupportFragmentManager().popBackStack();
        } else if (f instanceof TextPostFragment) {
            getSupportFragmentManager().popBackStack();
        } *//*else if (f instanceof TrendingNowListFragment) {
            getSupportFragmentManager().popBackStack();
        } *//* else if (mBinding.uiNavigation.constraintBottomMenu.getVisibility() == View.GONE) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            closeBottomMenu();
            mBinding.btnAddPost.setRotation(0.0f);
        }*/

        if (mBinding.uiNavigation.constraintBottomMenu.getVisibility() == View.GONE) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            closeBottomMenu();
            mBinding.btnAddPost.setRotation(0.0f);
        }
    }

    private void showDialogForVideo() {
        new AlertDialog.Builder(MenuActivity.this)
                .setTitle("Select video")
                .setMessage("Please choose video selection type")
                .setPositiveButton("CAMERA", (dialog, which) -> openVideoCapture())
                .setNegativeButton("GALLERY", (dialog, which) -> pickFromGallery())
                .show();
    }

    private void openVideoCapture() {
        Intent videoCapture = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        startActivityForResult(videoCapture, REQUEST_VIDEO_TRIMMER);
    }

    private void pickFromGallery() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermission(getString(R.string.permission_read_storage_rationale));
        } else {
            Intent intent = new Intent();
            intent.setTypeAndNormalize("video/*");
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_video)), REQUEST_VIDEO_TRIMMER);
        }
    }

    private void getPermission() {
        String[] params = null;
        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
        String camera = Manifest.permission.CAMERA;

        int hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(this, writeExternalStorage);
        int hasReadExternalStoragePermission = ActivityCompat.checkSelfPermission(this, readExternalStorage);
        int hasCameraPermission = ActivityCompat.checkSelfPermission(this, camera);
        List<String> permissions = new ArrayList<String>();

        if (hasWriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(writeExternalStorage);
        if (hasReadExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(readExternalStorage);
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(camera);

        if (!permissions.isEmpty()) {
            params = permissions.toArray(new String[permissions.size()]);
        }
        if (params != null && params.length > 0) {
            ActivityCompat.requestPermissions(MenuActivity.this,
                    params,
                    100);
        } else
            showDialogForVideo();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);
        if (f instanceof PostManagementFragment) {
            bottomMenuClick();

            bottomSheetClick();
        } else if (f instanceof FragmentNavigation) {
            bottomMenuClick();

            bottomSheetClick();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_replace, PostManagementFragment.newInstance(), "")
                    .commit();

            bottomMenuClick();

            bottomSheetClick();
        }
    }

    private void defaultViewTranslation() {
        if (mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight() == 0) {
            mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.INVISIBLE);
            ViewTreeObserver viewTreeObserverList = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getViewTreeObserver();
            viewTreeObserverList.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int width = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getMeasuredWidth();
                    int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getMeasuredHeight();
                    mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
                    translationAnimator = ObjectAnimator
                            .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                            .setDuration(0);
                    translationAnimator.start();
                }
            });
        } else {
            mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight())
                    .setDuration(0);
            translationAnimator.start();
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        Log.e("onactivityResult", "done");
        if (reqCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);

            boolean requirePermissions = false;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    isUriRequiresPermissions(imageUri)) {
                requirePermissions = true;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            }

            if (!requirePermissions) {
                originalImageUri = imageUri;
                startCropImageActivity(imageUri);
            }
        }

        if (reqCode == REQUEST_VIDEO_TRIMMER && resultCode == RESULT_OK) {
            final Uri selectedUri = data.getData();
            if (selectedUri != null) {
                videoUri = String.valueOf(selectedUri);

                try {
                    Log.e("nsData", "==>" + Utils.getGeneralSettingsData(MenuActivity.this));
                    GeneralSettingsData nsData = Utils.getGeneralSettingsData(MenuActivity.this);

                    if (nsData != null && nsData.isSuccess() && nsData.getData() != null) {
                        if (nsData.getData().get(0).isSaveVideos())
                            Utils.saveVideoToExternal(MenuActivity.this, selectedUri);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                startTrimActivity(selectedUri);
            } else {
                Toast.makeText(MenuActivity.this, R.string.toast_cannot_retrieve_selected_video, Toast.LENGTH_SHORT).show();
            }
        }

        if (reqCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                fileImage = new File(mImageUri.getPath());

                try {
                    GeneralSettingsData nsData = Utils.getGeneralSettingsData(MenuActivity.this);

                    if (nsData != null && nsData.getData() != null && nsData.isSuccess()) {
                        if (nsData.getData().get(0).isSavePhotos()) {
                            String imageUri = (String.valueOf(mImageUri)).split("file://")[1];
                            final BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(imageUri, options);

                            // Calculate inSampleSize
                            options.inSampleSize = Utils.calculateInSampleSize(options, 800, 800);

                            // Decode bitmap with inSampleSize set
                            options.inJustDecodeBounds = false;
                            Bitmap bitmap = BitmapFactory.decodeFile(imageUri, options);

                            Utils.saveImageToExternal(MenuActivity.this, String.valueOf(System.currentTimeMillis()), bitmap);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Bundle extra = data.getExtras();
                if (null != extra) {

                    closeBottomMenu();
                    mBinding.btnAddPost.setRotation(0.0f);

                    int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
                    translationAnimator = ObjectAnimator
                            .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                            .setDuration(400);
                    translationAnimator.start();
                    translationAnimator.addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);

                            Bundle bundle = new Bundle();
                            bundle.putInt("postType", postType);
                            bundle.putString("imageUri", String.valueOf(mImageUri));
                            bundle.putString("originalImageUri", String.valueOf(originalImageUri));
                            bundle.putString("videoUri", "");
                            /*getSupportFragmentManager()
                                    .beginTransaction()
                                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                                    .replace(R.id.fragment_replace, ImageFilterFragment.newInstance(bundle), "ImageFilterFragment")
//                            .addToBackStack(null)
                                    .commit();*/

                            Intent intent = new Intent(getApplicationContext(), PostFilteringActivity.class);
                            intent.putExtra("filterFragment", "imageFilter");
                            intent.putExtras(bundle);
                            startActivity(intent);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            overridePendingTransition(0, 0);


                          /*  mBinding.uiNavigation.discovrusNews.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.newsfeed_selected));
                            mBinding.uiNavigation.discovrusProfile.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.profile_layout));
                            mBinding.uiNavigation.discovrusNavigation.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.discover_navigation));
                            mBinding.uiNavigation.discovrusChat.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.chat));*/
//                    fragmentView = 2;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    });

                   /* Bundle bundle = new Bundle();
                    bundle.putInt("postType", postType);
                    bundle.putString("imageUri", String.valueOf(mImageUri));
                    bundle.putString("videoUri", "");
// set Fragmentclass Arguments
                  *//*  PostManagementFragment postManagementFragment = new PostManagementFragment();
                    postManagementFragment.setArguments(bundle);*//*
                     *//* Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);
                    if (f instanceof ImageFilterFragment) {
                        getSupportFragmentManager().popBackStack();
                    } else if (f instanceof VideoFilterFrgament) {
                        getSupportFragmentManager().popBackStack();
                    } else if (f instanceof TextPostFragment) {
                        getSupportFragmentManager().popBackStack();
                    }*//*

                    getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, ImageFilterFragment.newInstance(bundle), "ImageFilterFragment")
//                            .addToBackStack(null)
                            .commit();


                    mBinding.uiNavigation.discovrusNews.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.newsfeed_selected));
                    mBinding.uiNavigation.discovrusProfile.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.profile_layout));
                    mBinding.uiNavigation.discovrusNavigation.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.discover_navigation));
                    mBinding.uiNavigation.discovrusChat.setImageDrawable(MenuActivity.this.getResources().getDrawable(R.drawable.chat));
//                    fragmentView = 2;*/

                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // showError("Cropping failed: " + result.getError());
            }
        }
        if (reqCode == SELECT_CATEGORY && resultCode == RESULT_OK && data != null) {
            //* mBinding.uiBusiness.edtCategory.setText(data.getStringExtra(CATEGORY_NAME));
            // bussinessCategoryId = data.getIntExtra(CATEGORY_ID, 0);*//*
        }

        if (reqCode == 3000 && resultCode == RESULT_OK && data != null) {
//            closeBottomMenu();
            mBinding.btnAddPost.setRotation(0.0f);

            int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                    .setDuration(400);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
                    Bundle bundle = new Bundle();
                    bundle.putInt("postType", 4);
                    bundle.putString("imageUri", "");
                    bundle.putString("videoUri", VideoTrimActivity.VIDEO_URL);

                   /* Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);
                    if (f instanceof ImageFilterFragment) {
                        getSupportFragmentManager().popBackStack();
                    } else if (f instanceof VideoFilterFrgament) {
                        getSupportFragmentManager().popBackStack();
                    } else if (f instanceof TextPostFragment) {
                        getSupportFragmentManager().popBackStack();
                    }*/

                  /*  getSupportFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                            .replace(R.id.fragment_replace, VideoFilterFrgament.newInstance(bundle), "VideoFilterFrgament")
                            .commit();*/

                    Intent intent = new Intent(getApplicationContext(), PostFilteringActivity.class);
                    intent.putExtra("filterFragment", "videoFilter");
                    intent.putExtras(bundle);
                    startActivity(intent);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    overridePendingTransition(0, 0);


//                    fragmentView = 2;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });


        }

        if (reqCode == 510) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_replace);
            if (fragment != null) {
                fragment.onActivityResult(reqCode, resultCode, data);
            }
        }

    }

    /*
     * saves image to camera gallery
     * */
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Video.VideoColumns.DATA);
        return cursor.getString(idx);
    }

    private void startTrimActivity(@NonNull Uri uri) {
        Intent intent = new Intent(this, VideoTrimActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATH, FileUtils.getPath(this, uri));
        intent.putExtra(VIDEO_TOTAL_DURATION, getMediaDuration(uri));
        startActivityForResult(intent, 3000);
    }


    private int getMediaDuration(Uri uriOfFile) {
        MediaPlayer mp = MediaPlayer.create(this, uriOfFile);
        return mp.getDuration();
    }

    public void imgPick() {
        final CharSequence[] items = {"Capture Photo", "Choose from Gallery", "Cancel"};
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();
        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));
        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    public boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            if (stream != null) {
                stream.close();
            }
            return false;
        } catch (FileNotFoundException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (e.getCause() instanceof ErrnoException) {
                    return true;
                }
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    private void requestPermission(String rationale) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.permission_title_rationale));
            builder.setMessage(rationale);
            builder.setPositiveButton(getString(R.string.Ok), (dialog, which) -> ActivityCompat.requestPermissions(MenuActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MenuActivity.REQUEST_STORAGE_READ_ACCESS_PERMISSION));
            builder.setNegativeButton(getString(R.string.cancel), null);
            builder.show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MenuActivity.REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_STORAGE_READ_ACCESS_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickFromGallery();
                }
                break;
            case 102:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    imgPick();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void closeBottomMenu() {
        int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
        translationAnimator = ObjectAnimator
                .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, 0f, height)
                .setDuration(400);
        translationAnimator.start();
        translationAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

    }

    public void openBottomMenu() {
        mBinding.uiNavigation.constraintBottomMenu.setVisibility(View.VISIBLE);
        int height = mBinding.uiNavigation.uiBottomsheet.constBottomsheet.getHeight();
        translationAnimator = ObjectAnimator
                .ofFloat(mBinding.uiNavigation.constraintBottomMenu, View.TRANSLATION_Y, height, 0f)
                .setDuration(400);
        translationAnimator.start();
    }
/*
    public void handleClicks(View v) {
*//*        if(fragment!=null)
      //  fragment.handleClicks(v);
        else if(fragmentNavigation!=null)
        fragmentNavigation.handleClicks(v);*//*
    }*/
}
