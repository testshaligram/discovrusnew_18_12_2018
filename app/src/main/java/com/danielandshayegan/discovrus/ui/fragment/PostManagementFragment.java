package com.danielandshayegan.discovrus.ui.fragment;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.BaseAdapter;
import com.danielandshayegan.discovrus.adapter.FollowersListAdapter;
import com.danielandshayegan.discovrus.adapter.PostListAdapter;
import com.danielandshayegan.discovrus.adapter.TrendingNowAdapter;
import com.danielandshayegan.discovrus.custome_veiws.OnSwipeTouchListener;
import com.danielandshayegan.discovrus.databinding.FragmentPostManagementBinding;
import com.danielandshayegan.discovrus.eventbus.EventDataObject;
import com.danielandshayegan.discovrus.eventbus.GlobalBus;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.FollowPeopleData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.SaveLikeData;
import com.danielandshayegan.discovrus.models.SavePost;
import com.danielandshayegan.discovrus.models.SavePostViewData;
import com.danielandshayegan.discovrus.models.TrendingNowModel;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.models.VideoPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailActivity;
import com.danielandshayegan.discovrus.ui.activity.ClickPostDetailVideoActivity;
import com.danielandshayegan.discovrus.ui.activity.LaunchpadActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.ui.activity.ProfileDetailActivity;
import com.danielandshayegan.discovrus.utils.Utils;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import okhttp3.MultipartBody;

import static android.app.Activity.RESULT_OK;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveHiddenUserDetails;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deletePost;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.followPeople;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postPhoto;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.saveLike;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePostView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostManagementFragment extends BaseFragment implements PostListAdapter.OnItemClickListener, PostListAdapter.OnReloadClickListener, SingleCallback, TrendingNowAdapter.TrensingDataClicListner {

    FragmentPostManagementBinding mBinding;
    View view;
    PostListAdapter postListAdapter;
    FollowersListAdapter followersListAdapter;
    ArrayList<String> followersList = new ArrayList<>();
    private CompositeDisposable disposable = new CompositeDisposable();
    private WebserviceBuilder apiService;
    List<PostListData.PostList> postDataList = new ArrayList<>();
    int userId;
    public static PostManagementFragment managementFragment;
    private LinearLayoutManager layoutManagerPost;
    int postType;
    boolean isTrending = false;
    ImageView imagePreview;
    MultipartBody.Part body;
    TabLayout tabLayout;
    MediaPlayer mediaPlayer;
    ViewPager viewPager, viewPagerVideo;
    private ProgressDialog mProgressDialog;
    List<TrendingNowModel.DataBean> trendingDataList = new ArrayList<>();
    TrendingNowAdapter trendingNowAdapter;
    Animator translationAnimator;
    public static boolean isDisable = false;
    private int currentPage = 1, pageSize = 10;
    private boolean isLoading = true;
    private boolean isLastPage = true;
    public static final int PAGE_SIZE = 1;
    TrendingNowModel.DataBean trendingData;
    public static int firstVisiblePosition;
    int selectedPostition;
    // LaunchpadFragment fragment;
    LinearLayoutManager layoutManagerTrending;

    public PostManagementFragment() {
        // Required empty public constructor
    }

    public static PostManagementFragment newInstance() {
        return new PostManagementFragment();
    }

    public static PostManagementFragment newInstance(Bundle extras) {
        PostManagementFragment fragment = new PostManagementFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Subscribe
    public void onEvent(EventDataObject.CommentDelete event) {
      /* boolean isLike = event.isDetelete();
       if(isLike){
           postListAdapter.getItem(4).getPost().get(selectedPostition).setLiked(true);
           postListAdapter.notifyDataSetChanged();
       }*/
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_post_management, container, false);
        view = mBinding.getRoot();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        managementFragment = this;
        GlobalBus.getBus().register(managementFragment);

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
            Log.e("ask for permission", "true");
        }

        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Uploading video...");
        apiService = ApiClient.getClient(mContext).create(WebserviceBuilder.class);
        imagePreview = view.findViewById(R.id.image_preview);
        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.viewpager);
        viewPagerVideo = view.findViewById(R.id.viewpager_video);
        mediaPlayer = new MediaPlayer();
        layoutManagerPost = new LinearLayoutManager(mContext);
        layoutManagerPost.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.uiPostManagement.postsRv.setLayoutManager(layoutManagerPost);
        mBinding.uiPostManagement.postsRv.setHasFixedSize(true);

        postListAdapter = new PostListAdapter(PostManagementFragment.this, mBinding.uiPostManagement.postsRv);
        postListAdapter.setOnItemClickListener(this);
        postListAdapter.setOnReloadClickListener(this);
        mBinding.uiPostManagement.postsRv.setAdapter(postListAdapter);
        mBinding.uiPostManagement.postsRv.addOnScrollListener(recycleReload);

        /*mBinding.uiTrendingNow.trendingRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.uiTrendingNow.recyclerTrendingNow.scrollToPosition(layoutManagerTrending.findFirstCompletelyVisibleItemPosition() + 1);
                mBinding.uiTrendingNow.trendingLeft.setVisibility(VISIBLE);
                if ((layoutManagerTrending.findFirstCompletelyVisibleItemPosition() + 2) == trendingDataList.size()) {
                    mBinding.uiTrendingNow.trendingRight.setVisibility(View.GONE);
                }
            }
        });

        mBinding.uiTrendingNow.trendingLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutManagerTrending.findFirstCompletelyVisibleItemPosition() != 0) {
                    mBinding.uiTrendingNow.trendingRight.setVisibility(VISIBLE);

                    mBinding.uiTrendingNow.recyclerTrendingNow.scrollToPosition(layoutManagerTrending.findFirstCompletelyVisibleItemPosition() - 1);
                    if (layoutManagerTrending.findFirstCompletelyVisibleItemPosition() == 1) {
                        mBinding.uiTrendingNow.trendingLeft.setVisibility(View.GONE);
                    }
                }
            }
        });*/

        if (getArguments() != null) {

            postData();
        }

        if (Utils.FROM_SPLASH) {
            Intent intent = mActivity.getIntent();
            if (intent != null) {
                if (intent.getExtras().containsKey("trendingId")) {
                    String trendingId = intent.getExtras().getString("trendingId");
                    if (trendingId != null) {
                        //  Toast.makeText(getActivity(), "insidePost" + trendingId, Toast.LENGTH_SHORT).show();
                        Bundle bundle = new Bundle();
                        bundle.putInt("trendingId", Integer.parseInt(trendingId));
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_up_trending, R.anim.no_anim)
                                .replace(R.id.frame_post_management, TrendingNowDetailFragment.newInstance(bundle), "")
                                .commit();


                    }
                    intent.setData(null);
                    Utils.FROM_SPLASH = false;
                }
                if (intent.getExtras().containsKey("postId")) {
                    String postId = intent.getExtras().getString("postId");
                    String type = intent.getExtras().getString("type");
                    if (postId != null) {
                        if (type != null) {
                            if (type.equalsIgnoreCase("postVideo")) {
                                Bundle bundle = new Bundle();
                                bundle.putString("postId", postId);
                                Intent intent2 = new Intent(getActivity(), ClickPostDetailVideoActivity.class);
                                intent2.putExtra("clickFragmentName", "postVideo");
                                intent2.putExtras(bundle);
                                startActivityForResult(intent2, 510);
                                getActivity().overridePendingTransition(0, 0);
                            } else {
                                Bundle bundle = new Bundle();
                                bundle.putString("postId", postId);
                                Intent intent1 = new Intent(getActivity(), ClickPostDetailActivity.class);
                                intent1.putExtra("clickFragmentName", type);
                                intent1.putExtras(bundle);
                                startActivityForResult(intent1, 510);
                                getActivity().overridePendingTransition(0, 0);
                            }
                        }
                        //  Toast.makeText(getActivity(), "insidePost" + trendingId, Toast.LENGTH_SHORT).show();
                        /*Bundle bundle = new Bundle();
                        bundle.putInt("trendingId", Integer.parseInt(trendingId));
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.slide_in_up_trending, R.anim.no_anim)
                                .replace(R.id.frame_post_management, TrendingNowDetailFragment.newInstance(bundle), "")
                                .commit();*/


                    }
                    intent.setData(null);
                    Utils.FROM_SPLASH = false;
                }
            }
        }
        mBinding.uiTrendingNow.imgDialogClose.setOnClickListener(v -> {

            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiTrendingNow.trendingRelativeMain, View.TRANSLATION_Y, 0f, -1500f)
                    .setDuration(400);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiTrendingNow.trendingRelativeMain.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        });

        mBinding.uiPostManagement.imageView.setOnClickListener(v -> {
            getActivity().startActivity(new Intent(getActivity(), LaunchpadActivity.class));

            /*fragment = LaunchpadFragment.newInstance();
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.frame_post_management, fragment, "")
                    .commit();*/
        });

      /*  mBinding.uiPostManagement.postsRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
             *//*   int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
               *//**//* if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                        loadMoreItems();
                    }
                }*//*

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
                if (!isLoading && !isLastPage) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {
                        loadMoreItems();
                    }
                }
                if (dy > 0) {

                    if (isDisable) {
                        trendinUi();
                    }
                    if (mBinding.uiPostManagement.constraintFollowersList.getVisibility() == VISIBLE) {
                        mBinding.uiPostManagement.followersArrowIv.setImageDrawable(getResources().getDrawable(R.drawable.followers_arrow_down));
                        slideUpFollowersListLayout();
                    }
                }
            }
        });
*/
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mBinding.uiPostManagement.followersRv.setLayoutManager(linearLayoutManager1);
        followersList.add("Add new");
        followersList.add("sgSDZvsdsdg");
        followersList.add("sgszxfvdg");
        followersList.add("sxgsdg");
        followersList.add("sgsfcdg");
        followersList.add("sgvcsdg");
        followersListAdapter = new FollowersListAdapter(mContext);
        mBinding.uiPostManagement.followersRv.setAdapter(followersListAdapter);
        followersListAdapter.clear();
        followersListAdapter.addAll(followersList);

        mBinding.uiPostManagement.swipeRefreshLayout.setOnRefreshListener(() -> {
            currentPage = 1;
            mBinding.uiPostManagement.swipeRefreshLayout.setRefreshing(true);

            if (Utils.isNetworkAvailable(getActivity())) {
                mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.VISIBLE);
                mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.GONE);
                callAPI(true, "refresh");
            } else {
                mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.GONE);
                mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.VISIBLE);
                mBinding.uiPostManagement.uiErrorLayout.errorTv.setText(getResources().getString(R.string.check_your_network_connection));
            }
        });

        setClickListeners();
        if (Utils.isNetworkAvailable(getActivity())) {
            mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.VISIBLE);
            mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.GONE);
           // callAPI(false, "main");
        } else {
            mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.GONE);
            mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.VISIBLE);
            mBinding.uiPostManagement.uiErrorLayout.errorTv.setText(getResources().getString(R.string.check_your_network_connection));
        }

        mBinding.uiPostManagement.uiErrorLayout.reloadBtn.setOnClickListener(view -> {
            if (Utils.isNetworkAvailable(getActivity())) {
                mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.VISIBLE);
                mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.GONE);
                callAPI(false, "reload");
            } else {
                mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.GONE);
                mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.VISIBLE);
                mBinding.uiPostManagement.uiErrorLayout.errorTv.setText(getResources().getString(R.string.check_your_network_connection));
            }
        });

        mBinding.contentFilter.imgBack.setOnClickListener(v -> {
            mBinding.contentFilter.relativeFilter.setVisibility(View.GONE);
            mBinding.contentFilter.imagePreview.setVisibility(View.GONE);
            mBinding.contentFilter.viewpager.setVisibility(View.GONE);
            mBinding.contentFilter.edtTitle.setVisibility(View.GONE);
            mBinding.contentFilter.edtDesc.setVisibility(View.GONE);
        });


        mBinding.contentFilter.btnDone.setOnClickListener(v -> {

            switch (postType) {
                case 1:
                case 3:
                    if (validation()) {
                        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
                        disableScreen(true);
                        String description, title;
                        description = mBinding.contentFilter.edtDesc.getText().toString();
                        title = mBinding.contentFilter.edtTitle.getText().toString();
                        Log.d("textData", description + " " + title);
                        ObserverUtil
                                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                                create(WebserviceBuilder.class).
                                                uploadPostDataText(description, title, userId)
                                        , getCompositeDisposable(), postPhoto, this);
/*
                    ObserverUtil
                            .subscribeToSingle(ApiClient.getClient(getActivity()).
                                            create(WebserviceBuilder.class).
                                            uploadPostDataText("test of text", "hello text", userId)
                                    , getCompositeDisposable(), postPhoto, this);*/
                    }
                    break;

            }
        });

        layoutManagerTrending = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.uiTrendingNow.recyclerTrendingNow.setLayoutManager(layoutManagerTrending);

//        firstVisiblePosition = layoutManagerTrending.findLastVisibleItemPosition();
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        mBinding.uiTrendingNow.recyclerTrendingNow.setOnFlingListener(null);
        mBinding.uiTrendingNow.recyclerTrendingNow.clearOnScrollListeners();
        snapHelper.attachToRecyclerView(mBinding.uiTrendingNow.recyclerTrendingNow);

        trendingNowAdapter = new TrendingNowAdapter(trendingDataList, getActivity(), this);

        mBinding.uiTrendingNow.recyclerTrendingNow.setItemAnimator(new SlideInUpAnimator());
        mBinding.uiTrendingNow.recyclerTrendingNow.setAdapter(trendingNowAdapter);

        mBinding.uiTrendingNow.recyclerTrendingNow.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollHorizontally(1)) {
                    //  Toast.makeText(getActivity(),"LAst",Toast.LENGTH_LONG).show();
                    App_pref.setTrendingListSize(getActivity(), trendingDataList.size());
                    mBinding.uiPostManagement.btnTrendingNow.setBackground(getResources().getDrawable(R.drawable.shape_gray_round));
                    mBinding.uiPostManagement.btnTrendingNowTop.setBackground(getResources().getDrawable(R.drawable.shape_gray_round));

                    mBinding.uiPostManagement.btnTrendingNow.setTextColor(getResources().getColor(R.color.colorAccent));
                    mBinding.uiPostManagement.btnTrendingNowTop.setTextColor(getResources().getColor(R.color.colorAccent));

                }
            }
        });

        if (getArguments() != null && getArguments().containsKey("userId") && getArguments().containsKey("userType")) {
            Bundle bundle = new Bundle();
            bundle.putInt("userId", getArguments().getInt("userId"));

            Intent intent = new Intent(mActivity, ProfileDetailActivity.class);
            if (getArguments().getString("userType").equalsIgnoreCase("Business"))
                intent.putExtra("clickFragmentName", "BusinessProfile");
            else
                intent.putExtra("clickFragmentName", "IndividualProfile");
            intent.putExtra("fromProfileTab", false);
            intent.putExtras(bundle);
            mActivity.startActivity(intent);

        }
        return view;
    }


    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_FINE_LOCATION))
            Log.e("requestPermission: ", "permission requested");
        else
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }

    public boolean validation() {

        boolean value = true;
        if (mBinding.contentFilter.edtTitle.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter title", Toast.LENGTH_SHORT).show();
            value = false;
        }
        if (mBinding.contentFilter.edtDesc.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Please enter description", Toast.LENGTH_SHORT).show();
            value = false;
        }

        return value;

    }


    //    @SuppressLint("ClickableViewAccessibility")
    public void setClickListeners() {
        if (!isDisable) {
            mBinding.uiPostManagement.addPostIv.setOnClickListener(v -> {
                if (mBinding.uiPostManagement.constraintFollowersList.getVisibility() == View.VISIBLE) {
                    mBinding.uiPostManagement.followersArrowIv.setImageDrawable(getResources().getDrawable(R.drawable.followers_arrow_down));
                    slideUpFollowersListLayout();
                } else {
                    mBinding.uiPostManagement.followersArrowIv.setImageDrawable(getResources().getDrawable(R.drawable.followers_arrow_up));
                    slideDownFollowersListLayout();
                }
            });
        } else {
            trendinUi();
        }
        mBinding.uiPostManagement.mainConstPost.setOnClickListener(v -> {
            if (isDisable) {
                trendinUi();
            }
        });
        mBinding.uiPostManagement.btnTrendingNow.setOnClickListener(v -> {

           /* TrendingNowDialog trendingNowDialog = new TrendingNowDialog();
            trendingNowDialog.setStyle(R.style.NewDialog, R.style.AppTheme);
            trendingNowDialog.show(getActivity().getFragmentManager(), "TrendingNow");*/
            // startActivity(new Intent(getActivity(), TrendingNowListFragment.class));
           /* getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_down, R.anim.no_anim)
                    .replace(R.id.frame_post_management, TrendingNowListFragment.newInstance(), "")
                    .commit();*/

            trendigNowClick();
            // mBinding.uiPostManagement.postsRv.smoothScrollToPosition(0);


        });

        mBinding.uiTrendingNow.relativeBg.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {
            public void onSwipeTop() {
                trendigNowClick();
            }
        });

        mBinding.fabScrollUp.setOnClickListener(v -> {
            mBinding.uiPostManagement.postsRv.smoothScrollToPosition(0);
            mBinding.fabScrollUp.setVisibility(View.GONE);


        });


        mBinding.uiTrendingNow.btnTrendingClose.setOnClickListener(v -> {

            if (trendingDataList != null && trendingDataList.size() > 0) {
                trendingData = trendingDataList.get(layoutManagerTrending.findFirstCompletelyVisibleItemPosition());
                Log.e("dataList", "" + trendingData.getTrendingID());
                trendigData(trendingData);
            }
            trendigData(trendingData);

            

           /* translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiTrendingNow.trendingRelativeMain, View.TRANSLATION_Y, 0f, -1500f)
                    .setDuration(300);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiTrendingNow.trendingRelativeMain.setVisibility(View.GONE);
                    isDisable = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });*/
        });

        mBinding.uiPostManagement.btnTrendingNowTop.setOnClickListener(v -> {
           /* getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(R.anim.slide_in_down, R.anim.no_anim)
                    .replace(R.id.frame_post_management, TrendingNowListFragment.newInstance(), "")
                    .commit();*/
/*
            TrendingNowDialog trendingNowDialog = new TrendingNowDialog();
            trendingNowDialog.setStyle(R.style.NewDialog, R.style.AppTheme);
            trendingNowDialog.show(getActivity().getFragmentManager(), "TrendingNow");
*/

            //  startActivity(new Intent(getActivity(), TrendingNowListFragment.class));
            trendigNowClick();
            // mBinding.uiPostManagement.postsRv.smoothScrollToPosition(0);

        });

    }

    public void trendinUi() {
        if (mBinding.uiTrendingNow.trendingRelativeMain.getVisibility() != View.GONE) {

            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiTrendingNow.trendingRelativeMain, View.TRANSLATION_Y, 0f, -1500f)
                    .setDuration(300);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiTrendingNow.trendingRelativeMain.setVisibility(View.GONE);
                    isDisable = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }

    private void trendigNowClick() {
        if (mBinding.uiTrendingNow.trendingRelativeMain.getVisibility() != VISIBLE) {
            isDisable = true;
            /*  callAPITrendingList();*/
            mBinding.uiTrendingNow.trendingRelativeMain.setVisibility(VISIBLE);
            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiTrendingNow.trendingRelativeMain, View.TRANSLATION_Y, -1500f, 0f)
                    .setDuration(300);
            translationAnimator.start();

        } else {

            translationAnimator = ObjectAnimator
                    .ofFloat(mBinding.uiTrendingNow.trendingRelativeMain, View.TRANSLATION_Y, 0f, -1500f)
                    .setDuration(300);
            translationAnimator.start();
            translationAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mBinding.uiTrendingNow.trendingRelativeMain.setVisibility(View.GONE);
                    isDisable = false;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }

    public void callAPI(boolean fromPullToRefresh, String from) {
        try {
            if (!fromPullToRefresh)
                mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getPostData(userId, currentPage, pageSize).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<PostListData>() {
                    @Override
                    public void onSuccess(PostListData value) {

                        if (value.isSuccess() && value.getData() != null && value.getData().size() > 0) {
                            isLoading = false;
                            isLastPage = false;
                            mBinding.uiPostManagement.uiEmptyLayout.emptyLinear.setVisibility(View.GONE);
                            mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.VISIBLE);
                            if (from.equalsIgnoreCase("refresh"))
                                postDataList.clear();
                            postDataList.addAll(value.getData());

                            postListAdapter.clear();
                            postListAdapter.addAll(postDataList);
                            postListAdapter.notifyDataSetChanged();
                            try {
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            mBinding.uiPostManagement.swipeRefreshLayout.setRefreshing(false);
                        } else if (value.isSuccess()) {
                            if (currentPage == 1 && value.getData() != null && value.getData().size() == 0) {
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                mBinding.uiPostManagement.swipeRefreshLayout.setRefreshing(false);
                                mBinding.uiPostManagement.uiEmptyLayout.emptyLinear.setVisibility(View.VISIBLE);
                                mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.GONE);
                            } else if (currentPage != 1 && value.getData().size() == 0) {
                                isLastPage = true;
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            }
                        } else {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                        if (currentPage == 1 && value.getData() != null && !value.isSuccess()) {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            mBinding.uiPostManagement.swipeRefreshLayout.setRefreshing(false);
                            mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.GONE);
                            mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.VISIBLE);
                            mBinding.uiPostManagement.uiErrorLayout.errorTv.setText(value.getMessage().get(0));
                            /*Bundle bundle = new Bundle();
                            bundle.putInt("userId", 32);
                            Intent intent = new Intent(mActivity, ProfileDetailActivity.class);
                            intent.putExtra("clickFragmentName", "IndividualProfile");
                            intent.putExtras(bundle);
                            mActivity.startActivity(intent);
                            mActivity.overridePendingTransition(0, 0);*/
                        }
                        /* else if (value.isSuccess() && value.getData().size() == 0) {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            mBinding.uiPostManagement.swipeRefreshLayout.setRefreshing(false);
                            mBinding.uiPostManagement.uiEmptyLayout.emptyLinear.setVisibility(View.VISIBLE);
                            mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.GONE);
                        } else {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            mBinding.uiPostManagement.swipeRefreshLayout.setRefreshing(false);
                            mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.GONE);
                            mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.VISIBLE);
                            mBinding.uiPostManagement.uiErrorLayout.errorTv.setText(value.getMessage().get(0));
                        }*/

                        if (value.getData() != null && value.getData().size() >= PAGE_SIZE) {
                            // videoListAdapter.addFooter();
                            postListAdapter.updateFooter(BaseAdapter.FooterType.LOAD_MORE);
                        } else {
                            isLastPage = true;
                            isLoading = true;
                            postListAdapter.updateFooter(BaseAdapter.FooterType.DEFAULT);
                        }

                        if (currentPage == 1)
                            callAPITrendingList();

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());

                        isLoading = false;
                        isLastPage = false;
                        postListAdapter.updateFooter(BaseAdapter.FooterType.DEFAULT);
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        mBinding.uiPostManagement.swipeRefreshLayout.setRefreshing(false);
                        mBinding.uiPostManagement.swipeRefreshLayout.setVisibility(View.GONE);
                        mBinding.uiPostManagement.uiErrorLayout.errorLl.setVisibility(View.VISIBLE);
                        mBinding.uiPostManagement.uiErrorLayout.errorTv.setText(getResources().getString(R.string.check_your_network_connection));
                    }
                }));
    }

    @Override
    public void onItemClick(int position, View view) {

        if (isDisable) {
            trendinUi();
        }

    }

    @Override
    public void onReloadClick() {

        if (isDisable) {
            trendinUi();
        }

    }


    @Override
    public void onResume() {
        super.onResume();

        mBinding.contentFilter.mVideoSurfaceView.onResume();
        MenuActivity.fragmentView = 2;

        callAPI(true, "resume");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        GlobalBus.getBus().unregister(managementFragment);
    }

    private void postData() {
        if (getArguments() != null) {

            postType = getArguments().getInt("postType");
            isTrending = getArguments().getBoolean("isTrending");
            if (isTrending) {
                callAPITrendingList();
                mBinding.uiTrendingNow.trendingRelativeMain.setVisibility(VISIBLE);
                translationAnimator = ObjectAnimator
                        .ofFloat(mBinding.uiTrendingNow.trendingRelativeMain, View.TRANSLATION_Y, 1500f, 0f)
                        .setDuration(1200);
                translationAnimator.start();
                isDisable = true;
            } else {
                mBinding.uiTrendingNow.trendingRelativeMain.setVisibility(View.GONE);
            }


            switch (postType) {
                case 3:
                    mBinding.contentFilter.relativeFilter.setVisibility(View.VISIBLE);
                    mBinding.contentFilter.edtTitle.setVisibility(View.VISIBLE);
                    mBinding.contentFilter.edtDesc.setVisibility(View.VISIBLE);
                    mBinding.contentFilter.imagePreview.setVisibility(View.GONE);
                    mBinding.contentFilter.mVideoSurfaceView.setVisibility(View.GONE);
                    viewPagerVideo.setVisibility(View.GONE);
                    viewPager.setVisibility(View.GONE);
                    mBinding.contentFilter.viewpager.setVisibility(View.GONE);

                    break;


            }
            //  Bitmap bitmap = BitmapUtils.getBitmapFromGallery(this, data.getData(), 800, 800);


        }
    }


    public void saveLikePost(int postId, boolean isActive, int postUserId) {

        if (isDisable) {
            trendinUi();
        } else {
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    saveLike(postId, postUserId, isActive, userId)
                            , getCompositeDisposable(), saveLike, this);
        }

    }

    public void followPeople(int followUserId) {
        if (isDisable) {
            trendinUi();
        } else {
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    followPeople(userId, followUserId)
                            , getCompositeDisposable(), followPeople, this);
        }
    }

    public void SaveHiddenUser(int hideUserId) {
        if (isDisable) {
            trendinUi();
        } else {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    SaveHiddenUserDetails(hideUserId, userId)
                            , getCompositeDisposable(), SaveHiddenUserDetails, this);
        }
    }

    public void savePostView(int postId) {
        if (isDisable) {
            trendinUi();
        } else {
            ObserverUtil
                    .subscribeToSingle(ApiClient.getClient(getActivity()).
                                    create(WebserviceBuilder.class).
                                    savePostView(postId, userId)
                            , getCompositeDisposable(), savePostView, this);
        }
    }

    public void getPostImageDetails(PostListData.Post postListData) {

        /* Utils.selectedPosition = position;*/
        Bundle bundle = new Bundle();
        bundle.putParcelable("postDetails", postListData);
      /*  getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_post_management, ClickPostImageFragment.newInstance(bundle), "")
                .commit();*/
        Intent intent = new Intent(getActivity(), ClickPostDetailActivity.class);
        intent.putExtra("clickFragmentName", "postImage");
        intent.putExtra("postDetails", (Parcelable) postListData);
        startActivityForResult(intent, 510);
        mActivity.overridePendingTransition(0, 0);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("onActivityResult", "From News Feed" + requestCode);
        if (requestCode == 510 && resultCode == RESULT_OK) {
            boolean isRefresh = data.getBooleanExtra("isRefresh", false);
            if (isRefresh) {
                currentPage = 1;
                postListAdapter.clear();
                callAPI(false, "refresh");
            }

        }
    }

    public void getPostVideoDetails(PostListData.Post postListData) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("postDetails", postListData);
       /* getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_post_management, ClickPostVideoFragment.newInstance(bundle), "")
                .commit();*/

        Intent intent = new Intent(getActivity(), ClickPostDetailVideoActivity.class);
        intent.putExtra("clickFragmentName", "postVideo");
        intent.putExtras(bundle);
        startActivityForResult(intent, 510);
        mActivity.overridePendingTransition(0, 0);

    }

    public void getPostTextDetails(PostListData.Post postListData, int position) {

        Bundle bundle = new Bundle();
        bundle.putParcelable("postDetails", postListData);
       /* getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_post_management, ClickPostTextFragment.newInstance(bundle), "")
                .commit();*/

        Intent intent = new Intent(getActivity(), ClickPostDetailActivity.class);
        intent.putExtra("clickFragmentName", "postText");
        intent.putExtra("postDetails", (Parcelable) postListData);
        startActivityForResult(intent, 510);
        mActivity.overridePendingTransition(0, 0);

    }

    public void deletePost(PostListData.Post post) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                deletePost(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), post.getPostId())
                        , getCompositeDisposable(), deletePost, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                SavePost savePostData = (SavePost) o;
                                Log.e("onSingleSuccess: ", "Success");
                                Toast.makeText(getActivity(), savePostData.getMessage().get(0), Toast.LENGTH_LONG).show();
                                currentPage = 1;
                                postListAdapter.clear();
                                callAPI(false, "refresh");
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");

                            }
                        });
    }

    public void callAPITrendingList() {

        disposable.add(apiService.getTrendingList().
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<TrendingNowModel>() {
                    @Override
                    public void onSuccess(TrendingNowModel value) {
                        mBinding.uiTrendingNow.trendingLeft.setVisibility(View.GONE);
                        mBinding.uiTrendingNow.uiLoading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        if (value.isSuccess() && value.getData().size() > 0) {
                            trendingDataList.clear();
                            trendingDataList.addAll(value.getData());
                            int trendingListPref = App_pref.getTrendingListSize(getActivity());
                            if (trendingDataList.size() == trendingListPref) {

                                mBinding.uiPostManagement.btnTrendingNow.setBackground(getResources().getDrawable(R.drawable.shape_gray_round));
                                mBinding.uiPostManagement.btnTrendingNowTop.setBackground(getResources().getDrawable(R.drawable.shape_gray_round));
                                mBinding.uiPostManagement.btnTrendingNowTop.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                                mBinding.uiPostManagement.btnTrendingNow.setTextColor(mActivity.getResources().getColor(R.color.colorAccent));
                            }
                            //  trendingNowAdapter = new TrendingNowAdapter(trendingDataList, getActivity(), getDI);
                            trendingNowAdapter.notifyDataSetChanged();

                        } else {
                            Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.uiTrendingNow.uiLoading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case saveLike:
                SaveLikeData saveLikeData = (SaveLikeData) o;
                //                    callAPI(false);
                if (saveLikeData.isSuccess()) Log.d("saveLikeData", "saveLikeData");
                else
                    Toast.makeText(getActivity(), saveLikeData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                break;
            case followPeople:
                FollowPeopleData followPeopleData = (FollowPeopleData) o;
                //                    mBinding.loading.progressBar.setVisibility(View.GONE);
                //                    disableScreen(false);
                //                    callAPI(false);
                if (followPeopleData.isSuccess()) Log.d("followPeopleData", "followPeopleData");
                else
                    Toast.makeText(getActivity(), followPeopleData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                break;
            case savePostView:
                SavePostViewData savePostViewData = (SavePostViewData) o;
                //                    Toast.makeText(getActivity(), savePostViewData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                if (savePostViewData.isSuccess()) Log.d("savePostViewData", "savePostViewData");
                else
                    Toast.makeText(getActivity(), savePostViewData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                break;
            case postPhoto:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                body = null;
/*
                mBinding.contentFilter.edtDesc.clearComposingText();
                mBinding.contentFilter.edtTitle.clearComposingText();
*/
                UploadPostData uploadPostData = (UploadPostData) o;
                if (uploadPostData.isSuccess()) {
                    Toast.makeText(getActivity(), uploadPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.contentFilter.relativeFilter.setVisibility(View.GONE);
                    mBinding.contentFilter.imagePreview.setVisibility(View.GONE);
                    mBinding.contentFilter.viewpager.setVisibility(View.GONE);
                    mBinding.contentFilter.edtTitle.setVisibility(View.GONE);
                    mBinding.contentFilter.edtDesc.setVisibility(View.GONE);
                    callAPI(false, "post");
                } else {
                    Toast.makeText(getActivity(), uploadPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;
            case postVideo:
                mProgressDialog.dismiss();
                disableScreen(false);
                VideoPostData videoPostData = (VideoPostData) o;
                if (videoPostData.isSuccess()) {

                    mBinding.contentFilter.relativeFilter.setVisibility(View.GONE);
                    mBinding.contentFilter.imagePreview.setVisibility(View.GONE);
                    mBinding.contentFilter.viewpager.setVisibility(View.GONE);
                    mBinding.contentFilter.edtTitle.setVisibility(View.GONE);
                    mBinding.contentFilter.edtDesc.setVisibility(View.GONE);
                    Log.i("videoResponse", "" + videoPostData);

                    Toast.makeText(getActivity(), videoPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(getActivity(), videoPostData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
            case SaveHiddenUserDetails:
                CommonApiResponse hiddenUserDetails = (CommonApiResponse) o;
                if (hiddenUserDetails.isSuccess()) {
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    currentPage = 1;
                    callAPI(false, "refresh");
                } else {
                    Toast.makeText(getActivity(), hiddenUserDetails.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
        if (throwable.getMessage().equals("connect timed out")) {
            Toast.makeText(getActivity(), "Connection timeout, Please try again after some time", Toast.LENGTH_SHORT).show();
        }
    }

    private void slideUpFollowersListLayout() {
        int height = mBinding.uiPostManagement.constraintFollowersList.getHeight() + mBinding.uiPostManagement.followersView.getHeight() + dpToPx(10);
        mBinding.uiPostManagement.constraintFollowersList.animate()
                .translationY(-height)
                .alpha(0.0f)
                .setDuration(1000)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        mBinding.uiPostManagement.constraintFollowersList.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        mBinding.uiPostManagement.constraintPostList.animate()
                                .translationY(-(mBinding.uiPostManagement.followersView.getHeight() + mBinding.uiPostManagement.followersView.getHeight() + dpToPx(10)))
                                .setDuration(1000)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        super.onAnimationStart(animation);
                                        new Handler().postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                mBinding.uiPostManagement.btnTrendingNow.setVisibility(View.GONE);
                                                mBinding.uiPostManagement.btnTrendingNowTop.setVisibility(View.VISIBLE);
                                            }
                                        }, 950);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                    }
                                });
                    }
                });

    }

    private void slideDownFollowersListLayout() {
        int height = mBinding.uiPostManagement.constraintFollowersList.getHeight() + mBinding.uiPostManagement.followersView.getHeight() + dpToPx(10);
        if (mBinding.uiPostManagement.constraintFollowersList.getHeight() == 0) {
            mBinding.uiPostManagement.constraintFollowersList.setVisibility(View.INVISIBLE);
            ViewTreeObserver viewTreeObserverList = mBinding.uiPostManagement.constraintFollowersList.getViewTreeObserver();
            viewTreeObserverList.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mBinding.uiPostManagement.followersRv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int width = mBinding.uiPostManagement.followersRv.getMeasuredWidth();
                    int listHeight = mBinding.uiPostManagement.followersRv.getMeasuredHeight();
                    int heightList = mBinding.uiPostManagement.constraintFollowersList.getMeasuredHeight() + mBinding.uiPostManagement.followersView.getHeight() + dpToPx(10);
                    Log.e("TAG", "constraintFollowersList.getHeight()*********** =====>" + heightList);
                    mBinding.uiPostManagement.constraintFollowersList.animate().translationY(-heightList);
                    mBinding.uiPostManagement.constraintFollowersList.setVisibility(View.GONE);
                    mBinding.uiPostManagement.constraintPostList.animate()
                            .translationY(heightList)
                            .alpha(1.0f)
                            .setDuration(1000)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    super.onAnimationStart(animation);
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            mBinding.uiPostManagement.btnTrendingNow.setVisibility(View.VISIBLE);
                                            mBinding.uiPostManagement.btnTrendingNowTop.setVisibility(View.GONE);
                                        }
                                    }, 200);
                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    mBinding.uiPostManagement.constraintFollowersList.setVisibility(View.VISIBLE);
                                    mBinding.uiPostManagement.constraintFollowersList.animate()
                                            .translationY(0)
                                            .alpha(1.0f)
                                            .setDuration(1000)
                                            .setListener(new AnimatorListenerAdapter() {
                                                @Override
                                                public void onAnimationEnd(Animator animation) {
                                                    super.onAnimationEnd(animation);

                                                }

                                                @Override
                                                public void onAnimationStart(Animator animation) {
                                                    super.onAnimationStart(animation);

                                                }
                                            });
                                }
                            });
                }
            });
        } else {
            mBinding.uiPostManagement.constraintPostList.animate()
                    .translationY(height)
                    .setDuration(1000)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            new Handler().postDelayed(() -> {
                                mBinding.uiPostManagement.btnTrendingNow.setVisibility(View.VISIBLE);
                                mBinding.uiPostManagement.btnTrendingNowTop.setVisibility(View.GONE);
                            }, 200);
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mBinding.uiPostManagement.constraintFollowersList.setVisibility(View.VISIBLE);
                            mBinding.uiPostManagement.constraintFollowersList.animate()
                                    .translationY(0)
                                    .alpha(1.0f)
                                    .setDuration(1000)
                                    .setListener(new AnimatorListenerAdapter() {
                                        @Override
                                        public void onAnimationEnd(Animator animation) {
                                            super.onAnimationEnd(animation);
                                        }

                                        @Override
                                        public void onAnimationStart(Animator animation) {
                                            super.onAnimationStart(animation);

                                        }
                                    });
                        }
                    });
        }

    }


    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public void onItemSelected(TrendingNowModel.DataBean trendingNowModel) {

        trendigData(trendingNowModel);

    }

    private void trendigData(TrendingNowModel.DataBean trendingNowModel) {

        translationAnimator = ObjectAnimator
                .ofFloat(mBinding.uiTrendingNow.trendingRelativeMain, View.TRANSLATION_Y, 0f, 1500f)
                .setDuration(200);
        translationAnimator.start();
        translationAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }


            @Override
            public void onAnimationEnd(Animator animation) {
                mBinding.uiTrendingNow.trendingRelativeMain.setVisibility(View.GONE);
                isDisable = false;
                Bundle bundle = new Bundle();
                // bundle.putParcelable("trendingDataModel", trendingNowModel);
                bundle.putInt("trendingId", trendingNowModel.getTrendingID());
                // startActivity(intent, bundle);

                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_up_trending, R.anim.no_anim)
                        .replace(R.id.frame_post_management, TrendingNowDetailFragment.newInstance(bundle), "")
                        .commit();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


    }

    private RecyclerView.OnScrollListener recycleReload = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == 0) {
                postListAdapter.setScrolled(layoutManagerPost.findFirstVisibleItemPosition());
            }
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManagerPost.getChildCount();
            int totalItemCount = layoutManagerPost.getItemCount();
            int firstVisibleItemPosition = layoutManagerPost.findFirstVisibleItemPosition();
            if (!isLoading && !isLastPage) {

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    mBinding.fabScrollUp.setVisibility(VISIBLE);
                    loadMoreItems();
                }
            }
            if (dy > 0) {

                if (isDisable) {
                    trendinUi();
                }
                if (mBinding.uiPostManagement.constraintFollowersList.getVisibility() == VISIBLE) {
                    mBinding.uiPostManagement.followersArrowIv.setImageDrawable(getResources().getDrawable(R.drawable.followers_arrow_down));
                    slideUpFollowersListLayout();
                }
            }
        }
    };

    private void loadMoreItems() {
        isLoading = true;
        currentPage += 1;
        callAPI(false, "loadmore");
    }

    @Override
    public void onStop() {
        super.onStop();
        postListAdapter.stopVideo(layoutManagerPost.findFirstVisibleItemPosition());
    }

    /*public void handleClicks(View v)
    {
        fragment.handleClicks(v);
    }*/
}


