package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.ProfilePostGridAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentBusinessProfileListBinding;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.UserProfileData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessProfilePostsFragment extends BaseFragment {

    FragmentBusinessProfileListBinding mBinding;
    View view;
    private WebserviceBuilder apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    int userId, logInUserId;
    public static BusinessProfilePostsFragment managementFragment;
    private GridLayoutManager layoutManager;
    private int currentPage = 1, pageSize = 5;
    private boolean isLoading = true;
    private boolean isLastPage = true;
    public static final int PAGE_SIZE = 1;
    ProfilePostGridAdapter postGridAdapter;
    List<PostListData.Post> postDataList = new ArrayList<>();

    public BusinessProfilePostsFragment() {
        // Required empty public constructor
    }

    public static BusinessProfilePostsFragment newInstance() {
        return new BusinessProfilePostsFragment();
    }

    public static BusinessProfilePostsFragment newInstance(Bundle extras) {
        BusinessProfilePostsFragment fragment = new BusinessProfilePostsFragment();
        fragment.setArguments(extras);
        return fragment;
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_business_profile_list, container, false);
        view = mBinding.getRoot();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        userId = BusinessProfileFragment.businessProfileFragment.userId;
        logInUserId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        managementFragment = this;

        layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mBinding.recyclerBusinessProfile.setLayoutManager(layoutManager);

        postGridAdapter = new ProfilePostGridAdapter(getActivity());

        mBinding.recyclerBusinessProfile.setItemAnimator(new SlideInUpAnimator());
        mBinding.recyclerBusinessProfile.setAdapter(postGridAdapter);
        mBinding.recyclerBusinessProfile.addOnScrollListener(recycleReload);

        currentPage = 1;
        callAPI();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void callAPI() {
        Log.e("Api call", "----" + currentPage);
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
//            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getProfile(userId, logInUserId, currentPage, pageSize).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<UserProfileData>() {
                    @Override
                    public void onSuccess(UserProfileData value) {
                        if (value.isSuccess()) {
                            isLoading = false;
                            UserProfileData.DataBean data = value.getData().get(0);
                            BusinessProfileFragment.businessProfileFragment.setData(data);


                            postDataList.clear();
                            postDataList.addAll(data.getPost());
                            if (postDataList.size() > 0)
                                isLastPage = false;
                            else
                                isLastPage = true;
                            if (currentPage == 1)
                                postGridAdapter.clear();
                            postGridAdapter.addAll(postDataList);
                            postGridAdapter.notifyDataSetChanged();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                        } else {
                            Toast.makeText(mActivity, value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
//                            disableScreen(false);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
//                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }
                }));
    }

    private RecyclerView.OnScrollListener recycleReload = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();
            if (!isLoading && !isLastPage) {

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems();
                }
            }
        }
    };

    private void loadMoreItems() {
        isLoading = true;
        currentPage += 1;
        callAPI();
    }

}


