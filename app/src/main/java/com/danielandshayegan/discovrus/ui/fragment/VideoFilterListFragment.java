package com.danielandshayegan.discovrus.ui.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.custome_veiws.SpacesItemDecoration;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.VideoFilterAdapter;
import com.danielandshayegan.discovrus.custome_veiws.videotrimming.VideoFilterNameModel;
import com.danielandshayegan.discovrus.databinding.FragmentVideoFilterBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFilterListFragment extends Fragment implements VideoFilterAdapter.VideoFilterListener {


    List<VideoFilterNameModel> videoFilterNameModelList;

    FilterVideoList listener;

    FragmentVideoFilterBinding mBinding;
    View view;

    VideoFilterAdapter mAdapter;
    String filterName;

    public VideoFilterListFragment() {
        // Required empty public constructor
    }

    public void setListener(FilterVideoList listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_video_filter, container, false);
        view = mBinding.getRoot();
        videoFilterNameModelList = new ArrayList<>();
        VideoFilterNameModel videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Normal");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Black & White");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Contrast");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Sepia effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Invert color effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Vignette effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Hue effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Brightness effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Gamma effect");
        videoFilterNameModelList.add(videoFilterNameModel);
        videoFilterNameModel = new VideoFilterNameModel();
        videoFilterNameModel.setFilterName("Posterized effect");
        videoFilterNameModelList.add(videoFilterNameModel);

        mAdapter = new VideoFilterAdapter(videoFilterNameModelList, this, getActivity());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.recyclerView.setLayoutManager(mLayoutManager);
        mBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        int space = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8,
                getResources().getDisplayMetrics());
        mBinding.recyclerView.addItemDecoration(new SpacesItemDecoration(space));
        mBinding.recyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onFilterSelected(VideoFilterNameModel videoFilterNameModel) {
        filterName = videoFilterNameModel.getFilterName();
        listener.onFilterSelected(filterName);
    }

    public interface FilterVideoList {
        void onFilterSelected(String videoFilterName);
    }
}
