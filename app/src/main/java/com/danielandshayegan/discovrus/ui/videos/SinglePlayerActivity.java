/*
 * Copyright (c) 2017 Nam Nguyen, nam@ene.im
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.danielandshayegan.discovrus.ui.videos;

import android.app.Activity;
import android.app.Fragment;
import android.app.SharedElementCallback;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.ActivitySinglePlayerLandscapeBinding;
import com.google.android.exoplayer2.ui.PlaybackControlView;
import com.google.android.exoplayer2.ui.PlayerView;

import java.util.List;
import java.util.Map;

import im.ene.toro.exoplayer.Playable;
import im.ene.toro.media.PlaybackInfo;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE;

public class SinglePlayerActivity extends FragmentActivity {

    // TODO replace by one parcelable Object
    static final String EXTRA_MEDIA_URI = "toro:demo:custom:player:uri";  // Uri
    static final String EXTRA_MEDIA_ORDER = "toro:demo:custom:player:order";  // int
    static final String EXTRA_MEDIA_DESCRIPTION = "toro:demo:custom:player:description"; // String
    static final String EXTRA_MEDIA_PLAYBACK_INFO = "toro:demo:custom:player:info"; // PlaybackInfo
    static final String EXTRA_MEDIA_PLAYER_SIZE = "toro:demo:custom:player:player_size";  // Point
    static final String EXTRA_MEDIA_VIDEO_SIZE = "toro:demo:custom:player:video_size";  // Point
    static final String EXTRA_DEFAULT_FULLSCREEN = "toro:demo:custom:player:fullscreen";  // boolean

    static final String STATE_MEDIA_PLAYBACK_INFO = "todo:demo:player:state:playback";

    public static final String RESULT_EXTRA_PLAYER_ORDER = "toro:demo:player:result:order";
    public static final String RESULT_EXTRA_PLAYBACK_INFO = "toro:demo:player:result:playback";
    private ActivitySinglePlayerLandscapeBinding binding;
    private View btnFullscreen;
    private View btnVolume;

    private int order;
    private Uri mediaUri;
    private PlaybackInfo playbackInfo;
    Point playerSize;
    Point videoSize;
    private boolean MUTE = false;
    private float VOLUME = 0;
    // ONLY start with fullscreen on landscape mode or not.
    // If true: this Activity starts in landscape mode, no changeable.
    // If false: this Activity starts in current screen mode, changeable by user (eg: rotate device).
    private boolean fullscreen;
    protected int adapterPosition;
    private Playable playerHelper;

    PlayerView playerView;

    @Override
    protected void onCreate(@Nullable Bundle state) {
        super.onCreate(state);
        // Only request transition if this Activity start from zero (not re-created).
        if (state == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            order = extras.getInt(EXTRA_MEDIA_ORDER);
            mediaUri = extras.getParcelable(EXTRA_MEDIA_URI);
            playbackInfo = extras.getParcelable(EXTRA_MEDIA_PLAYBACK_INFO);
            playerSize = extras.getParcelable(EXTRA_MEDIA_PLAYER_SIZE);
            videoSize = extras.getParcelable(EXTRA_MEDIA_VIDEO_SIZE);
            fullscreen = extras.getBoolean(EXTRA_DEFAULT_FULLSCREEN, false);
        }

        adapterPosition = order;

        if (fullscreen) {
            setRequestedOrientation(SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_single_player_landscape);
        playerView = findViewById(R.id.player_view);

        btnVolume = findViewById(R.id.btnVolume);
        btnVolume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                if (!MUTE) {
                    //mute
                    if (VOLUME == 0) {
                        //get the volume before set to mute
                        VOLUME = playerHelper.getVolume();
                    }
                    MUTE = true;
                    playerHelper.setVolume(0);
                } else {
                    //unmute
                    playerHelper.setVolume(VOLUME);
                    MUTE = false;
                }
            }
        });


        btnFullscreen = findViewById(R.id.btnFullScreen);
        btnFullscreen.setSelected(fullscreen);
        btnFullscreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setSelected(!v.isSelected());
                if (v.isSelected()) {
                    fullscreen = true;
                    setRequestedOrientation(SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
                } else {
                    fullscreen = false;
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                }
                controllerVisible(View.VISIBLE);
            }
        });

        if (state == null) {
            ViewCompat.setTransitionName(playerView, getString(R.string.transition_feed_video));
        } else {
            ViewCompat.setTransitionName(playerView, getString(R.string.transition_feed_video));
        }

        if (state != null && state.containsKey(STATE_MEDIA_PLAYBACK_INFO)) {
            playbackInfo = state.getParcelable(STATE_MEDIA_PLAYBACK_INFO);
        }

        playerHelper = ExoUtils.createExo(this).createPlayable(mediaUri);
        onScreenChange();

        supportPostponeEnterTransition();
        playerView.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        playerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        ActivityCompat.startPostponedEnterTransition(SinglePlayerActivity.this);
                    }
                });

        playerHelper.prepare(true);
        if (playbackInfo != null) playerHelper.setPlaybackInfo(playbackInfo);
        /*findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

        playerView.setControllerVisibilityListener(new PlaybackControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                controllerVisible(visibility);
            }
        });

        controllerVisible(View.GONE);
        VOLUME = playerHelper.getVolume();
    }

    private void controllerVisible(int visibility) {
        View parent = (View) btnFullscreen.getParent();
    }

    private void onScreenChange() {

//        Point windowSize = new Point();
//        getWindow().getWindowManager().getDefaultDisplay().getSize(windowSize);
//        boolean landscape = windowSize.y < windowSize.x;
//
//        // Optimize player view UI and resize mode.
//        Point size = usableSize(videoSize) ? videoSize : (usableSize(playerSize) ? playerSize : null);
//        if (size != null) {
//            if (landscape) {
//                // Landscape mode, we use resize_mode for Player view content.
//                int resizeMode = size.x * windowSize.y > windowSize.x * size.y ?  //
//                        RESIZE_MODE_FIXED_WIDTH : RESIZE_MODE_FIXED_HEIGHT;
//                playerView.setResizeMode(resizeMode);
//            } else {
//                float ratio = size.y / (float) size.x;
//                size.x = Math.max(size.x, windowSize.x);  // max width
//                size.y = (int) (size.x * ratio);
//                playerView.setMinimumHeight(size.y);
//                if (playerView.getLayoutParams() != null) {
//                    playerView.getLayoutParams().height = size.y;
//                }
//            }
//        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        playerHelper.setPlayerView(playerView);
        if (!playerHelper.isPlaying()) {
            playerHelper.play();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        playerHelper.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playerHelper.setPlayerView(null);
        playerHelper.release();
        playerHelper = null;
    }

    @Override
    public void onBackPressed() {
        setRequestedOrientation(SCREEN_ORIENTATION_PORTRAIT);
        super.onBackPressed();
        playerHelper.setPlayerView(null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (playerHelper != null) {
            PlaybackInfo info = playerHelper.getPlaybackInfo();
            outState.putParcelable(STATE_MEDIA_PLAYBACK_INFO, info);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
            fullscreen = false;
        else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
            fullscreen = true;

        onScreenChange();
        controllerVisible(View.VISIBLE);
    }


    @Override
    public void finish() {
        if (playerHelper != null) {
            Intent intent = new Intent();
            intent.putExtra(RESULT_EXTRA_PLAYER_ORDER, order);
            intent.putExtra(RESULT_EXTRA_PLAYBACK_INFO, playerHelper.getPlaybackInfo());
            setResult(Activity.RESULT_OK, intent);
        }
        super.finish();
    }

//    public void finishAfterTransition() {
//        Intent intent = new Intent();
//        intent.putExtra("exit_position", current);
//        setResult(RESULT_OK, intent);
//        setSharedElementCallback(playerView);
//        super.finishAfterTransition();
//    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setSharedElementCallback(final View view) {
        setEnterSharedElementCallback(new SharedElementCallback() {
            @Override
            public void onMapSharedElements(List<String> names, Map<String, View> sharedElements) {
                names.clear();
                sharedElements.clear();
                names.add(ViewCompat.getTransitionName(view));
                sharedElements.put(ViewCompat.getTransitionName(view), view);
            }
        });
    }

    static boolean usableSize(Point size) {
        return size != null && size.x > 0 && size.y > 0;
    }
}
