package com.danielandshayegan.discovrus.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.adapter.BaseAdapter;
import com.danielandshayegan.discovrus.adapter.CardListAdapter;
import com.danielandshayegan.discovrus.databinding.FragmentDetailPostPaymentBinding;
import com.danielandshayegan.discovrus.dialog.StripeDialog;
import com.danielandshayegan.discovrus.models.AddCardData;
import com.danielandshayegan.discovrus.models.CardListEnvelope;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.DetailPostPhotoData;
import com.danielandshayegan.discovrus.models.DetailPostVideoData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.BaseFragment;
import com.danielandshayegan.discovrus.ui.activity.DetailPostPaymentActivity;
import com.danielandshayegan.discovrus.ui.activity.MenuActivity;
import com.danielandshayegan.discovrus.ui.activity.PaymentSuccessActivity;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static android.view.View.VISIBLE;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.addCustomerCard;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.makePayment;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postDetailPhoto;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postDetailText;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postDetailVideo;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.postPhoto;
import static com.danielandshayegan.discovrus.ui.activity.CommonIntentActivity.FRAGMENT_MAIN_MENU;
import static com.danielandshayegan.discovrus.ui.activity.MainActivity.ACTIVITY_INTENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPostPaymentFragment extends BaseFragment implements SingleCallback {

    FragmentDetailPostPaymentBinding mBinding;
    View view;
    int userId;
    String postType;
    public Uri mImageUri;
    Bitmap bitmap;
    MultipartBody.Part body;
    private String tokenId = "";
    private boolean isSave;

    Activity activityMain;
    private WebserviceBuilder apiService;
    private CompositeDisposable disposable = new CompositeDisposable();
    List<CardListEnvelope.DataBean> cardDatas = new ArrayList<>();
    CardListAdapter cardAdapter;
    int cardSelectedPosition;
    String customerStripeId, cardId;


    public DetailPostPaymentFragment() {
        // Required empty public constructor
    }

    public static DetailPostPaymentFragment newInstance() {
        return new DetailPostPaymentFragment();
    }

    public static DetailPostPaymentFragment newInstance(Bundle extras) {
        DetailPostPaymentFragment fragment = new DetailPostPaymentFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_detail_post_payment, container, false);
        view = mBinding.getRoot();
        activityMain = this.getActivity();
        userId = App_pref.getAuthorizedUser(getActivity()).getData().getUserId();
        apiService = ApiClient.getClient(getActivity()).create(WebserviceBuilder.class);
        postType = getArguments().getString("Type", "");
        getCard();
        setClicks();

        return view;
    }

    public void setClicks() {
        mBinding.uiPaymentDetailPosting.imgBack.setOnClickListener(v -> {
            DetailPostPaymentActivity.paymentActivity.onBackPressed();
        });

        mBinding.uiPaymentDetailPosting.btnPay.setOnClickListener(v -> {
            CardListEnvelope.DataBean data = cardDatas.get(cardSelectedPosition);
            makePaymentApiCall(data.getCustomerId(), data.getId(), getArguments().getDouble("amount"), false);
        });

        mBinding.uiPaymentDetailPosting.btnAddNewCard.setOnClickListener(v -> {
            StripeDialog stripeDialog = new StripeDialog();
            stripeDialog.setTargetFragment(this, 500);
            stripeDialog.show(getFragmentManager(), "Add New Card");
        });

    }

    public void getCard() {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.getCardList(userId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<CardListEnvelope>() {
                    @Override
                    public void onSuccess(CardListEnvelope value) {
                        if (value.isSuccess()) {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            cardDatas = value.getData();
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            mBinding.uiPaymentDetailPosting.cardListRecycler.setLayoutManager(linearLayoutManager);
                            cardAdapter = new CardListAdapter(getActivity(), cardDatas, cardSelectedPosition, listener);
                            mBinding.uiPaymentDetailPosting.cardListRecycler.setAdapter(cardAdapter);
                            cardAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));

    }

    public void deleteCard(int position, String customerId, String cardId, boolean isRemove) {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        disposable.add(apiService.deleteCard(customerId, cardId).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<CardListEnvelope>() {
                    @Override
                    public void onSuccess(CardListEnvelope value) {
                        if (value.isSuccess()) {
                            if (isRemove) {
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                                cardDatas.remove(position);
                                cardAdapter.notifyDataSetChanged();
                            } else {
                                callApiSaveData();
                            }
                        } else {
                            Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));

    }

    private BaseAdapter.OnItemClickListener listener = new BaseAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(int position, View view) {
            if (view.getId() == R.id.img_delete) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Delete Alert")
                        .setMessage("Are you sure you want to delete this card?")
                        .setPositiveButton(R.string.Ok, (dialog, whichButton) -> {
                            dialog.dismiss();
                            deleteCard(position, cardDatas.get(position).getCustomerId(), cardDatas.get(position).getId(), true);
                        })
                        .setNegativeButton(R.string.cancel, (dialogInterface, i) -> {
                            dialogInterface.dismiss();
                        })
                        .show();
            } else {
                cardSelectedPosition = position;
                cardAdapter.setPosition(position);
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 500 && resultCode == RESULT_OK) {
            tokenId = data.getStringExtra("tokenId");
            isSave = data.getBooleanExtra("isSave", false);
            addCard();
        }
    }


    private void addCard() {
        mBinding.loading.progressBar.setVisibility(VISIBLE);
        disableScreen(true);
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(activityMain).
                                create(WebserviceBuilder.class).
                                addCustomerCard(tokenId, userId)
                        , getCompositeDisposable(), addCustomerCard, this);
    }

    public void makePaymentApiCall(String customerStripeId, String cardId, Double amount, boolean isFromAddCard) {
        try {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String description = App_pref.getAuthorizedUser(activityMain).getData().getFirstName() + " " + App_pref.getAuthorizedUser(activityMain).getData().getLastName() +
                " (" + App_pref.getAuthorizedUser(activityMain).getData().getEmail() + ") " + getArguments().getString("distance") + ", " + getArguments().getString("time");
        disposable.add(apiService.makePayment(customerStripeId, cardId, new DecimalFormat("0.00").format(amount), description).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribeWith(new DisposableSingleObserver<CommonApiResponse>() {
                    @Override
                    public void onSuccess(CommonApiResponse value) {
                        if (value.isSuccess()) {
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            if (isFromAddCard) {
                                if (!isSave)
                                    deleteCard(0, customerStripeId, cardId, false);
                                else
                                    callApiSaveData();
                            } else {
                                callApiSaveData();
                            }
                        } else {
                            Toast.makeText(getActivity(), value.getMessage().get(0), Toast.LENGTH_LONG).show();
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("fail", e.toString());
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        disableScreen(false);
                        Toast.makeText(mContext, getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                    }

                }));

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {

        switch (apiNames) {
            case addCustomerCard:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);
                AddCardData addCardData = (AddCardData) o;
                if (addCardData.isSuccess()) {
                    makePaymentApiCall(addCardData.getData().getCustomerStripeId(), addCardData.getData().getCardID(), getArguments().getDouble("amount"), true);
                } else {
                    Toast.makeText(activityMain, addCardData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;

            case postDetailPhoto:
                DetailPostPhotoData detailPostPhotoData = (DetailPostPhotoData) o;
                if (detailPostPhotoData.isSuccess()) {
                    deleteImage(getArguments().getString("uploadImgUri"));
                    if (getArguments().containsKey("originalImgUri"))
                        deleteImage(getArguments().getString("originalImgUri"));
                    Toast.makeText(activityMain, detailPostPhotoData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    Intent intent = new Intent(activityMain, PaymentSuccessActivity.class);
                    activityMain.startActivity(intent);
                    activityMain.overridePendingTransition(0, 0);
                } else {
                    Toast.makeText(activityMain, detailPostPhotoData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                }

                break;

            case postDetailVideo:
                DetailPostVideoData detailPostVideoData = (DetailPostVideoData) o;
                if (detailPostVideoData.isSuccess()) {
                    deleteImage(getArguments().getString("Thumb"));
                    Toast.makeText(activityMain, detailPostVideoData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                    Intent intent = new Intent(activityMain, PaymentSuccessActivity.class);
                    activityMain.startActivity(intent);
                    activityMain.overridePendingTransition(0, 0);
                } else {
                    Toast.makeText(activityMain, detailPostVideoData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                    disableScreen(false);
                }

                break;

            case postDetailText:
                mBinding.loading.progressBar.setVisibility(View.GONE);
                disableScreen(false);

                DetailPostPhotoData detailPostTextData = (DetailPostPhotoData) o;
                if (detailPostTextData.isSuccess()) {
                    Toast.makeText(activityMain, detailPostTextData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(activityMain, PaymentSuccessActivity.class);
                    activityMain.startActivity(intent);
                    activityMain.overridePendingTransition(0, 0);
                } else {
                    Toast.makeText(activityMain, detailPostTextData.getMessage().get(0), Toast.LENGTH_SHORT).show();
                }

                break;

        }

    }

    @Override
    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
        mBinding.loading.progressBar.setVisibility(View.GONE);
        disableScreen(false);
    }

    private void callApiSaveData() {
        double amount = getArguments().getDouble("amount");
        switch (postType) {
            case "Image":
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                MultipartBody.Part originalImageBody = null;
                MultipartBody.Part body = null;

                byte[] uploadBytes = loadImageFromStorage(getArguments().getString("uploadImgUri"));
                RequestBody uploadRequestFile = RequestBody.create(MediaType.parse("multipart/form-data"), uploadBytes);
                body = MultipartBody.Part.createFormData("UploadPhoto", userId + "post_image.png", uploadRequestFile);

                if (getArguments().containsKey("originalImgUri")) {
                    byte[] bytes = loadImageFromStorage(getArguments().getString("originalImgUri"));
                    RequestBody originalRequestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    originalImageBody = MultipartBody.Part.createFormData("OriginalImage", userId + "original_post_image.png", originalRequestFile);
                }

                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(activityMain).
                                        create(WebserviceBuilder.class).
                                        postDetailPhoto(body, originalImageBody, "", "", userId,
                                                getArguments().getString("tagUserID"), getArguments().getString("lat"), getArguments().getString("lng"),
                                                getArguments().getString("distance"), getArguments().getString("time"),
                                                getArguments().getString("hashTag"), getArguments().getBoolean("isHighlight"), true, new DecimalFormat("0.00").format(amount))
                                , getCompositeDisposable(), postDetailPhoto, this);

                break;

            case "Text":
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                String desc = getArguments().getString("TextDesc", "");
                String tit = getArguments().getString("TextTitle", "");
                String descToSend = "";
                try {
                    descToSend = URLEncoder.encode(desc, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String titToSend = "";
                try {
                    titToSend = URLEncoder.encode(tit, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(activityMain).
                                        create(WebserviceBuilder.class).
                                        postDetailText(descToSend, titToSend, userId, getArguments().getString("tagUserID"),
                                                getArguments().getString("lat"), getArguments().getString("lng"),
                                                getArguments().getString("distance"), getArguments().getString("time"),
                                                getArguments().getString("hashTag"), getArguments().getBoolean("isHighlight"),
                                                true, new DecimalFormat("0.00").format(amount))
                                , getCompositeDisposable(), postDetailText, this);

                break;

            case "ImageWithText":
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                MultipartBody.Part originalImgBody = null;
                MultipartBody.Part uploadBody = null;

                byte[] uploadByteArray = loadImageFromStorage(getArguments().getString("uploadImgUri"));
                RequestBody uploadRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), uploadByteArray);
                uploadBody = MultipartBody.Part.createFormData("UploadPhoto", userId + "post_image.png", uploadRequestBody);

                if (getArguments().containsKey("originalImgUri")) {
                    byte[] bytes = loadImageFromStorage(getArguments().getString("originalImgUri"));
                    RequestBody originalRequestFile = RequestBody.create(MediaType.parse("multipart/form-data"), bytes);
                    originalImgBody = MultipartBody.Part.createFormData("OriginalImage", userId + "original_post_image.png", originalRequestFile);
                }

                String description = getArguments().getString("TextDesc", "");
                String title = getArguments().getString("TextTitle", "");
                String descriptionToSend = "";
                try {
                    descriptionToSend = URLEncoder.encode(description, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String titleToSend = "";
                try {
                    titleToSend = URLEncoder.encode(title, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(activityMain).
                                        create(WebserviceBuilder.class).
                                        postDetailPhoto(uploadBody, originalImgBody, descriptionToSend, titleToSend, userId,
                                                getArguments().getString("tagUserID"), getArguments().getString("lat"),
                                                getArguments().getString("lng"), getArguments().getString("distance"),
                                                getArguments().getString("time"), getArguments().getString("hashTag"),
                                                getArguments().getBoolean("isHighlight"), true, new DecimalFormat("0.00").format(amount))
                                , getCompositeDisposable(), postDetailPhoto, this);
                break;

            case "Video":
                mBinding.loading.progressBar.setVisibility(VISIBLE);
                disableScreen(true);
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), loadImageFromStorage(getArguments().getString("Thumb")));
                MultipartBody.Part thumbBody = MultipartBody.Part.createFormData("UploadThumbnail", userId + "thumbnile.png", requestFile);
                File file = new File(getArguments().getString("VideoFile"));
                RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), file);
                MultipartBody.Part vFile = MultipartBody.Part.createFormData("UploadVideo", file.getName(), videoBody);

                ObserverUtil
                        .subscribeToSingle(ApiClient.getClient(activityMain).
                                        create(WebserviceBuilder.class).
                                        postDetailVideo(vFile, thumbBody, getArguments().getString("caption"), userId,
                                                getArguments().getString("tagUserID"), getArguments().getString("lat"),
                                                getArguments().getString("lng"), getArguments().getString("distance"),
                                                getArguments().getString("time"), getArguments().getString("hashTag"),
                                                getArguments().getBoolean("isHighlight"), true, new DecimalFormat("0.00").format(amount))
                                , getCompositeDisposable(), postDetailVideo, this);

                break;


        }

    }

    private byte[] loadImageFromStorage(String path) {
        try {
            File f = new File(path);
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(f));
            ByteArrayOutputStream uploadStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 50, uploadStream);
            return uploadStream.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteImage(String path) {
        File fDelete = new File(path);
        if (fDelete.exists()) {
            if (fDelete.delete()) {
                Log.e("delete image : ", "file Deleted :" + path);
            } else {
                Log.e("delete image : ", "file not Deleted :" + path);
            }
        }
    }

}
