package com.danielandshayegan.discovrus.eventbus;

/**
 * Created by sit107 on 05-01-2018.
 */

public class EventDataObject {

    public static class VideoRotation {

        boolean isRotate;

        public VideoRotation(boolean isRotate) {
            this.isRotate = isRotate;
        }

        public boolean isRotate() {
            return isRotate;
        }
    }

    public static class CommentDelete {
        boolean isDetelete;

        public CommentDelete(boolean isDetelete) {
            this.isDetelete = isDetelete;
        }

        public boolean isDetelete() {
            return isDetelete;
        }
    }

}
