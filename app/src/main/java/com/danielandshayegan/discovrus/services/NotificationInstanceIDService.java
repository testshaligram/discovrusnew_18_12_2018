package com.danielandshayegan.discovrus.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class NotificationInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        storeRegIdInPref(refreshedToken);
    }


    private void storeRegIdInPref(String token) {
        NotificationToken notificationToken = new NotificationToken(token);
        Log.e(TAG, "storeRegIdInPref: " + notificationToken.getAccessToken());

        //     VirtualManagerPrefs.saveNotificationToken(getApplicationContext(), notificationToken);

    }


}
