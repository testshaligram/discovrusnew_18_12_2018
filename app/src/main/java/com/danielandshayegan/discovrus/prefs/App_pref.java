package com.danielandshayegan.discovrus.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.danielandshayegan.discovrus.models.LoginData;
import com.danielandshayegan.discovrus.models.SocialMediaLoginData;
import com.google.gson.Gson;


public class App_pref {

    public static final String KEY_AUTHORIZED_USER = "KEY_AUTHORIZED_USER";
    public static final String APP_PREF = "App_pref";
    public static final String IS_FIRST_TIME_USER = "IS_FIRST_TIME_USER";
    public static final String TRENDING_DATA = "TRENDING_NOW";


    // endregion

    // region Constructors
    private App_pref() {
        //no instance
    }

    private static SharedPreferences.Editor getEditor(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.edit();
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
    }

    public static void saveAuthorizedUser(Context context, LoginData authorizedUser) {
        SharedPreferences.Editor editor = getEditor(context);
        Gson gson = new Gson();
        String json = gson.toJson(authorizedUser);
        editor.putString(KEY_AUTHORIZED_USER, json)
                .apply();
    }

    public static LoginData getAuthorizedUser(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        Gson gson = new Gson();
        String json = preferences.getString(KEY_AUTHORIZED_USER, "");
        LoginData loginData = gson.fromJson(json, LoginData.class);
        return loginData;
    }

   /* public static LoginModel getAuthorizedUser(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        Gson gson = new Gson();
        String json = preferences.getString(KEY_AUTHORIZED_USER, "");
        LoginModel loginModel = gson.fromJson(json, LoginModel.class);
        return loginModel;
    }*/

    public static void signOut(Context context) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.remove(KEY_AUTHORIZED_USER)
                .apply();
    }

    public static boolean isFirstTimeUser(Context context) {
        SharedPreferences preferences = getSharedPreferences(context);
        return preferences.getBoolean(IS_FIRST_TIME_USER, true);
    }

    public static void setIsFirstTimeUser(Context context, boolean isFirstTime) {
        SharedPreferences.Editor editor = getEditor(context);
        editor.putBoolean(IS_FIRST_TIME_USER, isFirstTime)
                .apply();
    }

    public static void setTrendingListSize(Context context, int listSize){
        SharedPreferences.Editor editor = getEditor(context);
        editor.putInt(TRENDING_DATA, listSize)
                .apply();
    }
    public static int getTrendingListSize(Context context){
        SharedPreferences preferences = getSharedPreferences(context);
        int sellRate = preferences.getInt(TRENDING_DATA, 0);
        return sellRate;
    }



    // endregion
}
