package com.danielandshayegan.discovrus.dialog;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.DialogOptionMenuBinding;
import com.danielandshayegan.discovrus.models.AddToFavourite;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.SavePost;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostImageFragment;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostTextFragment;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostVideoFragment;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveBlockUserDetails;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.addToFavourite;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.deletePost;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.savePost;
import static com.danielandshayegan.discovrus.utils.Utils.getCompositeDisposable;

public class OptionMenuDialog extends DialogFragment {

    DialogOptionMenuBinding mBinding;
    View view;
    PostListData.Post postData;
    Context context;
    String from = "";

    public OptionMenuDialog() {
    }

    public static OptionMenuDialog newInstance() {
        OptionMenuDialog frag = new OptionMenuDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_option_menu, container, false);
        view = mBinding.getRoot();
        context = inflater.getContext();

        Bundle args = getArguments();
        if (args != null) {
            postData = args.getParcelable("postDetails");
            from = args.getString("from");
            if (postData != null) {
                if (postData.isSave()) {
                    mBinding.txtUnsavePost.setVisibility(View.VISIBLE);
                    mBinding.viewUnsave.setVisibility(View.VISIBLE);
                    mBinding.txtSavePost.setVisibility(View.GONE);
                    mBinding.viewSave.setVisibility(View.GONE);
                } else {
                    mBinding.txtUnsavePost.setVisibility(View.GONE);
                    mBinding.viewUnsave.setVisibility(View.GONE);
                    mBinding.txtSavePost.setVisibility(View.VISIBLE);
                    mBinding.viewSave.setVisibility(View.VISIBLE);
                }
            }
        }

        if (postData != null && postData.getUserID() == App_pref.getAuthorizedUser(getActivity()).getData().getUserId()) {
            mBinding.view4.setVisibility(View.GONE);
            mBinding.tvBlock.setVisibility(View.GONE);
            mBinding.view5.setVisibility(View.VISIBLE);
            mBinding.txtDeletePost.setVisibility(View.VISIBLE);
        } else {
            mBinding.view4.setVisibility(View.VISIBLE);
            mBinding.tvBlock.setVisibility(View.VISIBLE);
            mBinding.view5.setVisibility(View.GONE);
            mBinding.txtDeletePost.setVisibility(View.GONE);
        }

        setClick();

        return view;
    }

    public void setClick() {
        mBinding.txtCancel.setOnClickListener(view -> {
            getDialog().dismiss();
        });

        mBinding.txtSavePost.setOnClickListener(view -> {
            call_SavePost();
        });

        mBinding.txtUnsavePost.setOnClickListener(view -> call_SavePost());

        mBinding.txtAddToFav.setOnClickListener(view -> {
            call_AddToFavouritesOrHighlights(true);
        });

        mBinding.txtAddToHighlights.setOnClickListener(view -> {
            call_AddToFavouritesOrHighlights(false);
        });

        mBinding.txtDeletePost.setOnClickListener(view -> call_DeletePost());

        mBinding.tvBlock.setOnClickListener(view -> {
    /*        if(from.equalsIgnoreCase("ClickPostImageFragment"))
            {
                CallbackTask ct =  ClickPostImageFragment.newInstance();
                ct.onSuccess(null);
            }*/
            callBlockUserApi();

        });

    }

    private void call_SavePost() {
        boolean isSave = !postData.isSave();
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                savePost(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), postData.getPostId(), isSave)
                        , getCompositeDisposable(), savePost, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                SavePost savePostData = (SavePost) o;
                                Log.e("onSingleSuccess: ", "Success");
                                Toast.makeText(getActivity(), savePostData.getMessage().get(0), Toast.LENGTH_LONG).show();
                                if (from.equalsIgnoreCase("ClickPostImageFragment"))
                                    ClickPostImageFragment.postImageFragment.dataBean.setSave(isSave);
                                else if (from.equalsIgnoreCase("ClickPostTextFragment"))
                                    ClickPostTextFragment.postTextFragment.dataBean.setSave(isSave);
                                else if (from.equalsIgnoreCase("ClickPostVideoFragment"))
                                    ClickPostVideoFragment.postVideoFragment.dataBean.setSave(isSave);
                                getDialog().dismiss();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Log.e("onFailure: ", "Failed");

                            }
                        });
    }

    private void call_DeletePost() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                deletePost(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), postData.getPostId())
                        , getCompositeDisposable(), deletePost, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                SavePost savePostData = (SavePost) o;
                                Log.e("onSingleSuccess: ", "Success");
                                Toast.makeText(getActivity(), savePostData.getMessage().get(0), Toast.LENGTH_LONG).show();
                                if (from.equalsIgnoreCase("ClickPostImageFragment"))
                                    ClickPostImageFragment.postImageFragment.simpleMethod();
                                else if (from.equalsIgnoreCase("ClickPostTextFragment"))
                                    ClickPostTextFragment.postTextFragment.simpleMethod();
                                else if (from.equalsIgnoreCase("ClickPostVideoFragment"))
                                    ClickPostVideoFragment.postVideoFragment.simpleMethod();
                                getDialog().dismiss();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("onFailure: ", "Failed");

                            }
                        });
    }

    private void call_AddToFavouritesOrHighlights(Boolean isFav) {

        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                addToFavourite(App_pref.getAuthorizedUser(getActivity()).getData().getUserId(), postData.getPostId(), !isFav, isFav)
                        , getCompositeDisposable(), addToFavourite, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                AddToFavourite addToFavourite = (AddToFavourite) o;
                                Toast.makeText(context, addToFavourite.getMessage().get(0), Toast.LENGTH_SHORT).show();
                                Log.e("onSingleSuccess: ", "Success");
                                getDialog().dismiss();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                Toast.makeText(context, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                Log.e("onFailure: ", "Failed");
                            }
                        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void callBlockUserApi() {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(getActivity()).
                                create(WebserviceBuilder.class).
                                SaveBlockUserDetails(postData.getUserID(), App_pref.getAuthorizedUser(getActivity()).getData().getUserId())
                        , getCompositeDisposable(), SaveBlockUserDetails, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                builder.setMessage(R.string.user_blocked_successfully);
                                builder.setPositiveButton(getString(R.string.Ok), (dialog, which) -> {
                                    dialog.dismiss();
                                    getActivity().finish();
                                    getDialog().dismiss();
                                });
                                builder.show();
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {

                            }
                        });
    }
}
