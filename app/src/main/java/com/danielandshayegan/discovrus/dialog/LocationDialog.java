package com.danielandshayegan.discovrus.dialog;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.DialogLocationBinding;
import com.danielandshayegan.discovrus.databinding.FragmentRegisterBinding;
import com.danielandshayegan.discovrus.models.AddressResult;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.GoogleAddressResult;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.utils.FusedLocationProviderClass;
import com.danielandshayegan.discovrus.utils.MyLocationChangeListner;
import com.danielandshayegan.discovrus.utils.Utils;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class LocationDialog extends DialogFragment implements MyLocationChangeListner {

    DialogLocationBinding mBinding;
    View view;
    private CompositeDisposable disposable = new CompositeDisposable();
    List<GoogleAddressResult.ResultsBean> resultsList = new ArrayList<>();
    double Latitude, longitude;
    String address;
    FusedLocationProviderClass fusedLocationProviderClass;
    private FusedLocationProviderClient fusedLocationProviderClient;
    String TAG = LocationDialog.class.getName();
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public LocationDialog() {
    }

    public static LocationDialog newInstance() {
        LocationDialog frag = new LocationDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_location, container, false);
        view = mBinding.getRoot();
        fusedLocationProviderClient = new FusedLocationProviderClient(getActivity());
        fusedLocationProviderClass = new FusedLocationProviderClass(getActivity(), fusedLocationProviderClient, this);

        setClick();

        return view;
    }

    protected void buildLocationSettingsRequest() {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    public void setClick() {
        mBinding.btnNext.setOnClickListener(view -> {
            if (mBinding.btnNext.getText().toString().equalsIgnoreCase("Search")) {
                if (mBinding.edtPostalCode.getText().toString().equalsIgnoreCase("")) {
                    mBinding.txtErrorPostalCode.setText(R.string.null_postal_code);
                    mBinding.txtErrorPostalCode.setVisibility(View.VISIBLE);
                } else {
                    callLocationApi();
                }
            } else {
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent()
                        .putExtra("lat", Latitude)
                        .putExtra("lng", longitude)
                        .putExtra("address", address)
                        .putExtra("postalCode", mBinding.edtPostalCode.getText().toString()));
                getDialog().dismiss();
            }
        });

        mBinding.txtCurrentLocation.setOnClickListener(view -> {
            buildLocationSettingsRequest();
        });

        mBinding.edtPostalCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    mBinding.txtErrorPostalCode.setText(R.string.null_postal_code);
                    mBinding.txtErrorPostalCode.setVisibility(View.VISIBLE);
                    mBinding.btnNext.setText("Search");
                } else {
                    mBinding.txtErrorPostalCode.setText(R.string.null_postal_code);
                    mBinding.txtErrorPostalCode.setVisibility(View.GONE);
                    mBinding.btnNext.setText("Search");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    public void callLocationApi() {
        if (Utils.isNetworkAvailable(getActivity())) {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();

            WebserviceBuilder service = retrofit.create(WebserviceBuilder.class);
            String postalCodeString = "postal_code:" + mBinding.edtPostalCode.getText().toString();
            disposable.add(service.getAddress(postalCodeString, true, getString(R.string.google_maps_key)).
                    subscribeOn(Schedulers.io()).
                    observeOn(AndroidSchedulers.mainThread()).
                    subscribeWith(new DisposableSingleObserver<GoogleAddressResult>() {
                        @Override
                        public void onSuccess(GoogleAddressResult value) {
                            if (value.getStatus().equalsIgnoreCase("OK")) {
                                resultsList = value.getResults();
                                Latitude = resultsList.get(0).getGeometry().getLocation().getLat();
                                longitude = resultsList.get(0).getGeometry().getLocation().getLng();
                                address = resultsList.get(0).getFormattedAddress();
                                mBinding.txtAddress.setText(resultsList.get(0).getFormattedAddress());
                                mBinding.btnNext.setText("Verify");
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            } else {
                                Log.e("Google api call", "onSuccess: ");
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("fail", e.toString());
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                        }

                    }));

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
            mBinding.loading.progressBar.setVisibility(View.GONE);
            disableScreen(false);
        }
    }

    public void getLocation() {
       /* GPSTracker gpsTracker = new GPSTracker(getActivity());
        Location gpsLocation = gpsTracker.getLocation();
        if (gpsLocation != null) {
            getData(gpsLocation);
            Log.e("Get LatLong", "getLocation: " + gpsLocation.getLongitude() + " " + gpsLocation.getLongitude());
        }*/
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                getCurrentLocation();

            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            getCurrentLocation();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation();
                    // permission was granted, yay! Do the
                    // location-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void getCurrentLocation() {
        fusedLocationProviderClass.getLocation();
    }

    @Override
    public void onLocationChange(Location object) {
        Log.e(TAG, "Lat - " + object.getLatitude() + "Long - " + object.getLongitude());
        callApi(object);
    }

    public void callApi(Location object) {
        if (Utils.isNetworkAvailable(getActivity())) {
            mBinding.loading.progressBar.setVisibility(View.VISIBLE);
            disableScreen(true);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://maps.googleapis.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();

            WebserviceBuilder service = retrofit.create(WebserviceBuilder.class);
            String postalCodeString = object.getLatitude() + "," + object.getLongitude();
            disposable.add(service.getAddressFromLatLong(postalCodeString, true, getString(R.string.google_maps_key)).
                    subscribeOn(Schedulers.io()).
                    observeOn(AndroidSchedulers.mainThread()).
                    subscribeWith(new DisposableSingleObserver<AddressResult>() {
                        @Override
                        public void onSuccess(AddressResult value) {
                            if (value.getStatus().equalsIgnoreCase("OK")) {
                                List<AddressResult.ResultsBean> resultsList = value.getResults();
                                mBinding.txtAddress.setText(resultsList.get(0).getFormatted_address());
                                Latitude = resultsList.get(0).getGeometry().getLocation().getLat();
                                longitude = resultsList.get(0).getGeometry().getLocation().getLng();
                                address = resultsList.get(0).getFormatted_address();
                                int addressComponentSize = resultsList.get(0).getAddress_components().size();
                                String postalCode = resultsList.get(0).getAddress_components().get(addressComponentSize - 1).getLong_name();
                                mBinding.edtPostalCode.setText(postalCode);
                                mBinding.btnNext.setText("Verify");
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            } else {
                                Log.e("Google api call", "onSuccess: ");
                                mBinding.loading.progressBar.setVisibility(View.GONE);
                                disableScreen(false);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("fail", e.toString());
                            mBinding.loading.progressBar.setVisibility(View.GONE);
                            disableScreen(false);
                            Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
                        }

                    }));

        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.check_your_network_connection), Toast.LENGTH_LONG).show();
            mBinding.loading.progressBar.setVisibility(View.GONE);
            disableScreen(false);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        fusedLocationProviderClass.setMyLocationChangeListner(null);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);//if you need three fix imageview in width
        int devicewidth = (int) (displaymetrics.widthPixels - getActivity().getResources().getDimension(R.dimen._10sdp));
        if (dialog != null) {
//            dialog.getWindow().setLayout(devicewidth, devicewidth);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setGravity(Gravity.CENTER);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

}
