package com.danielandshayegan.discovrus.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.databinding.DialogLocationBinding;
import com.danielandshayegan.discovrus.databinding.DialogStripeBinding;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import io.reactivex.disposables.CompositeDisposable;

public class StripeDialog extends DialogFragment {

    DialogStripeBinding mBinding;
    View view;
    private CompositeDisposable disposable = new CompositeDisposable();

    public StripeDialog() {
    }

    public static StripeDialog newInstance() {
        StripeDialog frag = new StripeDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_stripe, container, false);
        view = mBinding.getRoot();

        setClick();

        return view;
    }

    public void setClick() {
        mBinding.btnAddCard.setOnClickListener(view -> {
            validateCard();
        });

        mBinding.btnCancel.setOnClickListener(view -> {
            getDialog().dismiss();
        });

    }

    private void validateCard() {
        mBinding.loading.progressBar.setVisibility(View.VISIBLE);
        Card card = mBinding.cardInputWidget.getCard();

        if (card != null) {
            if (!card.validateNumber()) {
                mBinding.txtErrorCardNo.setText(R.string.invalid_card_number);
                mBinding.loading.progressBar.setVisibility(View.GONE);
                return;
            }
            if (!card.validateCVC()) {
                mBinding.txtErrorCardNo.setText(R.string.invalid_cvc);
                mBinding.loading.progressBar.setVisibility(View.GONE);
                return;
            }
            if (!card.validateExpiryDate()) {
                mBinding.txtErrorCardNo.setText(R.string.invalid_expiry_date);
                mBinding.loading.progressBar.setVisibility(View.GONE);
                return;
            }
            if (!card.validateCard()) {
                Snackbar.make(getActivity().findViewById(android.R.id.content), "Please Check your card details! and try again!", Snackbar.LENGTH_LONG).show();
                mBinding.loading.progressBar.setVisibility(View.GONE);
            } else {
                try {
                    card.setName(App_pref.getAuthorizedUser(getActivity()).getData().getFirstName());
                    card.setAddressLine1(App_pref.getAuthorizedUser(getActivity()).getData().getLocation());
//                    card.setAddressZip(App_pref.getAuthorizedUser(getActivity()).getData().getZipCode());
                    crateToken(card);
                } catch (AuthenticationException e) {
                    e.printStackTrace();
                    mBinding.loading.progressBar.setVisibility(View.GONE);
                }
            }
        } else {
            mBinding.loading.progressBar.setVisibility(View.GONE);
            mBinding.txtErrorCardNo.setText(R.string.invalid_card_error);
        }
    }

    private void crateToken(Card card) throws AuthenticationException {
        Stripe stripe = new Stripe(getActivity(), "pk_test_qDAlooLZCUL0sS9l6VScRyz0");// for test
        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        String tokenId = token.getId();
                        Log.e("CreatedToken", "" + tokenId);
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, new Intent()
                                .putExtra("tokenId", tokenId). putExtra("isSave", mBinding.checkboxSave.isChecked()));
                        getDialog().dismiss();
                    }

                    public void onError(Exception error) {
                        error.printStackTrace();
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Error creating Payment Token Try Later!!", Snackbar.LENGTH_LONG).show();
                        mBinding.loading.progressBar.setVisibility(View.GONE);
                    }
                }
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setGravity(Gravity.CENTER);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void disableScreen(boolean disable) {
        if (disable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

}
