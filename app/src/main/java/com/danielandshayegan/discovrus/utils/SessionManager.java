package com.danielandshayegan.discovrus.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.danielandshayegan.discovrus.R;


public class SessionManager {

    private Editor editor;
    private SharedPreferences pref;

    public static final String KEY_IS_DAILY_ALERT = "is_daily_alert";
    public static final String KEY_HOUR = "hour";
    public static final String KEY_MINUTE = "min";


    public static final String KEY_IS_GOTO_HOME = "is_goto_home";
    public static final String KEY_IS_CHALLENGE_PAUSE = "is_challenge_pause";
    public static final String KEY_IS_CHALLENGE_COMPLETED = "is_challenge_completed";

    public static final String KEY_CURRENT_DAY = "current_day";



    public SessionManager(Context context) {
        String PREF_NAME = context.getResources().getString(R.string.app_name);
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    // Daily Alert
    public void storeDailyAlertDetail(int hour, int min) {
        editor.putInt(KEY_HOUR, hour);
        editor.putInt(KEY_MINUTE, min);
        editor.putBoolean(KEY_IS_DAILY_ALERT, true);
        editor.commit();
    }

    public void clearDailyAlertDetail() {
        editor.putInt(KEY_HOUR, 0);
        editor.putInt(KEY_MINUTE, 0);
        editor.putBoolean(KEY_IS_DAILY_ALERT, false);
        editor.commit();
    }

    public void setStringDataByKey(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringDataByKey(String key) {
        return pref.getString(key, "");
    }

    public int getIntegerDataByKey(String key) {
        return pref.getInt(key, 0);
    }

    public boolean getBooleanDataByKey(String key) {
        return pref.getBoolean(key, false);
    }

    public void setBooleanDataByKey(String key, boolean isTrue) {
        editor.putBoolean(key, isTrue);
        editor.commit();
    }

    // Reset Challenge
    public void resetSession() {
        clearDailyAlertDetail();
        setBooleanDataByKey(SessionManager.KEY_IS_GOTO_HOME, false);
        setBooleanDataByKey(SessionManager.KEY_IS_CHALLENGE_PAUSE, false);
        setBooleanDataByKey(SessionManager.KEY_IS_CHALLENGE_COMPLETED, false);
        setStringDataByKey(SessionManager.KEY_CURRENT_DAY, "1");
    }
}
