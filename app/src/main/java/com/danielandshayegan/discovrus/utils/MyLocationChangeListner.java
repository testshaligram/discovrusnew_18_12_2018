package com.danielandshayegan.discovrus.utils;

import android.location.Location;

public interface MyLocationChangeListner {
    void onLocationChange(Location location);
}
