package com.danielandshayegan.discovrus.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;

public class LayoutToImage {

    View _view;
    Context _context;

    Bitmap bMap;

    public LayoutToImage(Context context, View view) {
        this._context = context;
        this._view = view;
    }

    public Bitmap convert_layout() {
        /*_view.setDrawingCacheEnabled(true);

        _view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        _view.layout(0, 0, _view.getMeasuredWidth(), _view.getMeasuredHeight());

        _view.buildDrawingCache(true);

        bMap = Bitmap.createBitmap(_view.getDrawingCache());

        return bMap;*/
        if (_view.getMeasuredHeight() <= 0) {
            _view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            Bitmap b = Bitmap.createBitmap(_view.getMeasuredWidth(), _view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            _view.layout(_view.getLeft(), _view.getTop(), _view.getRight(), _view.getBottom());
            _view.draw(c);
            return b;
        } else {
            Bitmap b = Bitmap.createBitmap(_view.getMeasuredWidth(),  _view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            _view.layout(_view.getLeft(), _view.getTop(), _view.getRight(), _view.getBottom());
            _view.draw(c);
            return b;
        }
    }
}
