package com.danielandshayegan.discovrus.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

import java.util.List;

public class FusedLocationProviderClass {
    private Context context;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest mLocationRequest;
    private MyLocationChangeListner myLocationChangeListner;


    public FusedLocationProviderClass(Context context, FusedLocationProviderClient fusedLocationProviderClient, MyLocationChangeListner myLocationChangeListner) {
        this.context = context;
        this.fusedLocationProviderClient = fusedLocationProviderClient;
        this.myLocationChangeListner = myLocationChangeListner;

    }

    public void setMyLocationChangeListner(MyLocationChangeListner myLocationChangeListner) {
        this.myLocationChangeListner = myLocationChangeListner;

    }

    public void getLocation() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000); // two minute interval
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


        }
        fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }


    private LocationCallback mLocationCallback = new LocationCallback() {

        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            Log.d("isLocationAvailable ", "=  " + locationAvailability.isLocationAvailable());
        }

        @Override
        public void onLocationResult(LocationResult locationResult) {
            Log.d("onLocationResult call ", "=  " + locationResult.getLastLocation());

            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location fuselocation = locationList.get(locationList.size() - 1);
                Log.i("MapsActivity", "Location: " + fuselocation.getLatitude() + " " + fuselocation.getLongitude());
                if (myLocationChangeListner != null) {
                    myLocationChangeListner.onLocationChange(fuselocation);
                }

            }
        }
    };

}
