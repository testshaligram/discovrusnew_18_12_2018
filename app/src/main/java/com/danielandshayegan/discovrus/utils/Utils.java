package com.danielandshayegan.discovrus.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.danielandshayegan.discovrus.R;
import com.danielandshayegan.discovrus.interfaces.CallbackTask;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.models.LikeListData;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.ui.fragment.ClickPostImageFragment;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import io.reactivex.disposables.CompositeDisposable;

import static com.danielandshayegan.discovrus.custome_veiws.VerticalDividerDecoration.vertOverlap;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getGeneralSettings;
import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.getLikeListData;
import static com.google.android.gms.common.util.IOUtils.copyStream;

public class Utils {

    public static final String CLIENT_ID = "cd88ba4ed5be41878c08d4a8cedf5323";
    public static final String CLIENT_SECRATE_ID = "2bc39e1b55ab4af19a7df25d9419a221";
    public static final String REDIRECT_URI = "http://www.shaligraminfotech.com/";
    public static String fontName = "";
    public static double latitude;
    public static double longitude;
    private static Dialog dialog;
    private static DisplayMetrics displaymetrics;

    public static boolean FROM_SPLASH = false;

    public static int selectedPosition;
    public static CompositeDisposable compositeDisposable;

    public static boolean isNetworkAvailable(Context mContext) {

        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        boolean is = (netInfo != null && netInfo.isConnectedOrConnecting());
        return is;
    }

    public static String convertLikes(int likes) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        if (likes >= 1000) {
            Double division = Double.valueOf((likes * 1.00) / 1000);
            return String.valueOf(formatter.format(division)) + "k";
        } else if (likes >= 1000000) {
            Double division = Double.valueOf((likes * 1.00) / 1000000);
            return String.valueOf(formatter.format(division)) + "m";
        } else {
            return String.valueOf(likes);
        }
    }

    public static String convertToOriginalCount(String likes) {
        if (likes.contains("k")) {
            Double likeToCOnvert = Double.valueOf(likes.toString().replaceAll("k", ""));
            return String.valueOf(likeToCOnvert * 1000);
        } else if (likes.contains("m")) {
            Double likeToCOnvert = Double.valueOf(likes.toString().replaceAll("m", ""));
            return String.valueOf(likeToCOnvert * 1000000);
        } else {
            return String.valueOf(likes);
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static void saveImageToExternal(Context me, String imgName, Bitmap bm) throws IOException {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        path.mkdirs();
        File imageFile = new File(path, imgName + ".jpg"); // Imagename.jpg
        FileOutputStream out = new FileOutputStream(imageFile);
        try {
            bm.compress(Bitmap.CompressFormat.JPEG, 100, out); // Compress Image
            out.flush();
            out.close();

            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(me, new String[]{imageFile.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
            });
        } catch (Exception e) {
            throw new IOException();
        }
    }

    public static void saveVideoToExternal(Context me, Uri videoUri) throws IOException {
        try {
            String Video_DIRECTORY = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getAbsolutePath();
            File storeDirectory = new File(Video_DIRECTORY);

            try {
                if (storeDirectory.exists() == false) {
                    storeDirectory.mkdirs();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
            File storeDirectory12 = new File(storeDirectory, System.currentTimeMillis() + ".mp4");
            InputStream inputStream = me.getContentResolver().openInputStream(videoUri);
            FileOutputStream fileOutputStream = new FileOutputStream(storeDirectory12);
            copyStream(inputStream, fileOutputStream);
            fileOutputStream.close();
            inputStream.close();

            MediaScannerConnection.scanFile(me, new String[]{storeDirectory.getAbsolutePath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                    Log.i("ExternalStorage", "Scanned " + path + ":");
                    Log.i("ExternalStorage", "-> uri=" + uri);
                }
            });
        } catch (FileNotFoundException e) {
            Log.e("Exception", "" + e);
        } catch (IOException e) {
            Log.e("Exception", "" + e);
        }
    }

    public static void callGetGeneralSettings(Context me, int userId, final CallbackTask callBack) {
        ObserverUtil.subscribeToSingle(ApiClient.getClient(me).
                        create(WebserviceBuilder.class).
                        getGeneralSettings(userId)
                , getCompositeDisposable(), getGeneralSettings, new SingleCallback() {
                    @Override
                    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                        if (callBack != null)
                            callBack.onSuccess(o);
                    }

                    @Override
                    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                        if (callBack != null)
                            callBack.onFailure(throwable);
                    }
                });
    }

    public static CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null) {
            compositeDisposable = new CompositeDisposable();
        }
        if (compositeDisposable.isDisposed()) compositeDisposable = new CompositeDisposable();
        return compositeDisposable;
    }

    public static GeneralSettingsData getGeneralSettingsData(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        Gson gson = new Gson();
        Type type = new TypeToken<GeneralSettingsData>() {
        }.getType();

        return gson.fromJson(prefs.getString("GeneralSettings", ""), type);
    }

    public static void setGeneralSettingsData(Context context, GeneralSettingsData mData) {
        Gson gson = new Gson();
        Type type = new TypeToken<GeneralSettingsData>() {
        }.getType();
        String strObject = gson.toJson(mData, type);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString("GeneralSettings", strObject).apply();
    }

    public static void getLikeListData(Context me, int userId, final CallbackTask callBack) {
        ObserverUtil.subscribeToSingle(ApiClient.getClient(me).
                        create(WebserviceBuilder.class).
                        getLikeListData(App_pref.getAuthorizedUser(me).getData().getUserId(), userId)
                , getCompositeDisposable(), getLikeListData, new SingleCallback() {
                    @Override
                    public void onSingleSuccess(Object o, WebserviceBuilder.ApiNames apiNames) {
                        if (callBack != null)
                            callBack.onSuccess(o);
                    }

                    @Override
                    public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                        if (callBack != null)
                            callBack.onFailure(throwable);
                    }
                });
    }

    public static void showProgressBar(Context context) {
        dismissProgressBar();

        try {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setContentView(R.layout.loading_bar);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            Log.e("showProgressBar: ", "==>" + e);
            e.printStackTrace();
        }
    }

    public static void dismissProgressBar() {
        try {
            if (dialog != null) {
                dialog.dismiss();
                dialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    Log.e("Network", "NETWORK NAME: " + networkInfo.getTypeName());
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            Log.e("Network", "NETWORK NAME: " + anInfo.getTypeName());
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public static int getDeviceWidth(Context context) {
        if (displaymetrics == null)
            displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return (displaymetrics.widthPixels);
    }

    public static int getDeviceWidthHalf(Context context) {
        if (displaymetrics == null)
            displaymetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return (displaymetrics.widthPixels - convertDpToPixels(context, vertOverlap)) / 2;
    }

    public static void saveHalfScreenWidth(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putInt("HalfWidth", getDeviceWidthHalf(context)).apply();
    }

    public static int getHalfScreenWidth(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int n = prefs.getInt("HalfWidth", 0);
        if (n == 0) {
            saveHalfScreenWidth(context);
            n = prefs.getInt("HalfWidth", 0);
            return n;
        } else {
            return n;
        }
    }

    public static int convertDpToPixels(Context me, float dp)
    {
        Resources r = me.getResources();
        float px = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );
        return (int) px;
    }
}