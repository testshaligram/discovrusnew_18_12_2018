package com.danielandshayegan.discovrus;

import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.Context;
import android.os.StrictMode;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.network.ApiClient;
import com.danielandshayegan.discovrus.network.ObserverUtil;
import com.danielandshayegan.discovrus.network.SingleCallback;
import com.danielandshayegan.discovrus.network.WebserviceBuilder;
import com.danielandshayegan.discovrus.prefs.App_pref;
import com.danielandshayegan.discovrus.utils.Utils;
import com.danikula.videocache.HttpProxyCacheServer;

import java.io.File;

import static com.danielandshayegan.discovrus.network.WebserviceBuilder.ApiNames.SaveOnlineUserList;

/**
 * Created by Hiren.Dixit
 * (Sr. Android Developer)
 * on 06-June-18
 */

public class ApplicationClass extends Application implements LifecycleObserver {


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    // region Static Variables
    private static ApplicationClass currentApplication = null;
    // endregion

    private static HttpProxyCacheServer proxy;

    public static HttpProxyCacheServer getProxy() {
        return proxy;
    }

    // region Lifecycle Methods
    @Override
    public void onCreate() {
        super.onCreate();

        currentApplication = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        proxy = new HttpProxyCacheServer(this);

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }
 /*   private void initializeRealm() {
        io.realm.Realm.init(getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name(io.realm.Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        io.realm.Realm.setDefaultConfiguration(realmConfiguration);*/


    // region Helper Methods
    public static ApplicationClass getInstance() {
        return currentApplication;
    }

    public static File getCacheDirectory() {
        return currentApplication.getCacheDir();
    }

    public static void OnlineUserListAPI(Context me, boolean isOnline) {
        ObserverUtil
                .subscribeToSingle(ApiClient.getClient(me).
                                create(WebserviceBuilder.class).
                                SaveOnlineUserList(App_pref.getAuthorizedUser(me).getData().getUserId(), isOnline)
                        , Utils.getCompositeDisposable(), SaveOnlineUserList, new SingleCallback() {
                            @Override
                            public void onSingleSuccess(Object object, WebserviceBuilder.ApiNames apiNames) {
                                CommonApiResponse commonApiResponse = (CommonApiResponse) object;
                                Log.e("OnlineUserListAPI: ", commonApiResponse.getMessage().get(0));
                            }

                            @Override
                            public void onFailure(Throwable throwable, WebserviceBuilder.ApiNames apiNames) {
                                throwable.printStackTrace();
                            }
                        });
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private void onAppBackgrounded() {
        Log.e("Awww", "App in background");
        if (App_pref.getAuthorizedUser(this) != null) {
            OnlineUserListAPI(this, false);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private void onAppForegrounded() {
        Log.e("Yeeey", "App in foreground");
        if (App_pref.getAuthorizedUser(this) != null) {
            OnlineUserListAPI(this, true);
        }
    }
}