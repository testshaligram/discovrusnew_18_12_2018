package com.danielandshayegan.discovrus.interfaces;

public interface OnLoadMoreListener {
    void onLoadMore(int page);
}