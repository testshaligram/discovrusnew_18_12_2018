package com.danielandshayegan.discovrus.interfaces;

public interface FragmentLifeCycle {
    void onPauseFragment();

    void onResumeFragment();
}
