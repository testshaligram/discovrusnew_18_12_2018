package com.danielandshayegan.discovrus.interfaces;

/**
 * Created by cisner-2 on 25/1/18.
 */

public interface CallbackTask {
    void onFail(Object object);

    void onSuccess(Object object);

    void onFailure(Throwable t);
}
