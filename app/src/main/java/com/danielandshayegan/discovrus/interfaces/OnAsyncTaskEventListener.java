package com.danielandshayegan.discovrus.interfaces;

public interface OnAsyncTaskEventListener<T> {
    void onSuccess(T object);

    void onFailure(Exception e);
}
