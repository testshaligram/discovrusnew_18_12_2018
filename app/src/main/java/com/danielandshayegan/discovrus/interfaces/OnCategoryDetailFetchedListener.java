package com.danielandshayegan.discovrus.interfaces;

import com.danielandshayegan.discovrus.db.entity.CategoriesTableModel;

public interface OnCategoryDetailFetchedListener {
    void onCategoryDetailFetched(CategoriesTableModel mDayModel);
}
