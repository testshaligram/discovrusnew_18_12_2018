package com.danielandshayegan.discovrus.network;


import com.danielandshayegan.discovrus.models.AddCardData;
import com.danielandshayegan.discovrus.models.AddToFavourite;
import com.danielandshayegan.discovrus.models.AddressResult;
import com.danielandshayegan.discovrus.models.AnalyticsData;
import com.danielandshayegan.discovrus.models.BlockAccountListData;
import com.danielandshayegan.discovrus.models.BusinessRegisterData;
import com.danielandshayegan.discovrus.models.CardListEnvelope;
import com.danielandshayegan.discovrus.models.CategoryList;
import com.danielandshayegan.discovrus.models.ChartDataList;
import com.danielandshayegan.discovrus.models.ClickPostCommentData;
import com.danielandshayegan.discovrus.models.ClickieList;
import com.danielandshayegan.discovrus.models.CommentReplyData;
import com.danielandshayegan.discovrus.models.CommonApiResponse;
import com.danielandshayegan.discovrus.models.ConfirmMailData;
import com.danielandshayegan.discovrus.models.ConnectPeopleData;
import com.danielandshayegan.discovrus.models.DataPolicyModel;
import com.danielandshayegan.discovrus.models.DeleteCommentData;
import com.danielandshayegan.discovrus.models.DetailPostPhotoData;
import com.danielandshayegan.discovrus.models.DetailPostVideoData;
import com.danielandshayegan.discovrus.models.FollowPeopleData;
import com.danielandshayegan.discovrus.models.FollowersUserListData;
import com.danielandshayegan.discovrus.models.ForgotPasswordData;
import com.danielandshayegan.discovrus.models.GeneralSettingsData;
import com.danielandshayegan.discovrus.models.GoogleAddressResult;
import com.danielandshayegan.discovrus.models.HashTagListModel;
import com.danielandshayegan.discovrus.models.HiddenDataListModel;
import com.danielandshayegan.discovrus.models.IndividualRegisterData;
import com.danielandshayegan.discovrus.models.LikeListData;
import com.danielandshayegan.discovrus.models.LoginData;
import com.danielandshayegan.discovrus.models.MapListData;
import com.danielandshayegan.discovrus.models.MapPostDetail;
import com.danielandshayegan.discovrus.models.PaperClippedListData;
import com.danielandshayegan.discovrus.models.PostCommentData;
import com.danielandshayegan.discovrus.models.PostListData;
import com.danielandshayegan.discovrus.models.RefenrenceModel;
import com.danielandshayegan.discovrus.models.ReferenceListByUserId;
import com.danielandshayegan.discovrus.models.ReferencesListModel;
import com.danielandshayegan.discovrus.models.SaveCommentLikeData;
import com.danielandshayegan.discovrus.models.SaveLikeData;
import com.danielandshayegan.discovrus.models.SavePost;
import com.danielandshayegan.discovrus.models.SavePostCommentReplyData;
import com.danielandshayegan.discovrus.models.SavePostListData;
import com.danielandshayegan.discovrus.models.SavePostViewData;
import com.danielandshayegan.discovrus.models.SaveReferenceData;
import com.danielandshayegan.discovrus.models.TermsOfuseModel;
import com.danielandshayegan.discovrus.models.TrendingDetailsByIdModel;
import com.danielandshayegan.discovrus.models.TrendingNowModel;
import com.danielandshayegan.discovrus.models.UploadPostData;
import com.danielandshayegan.discovrus.models.UserProfileData;
import com.danielandshayegan.discovrus.models.VideoPostData;
import com.danielandshayegan.discovrus.models.WorkingAtData;
import com.danielandshayegan.discovrus.models.postDetailData;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;


/**
 * Declare all the APIs in this class with specific interface
 * e.g. Profile for Login/Register Apis
 */
public interface WebserviceBuilder {


    @FormUrlEncoded
    @POST("LoginAPI/ValidateUser")
    Observable<LoginData> userLogin(@Field("Email") String Email,
                                    @Field("SocialmediaId") String SocialmediaId,
                                    @Field("Password") String Password,
                                    @Field("IsEmail") Boolean IsEmail,
                                    @Field("IsLinkedin") Boolean IsLinkedin,
                                    @Field("IsInstagram") Boolean IsInstagram, @Field("IsFacebook") Boolean IsFacebook);


    @GET("UserAPI/SearchAdminUser")
    Single<WorkingAtData> workingAtData(@Query("WorkingAt") String WorkingAt);

    @Multipart
    @POST("UserAPI/SaveMobileUser")
    Observable<IndividualRegisterData> userRegister(@Query("FirstName") String FirstName, @Query("LastName") String LastName, @Query("Email") String Email, @Query("Password") String Password,
                                                    @Query("Location") String Location, @Query("UserRole") String UserRole, @Query("WorkingAt") String WorkingAt, @Query("CategoryId") int CategoryId,
                                                    @Query("IsEmail") boolean IsEmail, @Query("IsLinkedin") boolean IsLinkedin, @Query("IsInstagram") boolean IsInstagram, @Query("IsFacebook") boolean IsFacebook,
                                                    @Query("AndroidDeviceID") String AndroidDeviceID, @Query("IOSDeviceID") String IOSDeviceID,
                                                    @Query("Lat") double lat, @Query("Long") double lng, @Query("ZipCode") String zipCode, @Part MultipartBody.Part UserPhoto);


    @Multipart
    @POST("UserAPI/SaveMobileUser")
    Observable<IndividualRegisterData> userRegisterSocialMedia(@Query("FirstName") String FirstName, @Query("LastName") String LastName, @Query("Email") String Email,
                                                               @Query("Location") String Location, @Query("UserRole") String UserRole, @Query("WorkingAt") String WorkingAt, @Query("CategoryId") int CategoryId,
                                                               @Query("IsEmail") boolean IsEmail, @Query("IsLinkedin") boolean IsLinkedin, @Query("IsInstagram") boolean IsInstagram, @Query("IsFacebook") boolean IsFacebook,
                                                               @Query("SocialmediaId") String SocialmediaId, @Query("AndroidDeviceID") String AndroidDeviceID, @Query("IOSDeviceID") String IOSDeviceID,
                                                               @Query("Lat") double lat, @Query("Long") double lng, @Query("ZipCode") String zipCode, @Part MultipartBody.Part UserPhoto);


    @Multipart
    @POST("UserAPI/SaveMobileUser")
    Observable<BusinessRegisterData> userBusinessRegisterEmail(@Query("FirstName") String FirstName, @Query("LastName") String LastName, @Query("Email") String Email, @Query("Password") String Password,
                                                               @Query("Location") String Location, @Query("UserRole") String UserRole, @Query("CategoryId") int CategoryId,
                                                               @Query("BusinessName") String BusinessName, @Query("IsEmail") boolean IsEmail, @Query("IsLinkedin") boolean IsLinkedin,
                                                               @Query("IsInstagram") boolean IsInstagram, @Query("IsFacebook") boolean IsFacebook, @Query("AndroidDeviceID") String AndroidDeviceID, @Query("IOSDeviceID") String IOSDeviceID,
                                                               @Query("Lat") double lat, @Query("Long") double lng, @Query("ZipCode") String zipCode, @Part MultipartBody.Part UserPhoto);

    @Multipart
    @POST("UserAPI/SaveMobileUser")
    Observable<BusinessRegisterData> userBusinessRegisterSocialMedia(@Query("FirstName") String FirstName, @Query("LastName") String LastName, @Query("Email") String Email, @Query("Location") String Location,
                                                                     @Query("UserRole") String UserRole, @Query("CategoryId") int CategoryId, @Query("BusinessName") String BusinessName, @Query("IsEmail") boolean IsEmail,
                                                                     @Query("IsLinkedin") boolean IsLinkedin, @Query("IsInstagram") boolean IsInstagram, @Query("IsFacebook") boolean IsFacebook, @Query("SocialmediaId") String SocialmediaId,
                                                                     @Query("AndroidDeviceID") String AndroidDeviceID, @Query("IOSDeviceID") String IOSDeviceID,
                                                                     @Query("Lat") double lat, @Query("Long") double lng, @Query("ZipCode") String zipCode, @Part MultipartBody.Part UserPhoto);


    @POST("LoginAPI/CheckForgotSocialMedia")
    Observable<ForgotPasswordData> userForgotPassword(@Query("Email") String Email);

    @FormUrlEncoded
    @POST("LoginAPI/ChangePasswordForMobile")
    Observable<CommonApiResponse> userChangePassword(@Field("UserID") int UserID, @Field("OldPassword") String OldPassword, @Field("NewPassword") String NewPassword);

    @FormUrlEncoded
    @POST("UserAPI/SendVerifyCode")
    Observable<ConfirmMailData> confirmMail(@Field("Email") String Email, @Field("Code") String Code);

    @FormUrlEncoded
    @POST("UserAPI/VerifyEmailCode")
    Observable<LoginData> confirmCode(@Field("Email") String Email, @Field("Code") String Code);

    @GET("CategoryAPI/GetCategoryList")
    Observable<CategoryList> getCategoryList();

    @GET("PostAPI/GetPostPagging")
    Single<PostListData> getPostData(@Query("UserID") int UserID, @Query("PageNumber") int PageNumber, @Query("PageSize") int PageSize);

    @GET("PostAPI/GetPostPagging")
    Single<PostListData> getProfileData(@Query("UserID") int UserID, @Query("PageNumber") int PageNumber, @Query("PageSize") int PageSize);

    /*@GET("PostAPI/GetPost")
    Single<PostListData> getPostData(@Query("UserID") int UserID);*/

    @FormUrlEncoded
    @POST("LikeAPI/SaveLike")
    Observable<SaveLikeData> saveLike(@Field("PostID") int PostID, @Field("UserID") int UserID, @Field("IsActive") boolean IsActive, @Field("LoginID") int LoginID);

    @FormUrlEncoded
    @POST("FollowPeopleAPI/FollowPeople")
    Observable<FollowPeopleData> followPeople(@Field("UserID") int UserID, @Field("FollowUserID") int FollowUserID);

    @Multipart
    @POST("NewsFeedAPI/UploadPhoto")
    Observable<UploadPostData> uploadPostData(@Query("Description") String Description, @Query("Title") String Title, @Query("UserId") int UserId, @Part MultipartBody.Part UploadPhoto);


    @Multipart
    @POST("NewsFeedAPI/UploadVideo")
    Observable<VideoPostData> uploadVideoPostData(@Query("textDisplay") String textDisplay, @Query("UserId") int UserId, @Part MultipartBody.Part UploadVideo, @Part MultipartBody.Part UploadThumbnail);


    @FormUrlEncoded
    @POST("NewsFeedAPI/UploadPhoto")
    Observable<UploadPostData> uploadPostDataText(@Field("Description") String Description, @Field("Title") String Title, @Field("UserId") int UserId);

    @FormUrlEncoded
    @POST("PostViewAPI/SavePostView")
    Observable<SavePostViewData> savePostView(@Field("PostID") int PostID, @Field("LoginID") int LoginID);

    @GET("PostAPI/GetPostById")
    Single<PostListData> getPostById(@Query("UserID") int UserID);

    @GET("TrendingAPI/GetTrendingList")
    Single<TrendingNowModel> getTrendingList();

    @GET("TrendingAPI/GetTrendingListByID")
    Single<TrendingDetailsByIdModel> getTrendingListById(@Query("TrendingID") int TrendingID);

    @GET("PostsCommentAPI/GetCommentListByPost")
    Observable<ClickPostCommentData> getCommentList(@Query("PostID") int PostID, @Query("LoginID") int LoginID);

    @POST("BlockAccount/GetBlockUserList")
    Observable<BlockAccountListData> getBlockList(@Query("LoginID") int LoginID);

    @POST("BlockAccount/GetHiddenUserList")
    Observable<HiddenDataListModel> getHiddenList(@Query("LoginID") int UserID);

    @GET("FollowPeopleAPI/GetFollowerFollowingPeople")
    Observable<FollowersUserListData> getFollowList(@Query("UserID") int LoginID, @Query("LoginId") int LoginId, @Query("IsFollowing") Boolean IsFollowing);

    @GET("PublicProfileAPI/GetProfile")
    Single<UserProfileData> getProfile(@Query("UserID") int UserID, @Query("LoginID") int LoginID,
                                       @Query("PageNumber") int PageNumber, @Query("PageSize") int PageSize);

    @GET("UserReferenceAPI/GetReferences")
    Single<ReferencesListModel> getReferences(@Query("type") String type);

    @GET("DataPolicyAPI/GetDataPolicy")
    Observable<DataPolicyModel> getDataPolicy();

    @GET("TermsOfUseAPI/GetTermsOfUse")
    Observable<TermsOfuseModel> getTermsOfUse();

    @GET("UserReferenceAPI/GetReferencesByUserId")
    Single<ReferenceListByUserId> getReferencesByUserId(@Query("UserID") int UserID,
                                                        @Query("LoginID") int LoginID,
                                                        @Query("Type") String Type,
                                                        @Query("PageNumber") int PageNumber,
                                                        @Query("PageSize") int PageSize);

    @GET("PublicProfileAPI/GetHighlightedPost")
    Single<UserProfileData> getHighlightedPost(@Query("UserID") int UserID, @Query("LoginID") int LoginID,
                                               @Query("PageNumber") int PageNumber, @Query("PageSize") int PageSize);

    @FormUrlEncoded
    @POST("UserReferenceAPI/SaveUserReference")
    Single<CommonApiResponse> saveUserReference(@Field("FromUserID") int FromUserID, @Field("ToUserID") int ToUserID, @Field("ReferenceIDs") String ReferenceIDs);

    @GET("PublicProfileAPI/GetChartList")
    Single<ChartDataList> getChartList(@Query("UserID") int UserID);

    @FormUrlEncoded
    @POST("PostsCommentAPI/DeletePostComment")
    Observable<DeleteCommentData> deleteComment(@Field("CommentId") String CommentId);

    @FormUrlEncoded
    @POST("PostsCommentAPI/SavePostComment")
    Observable<PostCommentData> postComment(@Field("CommentId") String CommentId, @Field("PostId") String PostId, @Field("UserId") String UserId, @Field("Comment") String Comment);

    @FormUrlEncoded
    @POST("CommentLikeAPI/SaveCommentLike")
    Observable<SaveCommentLikeData> saveCommentLike(@Field("CommentID") int CommentID, @Field("UserID") int UserID, @Field("PostID") int PostID);

    @GET("PaperClipUserAPI/GetPaperClipUserList")
    Observable<PaperClippedListData> getPperCllipedData(@Query("LoginID") int LoginID);

    @GET("UserReferenceAPI/GetReferences")
    Observable<RefenrenceModel> getReferencesSpinner(@Query("Type") String Type);

    @FormUrlEncoded
    @POST("UserReferenceAPI/SaveUserReference")
    Observable<SaveReferenceData> saveReference(@Field("FromUserID") int FromUserID, @Field("ToUserID") int ToUserID, @Field("ReferenceIDs") int ReferenceIDs);

    @GET("ClickieAPI/GetClickieList")
    Single<ClickieList> getClickieList(@Query("UserID") int UserID);

    @GET("maps/api/geocode/json")
    Single<GoogleAddressResult> getAddress(@Query("components") String components, @Query("sensor") boolean sensor, @Query("key") String key);

    @GET("maps/api/geocode/json")
    Single<AddressResult> getAddressFromLatLong(@Query("latlng") String latlng, @Query("sensor") boolean sensor, @Query("key") String key);

    @POST("ClickieAPI/DeleteClickie")
    Single<CommonApiResponse> deleteClickie(@Query("ID") int ID);

    @GET("UserAPI/ConnectPeopleSearch")
    Single<ConnectPeopleData> connectPeople(@Query("Search") String Search);

    @Multipart
    @POST("ClickieAPI/SaveUserClickie")
    Observable<CommonApiResponse> saveUserClickie(@Query("UserID") int UserID, @Part MultipartBody.Part UserClickieImage);

    @Multipart
    @POST("NewsFeedAPI/PostDetailPhoto")
    Observable<DetailPostPhotoData> postDetailPhoto(@Part MultipartBody.Part UploadPhoto, @Part MultipartBody.Part OriginalImage,
                                                    @Query("Description") String Description, @Query("Title") String Title,
                                                    @Query("UserId") int UserId, @Query("TagUserID") String TagUserID,
                                                    @Query("Lat") String Lat, @Query("Long") String Long, @Query("Distance") String Distance,
                                                    @Query("Time") String Time, @Query("HashTag") String HashTag, @Query("IsHighLight") boolean IsHighLight,
                                                    @Query("IsPaid") boolean IsPaid, @Query("Amount") String Amount);

    @POST("NewsFeedAPI/PostDetailPhoto")
    Observable<DetailPostPhotoData> postDetailText(@Query("Description") String Description, @Query("Title") String Title,
                                                   @Query("UserId") int UserId, @Query("TagUserID") String TagUserID,
                                                   @Query("Lat") String Lat, @Query("Long") String Long, @Query("Distance") String Distance,
                                                   @Query("Time") String Time, @Query("HashTag") String HashTag, @Query("IsHighLight") boolean IsHighLight,
                                                   @Query("IsPaid") boolean IsPaid, @Query("Amount") String Amount);

    @FormUrlEncoded
    @POST("HashTagAPI/GetHashTagMasterList")
    Single<HashTagListModel> getHashTagList(@Field("Search") String Search);

    @Multipart
    @POST("NewsFeedAPI/PostDetailVideo")
    Observable<DetailPostVideoData> postDetailVideo(@Part MultipartBody.Part UploadVideo, @Part MultipartBody.Part UploadThumbnail,
                                                    @Query("textDisplay") String textDisplay, @Query("UserId") int UserId, @Query("TagUserID") String TagUserID,
                                                    @Query("Lat") String Lat, @Query("Long") String Long, @Query("Distance") String Distance,
                                                    @Query("Time") String Time, @Query("HashTag") String HashTag, @Query("IsHighLight") boolean IsHighLight,
                                                    @Query("IsPaid") boolean IsPaid, @Query("Amount") String Amount);

    @POST("StripeAPI/AddCustomerCard")
    Observable<AddCardData> addCustomerCard(@Query("stripeToken") String stripeToken, @Query("CustomerId") int CustomerId);

    @POST("StripeAPI/MakePayment")
    Single<CommonApiResponse> makePayment(@Query("CustomerStripeId") String CustomerStripeId, @Query("CardId") String CardId,
                                          @Query("Amount") String Amount, @Query("Description") String Description);

    @GET("StripeAPI/GetCardList")
    Single<CardListEnvelope> getCardList(@Query("CustomerId") int CustomerId);

    @POST("StripeAPI/DeleteCard")
    Single<CardListEnvelope> deleteCard(@Query("CustomerStripeId") String CustomerStripeId, @Query("CardId") String CardId);

    @GET("PostAPI/GetPostDetail")
    Single<postDetailData> getPostDetail(@Query("PostId") String PostId);

    @FormUrlEncoded
    @POST("PostsCommentAPI/SavePostCommentReply")
    Observable<SavePostCommentReplyData> savePostCommentReply(@Field("PostId") int PostId,
                                                              @Field("UserId") int UserId,
                                                              @Field("Comment") String Comment,
                                                              @Field("CommentId") int CommentId);

    @GET("PostsCommentAPI/GetPostCommentReply")
    Single<CommentReplyData> getPostCommentReply(@Query("CommentId") int CommentId,
                                                 @Query("LoginID") int LoginID,
                                                 @Query("PostID") int PostID);

    @POST("PostsCommentAPI/DeletePostCommentReply")
    Observable<DeleteCommentData> deletePostCommentReply(@Query("CommentId") int CommentId);

    @GET("MapAPI/GetMapList")
    Single<MapListData> getMapList(@Query("Lat") double Lat, @Query("Long") double Long, @Query("Distance") int Distance, @Query("LoginID") int LoginID);

    @GET("MapAPI/GetMapByPostId")
    Observable<MapPostDetail> getMapByPostId(@Query("PostId") int PostId, @Query("UserID") int UserID,
                                             @Query("Lat") double Lat, @Query("Long") double Long);

    @FormUrlEncoded
    @POST("BlockAccount/SaveHiddenUserDetails")
    Observable<CommonApiResponse> SaveHiddenUserDetails(@Field("UserID") int UserID, @Field("LoginID") int LoginID);

    @FormUrlEncoded
    @POST("BlockAccount/SaveBlockAccountDetails")
    Observable<CommonApiResponse> SaveBlockUserDetails(@Field("UserID") int UserID, @Field("LoginID") int LoginID);

    @GET("GeneralSettingsAPI/GetGeneralSettings")
    Observable<GeneralSettingsData> getGeneralSettings(@Query("LoginId") int LoginId);

    @POST("GeneralSettingsAPI/SaveGeneralSettings")
    Observable<GeneralSettingsData> saveGeneralSettings(@Body RequestBody requestBody);

    @GET("AnalyticsAPI/GetAnalyticsDetails")
    Observable<AnalyticsData> getAnalyticsData(@Query("UserId") int UserId, @Query("IsMonthly") int IsMonthly);

    @FormUrlEncoded
    @POST("PostAPI/SaveOnlineUserList")
    Observable<CommonApiResponse> SaveOnlineUserList(@Field("UserID") int UserID, @Field("isOnline") boolean isOnline);

    @GET("PublicFavouriteAPI/GetFavouriteList")
    Observable<AddToFavourite> addToFavourite(@Query("UserID") int LoginID, @Query("PostId") int PostId,
                                              @Query("IsHighLight") Boolean IsHighLight, @Query("IsFavourite") Boolean IsFavourite);

    @POST("PostAPI/SavePost")
    Observable<SavePost> savePost(@Query("LoginId") int UserID, @Query("PostId") int PostId, @Query("IsSave") boolean IsSave);

    @POST("PostAPI/DeletePost")
    Observable<SavePost> deletePost(@Query("UserID") int UserID, @Query("PostID") int PostID);

    @GET("PostAPI/GetPersonalPostDetail")
    Observable<SavePostListData> getSavedPosts(@Query("LoginId") int LoginID, @Query("PageNumber") int PageNumber,
                                               @Query("PageSize") int PageSize);

    @GET("LikeAPI/GetLikeList")
    Observable<LikeListData> getLikeListData(@Query("LoginId") int LoginID, @Query("PostId") int PostID);


    /**
     * ApiNames to differentiate APIs
     */
    enum ApiNames {
        single, profile, list, forgotPassword, setPass, confirmCode, businessRegister, getPostData, saveLike, followPeople,
        getCategoryList, postPhoto, savePostView, postVideo, getPostById, getTrendingList, getCommentList, getProfile, getReferences,
        getReferencesByUserId, saveUserReference, getChartList, deleteComment, saveComments, saveCommentLike, getPaperClippedData,
        deleteClickie, referenceDataList, saveReferenceData, connectPeople, hashTagList, getHashTagSearch, saveUserClickie, postDetailPhoto,
        postDetailText, postDetailVideo, addCustomerCard, makePayment, getCardList, deleteCard, getPostDetail, savePostCommentReply, getPostCommentReply,
        deletePostCommentReply, getBlockList, getHiddenList, getMapByPostId, SaveHiddenUserDetails, getPolicy,
        userChangePassword, SaveBlockUserDetails, saveGeneralSettings, getGeneralSettings, getAnalyticsData, getFollowList, SaveOnlineUserList,
        addToFavourite, savePost, getSavedPosts, getLikeListData, deletePost
    }
}